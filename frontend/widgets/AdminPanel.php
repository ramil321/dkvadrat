<?php
namespace frontend\widgets;

use dmstr\cookiebutton\CookieButton;
use Yii;
use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;


class AdminPanel extends \yii\bootstrap\Widget
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        if(!Yii::$app->user->identity->isAdmin){
            return '';
        }

        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse',
            ],
        ]);
        $menuItems = [
            ['label' => 'Админка', 'url' => Yii::$app->urlManagerBackend->baseUrl],
        ];
        if (!Yii::$app->user->isGuest) {
            $menuItems[] = '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);

        NavBar::end();

        $css = <<< CSS
        .navbar-edit-area{
            color: #9d9d9d;
            /* margin-top: 8px; */
            padding: 15px;
            border: none;
            font-weight: normal;
            /* padding: 10px; */
            font-size: 14px;
        }


CSS;

        $script = <<< JS
           $(function() {
               $('.navbar-edit-area-block .btn-group').click(function(){
                   location.reload();
               });             
           })


JS;
        $this->view->registerCss($css);
        $this->view->registerJs($script);
    }
}
