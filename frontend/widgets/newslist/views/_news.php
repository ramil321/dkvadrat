<?php
/**@var \common\models\material\Material $model*/
?>
<div class="item <?=(!$model->getPreviewImageStyled()->getSrc() ? 'item--no-img' : '')?>">
    <figure class="item__figure">
        <div class="item__figure-top">
            <a href="<?= $model->mainCategory->getUrl() ?>" class="item__category">
                <?= $model->mainCategory->name ?>
            </a>
            <time class="item__time-wrapper">
                <div class="item__time-icon">
                    <svg viewBox="0 0 12 12" class="time-svg">
                        <use xlink:href="#time"></use>
                    </svg>
                </div><span class="item__time"><?=Yii::$app->formatter->asShortDate($model->publish_date);?></span>
            </time>
        </div>
        <div class="item__image <?=(!$model->getPreviewImageStyled()->getSrc() ? 'item__image--hide' : '')?>">
            <?if($model->getPreviewImageStyled()->getSrc()):?>
                <img src="<?=Yii::$app->resize->prop($model->getPreviewImageStyled()->getSrc(), 500, 500)?>" alt="" class="item__img <?=$model->getPreviewImageStyled()->getCssClass()?> ">
            <?endif?>
        </div>
        <figcaption class="item__figcaption">
            <h4 class="item__title">
                <a href="<?= $model->getUrl()?>" class="item__title-link"><?= $model->name ?></a>
            </h4>
        </figcaption>
    </figure>
</div>