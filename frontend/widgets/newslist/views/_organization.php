<?php
/**@var \common\models\material\Material $model*/
?>
<div class="item organizations">
    <figure class="item__figure">
        <a href="#" class="item__category">Организации</a>
        <div class="item__name-wrapper">
            <h4 class="item__title"><a href="<?= $model->getUrl()?>" class="item__title-link"><?=$model->name?></a></h4>
            <div class="item__image">
                <?php if($model->previewImageCrop): ?>
                    <img src="<?=Yii::$app->resize->crop($model->previewImageCrop->src, 120, 120)?>" alt="" class="person__img">
                <?php endif ?>
            </div>
        </div>
        <figcaption class="item__figcaption">
            <p class="item__text"><?=$model->preview_text?></p>
            <?php/*
            <a href="<?= $model->getUrl()?>" class="item__release">
                <svg viewBox="0 0 16 15" class="arrow-right-small-svg">
                    <use xlink:href="#arrow-right-small"></use>
                </svg><span class="item__span">Лето 2018 будет аномально жарким</span>
            </a>
            */?>
        </figcaption>
    </figure>
</div>