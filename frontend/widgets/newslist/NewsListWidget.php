<?php
namespace frontend\widgets\newslist;

use common\models\material\Material;
use frontend\components\material\ActiveDataProviderPriority;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;

;


class NewsListWidget extends Widget
{

    private $hasPriority = false;

    public $shortNewsPosition = 0;
    /**
     * @var ActiveDataProviderPriority
     */
    public $dataProvider;

    /**
     * @var string
     */
    public $itemTemplatePriority;

    /**
     * @var string
     */
    public $itemTemplate;

    /**
     * @var string
     */
    public $liClass;

    /**
     * @var string
     */
    public $ulClass;

    /**
     * @var boolean
     */
    public $disableDefaultUlClass = false;
    /**
     * {@inheritdoc}
     */
    public function run()
    {

        if((!$this->dataProvider->getPriorityMaterial()) && (!$this->disableDefaultUlClass)){
            $this->ulClass = 'template template--eight_blocks';
        }
        $html = '';

        if(!Yii::$app->request->post('pager')) {
            $html .= '<ul class="' . $this->ulClass . '">';
        }
        $html .= ListView::widget([
            'dataProvider' => $this->dataProvider,
            'options' => [
                'tag' => false,
                'class' => false,
                'id' => false,
            ],
            'itemOptions' => [
                'tag' => false,
                'class' => false,
            ],
            'layout' => "{items}{pager}",
            'summary'=>false,
            'itemView' => function ($model, $key, $index, $widget) {
                /** @var Material $model */
                    $html = '';

                    if( $this->shortNewsPosition > 0 &&
                        $this->shortNewsPosition == $index &&
                        (!Yii::$app->request->post('pager')) &&
                        $this->hasPriority
                    ) {
                        $html .= '<li class="'.$this->liClass.'">';
                        $html .= \frontend\widgets\shortnews\ShortNewsWidget::widget();
                        $html .= '</li>';
                    }

                    $html .= '<li class="'.$this->liClass.'">';
                    if($index == 0 && $model->priority && $this->itemTemplatePriority){
                        $this->hasPriority = true;
                        $html .= $this->render($this->itemTemplatePriority,['model' => $model]);
                    }else {
                        $html .= $this->render($this->itemTemplate, ['model' => $model]);
                    }
                    $html .= '</li>';
                    return $html;
            },
            'pager' => [
                'firstPageLabel' => false,
                'lastPageLabel' => false,
                'nextPageLabel' => 'Загрузить еще',
                'prevPageLabel' => false,
                'maxButtonCount' => 0,
                'disabledPageCssClass' => 'hide--more',
                'firstPageCssClass' => false,
                'lastPageCssClass' => false,
                'prevPageCssClass' => false,
                'nextPageCssClass' => false,
                'activePageCssClass' => false,
                'linkOptions' => ['class' => 'button button--more'],
                'options' => ['tag' => false],
                'linkContainerOptions' => ['tag' => 'div', 'class' => 'button-more', 'style' => 'width: 100%']
            ],


        ]);
        if(!Yii::$app->request->post('pager')) {
            $html .= '</ul>';
        }
        return $html;
    }

}
