<?php
namespace frontend\widgets\sortnews;
use common\models\material\Material;
use common\models\material\MaterialRegion;
use frontend\models\material\search\MaterialSearch;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

;


class SortNewsWidget extends Widget
{
    const START_FILTER_DATE = '2017-03-15';

    /**
     * @var MaterialSearch
    */
    public $model;

    public $categories;

    public $categoryId;

    private $monthsNames = [
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь'
    ];

    private $monthsNamesShort = [
        1 => 'Янв',
        2 => 'Фев',
        3 => 'Мар',
        4 => 'Апр',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Авг',
        9 => 'Сен',
        10 => 'Окт',
        11 => 'Ноя',
        12 => 'Дек'
    ];

    private function getMonthsRange($date1, $date2)
    {
        $years = [];
        $currDates = Yii::$app->db->createCommand('
        SELECT 
         MONTH(FROM_UNIXTIME(publish_date)),
         YEAR(FROM_UNIXTIME(publish_date))
        FROM material 
        '.($this->categoryId ? '
        LEFT JOIN `material_categories_material` 
          ON `material`.`id` = `material_categories_material`.`material_id` 
            LEFT JOIN `material_categories` 
              ON `material_categories_material`.`material_categories_id` = `material_categories`.`id` 
               WHERE `material_categories`.`id`='.$this->categoryId : '' ).'
        GROUP BY 
          MONTH(FROM_UNIXTIME(publish_date)),
          YEAR(FROM_UNIXTIME(publish_date))
        ORDER BY 
          YEAR(FROM_UNIXTIME(publish_date)) ASC,
          MONTH(FROM_UNIXTIME(publish_date)) ASC
        ')->queryAll();


        foreach ($currDates as $currDate){
            if($currDate['YEAR(FROM_UNIXTIME(publish_date))']) {
                $years[$currDate['YEAR(FROM_UNIXTIME(publish_date))']][] = $currDate['MONTH(FROM_UNIXTIME(publish_date))'];
            }
        }
        $result = [];
        foreach ($years as $year=>$months){
            for ($i = 1; $i < 13; $i++){
                $active = false;
                if(in_array($i, $months)){
                    $active = true;
                }
                $result[$year][$i] = $active;
            }
        };
        return $result;
    }


    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $regions = ArrayHelper::map(MaterialRegion::getElements(), 'id', 'name');
        $dateRange = $this->getMonthsRange(self::START_FILTER_DATE, date('Y-m-d'));

        if(!$this->model->year){
            $this->model->year = date('Y');
        }

        return $this->render('sort_news_widget',[
            'regions' => $regions,
            'categories' => $this->categories,
            'monthsNames' => $this->monthsNames,
            'monthsNamesShort' => $this->monthsNamesShort,
            'dateRange' => $dateRange,
            'model' => $this->model
        ]);
    }
}
