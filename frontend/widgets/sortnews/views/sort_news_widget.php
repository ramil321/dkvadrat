<?php
/** @var \frontend\models\material\filters\NewsFilter $model*/
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var array $regions*/
/** @var array $monthsNames*/
/** @var array $monthsNamesShort */
/** @var array $dateRange */
/** @var \common\models\material\MaterialCategories[] $categories */


?>
<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'news_filter',
        'class' => 'top-section__form filter-form',
    ],
    'action' => Url::to(['/'.Yii::$app->request->getPathInfo()]),
    'method' => 'get'
]); ?>
    <?= $form->field($model, 'filter')->hiddenInput(['value' => 1])->label(false);?>
    <div class="top-section__option">
        <div class="top-section__region select-region">
            <div class="button select-region__button">
                <svg viewBox="0 0 24 24" class="angle-svg angle-svg--top-section">
                    <use xlink:href="#angle"></use>
                </svg><span class="select-region__span">Регион:</span>
                <?= $form->field($model, 'region_id')->dropDownList($regions, [
                    'prompt' => ['text' => 'Все', 'options' => ['class' => 'select-region__option']],
                    'class' => 'select-region__select',
                    'options' => array_map(function () {
                        return ['class' => 'select-region__option'];
                    }, $regions)
                ])->label(false) ?>

            </div>
        </div>
        <div class="top-section__date"><a class="top-section__date-button"> <span class="top-section__date-span">Сентябрь’ 18</span>
                <svg viewBox="0 0 24 24" class="angle-svg angle-svg--date-button">
                    <use xlink:href="#angle"></use>
                </svg></a></div>
    </div>
    <div class="top-section__carousel carousel-time transition">
        <div class="container">
            <div class="carousel-time__wrapper">
                <div class="carousel-time__form">
                    <div class="carousel-time__years">
                        <label class="carousel-time__checkbox-label">
                            <input type="checkbox" name="checkboxYear" <?=($model->month == '' || $model->year == '' ? 'checked' : '')?> class="carousel-time__checkbox-input">
                            <span class="carousel-time__checkbox-span">За все время</span>
                        </label>
                        <?= $form->field($model, 'year')->dropDownList(array_combine(array_keys($dateRange),array_keys($dateRange)), [
                            'class' => 'select-year',
                            'options' => array_map(function () {
                                return ['class' => 'select-year__option'];
                            }, array_combine(array_keys($dateRange),array_keys($dateRange)))
                        ])->label(false) ?>
                        <div class="carousel-time__years-wrapper">
                            <div class="carousel-time__nav">
                                <?if(sizeof($dateRange) > 1):?>
                                <button class="carousel-time__button carousel-time__button-prev">
                                    <svg viewBox="0 0 16 16" class="angle-left-svg--carousel-prev">
                                        <use xlink:href="#angle-left"></use>
                                    </svg>
                                </button>
                                <button class="carousel-time__button carousel-time__button-next">
                                    <svg viewBox="0 0 16 16" class="angle-left-svg--carousel-next">
                                        <use xlink:href="#angle-left"></use>
                                    </svg>
                                </button>
                                <?endif?>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-time__month month-dk">
                        <?foreach ($dateRange as $year=>$months):?>
                            <select name="<?= Html::getInputName($model, 'month'); ?>" class="select-month">
                                <?foreach ($months as $month=>$active):?>
                                    <option  data-active="<?=($active ? 'true' : 'false')?>"  <?= ($month == $model->month && $year == $model->year) ? 'selected' : '';?> value="<?=$month?>" data-full-month="<?=$monthsNames[$month]?>" class="select-month__option"><?=$monthsNamesShort[$month]?></option>
                                <?endforeach;?>
                            </select>
                        <?endforeach;?>
                    </div>
                    <div class="carousel-time__button-apply">
                        <button type="submit" class="button button--apply">Применить</button>
                    </div>
                </div>
                <div class="close close--carousel-time">
                    <div class="close__line"></div>
                </div>
            </div>
        </div>
    </div>
    <?if($categories):?>
    <div class="filter-search">
        <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map($categories, 'id', 'name'), [
            'prompt' => ['text' => 'Все', 'options' => ['class' => 'filter-search__option']],
            'class' => 'filter-search__select',
            'options' => array_map(function () {
                return ['class' => 'filter-search__option'];
            }, $categories)
        ])->label(false) ?>
    </div>
    <?endif?>
<?php ActiveForm::end(); ?>
