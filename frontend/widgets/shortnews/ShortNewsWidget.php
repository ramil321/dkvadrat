<?php
namespace frontend\widgets\shortnews;

use backend\modules\rest\models\Materials;
use common\models\material\Material;
use common\models\material\MaterialCategories;
use yii\base\Widget;


class ShortNewsWidget extends Widget
{
    public static $cacheMaterials;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $materials = self::getMaterialsList();
        return $this->render('short_news',[
            'materials' => $materials
        ]);
    }

    /**
     * @return Material[]
     */
    public static function getMaterialsList(){
        if(self::$cacheMaterials){
            return self::$cacheMaterials;
        }
        $materials = Materials::find()
            ->select(['material.id', 'material.name', 'material.priority', 'material.publish_date'])
            ->joinWith('materialCategories')
            ->where(['material_categories.type_code' => 'news'])
            ->andWhere('material.is_published = true')
            ->andWhere(['<=', 'material.publish_date', time()])
            ->orderBy(['publish_date' => SORT_DESC])
            ->limit(5)
            ->all();
        self::$cacheMaterials = $materials;
        return $materials;
    }

}
