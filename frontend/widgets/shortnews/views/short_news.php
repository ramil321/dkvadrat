<?php


use common\models\material\Material;

/** @var Material[] $materials*/

?>
<div class="news-feed">
    <h4 class="news-feed__title">Лента новостей</h4>
    <ul class="news-feed__list">
        <?foreach ($materials as $material):?>
        <li class="news-feed__item">
            <a href="<?=$material->getUrl()?>" class="news-feed__link">
                <?=$material->name?>
            </a>
        </li>
        <?endforeach;?>
    </ul>
    <a href="/novosti" class="news-feed__more">Больше новостей</a>
</div>