<?php
namespace frontend\widgets\render;

use backend\modules\constructor\models\Constructor;
use backend\modules\constructor\models\RenderSections;
use backend\modules\rest\models\Materials;
use common\models\material\Material;
use yii\base\Widget;
use yii\helpers\Json;


class RenderConstructorWidget extends Widget
{

    public $constructorType = Constructor::TYPE_MAIN;
    /**
     * @var Constructor[]
     */
    public $constructorModels;
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $res = [];
        foreach ($this->constructorModels as $model) {
            $template = Json::decode($model->template);
            $template['settings'] = Json::decode($model->settings);
            $template['id'] = Json::decode($model->id);
            $res[] = $template;
        }
        $rs = new RenderSections();
        $sections = $rs->render($res, $this->constructorType);
        $headline = null;
        $otherSections = [];
        foreach ($sections as $section){
            if($section['template_code'] == 'headline'){
                $headline = $section;
            }else{
                $otherSections[] = $section;
            }
        }

        return $this->render('page',[
            'headline' => $headline,
            'otherSections' => $otherSections
        ]);
    }

}
