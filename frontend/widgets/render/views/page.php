<?php


use common\models\material\Material;

/** @var Material[] $materials*/
/** @var array $headline */
/** @var array $otherSections */
?>
<div class="home">
    <?= $headline['value']?>
    <div class="home__content">
        <div class="container">
            <div class="home__news-feed-content">
                <?=\frontend\widgets\shortnews\ShortNewsWidget::widget()?>
            </div>
            <? foreach ($otherSections as $section):?>
                <?=$section['value']?>
            <?endforeach;?>
        </div>
    </div>
</div>