<?php
namespace frontend\widgets\dlist;

use common\models\material\Material;
use frontend\components\material\ActiveDataProviderPriority;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;

;


class ListWidget extends Widget
{


    /**
     * @var ActiveDataProviderPriority
     */
    public $dataProvider;

    /**
     * @var string
     */
    public $itemTemplate;

    /**
     * @var string
     */
    public $liClassFirstActive;

    /**
     * @var string
     */
    public $liClass;

    /**
     * @var string
     */
    public $ulClass;

    /**
     * {@inheritdoc}
     */
    public function run()
    {


        $html = '';

        if(!Yii::$app->request->post('pager')) {
            $html .= '<ul class="' . $this->ulClass . '">';
        }
        $html .= ListView::widget([
            'dataProvider' => $this->dataProvider,
            'options' => [
                'tag' => false,
                'class' => false,
                'id' => false,
            ],
            'itemOptions' => [
                'tag' => false,
                'class' => false,
            ],
            'layout' => "{items}{pager}",
            'summary'=>false,
            'itemView' => function ($model, $key, $index, $widget) {
                /** @var Material $model */
                $html = '';
                $html .= '<li class="'.$this->liClass.($index == 0 && $this->liClassFirstActive ? ' '.$this->liClassFirstActive : '').'">';
                $html .= $this->render($this->itemTemplate, ['model' => $model]);
                $html .= '</li>';
                return $html;
            },
            'pager' => [
                'firstPageLabel' => false,
                'lastPageLabel' => false,
                'nextPageLabel' => 'Загрузить еще',
                'prevPageLabel' => false,
                'maxButtonCount' => 0,
                'disabledPageCssClass' => 'hide--more',
                'firstPageCssClass' => false,
                'lastPageCssClass' => false,
                'prevPageCssClass' => false,
                'nextPageCssClass' => false,
                'activePageCssClass' => false,
                'linkOptions' => ['class' => 'button button--more'],
                'options' => ['tag' => false],
                'linkContainerOptions' => ['tag' => 'div', 'class' => 'button-wrapper']
            ]
        ]);
        if(!Yii::$app->request->post('pager')) {
            $html .= '</ul>';
        }
        return $html;
    }

}
