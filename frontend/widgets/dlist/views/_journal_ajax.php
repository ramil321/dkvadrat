<?

/** @var \common\models\journal\Journal $model */

?>

<div class="journal-item">
    <a href="<?=$model->getUrl()?>" class="item__block-link journal-ajax"></a>
    <div class="journal-item__wrapper">
        <div class="journal-item__image">
            <img src="<?=Yii::$app->resize->prop($model->image->src, 135, 180)?>" alt="" class="journal-item__img">
            <div class="journal-item__overlay"><span class="journal-item__overlay-link">Читать</span></div>
        </div>
        <div class="journal-item__info"><span class="journal-item__number"><?=$model->number?></span>
            <time class="journal-item__date"><?=$model->getMonthFormated()?></time>
        </div>
        <div class="journal-item__links links">
            <div class="links__wrapper"><a href="<?=$model->file->src?>" class="links__link">
                    <svg viewBox="0 0 17 18" class="pdf-svg">
                        <use xlink:href="#pdf"></use>
                    </svg></a><a href="<?=$model->file->src?>" class="links__link">
                    <svg viewBox="0 0 11 12" class="arrow-down-svg">
                        <use xlink:href="#arrow-down"></use>
                    </svg></a></div>
        </div>
    </div>
</div>