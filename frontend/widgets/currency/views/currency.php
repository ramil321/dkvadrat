<?php


use common\models\material\Material;

/** @var array $result*/

?>
<div class="home__exchange-rates exchange-rates">
    <div class="exchange-rates__item">
        <span class="exchange-rates__count">
            <i class="exchange-rates__currency">$</i><?=$result['usd']['val']?>
        </span>

        <svg viewBox="0 0 8 10" class="exchange-rates-up-svg">
            <?if($result['usd']['type'] == '+'):?>
                <use xlink:href="#exchange-rates-up"></use>
            <?endif?>
            <?if($result['usd']['type'] == '-'):?>
                <use xlink:href="#exchange-rates-down"></use>
            <?endif?>
        </svg>
    </div>
    <div class="exchange-rates__item">
        <span class="exchange-rates__count">
            <i class="exchange-rates__currency">€</i><?=$result['eur']['val']?>
        </span>

        <svg viewBox="0 0 8 10" class="exchange-rates-up-svg">
            <?if($result['eur']['type'] == '+'):?>
                <use xlink:href="#exchange-rates-up"></use>
            <?endif?>
            <?if($result['eur']['type'] == '-'):?>
                <use xlink:href="#exchange-rates-down"></use>
            <?endif?>
        </svg>

    </div>
</div>