<?php
namespace frontend\widgets\currency;

use backend\modules\rest\models\Materials;
use common\models\material\Material;
use common\models\material\MaterialCategories;
use Exception;
use imanilchaudhari\CurrencyConverter\CurrencyConverter;
use yii\base\Widget;


class Currency extends Widget
{
    private $cacheKey = 'currency';
    private $tmpCacheKey = 'tmp_currency';
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->cacheKey = $this->cacheKey . '_' . date('d.m.Y');
        $cache = \Yii::$app->cache;
        $result = $cache->get($this->cacheKey);
        if ($result === false) {
            try{
                if($eur = $this->getCbrRes('R01239')){
                    $result['eur'] = $eur;
                }
                if($usd = $this->getCbrRes('R01235')){
                    $result['usd'] = $usd;
                }
            }
            catch (Exception $ex) {}

            if($result){
                $cache->set($this->cacheKey, $result, 3600*24);
                $cache->set($this->tmpCacheKey, $result);
            }else{
                $result = $cache->get($this->tmpCacheKey);
                if($result) {
                    $cache->set($this->cacheKey, $result, 3600 * 3);
                }
            }
        }
        return $this->render('currency',[
            'result' => $result
        ]);
    }

    public function getCbrRes($id)
    {
        $curentDay = date('d/m/Y');
        $prevDay = date('d/m/Y',strtotime('- 1day'));
        $url = 'http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1='.$prevDay.'&date_req2='.$curentDay.'&VAL_NM_RQ='.$id;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS,1);
        $cbr = curl_exec($ch);
        curl_close($ch);
        $cbr = json_decode(json_encode( simplexml_load_string($cbr, "SimpleXMLElement", LIBXML_NOCDATA)), TRUE);
        $result = [];
        if(!$cbr['Record']){
            return false;
        }
        foreach($cbr['Record'] as $valute){
            $valute['Value'] = str_replace(",",".",$valute['Value']);
            $result[] =  number_format( floatval ($valute['Value']), 2, '.', '');
        }
        if($result){
            $type = '';
            if($result[0] < $result[1]){
                $type = '-';
            }else{
                $type = '+';
            }
            return array('type' => $type, 'val' => $result[1]);
        }else{
            return false;
        }
    }

}
