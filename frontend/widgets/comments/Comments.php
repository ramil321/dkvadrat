<?php

namespace frontend\widgets\comments;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\caching\TagDependency;
use yii\data\ActiveDataProvider;
use ogheo\comments\assets\CommentsAsset;
use ogheo\comments\helpers\CommentsHelper;
use ogheo\comments\Module as CommentsModule;
use ogheo\comments\widget\Comments as CommentsOgheo;

/**
 * Class Comments
 * @package ogheo\comments\widget
 */
class Comments extends CommentsOgheo
{


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$app->getModule('comments');

        $this->formId = $this->getId() . '-' . $this->formId;
        $this->wrapperId = $this->getId() . '-' . $this->wrapperId;
        $this->showCommentsId = $this->getId() . '-' . $this->showCommentsId;
        $this->fullCommentsId = $this->getId() . '-' . $this->fullCommentsId;
        $this->pjaxContainerId = $this->getId() . '-' . $this->pjaxContainerId;
        $this->formContainerId = $this->getId() . '-' . $this->formContainerId;
        $this->submitButtonId = $this->getId() . '-' . $this->submitButtonId;

        if ($this->url === null) {
            $this->url = Url::canonical();
            Url::remember($this->url, CommentsModule::getInstance()->urlCacheSessionKey);
        } else {
            Url::remember($this->url, CommentsModule::getInstance()->urlCacheSessionKey);
        }

        if ($this->model instanceof Model) {
            $this->model = $this->model->tableName();
        }

        $this->registerAssets();
    }
}
