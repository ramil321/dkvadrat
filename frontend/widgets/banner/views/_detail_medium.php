<?
/**@var \common\models\banner\Banner $model*/
?>
<div class="article-page__banner article-page__banner--medium">
    <a
        href="<?=$model->link?>" <?=($model->image->src ? 'target="_blank"' : '')?>
        style="background-image: url(<?=Yii::$app->resize->prop($model->getImage()->src, 240, 600)?>)"
        class="banner banner--rectangle-detail"></a>
    <span class="article-page__banner-span"><?=$model->name?></span>
</div>
