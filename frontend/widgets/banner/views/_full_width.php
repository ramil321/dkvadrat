<?php
/**@var \common\models\banner\Banner $model */
?>
<a href="<?= $model->link ?>" <?= $model->blank ? 'target="_blank"' : '' ?> class="banner banner--full-width">
    <img src="<?= Yii::$app->resize->prop($model->image->src, 1920, 2000) ?>" alt="banner" class="banner__img">
</a>