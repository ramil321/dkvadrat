<?
/**@var \common\models\banner\Banner $model*/
?>
<div class="article-page__banner article-page__banner--small">
    <a href="<?=$model->link?>" <?=($model->blank ? 'target="_blank"' : '')?>
       style="background-image: url(<?=Yii::$app->resize->prop($model->image->src, 240, 400)?>)"
       class="banner banner--square-detail"></a>
    <span class="article-page__banner-span"><?= $model->name?></span>
</div>
