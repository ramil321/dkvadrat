<?php

namespace frontend\widgets\banner;

use common\models\banner\Banner;
use yii\base\Widget;

class BannerWidget extends Widget
{
    /**
     * @var string
     */
    public $position;

    /**
     * @var array
     */
    public $categoriesIds;

    /**
     * @var array
     */
    public $tagIds;
    /**
     * {@inheritdoc}
     */
    public function run()
    {

        /**@var \common\models\banner\Banner $model*/
        $query = Banner::find()
            ->joinWith('bannerPosition')
            ->joinWith('materialCategories')
            ->joinWith('tags')
            ->where(['banner_position.code' => $this->position])
            ->andWhere(['banner.active' => true])
            ->andWhere(['<=', 'banner.publish_date', time()])
            ->andWhere([
                'or',
                ['>=', 'banner.publish_end_date', time()],
                ['banner.publish_end_date' => null],
            ])
        ;

        $model = $query->one();

        if ($this->tagIds) {
            $findTag = false;
            if($model->tags) {
                foreach ($model->tags as $tag) {
                    if (in_array($tag->id, $this->tagIds)) {
                        $findTag = true;
                    }
                }
                if(!$findTag){
                    return '';
                }
            }
        }

        if ($this->categoriesIds) {
            $findCategory = false;
            if($model->materialCategories) {
                foreach ($model->materialCategories as $category) {
                    if (in_array($category->id, $this->categoriesIds)) {
                        $findCategory = true;
                    }
                }
                if(!$findCategory){
                    return '';
                }
            }
        }


        if(!$model->bannerPosition->template_name){
            return '';
        }
        if($model) {
            return $this->render($model->bannerPosition->template_name,[
                'model' => $model
            ]);
        }
        return '';
    }
}
