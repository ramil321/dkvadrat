<?php

use common\models\material\Material;
use frontend\widgets\newslist\NewsListWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
/**@var \frontend\models\search\SphinxSearchMaterial $model*/
/**@var \common\models\material\MaterialCategories[] $categories*/
/**@var \yii\data\ActiveDataProvider $dataProvider*/
/* @var $this yii\web\View */
?>

<div class="search-page page">
    <div class="search-page__wrapper page__wrapper">
        <div class="search-page__top-wrapper">
            <div class="container">
                <?php $form = ActiveForm::begin([
                    'action' => ['search/index'],
                    'method' => 'get',
                    'options' => ['class' => 'search-page__form'],
                    'fieldConfig' => [
                        'options' => [
                            'tag' => false,
                        ],
                    ],
                ]); ?>
                    <div class="search-page__input-wrapper">
                        <div class="search-page__field search-field">
                            <?= $form->field($model, 'query')->textInput([
                                'maxlength' => true,
                                'class' => 'search-field__input input-dk',
                                'required' => true,
                            ])->label(false); ?>
                            <button type="submit" class="search-field__button">
                                <svg viewBox="0 0 24 24" class="search_small-svg">
                                    <use xlink:href="#search_small"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="search-page__timeline timeline timeline--search-page">
                            <?
                            $items = [

                                date("d.m.Y", strtotime('-1 week')).','.date("d.m.Y") => 'неделя',
                                date("d.m.Y", strtotime('-1 month')).','.date("d.m.Y") => 'месяц',
                                (
                                $model->date != date("d.m.Y", strtotime('-1 week')).','.date("d.m.Y") &&
                                $model->date != '' &&
                                $model->date != date("d.m.Y", strtotime('-1 month')).','.date("d.m.Y") ?
                                    $model->date : 'from'
                                ) => 'от'
                            ];
                            ?>
                            <?= $form->field($model, 'date')->dropDownList(
                                $items,
                                [
                                    'class' => 'select-date',
                                    'prompt' => ['text' => 'За все время', 'options' => ['class' => 'select-date__option']],
                                    'options' => array_map(function () {
                                        return ['class' => 'select-date__option'];
                                    }, $items)
                                ]
                            )->label(false); ?>
                        </div>
                    </div>
                    <h1 class="search-page__title">Результаты поиска -
                        <span class="search-page__title-search"><?=$model->query?></span>
                    </h1>
                    <?if($categories):?>
                    <div class="search-page__filter">
                        <div class="filter-search">
                            <?= $form->field($model, 'category_id')->dropDownList(
                                    ArrayHelper::map($categories,'id','name'),
                                    [
                                        'prompt' => ['text' => 'Все', 'options' => ['class' => 'filter-search__option']],
                                        'class' => 'filter-search__select',
                                        'options' => array_map(function () {
                                            return ['class' => 'filter-search__option'];
                                        }, ArrayHelper::map($categories,'id','name'))
                                    ]
                            )->label(false); ?>
                        </div>
                    </div>
                    <?endif?>
                <?php ActiveForm::end() ?>
            </div>
        </div>

        <div class="container">
            <?if($dataProvider->totalCount > 0):?>
            <?=$this->render('list/_search_list',[
                'dataProvider' => $dataProvider
            ])?>
            <?else:?>
                <p class="search-page__nothing">Ничего не найдено</p>
            <?endif?>
        </div>
    </div>
</div>