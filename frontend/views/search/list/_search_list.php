<?

use frontend\widgets\newslist\NewsListWidget;

/** @var \yii\data\ActiveDataProvider $dataProvider */

echo NewsListWidget::widget([
    'ulClass' => 'search-page__list news__list',
    'liClass' => 'search-page__item',
    'dataProvider' => $dataProvider,
    'itemTemplate' => '_news',
    'itemTemplatePriority' => ''
]); ?>