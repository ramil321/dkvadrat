<?php

use frontend\widgets\newslist\NewsListWidget;

/** @var \yii\data\ActiveDataProvider $dataProvider */
?>
<?=NewsListWidget::widget([
    'ulClass' => 'template tags__content',
    'liClass' => 'tags__item',
    'dataProvider' => $dataProvider,
    'itemTemplate' => '_rubrics',
    'itemTemplatePriority' => false
]); ?>