<?php

use frontend\widgets\newslist\NewsListWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\material\Material;
use common\models\material\MaterialTags;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model MaterialTags */
/* @var $dataProvider frontend\models\material\search\MaterialSearch */
/* @var $categories \common\models\material\MaterialCategories[] */


?>
<div class="tags page">
    <div class="page__wrapper">
        <div class="tags__top-wrapper page__wrapper-top">
            <div class="container">
                <h1 class="page__title"><?=$model->name?></h1>
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'news_filter',
                        'class' => 'tags__form',
                    ],
                    'action' => Url::to(['/'.Yii::$app->request->getPathInfo()]),
                    'method' => 'get'
                ]); ?>
                    <div class="filter-search">
                        <?= $form->field($searchModel, 'materialCategoryId')->dropDownList(ArrayHelper::map($categories, 'id', 'name'), [
                            'prompt' => ['text' => 'Все', 'options' => ['class' => 'filter-search__select']],
                            'class' => 'filter-search__select',
                            'name' => 'materialCategoryId',
                            'options' => array_map(function () {
                                return ['class' => 'filter-search__option'];
                            }, $categories)
                        ])->label(false) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="container">
            <div class="tags__wrapper">
                <?=$this->render('list/_tags_material_all',[
                    'dataProvider' => $dataProvider
                ])?>
            </div>
        </div>
    </div>
</div>