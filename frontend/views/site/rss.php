<?
/** @var \common\models\material\Material[] $models */

?>
<?='<?xml version="1.0" encoding="UTF-8"?>'?>
<rss version="2.0" <?if($arParams["YANDEX"]) echo ' xmlns="http://search.yahoo.com/mrss/" xmlns:yandex="http://news.yandex.ru"';?>>
    <channel>
        <title>Список новостей</title>
        <link><?=Yii::$app->urlManager->createAbsoluteUrl('/')?></link>
        <description>Деловой квадрат</description>
        <lastBuildDate><?=date("r")?></lastBuildDate>
        <ttl>60</ttl>
        <?foreach($models as $model):?>
            <item>
                <title><?=$model->name?></title>
                <link><?=Yii::$app->urlManager->createAbsoluteUrl($model->getUrl())?></link>
                <description><?=$model->preview_text?></description>
                <?if(is_array($arItem['ELEMENT']['arr_DETAIL_PICTURE'])):?>
                    <enclosure url="<?=\Api\Helper::getProtocolLink().$_SERVER['SERVER_NAME']?><?=$arItem['ELEMENT']['arr_DETAIL_PICTURE']['SRC']?>" length="<?=$arItem['ELEMENT']['arr_DETAIL_PICTURE']['FILE_SIZE']?>" type="<?=$arItem['ELEMENT']['arr_DETAIL_PICTURE']['CONTENT_TYPE']?>"/>
                <?endif?>
                <?if($arItem["category"]):?>
                    <?
                    $exCat = explode("/",$arItem["category"]);
                    $arItem["category"] = $exCat[1];
                    ?>
                    <category><?=$arItem["category"]?></category>
                <?endif?>
                <?if($arParams["YANDEX"]):?>
                    <yandex:full-text><?=$arItem["full-text"]?></yandex:full-text>
                <?endif?>
                <pubDate><?=date("r", $model->publish_date)?></pubDate>
            </item>
        <?endforeach?>
    </channel>
</rss>