<?
/** @var \common\models\material\Material[] $models */

?>
<?='<?xml version="1.0" encoding="UTF-8"?>'?>
<rss version="2.0" <?echo 'xmlns="http://search.yahoo.com/mrss/" xmlns:yandex="http://news.yandex.ru"';?>>
    <channel>
        <title>Список новостей</title>
        <link><?=Yii::$app->urlManager->createAbsoluteUrl('/')?></link>
        <description>Деловой квадрат</description>
        <lastBuildDate><?=date("r")?></lastBuildDate>
        <ttl>60</ttl>
        <yandex:logo>
            <?=Yii::$app->urlManager->createAbsoluteUrl('/images/logo.png')?>
        </yandex:logo>
        <?foreach($models as $model):?>
            <item>
                <title><?=$model->name?></title>
                <link><?=Yii::$app->urlManager->createAbsoluteUrl($model->getUrl())?></link>
                <?if($model->previewImage || $model->detailImage):?>
                    <?$image = $model->previewImage ? $model->previewImage : $model->detailImage ?>
                    <enclosure
                        url="<?=Yii::$app->urlManager->createAbsoluteUrl($image->src)?>"
                        length="<?=$image->getSize()?>"
                        type="<?=$image->getFormat()?>"/>
                <?endif?>
                    <category><?=$model->mainCategory->name?></category>
                    <yandex:full-text><?=$model->preview_text?></yandex:full-text>
                <pubDate><?=date("r", $model->publish_date)?></pubDate>
            </item>
        <?endforeach?>
    </channel>
</rss>