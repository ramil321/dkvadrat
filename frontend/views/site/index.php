<?php

/* @var $this yii\web\View */
/* @var $models Constructor[] */
use backend\modules\area\widgets\AreaImage;
use backend\modules\constructor\models\Constructor;
use backend\modules\constructor\models\RenderSections;
use frontend\widgets\render\RenderConstructorWidget;
use yii\helpers\Json;

$this->title = 'Деловой квадрат';
?>
<?=RenderConstructorWidget::widget([
    'constructorModels' => $models,
    'constructorType' => Constructor::TYPE_MAIN
])?>