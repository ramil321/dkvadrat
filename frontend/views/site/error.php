<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<main class="main-dk">
    <div class="page-not-found">
        <div class="container">
            <div class="page-not-found__wrapper">
                <h1 class="page-not-found__title">Страницы не существует</h1>
                <div class="page-not-found__bg-wrapper">
                    <div class="page-not-found__background"></div>
                </div>
            </div>
        </div>
    </div>
</main>