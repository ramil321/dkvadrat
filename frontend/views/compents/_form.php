<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use ogheo\comments\helpers\CommentsHelper;

/** @var $commentModel */
/** @var $widget */

?>
<div id="<?= $widget->formContainerId ?>" class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 comments-input">

        <?php if (Yii::$app->user->isGuest && ($widget->guestComments === false)): ?>

            <div class="disabled-form">
                <?= Yii::t('comments', '<a href="{url}">Log in</a> to post a comment.', ['url' => Url::to(Yii::$app->getUser()->loginUrl)]) ?>
            </div>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'action' => Url::to(
                    [
                        '/comments/default/create', 'data' => CommentsHelper::encryptData(
                        [
                            'url' => $commentModel->url,
                            'model' => $commentModel->model,
                            'model_key' => $commentModel->model_key,
                        ]
                    )
                    ]
                ),
//                'validationUrl' => Url::to(['comments/default/validate']),
//                'validateOnChange' => false,
//                'validateOnBlur' => false,
                'options' => [
                    'id' => $widget->formId,
                    'class' => 'comments__form',
                ],
                'fieldConfig' => ['options' => ['style' => ['width' => '100%']]],
            ]) ?>
                    <?= $form->field($commentModel, 'content', ['template' => '{input}{error}'])->textInput(['class' => 'comments__input input-dk', 'placeholder' => 'Напишите свой комментарий']) ?>
                            <?php if (Yii::$app->user->isGuest): ?>
                                <?php if ($commentModel->username === null || $commentModel->email === null) { ?>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 user-data">
                                        <?= $form->field($commentModel, 'username', [
                                            'enableClientValidation' => true,
                                            'enableAjaxValidation' => true,
                                            'template' => '{input}'
                                        ])->textInput([
                                            'maxlength' => true,
                                            'class' => 'form-control input-sm',
                                            'placeholder' => Yii::t('comments', 'Name')
                                        ])->label(false) ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 user-data">
                                        <?= $form->field($commentModel, 'email', ['template' => '{input}'])->textInput([
                                            'maxlength' => true,
                                            'email' => true,
                                            'class' => 'form-control input-sm',
                                            'placeholder' => Yii::t('comments', 'Email')
                                        ])->label(false) ?>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 user-data">
                                        <?= Yii::t('comments', 'As') . ' <b>' . $commentModel->username . '</b>'; ?>
                                    </div>
                                <?php } ?>
                            <?php else: ?>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 user-data">
                                    <?= Yii::t('comments', 'As') . ' <b>' . Yii::$app->user->identity->username . '</b>'; ?>
                                </div>
                            <?php endif; ?>
<!--                            --><?//= Html::button(
//                                Yii::t('comments', 'Cancel'), [
//                                    'class' => 'btn btn-default btn-xs reply-cancel',
//                                    'type' => 'reset',
//                                    'data' => [
//                                        'action' => 'cancel-reply'
//                                    ]
//                                ]
//                            ) ?>
                            <button type="submit" class="button button--send comments__button">Отправить</button>


                    <!-- To check -->
                    <noscript>
                        <div class="media-buttons active-media-buttons text-right">
                            <?= Html::button(
                                Yii::t('comments', 'Cancel'), [
                                    'id' => 'reply-cancel',
                                    'class' => 'btn btn-default btn-xs reply-cancel',
                                    'type' => 'reset',
                                    'data' => [
                                        'action' => 'cancel-reply'
                                    ]
                                ]
                            )
                            ?>
                            <?= Html::submitButton(
                                Yii::t('comments', 'Post'), [
                                    'class' => 'btn btn-primary btn-xs comment-submit',
                                ]
                            )
                            ?>
                        </div>
                    </noscript>

            <?php $form->end(); ?>

        <?php endif; ?>

</div>
