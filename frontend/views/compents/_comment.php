<?php

use yii\helpers\Html;
use ogheo\comments\helpers\CommentsHelper;

/** @var $model */
/** @var $nestedLevel */
/** @var $maxNestedLevel */
/** @var $widget */

?>
<div class="comment">
    <div class="media-container">
        <div class="media-left">
            <?= $model->getAuthorUrl() === null ? (
            $model->getAuthorAvatar() === null ?
                Html::tag(
                    'span', '', ['class' => 'media-object img-rounded without-image']
                ) : Html::img(
                $model->getAuthorAvatar(),
                [
                    'class' => 'media-object img-rounded',
                    'alt' => $model->getAuthorName()
                ]
            )
            ) : Html::a(
                $model->getAuthorAvatar() === null ?
                    Html::tag(
                        'span', '', ['class' => 'media-object img-rounded without-image']
                    ) : Html::img(
                    $model->getAuthorAvatar(), [
                        'class' => 'media-object img-rounded',
                        'alt' => $model->getAuthorName()
                    ]
                ), [$model->getAuthorUrl()]
            ) ?>
        </div>
        <div class="media-body">
            <div class="media-info">
                <h4 class="media-heading comment__header">
                    <?= $model->getAuthorUrl() === null ? $model->getAuthorName() : Html::a(
                        $model->getAuthorName(), [$model->getAuthorUrl()]
                    ) ?>
                    <time class="comment__time"><?= $model->getPostedDate() ?></time>
                </h4>
                <?= Html::encode($model->content); ?>
                <div class="comment__footer">
                    <button class="button comment__answers">
                        <?php if($model->hasChildren()):?>
                        <div class="comment__svg">
                            <svg viewBox="0 0 12 12" class="angle-small-svg">
                                <use xlink:href="#angle-small"></use>
                            </svg>
                        </div>
                            <?php
                        $countChildren = count($model->getChildren());
                            echo $countChildren;
                            echo ($countChildren && $countChildren == 1) ? ' ответ' : ($countChildren < 5 ? ' ответа' : ' ответов');
                            ?>
                        <?php endif;?>
                    </button>
                    <?php if ($nestedLevel < $maxNestedLevel): ?>
                        <div class="comment-reply" data-action="reply">
                            <a href="#">Ответить</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="comment comment--answer">
                <?php if ($nestedLevel < $maxNestedLevel): ?>
                    <?php if ($model->hasChildren()) : ?>
                        <?php $nestedLevel++; ?>
                        <?php foreach ($model->getChildren() as $children) : ?>
                            <div class="media" data-key="<?php echo CommentsHelper::encodeId($children->id); ?>">
                                <?= $this->render('_comment', [
                                    'model' => $children,
                                    'nestedLevel' => $nestedLevel,
                                    'maxNestedLevel' => $maxNestedLevel,
                                    'widget' => $widget
                                ]) ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
