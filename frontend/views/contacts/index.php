<?php
/* @var $this yii\web\View */
/** @var \common\models\Feedback $feedback */
/** @var \common\models\company\Company[] $companies*/
/** @var boolean $feedBackSended */

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\Map;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>

<main class="main-dk">
    <div class="contacts page">
        <div class="page__wrapper">
            <div class="contacts__top-wrapper page__wrapper-top">
                <div class="container">
                    <h1 class="page__title">Контакты</h1>
                    <?php if($model):?>
                        <div class="contacts__top">
                            <div class="contacts__info-wrapper">
                                <ul class="contacts__list">
                                    <li class="contacts__item"><span class="contacts__name">Адрес редакции</span><span class="contacts__info"><?= $model->editorial_address ?></span></li>
                                    <li class="contacts__item"><span class="contacts__name">Телефон редакции и производственного отдела</span><a href="tel:<?= $model->phone_editors ?>" class="contacts__info _link"><?= $model->phone_editors ?></a></li>
                                    <li class="contacts__item"><span class="contacts__name">Телефон отдела рекламы</span><a href="tel:<?= $model->phone_advertising ?>" class="contacts__info _link"><?= $model->phone_advertising ?></a></li>
                                    <li class="contacts__item"><span class="contacts__name">E-mail</span><a href="mailto:<?= $model->email ?>" class="contacts__info _link"><?= $model->email ?></a></li>

                                </ul>
                                <div class="contacts__link-wrapper"><a href="<?= $model->advertisers_href ?>"
                                                                       class="contacts__link">Рекламодателям</a></div>
                            </div>
                            <div class="contacts__map">
                                <?php
                                $coord = new LatLng(['lat' => $model->lat, 'lng' => $model->lng]);

                                $map = new Map([
                                    'center' => $coord,
                                    'zoom' => 14,
                                    'height' => 280,
                                    'width' => 'auto',
                                ]);

                                $marker = new Marker([
                                    'position' => $coord,
                                    'title' => $model->map_title,
                                    'label' => $model->map_title,
                                ]);

                                $marker->attachInfoWindow(
                                    new InfoWindow([
                                        'content' => $model->map_title,
                                    ])
                                );
                                // Add marker to the map
                                $map->addOverlay($marker);

                                // Display the map -finally :)
                                echo $map->display();
                                ?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="container">
                <div class="contacts__feedback feedback">
                    <h3 class="feedback__title">Обратная связь</h3>
                    <p class="feedback__text">Оставьте свое сообщение и мы обязательно на него ответим!</p>
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'feedback__form '.($feedBackSended ? 'feedback__form--hide' : ''),
                        ],
                        'action' => '#',
                        'method' => false,
                        'fieldConfig' => [
                            'options' => [
                                'tag' => false,
                            ],
                        ]
                    ]); ?>
                    <div class="feedback__left">
                        <div class="feedback__field">
                            <?= $form->field($feedback, 'name',[
                                'template' => '{input}{label}{error}{hint}',
                            ])->textInput([
                                'maxlength' => true,
                                'class' => 'input-dk feedback__input',
                                'id' => 'feedback__name',
                                'required' => true
                            ])->label('Имя*',[
                                'class' => 'feedback__label'
                            ]) ?>
                        </div>
                        <div class="feedback__field">
                            <?= $form->field($feedback, 'email',[
                                'template' => '{input}{label}{error}{hint}',
                            ])->textInput([
                                'maxlength' => true,
                                'type' => 'email',
                                'class' => 'input-dk feedback__input',
                                'id' => 'feedback__email',
                                'required' => true
                            ])->label('E-mail для ответа*',[
                                'class' => 'feedback__label'
                            ]) ?>
                        </div>
                        <div class="feedback__field">
                            <?= $form->field($feedback, 'phone',[
                                'template' => '{input}{label}{error}{hint}',
                            ])->textInput([
                                'maxlength' => true,
                                'class' => 'input-dk feedback__input feedback__input--phone',
                                'id' => 'phone',
                                'required' => true
                            ])->label('Номер телефона*',[
                                'class' => 'feedback__label'
                            ]) ?>
                        </div>
                    </div>
                    <div class="feedback__right">
                        <div class="feedback__field">
                            <?= $form->field($feedback, 'description',[
                                'template' => '{input}{label}{error}{hint}',
                            ])->textarea([
                                'maxlength' => true,
                                'class' => 'textarea-dk feedback__textarea',
                                'id' => 'feedback__message',
                                'required' => true
                            ])->label('Сообщение*',[
                                'class' => 'feedback__label'
                            ]) ?>
                        </div>
                    </div>
                    <?= Html::submitButton('Отправить', ['class' => 'button button--send']) ?>

                    <?php ActiveForm::end(); ?>
                    <div class="feedback__success <?= $feedBackSended ? 'feedback__success--visible' : ''?>">
                        <h5 class="feedback__success-title">Сообщение успешно отправлено!</h5>
                        <button class="button button--apply feedback__button-apply">Отправить еще одно</button>
                    </div>
                </div>
                <?php foreach ($companies as $company): ?>
                <div class="contacts__employees section-employees">
                    <h2 class="section-employees__title"><?=$company->name?></h2>
                    <ul class="section-employees__list">
                        <?foreach ($company->persons as $person):?>
                        <li class="section-employees__item">
                            <div class="employee">
                                <div class="employee__image"><img src="<?= $person->photo->src ?>" alt="" class="employee__img"></div>
                                <div class="employee__content">
                                    <h4 class="employee__name"><?=$person->fio?></h4>
                                    <span class="employee__span"><?= $person->category->name ?></span>
                                    <ul class="employee__list">
                                        <?if($person->phone_stationary):?>
                                        <li class="employee__item">
                                            <span class="employee__info-name">Стационарный</span>
                                            <a href="tel:<?=$person->phone_stationary?>" class="employee__info _link"><?=$person->phone_stationary?></a>
                                        </li>
                                        <?endif?>
                                        <?if($person->fax):?>
                                        <li class="employee__item">
                                            <span class="employee__info-name">Факс</span>
                                            <a href="tel:<?=$person->fax?>" class="employee__info _link"><?=$person->fax?></a>
                                        </li>
                                        <?endif?>
                                        <?if($person->email):?>
                                        <li class="employee__item">
                                            <span class="employee__info-name">E-mail</span>
                                            <span class="employee__info-wrapper">
                                            <a href="mailto:<?=$person->email?>" class="employee__info _link"><?=$person->email?></a>
                                        </li>
                                        <?endif?>
                                    </ul>
                                    <div class="social">
                                        <ul class="social__list">
                                        <?if($person->vk_href):?>
                                            <li class="social__item">
                                                <a href="<?=$person->vk_href?>" class="social__link social__link--vk">
                                                    <div class="social__icon">
                                                        <svg viewBox="0 0 14 8" class="vk-svg">
                                                            <use xlink:href="#vk"></use>
                                                        </svg>
                                                    </div>
                                                </a>
                                            </li>
                                        <?endif?>
                                        <?if($person->fb_href):?>
                                            <li class="social__item">
                                                <a href="<?=$person->fb_href?>" class="social__link social__link--facebook">
                                                    <div class="social__icon">
                                                        <svg viewBox="0 0 6 13" class="facebook-svg">
                                                            <use xlink:href="#facebook"></use>
                                                        </svg>
                                                    </div>
                                                </a>
                                            </li>
                                        <?endif?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?endforeach;?>
                    </ul>
                </div>
                <?endforeach;?>
            </div>
        </div>
    </div>
</main>