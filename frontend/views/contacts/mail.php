<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
/* @var $model \common\models\Feedback string main view render result */
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    Имя: <?= $model->name ?> <br>
    Телефон: <?= $model->phone ?> <br>
    Email: <a href="mailto:<?= $model->email ?>"><?= $model->email ?></a> <br>
    Текст сообщения: <?= $model->description ?> <br>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>