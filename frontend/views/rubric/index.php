<?php
use common\models\material\Material;
use common\models\material\MaterialRegion;
use frontend\widgets\newslist\NewsListWidget;
use frontend\widgets\sortnews\SortNewsWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $newsFilter \frontend\models\material\filters\NewsFilter */
/* @var $model common\models\material\Material */
/* @var $materialsProvider frontend\models\material\search\MaterialSearch */
/* @var $materialCategories common\models\material\MaterialCategories */
/** @var \common\models\material\MaterialCategories[] $sections */
/** @var \common\models\material\MaterialCategories $currentCategory */

$this->title = $currentCategory->name;
?>
<div class="rubric page">
    <div class="page__wrapper">
        <div class="rubric__top">
            <div class="top-section">
                <div class="container">
                    <div class="top-section__popular">
                        <div class="popular">
                            <ul class="popular__list">
                                <?foreach ($tags as $tag):?>
                                    <li class="popular__item"><a href="<?=$tag->getUrl()?>" class="popular__link"><?=$tag->name?></a></li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="top-section__filters">
                        <h1 class="top-section__title"><?=$this->title?></h1>
                        <?=SortNewsWidget::widget([
                            'categories' => $categories,
                            'model' => $newsFilter,
                            'categoryId' => $currentCategory->id
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="rubric__content">
                <?=$this->render('list/_rubric_list',[
                    'materialsProvider' => $materialsProvider,
                ])?>
            </div>
        </div>
    </div>
</div>