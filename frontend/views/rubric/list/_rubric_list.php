<?php

use frontend\widgets\newslist\NewsListWidget;

/* @var $this yii\web\View */
/* @var $materialsProvider frontend\models\material\search\MaterialSearch */


?>
<?=NewsListWidget::widget([
    'ulClass' => 'template template--one_important_six_blocks content-more',
    'liClass' => 'template__item is-visible',
    'dataProvider' => $materialsProvider,
    'itemTemplate' => '_rubrics',
    'shortNewsPosition' => 3,
    'itemTemplatePriority' => '_rubrics_priority'
]); ?>