<?php


use frontend\models\journal\serach\JournalSearch;
use frontend\widgets\comments\Comments;
use common\models\journal\Journal;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\journal\serach\JournalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Журнал';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="journal-index">
    <div class="journal page">
        <div class="page__wrapper">
            <div class="journal__top-wrapper page__wrapper-top">
                <div class="container">
                    <h1 class="page__title"><?=$this->title?></h1>
                    <?php $form = ActiveForm::begin([
                            'method' => 'get',
                            'options' => [
                                'class' => 'journal__timeline timeline'
                            ],
                            'action' => Url::to(['/journal'])
                        ]) ?>
                        <?= $form->field($searchModel, 'year')->dropDownList(
                            JournalSearch::getYears(),
                            [
                                'class' => 'timeline__select',
                                'prompt' => 'За все время'
                            ]
                        )->label(false); ?>

                    <?php ActiveForm::end() ?>
                </div>
            </div>
            <div class="container">
                <div class="journal__tab-content">
                    <?=$this->render('list/_journal_list',[
                        'dataProvider' => $dataProvider,
                    ])?>
                </div>
            </div>
        </div>
    </div>
    <?php
    /*echo Comments::widget([
        'formView' => '@frontend/views/compents/_form',
        'commentsView' => '@frontend/views/compents/comments',
        'commentView' => '@frontend/views/compents/_comment',
//        'commentsPerPage' => 20,
        //Кол-во выводимых записей нужно ставить в двух местах More::quantity и в этом месте
    ]);*/
    ?>
</div>
