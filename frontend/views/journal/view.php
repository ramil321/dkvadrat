<?php

use frontend\widgets\dlist\ListWidget;
use frontend\widgets\newslist\NewsListWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \common\models\journal\Journal $model*/
/* @var $this yii\web\View */
/* @var $journal common\models\journal\Journal */
/* @var $journalDataProvider yii\data\ActiveDataProvider */
/* @var $materialDataProvider yii\data\ActiveDataProvider */

$this->title = $model->number;

?>


<div class="journal-slider slider">
    <div class="slider__nav">
        <div class="slider__prev-wrapper">
            <button class="slider__prev-button">
                <svg viewBox="0 0 10 18" class="arrow-left">
                    <use xlink:href="#arrow-left"></use>
                </svg>
            </button>
        </div>
        <div class="slider__next-wrapper">
            <button class="slider__next-button">
                <svg viewBox="0 0 10 18" class="arrow-right">
                    <use xlink:href="#arrow-right"></use>
                </svg>
            </button>
        </div>
    </div>
    <div class="container">
        <div class="slider__link-top"> <a href="<?=Url::to(['/journal'])?>" class="slider__link">К списку журналов
                <svg viewBox="0 0 11 12" class="arrow-svg arrow-svg--left">
                    <use xlink:href="#arrow"></use>
                </svg></a></div>
        <div class="slider__wrapper">
            <ul class="journal__list journal__list--slider">
            <?foreach ($journalDataProvider->getModels() as $slide):?>
                <li class="journal__item <?=($slide->id == $model->id ? 'journal__item--slide-active' : '')?>">
                    <div class="journal-item">
                        <a href="<?=$slide->getUrl()?>" class="journal-ajax item__block-link"></a>
                        <div class="journal-item__wrapper">
                            <div class="journal-item__image">
                                <img src="<?=Yii::$app->resize->prop($slide->image->src, 135, 180)?>" alt="" class="journal-item__img">
                                <div class="journal-item__overlay"><span class="journal-item__overlay-link">Читать</span></div>
                            </div>
                            <div class="journal-item__info"><span class="journal-item__number"><?=$slide->number?></span>
                                <time class="journal-item__date"><?=$slide->getMonthFormated()?></time>
                            </div>
                            <div class="journal-item__links links">
                                <div class="links__wrapper"><a href="<?=$slide->file->src?>" class="links__link">
                                        <svg viewBox="0 0 17 18" class="pdf-svg">
                                            <use xlink:href="#pdf"></use>
                                        </svg></a><a download href="<?=$slide->file->src?>" class="links__link">
                                        <svg viewBox="0 0 11 12" class="arrow-down-svg">
                                            <use xlink:href="#arrow-down"></use>
                                        </svg></a></div>
                            </div>
                        </div>
                    </div>
                </li>
            <?endforeach;?>
            </ul>
        </div>
    </div>
</div>
<div class="journal-view" id="journal_view">
    <?=$this->render('_journal_view',[
        'model' => $model,
        'materialDataProvider' => $materialDataProvider
    ])?>
</div>