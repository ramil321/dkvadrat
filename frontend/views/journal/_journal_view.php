<?
/** @var \yii\data\ActiveDataProvider $materialDataProvider */
/** @var \common\models\journal\Journal $model */
?>
<div class="journal-view__top-wrapper">
    <div class="container">
        <h1 class="journal-view__title"><?=$model->number?> <?=$model->getMonthFormated()?></h1>
        <div class="journal-view__social">
            <div class="social">
                <div class="social__wrapper">
                    <ul class="social__list">
                        <li class="social__item"><a href="#" data-social="vkontakte" class="social__link social__link--vk">
                                <div class="social__icon">
                                    <svg viewBox="0 0 14 8" class="vk-svg">
                                        <use xlink:href="#vk"></use>
                                    </svg>
                                </div>
                                <div data-counter="vkontakte" class="social__count"></div></a></li>
                        <li class="social__item"><a href="#" data-social="facebook" class="social__link social__link--facebook">
                                <div class="social__icon">
                                    <svg viewBox="0 0 6 13" class="facebook-svg">
                                        <use xlink:href="#facebook"></use>
                                    </svg>
                                </div>
                                <div data-counter="vkontakte" class="social__count"></div></a></li>
                        <li class="social__item"><a href="#" data-social="odnoklassniki" class="social__link social__link--ok">
                                <div class="social__icon">
                                    <svg viewBox="0 0 8 13" class="ok-svg">
                                        <use xlink:href="#ok"></use>
                                    </svg>
                                </div>
                                <div data-counter="odnoklassniki" class="social__count"></div></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <?=
    $this->render('list/_material_list',[
        'materialDataProvider' => $materialDataProvider
    ])
    ?>
</div>