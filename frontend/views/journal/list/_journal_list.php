<?php

use frontend\widgets\dlist\ListWidget;


/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var integer $categoryId */
?>
<?=

ListWidget::widget([
    'ulClass' => 'journal__list',
    'liClass' => 'journal__item',
    'dataProvider' => $dataProvider,
    'itemTemplate' => '_journal'
]); ?>

