<?

/**@var \yii\data\ActiveDataProvider $materialDataProvider*/

use frontend\widgets\newslist\NewsListWidget;


echo NewsListWidget::widget([
    'ulClass' => 'journal-view__list',
    'liClass' => 'journal-view__item',
    'disableDefaultUlClass' => false,
    'dataProvider' => $materialDataProvider,
    'itemTemplate' => '_news',
    'itemTemplatePriority' => ''
]); ?>