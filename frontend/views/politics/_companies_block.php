<?
/** @var \common\models\company\Company[] $companies*/


?>
<?php foreach ($companies as $company): ?>
    <div class="<?=$wrapperClass?> contacts-section">
        <h4 class="contacts-section__title"><?=$company->name?></h4>
        <ul class="contacts-section__list">
            <?foreach ($company->persons as $person):?>
                <li class="contacts-section__item">
                    <div class="employee">
                        <div class="employee__image"><img src="<?= $person->photo->src ?>" alt="" class="employee__img"></div>
                        <div class="employee__content">
                            <h4 class="employee__name"><?=$person->fio?></h4>
                            <span class="employee__span"><?= $person->category->name ?></span>
                            <ul class="employee__list">
                                <?if($person->phone_stationary):?>
                                    <li class="employee__item">
                                        <span class="employee__info-name">Стационарный</span>
                                        <a href="tel:<?=$person->phone_stationary?>" class="employee__info _link"><?=$person->phone_stationary?></a>
                                    </li>
                                <?endif?>
                                <?if($person->fax):?>
                                    <li class="employee__item">
                                        <span class="employee__info-name">Факс</span>
                                        <a href="tel:<?=$person->fax?>" class="employee__info _link"><?=$person->fax?></a>
                                    </li>
                                <?endif?>
                                <?if($person->email):?>
                                    <li class="employee__item">
                                        <span class="employee__info-name">E-mail</span>
                                        <span class="employee__info-wrapper"><a href="mailto:delkvadrat@mail.ru" class="employee__info _link"><?=$person->email?></a></span>
                                    </li>
                                <?endif?>
                            </ul>
                            <div class="social">
                                <ul class="social__list">
                                    <?if($person->vk_href):?>
                                        <li class="social__item">
                                            <a href="<?=$person->vk_href?>" class="social__link social__link--vk">
                                                <div class="social__icon">
                                                    <svg viewBox="0 0 14 8" class="vk-svg">
                                                        <use xlink:href="#vk"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                        </li>
                                    <?endif?>
                                    <?if($person->fb_href):?>
                                        <li class="social__item">
                                            <a href="<?=$person->fb_href?>" class="social__link social__link--facebook">
                                                <div class="social__icon">
                                                    <svg viewBox="0 0 6 13" class="facebook-svg">
                                                        <use xlink:href="#facebook"></use>
                                                    </svg>
                                                </div>
                                            </a>
                                        </li>
                                    <?endif?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            <?endforeach;?>
        </ul>
    </div>
<?endforeach;?>