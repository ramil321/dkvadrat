<?php
/** @var \common\models\company\Company[] $companies*/

$this->title = 'Рекламодателям';

?>
<div class="politics">
    <header class="politics__header">
        <div class="container">
            <div class="politics__wrapper">
                <div class="politics__title-wrapper">
                    <h1 class="politics__title">Рекламодателям</h1>
                    <p class="politics__text">Нам приятно сообщить вам, что в 2019 году редакция журнала «Деловой квадрат» предлагает комплексные решения в продвижении ваших товаров и услуг.  Мы ценим время и деньги наших клиентов и делаем все, чтобы работа с нами была удобной, а ваша реклама максимально эффективной. </p>
                    <button class="button button--politics">Скачать прайс</button>
                </div>
                <div class="politics__image">
                    <img src="images/politics/politics-header.svg" alt="" class="politics__img">
                </div>
            </div>
        </div>
    </header>
    <section class="politics__advantages advantages">
        <div class="advantages__top">
            <div class="container">
                <div class="advantages__inner">
                    <div class="advantages__wrapper">
                        <h3 class="advantages__title"><span class="advantages__title-question">Как нам это удается? </span>– спросите вы.<span class="advantages__title-span">С радостью отвечаем на ваши вопросы:</span></h3>
                        <ul class="advantages__list">
                            <li class="advantages__item">
                                <div class="advantages-block">
                                    <h3 class="advantages-block__title">серьезность</h3><span class="advantages-block__number">1</span>
                                    <p class="advantages-block__text">«Деловой квадрат» отличает набор <b>серьезных аналитических редакционных материалов, </b>которые максимально полезны нашим читателям.</p>
                                    <p class="advantages-block__text">Именно уникальность собственного контента и его высокая читабельность гарантируют востребованность как самого издания, так и ваших рекламных статей.</p>
                                </div>
                            </li>
                            <li class="advantages__item">
                                <div class="advantages-block">
                                    <h3 class="advantages-block__title">внимательность</h3><span class="advantages-block__number">2</span>
                                    <p class="advantages-block__text"> <b>Мы детально знаем нашего читателя </b>– это руководители высшего и среднего звена крупных, средних и малых предприятий Удмуртии из всех сфер экономики, а также профильные специалисты.</p>
                                </div>
                            </li>
                            <li class="advantages__item">
                                <div class="advantages-block">
                                    <h3 class="advantages-block__title">охват</h3><span class="advantages-block__number">3</span>
                                    <p class="advantages-block__text">Читательская аудитория обеспечивается обширной адресной рассылкой, которая <b>охватывает не только наш регион, но и выходит за его пределы. </b>Тираж издания – 5 000 экземпляров.</p>
                                </div>
                            </li>
                            <li class="advantages__item">
                                <div class="advantages-block">
                                    <h3 class="advantages-block__title">эффективность</h3><span class="advantages-block__number">4</span>
                                    <p class="advantages-block__text"> <b>Эффективность рекламы </b>в журнале гарантирована отличными текстами, яркими фотографиями и современной версткой ваших статей и модулей.</p>
                                </div>
                            </li>
                            <li class="advantages__item">
                                <div class="advantages-block">
                                    <h3 class="advantages-block__title">мультиканальность</h3><span class="advantages-block__number">5</span>
                                    <p class="advantages-block__text">Материалы каждого номера, включая рекламные, размещаются на нашем сайте, что <b>многократно умножает число читателей вашей рекламы.</b></p>
                                </div>
                            </li>
                            <li class="advantages__item">
                                <div class="advantages-block">
                                    <h3 class="advantages-block__title">доверие</h3><span class="advantages-block__number">6</span>
                                    <p class="advantages-block__text">Лучшее доказательство вышеизложенному – наши клиенты, которые работают с нами по многу лет, доверяя нам такие важные вопросы как <b>имидж и продвижение.</b></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="advantages__block-text-wrapper">
            <div class="container">
                <div class="advantages__inner">
                    <p class="advantages__block-text">Обращаем внимание тех, кто заинтересован в продаже товаров и услуг. Мы обеспечиваем вам высокодоходную группу населения нашей республики. Воспользуйтесь этим! </p>
                </div>
            </div>
        </div>
    </section>
    <section class="politics__advertising advertising">
        <div class="container">
            <h2 class="advertising__title">Как можно разместить у нас рекламу? - это очень просто сделать. </h2>
            <div class="advertising__contact advertising-contact">
                <div class="advertising-contact__item">
                    <h4 class="advertising-contact__title">Пишите нам по адресам</h4>
                    <div class="advertising-contact__content">
                        <svg viewBox="0 0 24 24" class="mail">
                            <use xlink:href="#mail"></use>
                        </svg>
                        <div class="advertising-contact__links"><a href="mailto:delovoykvadrat@udm.net.ru" class="advertising-contact__link _link">delovoykvadrat@udm.net.ru</a><a href="mailto:delovoy@udm.net.ru" class="advertising-contact__link _link">delovoy@udm.net.ru</a></div>
<!--                        <div class="advertising-contact__links"><span class="advertising-contact__link">delovoykvadrat@udm.net.ru</span><span class="advertising-contact__link">delovoy@udm.net.ru</span></div>-->
                    </div>
                </div>
                <div class="advertising-contact__item">
                    <h4 class="advertising-contact__title">Звоните в редакцию</h4>
                    <div class="advertising-contact__content">
                        <svg viewBox="0 0 24 24" class="phone-call">
                            <use xlink:href="#phone-call"></use>
                        </svg>
                        <div class="advertising-contact__links"><a href="tel:73412794150" class="advertising-contact__link _link">+7 (3412) 79-41-50</a><a href="tel:799550" class="advertising-contact__link _link">79-95-50</a></div>
<!--                        <div class="advertising-contact__links"><span class="advertising-contact__link">+7 (3412) 79-41-50</span><span class="advertising-contact__link">79-95-50</span></div>-->
                    </div>
                </div>
            </div>
            <?=$this->render('_companies_block',[
                'companies' => ($companies['administration'] ? [$companies['administration']] : []),
                'wrapperClass' => 'advertising__journalist'
            ])?>
            <div class="advertising__info info-section">
                <div class="info-section__wrapper">
                    <div class="info-section__title-wrapper">
                        <h1 class="info-section__title">Реклама на сайте</h1>
                        <p class="info-section__text">Новая версия нашего сайта также является эффективной рекламной площадкой. Сегодня наш сайт входит TOP 100 рейтинга Uralweb.</p>
                        <p class="info-section__text">Новости сайта учитывают интересы нашей целевой аудитории.</p>
                        <p class="info-section__text">На сайте вы можете разместить как баннерную, так и текстовую рекламу.</p>
                    </div>
                    <div class="info-section__image"><img src="images/politics/macbook.svg" alt="" class="info-section__img"></div>
                </div>
            </div>

            <?
            $bottomCompanies = [];
            if($companies['sellers']){
                $bottomCompanies[] = $companies['sellers'];
            }
            if($companies['journal']){
                $bottomCompanies[] = $companies['journal'];
            }
            ?>
            <?=$this->render('_companies_block',[
                'companies' => $bottomCompanies,
                'wrapperClass' => 'advertising__accommodation'
            ])?>
        </div>
    </section>
    <section class="politics__footer publisher">
        <div class="container">
            <div class="publisher__wrapper">
                <div class="publisher__image"><img src="images/politics/publisher.jpg" alt=""></div>
                <p class="publisher__text">Благодарим за то, что выбрали нас для вашей рекламы. <br>С уважением, Наталья Кондратьева, издатель</p>
            </div>
        </div>
    </section>
</div>