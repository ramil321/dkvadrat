<?
/**@var \common\models\person\Person $model */
?>
<li class="person__item">
    <div class="item person">
        <figure class="item__figure"><a href="#" class="item__category">Правительство УР</a>
            <div class="item__name-wrapper">
                <h4 class="item__title"><a href="#" class="item__title-link"><?=$model->fio?></a></h4>
                <div class="item__image"><img src="<?Yii::$app->resize->crop($model->photo->src)?>" alt="" class="person__img"></div>
            </div>
            <figcaption class="item__figcaption">
                <p class="item__text"><?=$model->position?></p>
            </figcaption>
        </figure>
    </div>
</li>