<?php

use common\models\material\Material;
use frontend\models\material\filters\OrganizationFilter;
use frontend\models\material\filters\PersonFilter;
use frontend\models\material\search\MaterialSearch;
use frontend\widgets\newslist\NewsListWidget;
use frontend\widgets\sortnews\SortNewsWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;



/** @var MaterialSearch $searchModel */
/** @var PersonFilter $personFilter */
/** @var OrganizationFilter $organizationFilter */
/** @var \common\models\material\MaterialCategories $currentCategory */
/** @var \common\models\material\MaterialCategories[] $tabs */
/** @var \common\models\material\MaterialCategories[] $organizationCategories */
/** @var Material[] $priorityMaterialToday */
/** @var \yii\data\ActiveDataProvider $personMaterialsProvider */
/** @var \yii\data\ActiveDataProvider $organizationMaterialsProvider */
/** @var \common\models\material\MaterialTags[] $tags*/

$this->title = $currentCategory->name;
?>
<div class="who page">
    <div class="page__wrapper">
        <div class="who__top-wrapper page__wrapper-top">
            <div class="container">
                <h1 class="page__title">Кто есть кто</h1>
                <div class="who__tabs tabs">
                    <?foreach ($tabs as $key=>$tab):?>
                    <button data-tab="#tab-<?=$key?>" class="button tabs__button <?= $tab->type_code == $currentCategory->type_code ? 'tabs__button--active' : ''?>">
                        <?=$tab->name?>
                    </button>
                    <?endforeach;?>
                </div>
            </div>
        </div>
        <div id="tab-person" class="who__content <?=($currentCategory->type_code == 'person' ? 'who__content--active' : '')?>">
            <div class="who__content-top">
                <div class="container">
                    <?php $form = ActiveForm::begin([
                         'options' => ['class' => 'who__personal-form'],
                         'method' => 'get',
                         'action' => Url::to($tabs['person']->getUrl())
                    ]); ?>
                        <div class="search-field">
                            <?= $form->field($personFilter, 'query')->textInput([
                                'class' => 'search-field__input input-dk',
                                'placeholder' => 'Поиск по фамилии'
                            ])->label(false) ?>
                            <button type="submit" class="search-field__button">
                                <svg viewBox="0 0 24 24" class="search_small-svg">
                                    <use xlink:href="#search_small"></use>
                                </svg>
                            </button>
                            <button type="submit" class="search-field__button search-field__button--close"></button>
                        </div>
                        <div class="who__filters">
                            <div class="who__filter-wrapper">
                                <?= $form->field($personFilter, 'sort')->dropDownList($personFilter::$orderFilterList, [
                                    'class' => 'who__select'
                                ])->label(false) ?>
                            </div>
                        </div>
                    <?ActiveForm::end();?>
                </div>
            </div>
            <div class="who__person person">
                <div class="container">
                    <?=$this->render('list/_person_list',[
                        'personMaterialsProvider' => $personMaterialsProvider,
                    ])?>
                </div>
            </div>
        </div>
        <div id="tab-organization" class="who__content <?=($currentCategory->type_code == 'organization' ? 'who__content--active' : '')?>">
            <div class="who__content-top">
                <div class="container">
                    <?php $form = ActiveForm::begin([
                        'options' => ['class' => 'who__form'],
                        'method' => 'get',
                        'action' => Url::to($tabs['organization']->getUrl())
                    ]); ?>
                        <div class="filter-search">
                            <?= $form->field($organizationFilter, 'category_id')->dropDownList(ArrayHelper::map($organizationCategories, 'id', 'name'), [
                                'prompt' => ['text' => 'Все', 'options' => ['class' => 'filter-search__option']],
                                'class' => 'filter-search__select',
                                'options' => array_map(function () {
                                    return ['class' => 'filter-search__option'];
                                }, $organizationCategories)
                            ])->label(false) ?>
                            <?/*
                            <select name="<?Html::getInputName($organizationFilter, 'category_id')?>" class="filter-search__select">
                                <option value="" <?= $currentCategory->lvl == 1 ? 'selected' : ''?> data-src="#" class="filter-search__option">Все</option>
                                <?foreach ($organizationCategories as $organizationCategory):?>
                                    <option value="<?=$organizationCategory->id?>" <?= $currentCategory->id == $organizationCategory->id ? 'selected' : ''?> data-src="<?=$organizationCategory->getUrl()?>" class="filter-search__option">
                                        <?=$organizationCategory->name?>
                                    </option>
                                <?endforeach;?>
                            </select>
                            */?>
                        </div>
                    <? ActiveForm::end()?>
                </div>
            </div>
            <div class="who__organizations organizations">
                <div class="container">
                    <?=$this->render('list/_organization_list',[
                        'organizationMaterialsProvider' => $organizationMaterialsProvider,
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>