<?php
use frontend\widgets\newslist\NewsListWidget;

/** @var \yii\data\ActiveDataProvider $personMaterialsProvider */
/** @var \yii\data\ActiveDataProvider $organizationMaterialsProvider */
?>
<?=NewsListWidget::widget([
    'ulClass' => 'organizations__list',
    'liClass' => 'organizations__item',
    'dataProvider' => $organizationMaterialsProvider,
    'itemTemplate' => '_organization',
    'itemTemplatePriority' => false
]); ?>