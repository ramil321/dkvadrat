<?php

use frontend\widgets\newslist\NewsListWidget;

/** @var \yii\data\ActiveDataProvider $personMaterialsProvider */
/** @var \yii\data\ActiveDataProvider $organizationMaterialsProvider */
?>
<?=NewsListWidget::widget([
    'ulClass' => 'person__list',
    'liClass' => 'template__item is-visible',
    'dataProvider' => $personMaterialsProvider,
    'itemTemplate' => '_person',
    'itemTemplatePriority' => false
]); ?>