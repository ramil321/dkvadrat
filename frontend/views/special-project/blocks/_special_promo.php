<?php
/**@var \common\models\SpecialProjects $model*/
?>
<div class="item promo item--promo">
    <a href="<?= $model->getUrl()?>" class="item__block-link"></a>
    <figure class="item__figure">
        <div class="item__image">
            <img src="<?=Yii::$app->resize->crop($model->image->src, 280, 263)?>" alt="" class="item__img">
        </div>
        <figcaption class="item__figcaption">
            <span class="item__span">Город</span>
            <div class="item__title"><?= $model->name?></div>
            <div class="item__info info-news">
                <ul class="info-news__list">
                    <li class="info-news__item">
                        <svg viewBox="0 0 12 12" class="time-svg">
                            <use xlink:href="#time"></use>
                        </svg><span class="info-news__span">Сегодня, 18:23</span>
                    </li>
                    <li class="info-news__item">
                        <svg viewBox="0 0 21 12" class="view-svg">
                            <use xlink:href="#view"></use>
                        </svg><span class="info-news__span">1 254</span>
                    </li>
                    <li class="info-news__item">
                        <svg viewBox="0 0 12 14" class="timer-svg">
                            <use xlink:href="#timer"></use>
                        </svg><span class="info-news__span">3 мин</span>
                    </li>
                </ul>
            </div>
        </figcaption>
    </figure>
</div>