<?php
/* @var $this yii\web\View */
/* @var $model common\models\material\Material */
/* @var $dataProvider frontend\models\material\search\MaterialSearch */
/* @var $specialProjects array*/
use yii\helpers\Url;

$this->title = 'Спецпроекты';

?>
<div class="special-projects page">
    <div class="page__wrapper">
        <div class="special-projects__top-wrapper page__wrapper-top">
            <div class="container">
                <h1 class="page__title"><?=$this->title?></h1>
            </div>
        </div>
        <div class="container">
            <ul class="template template--one_important_eight_blocks">
                <?
                    $model = $specialProjects[0];
                    unset($specialProjects[0]);
                ?>
                <li class="template__item">
                    <?=$this->render('blocks/_special_priority',[
                        'model' => $model
                    ])?>
                </li>
                <?foreach ($specialProjects as $model):?>
                <li class="template__item">
                    <?=$this->render('blocks/_special_promo',[
                        'model' => $model
                    ])?>
                </li>
                <?endforeach;?>
            </ul>
        </div>
    </div>
</div>