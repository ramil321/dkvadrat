<?php

/* @var $this yii\web\View */
/* @var $constructorModels Constructor[] */
/* @var $model \common\models\SpecialProjects */
use backend\modules\area\widgets\AreaImage;
use backend\modules\constructor\models\Constructor;
use backend\modules\constructor\models\RenderSections;
use frontend\widgets\render\RenderConstructorWidget;
use yii\helpers\Json;

$this->title = $model->name;
?>
<?=RenderConstructorWidget::widget([
    'constructorModels' => $constructorModels,
    'constructorType' => Constructor::TYPE_SPEC
])?>