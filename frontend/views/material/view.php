<?php

/* @var $this yii\web\View */
/* @var int $categoryId */
/* @var $model common\models\material\Material */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Материал', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if ($model->detail_image_id) {
    $this->registerMetaTag(['property' => 'og:image', 'content' => \yii\helpers\Url::to($model->detailImage->src, true)]);
}

?>

<div class="news-view">
    <div class="news-view__wrapper">
        <div class="news-view__main">
            <div class="news-view__top">
                <div class="container">
                    <div class="news-view__top-wrapper">
                        <div class="item item--article-top">
                            <ul class="info-news__list">
                                <li class="info-news__item">
                                    <svg viewBox="0 0 12 12" class="time-svg">
                                        <use xlink:href="#time"></use>
                                    </svg>
                                    <span class="info-news__span">
                                        <?= Yii::$app->formatter->asFullDate($model->publish_date) ?>
                                    </span>
                                </li>
                                <li class="info-news__item">
                                    <svg viewBox="0 0 21 12" class="view-svg">
                                        <use xlink:href="#view"></use>
                                    </svg>
                                    <span class="info-news__span">
                                        <?= number_format($model->views_count, 0, '.', ' ') ?>
                                    </span>
                                </li>
                                <? if ($model->durationRead): ?>
                                    <li class="info-news__item">
                                        <svg viewBox="0 0 12 14" class="timer-svg">
                                            <use xlink:href="#timer"></use>
                                        </svg>
                                        <span class="info-news__span">Время прочтения: <?= $model->durationRead ?></span>
                                    </li>
                                <? endif ?>
                            </ul>
                            <h1 class="item__title"><?= $model->name ?></h1>
                            <? if ($model->tags): ?>
                                <div class="categories">
                                    <ul class="categories__list">
                                        <? foreach ($model->tags as $tag): ?>
                                            <li class="categories__item">
                                                <a href="<?= $tag->getUrl() ?>" class="categories__category"><?= $tag->name ?></a>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            <? endif ?>
                        </div>
                        <div class="news-view__social">
                            <div class="social social--scrolled">
                                <div class="social__wrapper">
                                    <ul class="social__list">
                                        <li class="social__item">
                                            <a href="#" data-social="vkontakte" class="social__link social__link--vk">
                                                <div class="social__icon">
                                                    <svg viewBox="0 0 14 8" class="vk-svg">
                                                        <use xlink:href="#vk"></use>
                                                    </svg>
                                                </div>
                                                <div data-counter="vkontakte" class="social__count"></div>
                                            </a></li>
                                        <li class="social__item">
                                            <a href="#" data-social="facebook" class="social__link social__link--facebook">
                                                <div class="social__icon">
                                                    <svg viewBox="0 0 6 13" class="facebook-svg">
                                                        <use xlink:href="#facebook"></use>
                                                    </svg>
                                                </div>
                                                <div data-counter="vkontakte" class="social__count"></div>
                                            </a></li>
                                        <li class="social__item">
                                            <a href="#" data-social="odnoklassniki" class="social__link social__link--ok">
                                                <div class="social__icon">
                                                    <svg viewBox="0 0 8 13" class="ok-svg">
                                                        <use xlink:href="#ok"></use>
                                                    </svg>
                                                </div>
                                                <div data-counter="odnoklassniki" class="social__count"></div>
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="news-view__content">
                <div class="container">
                    <? if ($model->detailImage->src): ?>
                        <div class="preview-image <?= $model->getDetailImageStyled()->getFullClass() ?>">
                            <img
                                <?= $model->getDetailImageStyled()->getDataAttributes() ?>
                                    src="<?= Yii::$app->resize->prop($model->getDetailImageStyled()->getSrc(), 680, 400); ?>"
                                    class="preview-image__background-image <?= $model->getDetailImageStyled()->getCssClass() ?> "
                                    alt="<?= $model->name ?>"
                            >
                            <? if ($model->detail_image_option_author): ?>
                                <span class="preview-image__author">Фото: <?= $model->detail_image_option_author ?></span>
                            <? endif ?>
                        </div>
                    <? endif ?>
                    <div class="news-view__content-wrapper">
                        <?= \frontend\widgets\banner\BannerWidget::widget([
                            'position' => 'detail_material_medium',
                            'categoriesIds' => [$model->mainCategory->id],
                            'tagIds' => $model->tagIds
                        ]) ?>
                        <?= \frontend\widgets\banner\BannerWidget::widget([
                            'position' => 'detail_material_small',
                            'categoriesIds' => [$model->mainCategory->id],
                            'tagIds' => $model->tagIds
                        ]) ?>
                        <div class="news-view__title-main">
                            <span class="article-page__paragraph"><?= $model->preview_text ?></span>
                        </div>

                        <?= $model->detail_text ?>

                        <? if ($model->tags): ?>
                            <div class="news-view__categories">
                                <div class="categories">
                                    <ul class="categories__list">
                                        <? foreach ($model->tags as $tag): ?>
                                            <li class="categories__item">
                                                <a href="<?= $tag->getUrl() ?>" class="categories__category"><?= $tag->name ?></a>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        <? endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>