<?
/** @var \yii\web\View $this */
use frontend\models\material\filters\NewsFilter;

/** @var NewsFilter $newsFilter */
/** @var Material[] $materialsEarlier */
/** @var Material[] $materialsToday */
/** @var Material[] $materialEarlierPriority */
/** @var Material[] $priorityMaterialToday */
/** @var \yii\data\ActiveDataProvider $materialsProviderToday */
/** @var \yii\data\ActiveDataProvider $materialSearchProvider */
/** @var \yii\data\ActiveDataProvider $materialsProviderEarlier */
/** @var \common\models\material\MaterialTags[] $tags*/
/** @var \common\models\material\MaterialCategories $currentCategory */
?>
<?if((!$newsFilter->validate()) && $materialsProviderToday->totalCount > 0):?>
    <div class="section-dots">
        <div class="section-dots__title-wrapper">
            <div class="section-dots__title-header">
                <h2 class="section-dots__title">За сутки</h2>
                <div class="section-dots__right">
                    <time class="section-dots__time"><?= Yii::$app->formatter->asDate(time(), 'long') ?></time>
                </div>
            </div>
            <div class="dots">
                <div class="dots__wrapper">
                    <div class="dots__line"></div>
                    <div class="dots__line"></div>
                </div>
            </div>
        </div>
        <div class="section-dots__wrapper">
            <?=$this->render('list/_news_list_today',[
                'dataProvider' => $materialsProviderToday
            ])?>
        </div>
    </div>
<?endif?>
<?if((!$newsFilter->validate()) && $materialsProviderEarlier->totalCount > 0):?>
    <div class="section-dots">
        <div class="section-dots__title-wrapper">
            <div class="section-dots__title-header">
                <h2 class="section-dots__title">Ранее</h2>
            </div>
            <div class="dots">
                <div class="dots__wrapper">
                    <div class="dots__line"></div>
                    <div class="dots__line"></div>
                </div>
            </div>
        </div>
        <div class="section-dots__wrapper">
            <?=$this->render('list/_news_list_earlier',[
                'dataProvider' => $materialsProviderEarlier
            ])?>
        </div>
    </div>
<?endif?>
<?if($newsFilter->validate()):?>
    <div class="section-dots">
        <div class="section-dots__title-wrapper">
            <div class="section-dots__title-header">
                <h2 class="section-dots__title">Найденные</h2>
            </div>
            <div class="dots">
                <div class="dots__wrapper">
                    <div class="dots__line"></div>
                    <div class="dots__line"></div>
                </div>
            </div>
        </div>
        <div class="section-dots__wrapper">
            <?=$this->render('list/_news_list_all',[
                'dataProvider' => $materialSearchProvider
            ])?>
        </div>
    </div>
<?endif?>