<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\material\Material */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Материал', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="who-view who-view--organization">
    <div class="who-view__wrapper">
        <div class="who-view__main">
            <div class="container">
                <div class="who-view__top">
                    <div class="who-view__top-wrapper">
                        <h1 class="who-view__title"><?=$model->name?></h1>
                        <?if($model->tags):?>
                            <div class="categories">
                                <ul class="categories__list">
                                    <?foreach($model->tags as $tag):?>
                                        <li class="categories__item"><a href="<?=$tag->getUrl()?>" class="categories__category"><?=$tag->name?></a></li>
                                    <?endforeach;?>
                                </ul>
                            </div>
                        <?endif?>
                        <div class="social social--scrolled">
                            <div class="social__wrapper">
                                <ul class="social__list">
                                    <li class="social__item"><a href="#" data-social="vkontakte" class="social__link social__link--vk">
                                            <div class="social__icon">
                                                <svg viewBox="0 0 14 8" class="vk-svg">
                                                    <use xlink:href="#vk"></use>
                                                </svg>
                                            </div>
                                            <div data-counter="vkontakte" class="social__count"></div></a></li>
                                    <li class="social__item"><a href="#" data-social="facebook" class="social__link social__link--facebook">
                                            <div class="social__icon">
                                                <svg viewBox="0 0 6 13" class="facebook-svg">
                                                    <use xlink:href="#facebook"></use>
                                                </svg>
                                            </div>
                                            <div data-counter="vkontakte" class="social__count"></div></a></li>
                                    <li class="social__item"><a href="#" data-social="odnoklassniki" class="social__link social__link--ok">
                                            <div class="social__icon">
                                                <svg viewBox="0 0 8 13" class="ok-svg">
                                                    <use xlink:href="#ok"></use>
                                                </svg>
                                            </div>
                                            <div data-counter="odnoklassniki" class="social__count"></div></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="who-view__content">
                    <div class="who-view__card card">
                        <div class="card__image"><img src="<?=Yii::$app->resize->prop($model->detailImage->src, 200, 200);?>" alt=""></div>
                        <p class="card__text"><?=$model->preview_text?></p>
                    </div>
                    <?=$model->detail_text?>
                    <div class="who-view__categories">
                        <?if($model->tags):?>
                            <div class="categories">
                                <ul class="categories__list">
                                    <?foreach($model->tags as $tag):?>
                                        <li class="categories__item"><a href="<?=$tag->getUrl()?>" class="categories__category"><?=$tag->name?></a></li>
                                    <?endforeach;?>
                                </ul>
                            </div>
                        <?endif?>
                    </div>
                    <div class="who-view__social">
                        <div class="social">
                            <div class="social__wrapper">
                                <ul class="social__list">
                                    <li class="social__item"><a href="#" data-social="vkontakte" class="social__link social__link--vk">
                                            <div class="social__icon">
                                                <svg viewBox="0 0 14 8" class="vk-svg">
                                                    <use xlink:href="#vk"></use>
                                                </svg>
                                            </div>
                                            <div data-counter="vkontakte" class="social__count"></div></a></li>
                                    <li class="social__item"><a href="#" data-social="facebook" class="social__link social__link--facebook">
                                            <div class="social__icon">
                                                <svg viewBox="0 0 6 13" class="facebook-svg">
                                                    <use xlink:href="#facebook"></use>
                                                </svg>
                                            </div>
                                            <div data-counter="vkontakte" class="social__count"></div></a></li>
                                    <li class="social__item"><a href="#" data-social="odnoklassniki" class="social__link social__link--ok">
                                            <div class="social__icon">
                                                <svg viewBox="0 0 8 13" class="ok-svg">
                                                    <use xlink:href="#ok"></use>
                                                </svg>
                                            </div>
                                            <div data-counter="odnoklassniki" class="social__count"></div></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>