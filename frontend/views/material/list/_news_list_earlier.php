<?php

use frontend\widgets\newslist\NewsListWidget;
use yii\helpers\Url;


/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var integer $categoryId */
?>
<?=

NewsListWidget::widget([
    'ulClass' => 'template template--eight_blocks content-more',
    'liClass' => 'template__item is-visible',
    'dataProvider' => $dataProvider,
    'itemTemplate' => '_news',
    'itemTemplatePriority' => '',
]); ?>