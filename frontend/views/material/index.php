<?php

use common\models\material\Material;
use frontend\models\material\filters\NewsFilter;
use frontend\models\material\search\MaterialSearch;
use frontend\widgets\newslist\NewsListWidget;
use frontend\widgets\sortnews\SortNewsWidget;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var NewsFilter $newsFilter */
/** @var Material[] $materialsEarlier */
/** @var Material[] $materialsToday */
/** @var Material[] $materialEarlierPriority */
/** @var Material[] $priorityMaterialToday */
/** @var \yii\data\ActiveDataProvider $materialsProviderToday */
/** @var \yii\data\ActiveDataProvider $materialSearchProvider */
/** @var \yii\data\ActiveDataProvider $materialsProviderEarlier */
/** @var \common\models\material\MaterialTags[] $tags*/
/** @var \common\models\material\MaterialCategories $currentCategory */


$this->title = $currentCategory->name;
?>
<div class="news page">
    <div class="page__wrapper">
        <div class="top-section">
            <div class="container">
                <div class="top-section__popular">
                    <div class="popular">
                        <ul class="popular__list">
                            <?foreach ($tags as $tag):?>
                            <li class="popular__item"><a href="<?=$tag->getUrl()?>" class="popular__link"><?=$tag->name?></a></li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
                <div class="top-section__filters">
                    <h1 class="top-section__title"><?=$this->title?></h1>
                    <?= SortNewsWidget::widget([
                        'model' => $newsFilter,
                        'categoryId' => $currentCategory->id
                    ]) ?>
                </div>

            </div>
        </div>
        <div class="container">
            <?=$this->render('container',[
                'newsFilter' => $newsFilter,
                'materialsProviderToday' => $materialsProviderToday,
                'materialsProviderEarlier' => $materialsProviderEarlier,
                'materialSearchProvider' => $materialSearchProvider,
            ])?>
        </div>
    </div>
</div>
