User-agent: *
Disallow: /admin/
Disallow: /search
Disallow: /themes/

Host: http://www.d-kvadrat.ru/
Sitemap: http://www.d-kvadrat.ru/sitemap.xml