$(function(){
    $('body').on('click', '.button--more', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        var container = $(this).parents('ul');
        var currentButtonMore = $(this).parent();
        currentButtonMore.remove();
        $.ajax({
            type: "POST",
            url: link,
            data: {'pager': 'y'},
            success: function(data){
                container.append(data);
            },
        });
    });

    $('body').on('submit', '.filter-form', function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: $(this).attr('action') + "?" + $(this).serialize(),
            data: {'filter': 'y'},
            success: function(data){
                console.log(data);
            },
        });
    });

    ajaxToContainer('#journal_view','.journal-ajax');

    function ajaxToContainer(container, link) {
        $('body').on('click', link, function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            $.ajax({
                type: "POST",
                url: href,
                data: {'container': 'y'},
                success: function(data){
                    $(container).html(data);
                    //console.log(data);
                },
            });
        });
    }

});