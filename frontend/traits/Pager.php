<?php

namespace frontend\traits;

use Yii;

trait Pager
{
    /**
     * @param $dataProvider
     * @return bool
     */
    public function checkRequestPager($dataProvider){
        if(
            Yii::$app->request->get(($dataProvider->id ? $dataProvider->id.'-page' : 'page')) &&
            Yii::$app->request->isAjax &&
            Yii::$app->request->post('pager')
        ){
            return true;
        }
        return false;
    }
}