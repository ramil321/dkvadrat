<?php

use common\models\material\Material;
use himiklab\sitemap\behaviors\SitemapBehavior;
use himiklab\sitemap\Sitemap;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'frontend\components\material\DynamicMaterialRulesBootstrap',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        /*main-local params*/
        'newsletters' => [
            'class' => 'backend\modules\newsletters\Module',
            'key' => '',
            'senderEmail' => 'solovev@dev.picom.ru',
            'subscribePage' => 'http://kvadrat.picom.su/frontend/web/site/newsletters-subscribe/',
            'unsubscribePage' => 'http://kvadrat.picom.su/frontend/web/site/newsletters-unsubscribe/',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ],
        'sitemap' => [
            'class' => Sitemap::class,
            'models' => [
                // map для материалов
                [
                    'class' => Material::class,
                    'behaviors' => [
                        'sitemap' => [
                            'class' => SitemapBehavior::class,
                            'scope' => function ($model) {
                                $model->with(['mainCategory']);
                                $model->andWhere(['<=', 'publish_date', time()]);
                            },
                            'dataClosure' => function ($model) {
                                return [
                                    'loc' => $model->getUrl(),
                                    'lastmod' => $model->updated_at,
                                    'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                                    'priority' => 0.8,
                                ];
                            }
                        ]
                    ]
                ]
            ],
            'urls' => [
                // Взято из меню layout:
                ['loc' => '/', 'changefreq' => SitemapBehavior::CHANGEFREQ_HOURLY],
                ['loc' => '/novosti', 'changefreq' => SitemapBehavior::CHANGEFREQ_HOURLY],
                ['loc' => '/politika', 'changefreq' => SitemapBehavior::CHANGEFREQ_HOURLY],
                ['loc' => '/ekonomika', 'changefreq' => SitemapBehavior::CHANGEFREQ_HOURLY],
                ['loc' => '/obshchestvo', 'changefreq' => SitemapBehavior::CHANGEFREQ_HOURLY],
                ['loc' => '/kto-yest-kto/persony', 'changefreq' => SitemapBehavior::CHANGEFREQ_HOURLY],
                ['loc' => '/journal', 'changefreq' => SitemapBehavior::CHANGEFREQ_HOURLY],
                ['loc' => '/contacts', 'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY],
                ['loc' => '/politics', 'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY],
                ['loc' => '/privacy', 'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY],


            ],
            'enableGzip' => true,
            'cacheExpire' => 1,
        ],
        /*main-local params*/
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'site',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
           // 'enableStrictParsing' => false,
           // 'rules' => include Yii::getAlias('@common/config/url-rules.php'),
            'rules' => [
                'tags/<slug:>' => 'tags/index',
                'rss' => 'site/rss',
                'rss-yandex' => 'site/rss-yandex',
                'privacy' => 'site/privacy',
                'journal/<id:>' => 'journal/view',
                'special-project/<id:>' => 'special-project/view',
                [
                    'pattern' => 'sitemap',
                    'route' => 'sitemap/default/index',
                    'suffix' => '.xml'
                ],
                'robots.txt' => 'site/robots-txt',
            ]
        ],

        /*main-local params*/
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'urlManagerBackend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://kvadrat/backend/web',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        /*main-local params*/
        'assetManager' => [
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyASDmvI7vqSQEfjFakkkpm0weXlM7-cHW8',
                        'language' => 'ru',
                        'version' => '3.1.18',
                    ],
                ],
            ],
        ],

    ],
    'params' => $params,
];
