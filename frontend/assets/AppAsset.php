<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'styles/main.css?v5',
        'styles/dev.css?v5',
    ];
    public $js = [
        'scripts/main.js?v5',
        'scripts/dev.js?v5'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
