<?php

namespace frontend\components\material;

use common\models\material\MaterialCategories;
use yii\base\Action;
use yii\base\Application;
use yii\base\BootstrapInterface;

class DynamicMaterialRulesBootstrap implements BootstrapInterface
{

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules($this->getRules(), false);
    }

    public function getRules()
    {
        $pathList = MaterialCategories::getSlugPathsList();
        $rules = [];

        foreach ($pathList as $id=>$item){

            $node = $item['node'];
            /** @var MaterialCategories $node */
            $type = $node->getType();

            $rules[] = [
                'pattern'  => $item['slug'],
                'route'    => ($type['controllerRoutePage'] ? $type['controllerRoutePage'] : 'site/'.$item['slug']),
                'defaults' => ['categoryId' => $id],
            ];

            $rules[] = [
                'pattern'  => $item['slug'].'/<id:\d+>',
                'route'    => ($type['controllerRouteDetail'] ? $type['controllerRouteDetail'] : 'site/'.$item['slug'].'-detail'),
                'defaults' => ['categoryId' => $id],
            ];

        }
        return $rules;
    }

}