<?php
namespace frontend\components\material;

use yii\data\ActiveDataProvider;

class ActiveDataProviderPriority extends ActiveDataProvider
{
    private $priorityMaterial;

    /**
     * @return mixed
     */
    public function getPriorityMaterial()
    {
        return $this->priorityMaterial;
    }

    /**
     * @param mixed $priorityMaterial
     */
    public function setPriorityMaterial($priorityMaterial)
    {
        $this->priorityMaterial = $priorityMaterial;
    }

}