<?php

namespace frontend\models;

use yii\base\Model;
use common\models\material\Material;
use frontend\components\material\ActiveDataProviderPriority as ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * MaterialSearch represents the model behind the search form of `common\models\material\Material`.
 * @property Material $priorityMaterialToday
 * @property Material $priorityMaterialEarlier
 */
class MaterialList extends Material
{


    private $categoryId;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param integer $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @param ActiveQuery $query
     * @return null
     */
    private function findPriorityMaterial(ActiveQuery $query)
    {
        $query->andWhere(['priority' => 1]);
        $query->addOrderBy(['publish_date' => SORT_DESC]);
        $query->joinWith('materialCategories');
        $query->andWhere('is_published = true');
        $query->andWhere(['<=', 'publish_date', time()]);
        if($this->categoryId) {
            $query->andWhere(['material_categories.id' => $this->categoryId]);
        }
        $query->limit(1);
    }

    /**
     * @return Material
     */
    private function findPriorityMaterialToday()
    {
        $query = Material::Find();
        $this->findPriorityMaterial($query);
        $query->andWhere(['between', 'material.publish_date', strtotime("-1 day") ,time()]);
        return $query->one();
    }
    /**
     * @return Material
     */
    private function findPriorityMaterialAllTime()
    {
        $query = Material::Find();
        $this->findPriorityMaterial($query);
        return $query->one();
    }
    /**
     * @return Material
     */
    private function findPriorityMaterialEarlier()
    {
        $query = Material::Find();
        $this->findPriorityMaterial($query);
        $query->andWhere(['<', 'material.publish_date', strtotime("-1 day")]);
        return $query->one();
    }


    /**
     * Поиск новостей за день с приоритетным материалом
     * @param bool $priority
     * @return ActiveDataProvider
     */
    public function getDataProviderToday($priority = false)
    {
        $dataProvider = $this->getDataProvider();
        $dataProvider->query->andWhere(['between', 'material.publish_date', strtotime("-1 day") ,time()]);

        if(!$priority){
            return $dataProvider;
        }

        $priorityMaterialToday = $this->findPriorityMaterialToday();
        if($priorityMaterialToday){
            $dataProvider->setPriorityMaterial($priorityMaterialToday);
            $dataProvider->query->addOrderBy([new Expression('FIELD (material.id, ' . $priorityMaterialToday->id . ') DESC')]);
        }
        $dataProvider->query->addOrderBy(['publish_date' => SORT_DESC]);
        return $dataProvider;
    }


    /**
     * Поиск новостей после первого дня с приоритетным материалом
     * @param bool $priority
     * @return ActiveDataProvider
     */
    public function getDataProviderEarlier($priority = false)
    {
        $dataProvider = $this->getDataProvider();
        $dataProvider->query->andWhere(['<', 'material.publish_date', strtotime("-1 day")]);

        if(!$priority){
            return $dataProvider;
        }

        $priorityMaterialToday = $this->findPriorityMaterialEarlier();
        if($priorityMaterialToday){
            $dataProvider->setPriorityMaterial($priorityMaterialToday);
            $dataProvider->query->addOrderBy([new Expression('FIELD (material.id, ' . $priorityMaterialToday->id . ') DESC')]);
        }
        $dataProvider->query->addOrderBy(['publish_date' => SORT_DESC]);
        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     * @param bool $priority
     * @return ActiveDataProvider
     */
    public function getDataProvider($priority = false)
    {
        $query = Material::Find();
        $query->joinWith('tags');
        $query->joinWith('detailImage');
        $query->joinWith('themes');
        $query->joinWith('authors.profile');
        $query->joinWith('materialCategories');
        $query->joinWith('mainCategory mainCategory');

        $query->joinWith('region');
        $query->groupBy('material.id');

        $query->andWhere('is_published = true');
        $query->andWhere(['<=', 'publish_date', time()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'authorsIds' => [
                        'asc' => ['material_user.user_id' => SORT_ASC],
                        'desc' => ['material_user.user_id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'materialCategories' => [
                        'asc' => ['material_categories.name' => SORT_ASC],
                        'desc' => ['material_categories.name' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'created_at',
                    'id',
                    'priority',
                    'publish_date',
                    'name',
                    'preview_text',
                    'material_region_id',
                ]
            ],
        ]);

        $dataProvider->pagination->pageSize = 8;

        if($this->categoryId) {
            $query->andWhere(['material_categories.id' => $this->categoryId]);
        }

        if(!$priority){
            $dataProvider->query->addOrderBy(['publish_date' => SORT_DESC]);
            return $dataProvider;
        }

        $priorityMaterial = $this->findPriorityMaterialAllTime();

        if($priorityMaterial){
            $dataProvider->setPriorityMaterial($priorityMaterial);
            $dataProvider->query->addOrderBy([new Expression('FIELD (material.id, ' . $priorityMaterial->id . ') DESC')]);
        }
        $dataProvider->query->addOrderBy(['publish_date' => SORT_DESC]);
        return $dataProvider;
    }


}
