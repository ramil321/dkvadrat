<?php

namespace frontend\models\material\search;

namespace frontend\models\search;

use common\models\material\MaterialCategories;
use common\models\material\MaterialTags;
use common\models\material\MaterialTagsMaterialCategoriesTmpViews;
use common\models\material\search\MaterialSphinxIndex;
use frontend\models\material\search\MaterialSearch;
use frontend\models\MaterialList;
use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use common\models\material\Material;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\sphinx\Query;

/**
 * MaterialSearch represents the model behind the search form of `common\models\material\Material`.
 */
class SphinxSearchMaterial extends Model
{
    public $dateRange;
    public $dateRangeStart;
    public $dateRangeEnd;

    public $query;

    public $category_id;

    public $date;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query','date'], 'string'],
            ['category_id', 'integer']
        ];
    }

    public function sphinxSearchMaterialIds()
    {
        $results = (new Query())->from(MaterialSphinxIndex::tableName())->match( $this->query )->all();
        $ids = ArrayHelper::map($results, 'id' ,'id');
        return $ids;
    }

    /**
     * @param array $materialIds
     * @return MaterialCategories[]
     */
    public function findCategories(array $materialIds)
    {
        if(!$materialIds){
            return [];
        }
        $categories = MaterialCategories::find()
            ->joinWith('materialCategoriesMaterial')
            ->where(['IN', 'material_categories_material.material_id', $materialIds])
            ->groupBy(['material_categories.id'])
            ->all()
        ;
        return $categories;
    }

    /**
     * @param array $ids
     * @return null|ActiveDataProvider
     */
    public function search(array $ids)
    {
        if(!$ids){
            return null;
        }

        $categoryId = $this->category_id;
        $date = $this->date;
        $materialList = new MaterialList();
        $dataProvider = $materialList->getDataProvider();

        if($categoryId){
            $dataProvider->query->andWhere([
                'IN',
                'material_categories.id',
                $categoryId
            ]);
        }
        if($date){
            $date = explode(',', $date);
            if($date[0] && $date[1]){
                $dataProvider->query->andWhere([
                    'between',
                    'publish_date',
                    strtotime($date[0].' 00:00:00') , strtotime($date[1].' 23:59:59')
                ]);
            }
        }
        $dataProvider->query->andWhere(['material.id' => $ids]);

        return $dataProvider;
    }
}
