<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $editorial_address
 * @property int $phone_editors
 * @property int $phone_advertising
 * @property string $email
 * @property string $advertisers_href
 * @property string $lat
 * @property string $lng
 * @property string $map_title
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['editorial_address', 'email', 'advertisers_href', 'lat', 'lng', 'map_title'], 'string'],
            [['phone_editors', 'phone_advertising'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'editorial_address' => 'Editorial Address',
            'phone_editors' => 'Phone Editors',
            'phone_advertising' => 'Phone Advertising',
            'email' => 'Email',
            'advertisers_href' => 'Advertisers Href',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'map_title' => 'Map Title',
        ];
    }
}
