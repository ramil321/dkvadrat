<?php

namespace frontend\models\material\filters;


use frontend\models\MaterialList;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class OrganizationFilter extends Model
{
    public $category_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer']
        ];
    }

    public function search($request, $categoryId)
    {
        $materialList = new MaterialList();
        $materialList->setCategoryId($categoryId);
        $dataProvider = $materialList->getDataProvider();
        if($this->load($request) && $this->validate()){
            if($this->category_id){
                $dataProvider->query->andWhere(['material_categories.id' => $this->category_id]);
            }

        }
        return $dataProvider;
    }

}
