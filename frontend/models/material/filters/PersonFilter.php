<?php

namespace frontend\models\material\filters;

use common\models\material\Material;
use frontend\models\material\search\MaterialSearch;
use frontend\models\MaterialList;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class PersonFilter extends Model
{

    public static $orderFilterList = ['popular' => 'Популярные', 'name' => 'По алфавиту'];

    public $query;
    public $sort;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['query', 'sort'], 'string'],
        ];
    }

    public function search($request, $categoryId = false)
    {
        $materialList = new MaterialList();
        $materialList->setCategoryId($categoryId);
        $dataProvider = $materialList->getDataProvider();
        if($this->load($request) && $this->validate()){
            if($this->sort == 'popular'){
                $dataProvider->query->orderBy(['material.views_count' => SORT_DESC]);
            }
            if($this->sort == 'name'){
                $dataProvider->query->orderBy(['material.name' => SORT_ASC]);
            }
            if($this->query){
                $dataProvider->query->andWhere(['like', 'material.name', $this->query]);
            }
        }
        return $dataProvider;
    }

}
