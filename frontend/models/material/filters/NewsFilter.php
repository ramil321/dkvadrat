<?php

namespace frontend\models\material\filters;


use frontend\models\MaterialList;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class NewsFilter extends Model
{
    public $year;
    public $month;
    public $region_id;
    public $category_id;
    public $filter;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year', 'month', 'region_id', 'category_id'], 'integer'],
            ['filter','boolean'],
            ['filter','required']
        ];
    }

    public function search($request, $categoryId, $priority = false)
    {
        $materialList = new MaterialList();
        if($this->load($request) && $this->validate()){
            $materialList->setCategoryId($this->category_id);
        }else{
            $materialList->setCategoryId($categoryId);
            $this->category_id = $categoryId;
        }
        $dataProvider = $materialList->getDataProvider($priority);
        if($this->load($request) && $this->validate()){

            if($this->region_id){
                $dataProvider->query->andWhere(['material_region_id' => $this->region_id]);
            }

            if($this->year && $this->month){
                $date = $this->year."-".$this->month."-01";
                $lastDay = date("t", strtotime($date));
                $month = date("m", strtotime($date));
                $dataProvider->query->andWhere([
                    'between',
                    'publish_date',
                    strtotime($this->year.'-'.$month.'-'.'01'.' 00:00:00'),
                    strtotime($this->year.'-'.$month.'-'.$lastDay.' 23:59:59')
                ]);
            }

        }
        return $dataProvider;
    }

}
