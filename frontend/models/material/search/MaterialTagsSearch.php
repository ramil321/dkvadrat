<?php

namespace frontend\models\material\search;

use common\models\material\MaterialTags;
use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\material\Material;
use yii\helpers\ArrayHelper;

/**
 * MaterialSearch represents the model behind the search form of `common\models\material\Material`.
 */
class MaterialTagsSearch extends MaterialTags
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'view_count'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = MaterialTags::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'view_count' => $this->view_count,
            'name' => $this->name,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'view_count', $this->view_count]);

        return $dataProvider;
    }
}
