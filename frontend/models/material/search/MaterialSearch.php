<?php

namespace frontend\models\material\search;

use common\models\material\MaterialCategories;
use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use frontend\components\material\ActiveDataProviderPriority as ActiveDataProvider;
use common\models\material\Material;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\db\conditions\OrCondition;

/**
 * MaterialSearch represents the model behind the search form of `common\models\material\Material`.
 * @property Material $priorityMaterialToday
 * @property Material $priorityMaterialEarlier
 */
class MaterialSearch extends Material
{
    public $createdAtRange;
    public $createdAtStart;
    public $createdAtEnd;

    public $updatedAtRange;
    public $updatedAtStart;
    public $updatedAtEnd;

    public $filter;

    public $categoriesIds;
    public $tagIds;
    public $themeIds;
    public $materialCategories;
    public $materialCategoriesNot;

    public $year;
    public $month;

    public $priorityMaterialToday;
    public $priorityMaterialEarlier;
    public $priorityMaterialAllTime;

    public $materialCategoryId;
    private $tagId;

    public $materialTagsMaterialCategoryId;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),
            [
                [
                    'class' => DateRangeBehavior::className(),
                    'attribute' => 'createdAtRange',
                    'dateStartAttribute' => 'createdAtStart',
                    'dateEndAttribute' => 'createdAtEnd',
                ],
                [
                    'class' => DateRangeBehavior::className(),
                    'attribute' => 'updatedAtRange',
                    'dateStartAttribute' => 'updatedAtStart',
                    'dateEndAttribute' => 'updatedAtEnd',
                ]
            ]);
    }

    public function setTagIds($val)
    {
        $this->tagIds = $val;
    }

    public function getTagIds()
    {
        return $this->tagIds;
    }

    public function setThemeIds($val)
    {
        $this->themeIds = $val;
    }

    public function getThemeIds()
    {
        return $this->themeIds;
    }

    /**
     * @param int $id
     */
    public function setCategoryId($id){
        $this->materialCategoryId = $id;
    }
    /**
     * @param int $id
     */
    public function setTagId($id){
        $this->tagId = $id;
    }
    /*public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'createdAtRange',
                'dateStartAttribute' => 'createdAtStart',
                'dateEndAttribute' => 'createdAtEnd',
            ]
        ];
    }*/


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createdAtRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['updatedAtRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['material_region_id', 'month', 'year'], 'integer'],
            [['materialCategoryId'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'createdAtRange' => 'Дата создания',
            'materialCategories' => 'Категории',
            'updatedAtRange' => 'Дата обновления',
            'themeIds' => 'Темы'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    private function earlierQueryModifier(ActiveQuery $query){
        if (!$this->validate()) {
            $this->findPriorityMaterialEarlier();
        }
        if($this->priorityMaterialEarlier){
            $query->addOrderBy([new Expression('FIELD (material.id, ' . $this->priorityMaterialEarlier->id . ') DESC')]);
        }
        $query->andWhere(['<', 'material.publish_date', strtotime("-1 day")]);
    }

    private function todayQueryModifier(ActiveQuery $query){
        if (!$this->validate()) {
            $this->findPriorityMaterialToday();
        }
        if($this->priorityMaterialToday){
            $query->addOrderBy([new Expression('FIELD (material.id, ' . $this->priorityMaterialToday->id . ') DESC')]);
        }
        $query->andWhere(['between', 'material.publish_date', strtotime("-1 day") ,time()]);
    }

    private function allTimeQueryModifier(ActiveQuery $query){
        if (!$this->validate()) {
            $this->findPriorityMaterialAllTime();
        }
        if($this->priorityMaterialAllTime){
            $query->addOrderBy([new Expression('FIELD (material.id, ' . $this->priorityMaterialAllTime->id . ') DESC')]);
        }
    }

    /**
     * @param ActiveQuery $query
     * @return null
     */
    private function findPriorityMaterial(ActiveQuery $query){
        $query->andWhere(['priority' => 1]);
        $query->addOrderBy(['publish_date' => SORT_DESC]);
        $query->limit(1);
    }

    /**
     * @return Material
     */
    private function findPriorityMaterialToday(){
        $query = Material::Find();
        $this->findPriorityMaterial($query);
        $query->andWhere(['between', 'material.publish_date', strtotime("-1 day") ,time()]);
        $this->priorityMaterialToday = $query->one();
    }
    /**
     * @return Material
     */
    private function findPriorityMaterialAllTime(){
        $query = Material::Find();
        $this->findPriorityMaterial($query);
        $this->priorityMaterialAllTime = $query->one();
    }
    /**
     * @return Material
     */
    private function findPriorityMaterialEarlier(){
        $query = Material::Find();
        $this->findPriorityMaterial($query);
        $query->andWhere(['<', 'material.publish_date', strtotime("-1 day")]);
        $this->priorityMaterialEarlier = $query->one();
    }


    /**
     * Поиск новостей за все время с приоритетным материалом
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchAllTime($params){
        $dataProvider = $this->search($params, function($query){
            $this->allTimeQueryModifier($query);
        });
        return $dataProvider;
    }

    /**
     * Поиск новостей за день с приоритетным материалом
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchToday($params){
        $dataProvider = $this->search($params, function($query){
            $this->todayQueryModifier($query);
        });
        return $dataProvider;
    }

    /**
     * Поиск новостей после первого дня с приоритетным материалом
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchEarlier($params){
        $dataProvider = $this->search($params, function($query){
            $this->earlierQueryModifier($query);
        });
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @param callable $queryModifier
     * @return ActiveDataProvider
     */
    public function search($params, callable $queryModifier = null)
    {
        $query = Material::Find();
        $query->joinWith('tags', true);
        $query->joinWith('detailImage', true);
        $query->joinWith('themes', true);
        $query->joinWith('authors.profile', true);
        $query->joinWith('materialCategories', true);
        $query->joinWith([
            'mainCategory' => function ($query) {
                /** @var ActiveQuery $query */
                $query->from(['main_category' => MaterialCategories::tableName()]);
            }
        ]);
        $query->joinWith('region', true);
        $query->groupBy('material.id');
        // add conditions that should always apply here
        $this->load($params, '');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'pagination' => false,
            'sort' => [
                //'defaultOrder' => ['publish_date' => SORT_DESC],
                'attributes' => [
                    'authorsIds' => [
                        'asc' => ['material_user.user_id' => SORT_ASC],
                        'desc' => ['material_user.user_id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'materialCategories' => [
                        'asc' => ['material_categories.name' => SORT_ASC],
                        'desc' => ['material_categories.name' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'created_at',
                    'id',
                    'priority',
                    'publish_date',
                    'name',
                    'preview_text',
                    'material_region_id',
                ]
            ]
        ]);
        if($queryModifier){
            $queryModifier($query);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->materialCategoryId) {
            $query->andWhere(['material_categories.id' => $this->materialCategoryId]);
        }
        if($this->tagId) {
            $query->andWhere(['material_tags.id' => $this->tagId]);
        }


        if($this->material_region_id){
            $query->andWhere(['material_tags.id' => $this->material_region_id]);
        }

        return $dataProvider;
    }
}
