<?php

namespace frontend\models\journal\serach;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\journal\Journal;

/**
 * JournalSearch represents the model behind the search form of `common\models\journal\Journal`.
 */
class JournalSearch extends Journal
{
    const START_YEAR = 2018;
    public $year;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function getYears(){
        $years = [];
        for($i = self::START_YEAR; $i <= date('Y'); $i++){
            $years[$i] = $i;
        }
        return $years;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Journal::find();
        $query->joinWith('file');
        $query->orderBy(['date_from' => SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->year) {
             $query->andFilterWhere(['between', 'date_to', strtotime("01-01-{$this->year}"), strtotime("31-12-{$this->year}")]);
        }

        return $dataProvider;
    }
}
