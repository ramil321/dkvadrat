<?php

namespace frontend\controllers;

use common\models\material\Material;
use frontend\traits\Pager;
use yii\web\NotFoundHttpException;

class MaterialDetailController extends \yii\web\Controller
{
    use Pager;

    public function actionDetail($categoryId, $id)
    {
        if (!$categoryId) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = $this->findModel($id, $categoryId);
        Material::viewIncrement($id);

        if($model->mainCategory->type_code == 'organization'){
            return $this->render('@frontend/views/material/organization_view', [
                'model' => $model,
            ]);
        }
        if($model->mainCategory->type_code == 'person'){
            return $this->render('@frontend/views/material/person_view', [
                'model' => $model,
            ]);
        }

        return $this->render('@frontend/views/material/view', [
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     * @param int $categoryId
     * @return Material
     * @throws NotFoundHttpException
     */
    protected function findModel($id, $categoryId)
    {
        if (!$id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if (!$categoryId) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $query = Material::Find();
        $query->joinWith('mainCategory', true);
        $query->where([
            'material.id' => $id,
            'main_category.id' => $categoryId
        ]);
        $model = $query->one();
        if ($model) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}