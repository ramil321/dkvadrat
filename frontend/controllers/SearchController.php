<?php

namespace frontend\controllers;


use frontend\models\search\SphinxSearchMaterial;
use Yii;

//use yii\sphinx\ActiveDataProvider;

class SearchController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new SphinxSearchMaterial();
        if ($model->load(Yii::$app->request->get()) && $model->validate()) {
            $materialIds = $model->sphinxSearchMaterialIds();
            $dataProvider = $model->search($materialIds);
            $categories = $model->findCategories($materialIds);

            if(Yii::$app->request->isAjax){
                return $this->renderPartial('list/_search_list',[
                    'dataProvider' => $dataProvider
                ]);
            }

            return $this->render('index', [
                'model' => $model,
                'categories' => $categories,
                'dataProvider' => $dataProvider,
            ]);
        }



        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
