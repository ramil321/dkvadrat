<?php

namespace frontend\controllers;

use backend\modules\constructor\models\Constructor;
use backend\modules\constructor\models\ConstructorTemplates;
use common\models\SpecialProjects;
use frontend\models\material\search\MaterialSearch;
use common\models\material\Material;
use Yii;
use yii\web\NotFoundHttpException;


class SpecialProjectController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $specialProjects = SpecialProjects::find()
            ->where(['active' => 1])
            ->orderBy(['position' => SORT_ASC])
            ->all();
        return $this->render('index', [
            'specialProjects' => $specialProjects,
        ]);
    }

    public function actionView($id)
    {

        $model = $this->findModel($id);
        $constructorModels = Constructor::find()
            ->where(['type' => Constructor::TYPE_SPEC, 'special_projects_id' => $model->id])
            ->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
            ->all();
        return $this->render('view', [
            'model' => $model,
            'constructorModels' => $constructorModels
        ]);
    }

    protected function findModel($id)
    {
        if (($model = SpecialProjects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
