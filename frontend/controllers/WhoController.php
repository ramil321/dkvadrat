<?php

namespace frontend\controllers;


use common\models\material\MaterialCategories;
use frontend\models\material\filters\OrganizationFilter;
use frontend\models\material\filters\PersonFilter;
use frontend\models\MaterialList;
use Yii;

class WhoController extends MaterialDetailController
{
    public function actionCategory($categoryId)
    {
        /** @var \common\models\material\MaterialCategories $currentCategory */
        /** @var \common\models\material\MaterialCategories[] $tabs */
        /** @var \common\models\material\MaterialCategories[] $organizationCategories */

        $tabs = MaterialCategories::find()
            ->where(['lvl' => 1,'type_code' => ['person', 'organization']])
            ->orderBy(['type_code' => SORT_DESC])
            ->indexBy('type_code')
            ->all();
        $organizationCategories = MaterialCategories::find()
            ->where(['lvl' => 2,'type_code' => 'organization'])
            ->all();
        $currentCategory = MaterialCategories::findOne($categoryId);

        $organizationFilter = new OrganizationFilter();
        $organizationMaterialsProvider = $organizationFilter->search(Yii::$app->request->queryParams, $tabs['organization']->id);

        $personFilter = new PersonFilter();
        $personMaterialsProvider = $personFilter->search(Yii::$app->request->queryParams, $tabs['person']->id);

        if($this->checkRequestPager($organizationCategories)) {
            return $this->renderPartial('list/_organization_list', [
                'organizationMaterialsProvider' => $organizationMaterialsProvider,
            ]);
        }

        if($this->checkRequestPager($personMaterialsProvider)) {
            return $this->renderPartial('list/_person_list', [
                'personMaterialsProvider' => $personMaterialsProvider,
            ]);
        }

        return $this->render('index', [
            'currentCategory' => $currentCategory,
            'tabs' => $tabs,
            'organizationCategories' => $organizationCategories,
            'personFilter' => $personFilter,
            'organizationFilter' => $organizationFilter,
            'personMaterialsProvider' => $personMaterialsProvider,
            'organizationMaterialsProvider' => $organizationMaterialsProvider
        ]);
    }

}