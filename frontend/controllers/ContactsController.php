<?php

namespace frontend\controllers;

use common\models\Feedback;
use common\models\contacts\Contacts;
use common\models\company\Company;
use Yii;
use yii\web\NotFoundHttpException;

class ContactsController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $feedback = new Feedback();
        $feedBackSended = false;
        if ($feedback->load(Yii::$app->request->post())) {
            $feedback->save();
            if ($feedback->sendEmail()) {
                $feedback = new Feedback();
                $feedBackSended = true;
            }
        }

        $companies = Company::find()
            ->joinWith('persons')
            ->all();

        if (Contacts::findOne(1) !== null) {
            return $this->render('index', [
                'model' => $this->findModel(1),
                'feedback' => $feedback,
                'companies' => $companies,
                'feedBackSended' => $feedBackSended,
            ]);
        }
        return $this->render('index',[
            'feedback' => $feedback,
            'companies' => $companies,
            'feedBackSended' => $feedBackSended,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Contacts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
