<?php

namespace frontend\controllers;

use common\models\material\MaterialCategories;
use common\models\material\MaterialTagsMaterialCategoriesTmpViews;
use frontend\models\material\filters\NewsFilter;
use frontend\models\material\search\MaterialSearch;
use common\models\material\Material;
use frontend\models\MaterialList;
use yii\helpers\ArrayHelper;
use Yii;
use yii\web\NotFoundHttpException;

class MaterialController extends MaterialDetailController
{

    public function actionCategory($categoryId)
    {
        if(!$categoryId){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $newsFilter = new NewsFilter();
        $materialSearchProvider = $newsFilter->search(Yii::$app->request->queryParams, $categoryId);

        $materialList = new MaterialList();
        $materialList->setCategoryId($categoryId);
        $materialsProviderToday = $materialList->getDataProviderToday(true);
        $materialsProviderEarlier = $materialList->getDataProviderEarlier(true);

        $tags = MaterialTagsMaterialCategoriesTmpViews::findTagsForCategoryId($categoryId);



        if($this->checkRequestPager($materialsProviderToday)){
            return $this->renderPartial('list/_news_list_today',[
                'dataProvider' => $materialsProviderToday
            ]);
        }

        if($this->checkRequestPager($materialsProviderEarlier)){
            return $this->renderPartial('list/_news_list_earlier',[
                'dataProvider' => $materialsProviderEarlier
            ]);
        }

        if($this->checkRequestPager($materialSearchProvider)){
            return $this->renderPartial('list/_news_list_all',[
                'dataProvider' => $materialSearchProvider
            ]);
        }

        $currentCategory = MaterialCategories::findOne($categoryId);

        return $this->render('index', [
            'categoryId' => $categoryId,
            'newsFilter' => $newsFilter,
            'materialsProviderToday' => $materialsProviderToday,
            'tags' => $tags,
            'materialsProviderEarlier' => $materialsProviderEarlier,
            'materialSearchProvider' => $materialSearchProvider,
            'currentCategory' => $currentCategory,
        ]);
    }
}
