<?php

namespace frontend\controllers;

use common\models\material\Material;
use common\models\material\MaterialCategories;
use common\models\material\MaterialTagsMaterialCategoriesTmpViews;
use frontend\models\material\filters\NewsFilter;
use frontend\models\material\search\MaterialSearch;
use frontend\models\MaterialList;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class RubricController extends MaterialDetailController
{
    public function actionCategory($categoryId)
    {
      //  if (!$categoryId) {
       //     throw new NotFoundHttpException('The requested page does not exist.');
       // }

        $newsFilter = new NewsFilter();
        $materialsProvider = $newsFilter->search(Yii::$app->request->queryParams, $categoryId, true);
        $currentCategory = MaterialCategories::findOne($categoryId);
        $categories = MaterialCategories::find()->where(['root' => $currentCategory->root, 'lvl' => 1])->all();
        $tags = MaterialTagsMaterialCategoriesTmpViews::findTagsForCategoryId($categoryId);

        if($this->checkRequestPager($materialsProvider)) {

            return $this->renderPartial('list/_rubric_list', [
                'materialsProvider' => $materialsProvider,
            ]);
        }
        return $this->render('index', [
            'newsFilter' => $newsFilter,
            'categories' => $categories,
            'currentCategory' => $currentCategory,
            'tags' => $tags,
            'materialsProvider' => $materialsProvider,
        ]);
    }

}