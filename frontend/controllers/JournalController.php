<?php

namespace frontend\controllers;

use backend\modules\rest\models\Journals;
use frontend\models\MaterialList;
use Yii;
use common\models\journal\Journal;
use frontend\models\journal\serach\JournalSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\material\search\MaterialSearch;

/**
 * JournalController implements the CRUD actions for Journal model.
 */
class JournalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Journal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $journal = new Journal();
        $searchModel = new JournalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'journal' => $journal,
        ]);
    }

    /**
     * Displays a single Journal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $year = date('Y',$model->date_to);
        $journalsQuery = Journals::find()
            ->joinWith('image')
            ->where(["between", "date_to", strtotime($year.'-01-01'), strtotime($year.'-12-31')]);
        $journalDataProvider = new ActiveDataProvider([
            'query' => $journalsQuery,
        ]);

        $materialList = new MaterialList();
        $materialDataProvider = $materialList->getDataProvider();
        $materialDataProvider->pagination->pageSize = 200;

        $materialDataProvider->query->andWhere(['journal_id' => $model->id]);

        if($this->checkRequestPager($materialDataProvider)) {
            return $this->renderPartial('list/_material_list', [
                'materialDataProvider' => $materialDataProvider
            ]);
        }

        if(Yii::$app->request->isAjax){
            return $this->renderPartial('_journal_view',[
                'model' => $model,
                'materialDataProvider' => $materialDataProvider
            ]);
        }

        return $this->render('view', [
           // 'dataProvider' => $dataProvider,
            'materialDataProvider' => $materialDataProvider,
            'journalDataProvider' => $journalDataProvider,
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Journal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $dataProvider
     * @return bool
     */
    protected function checkRequestPager($dataProvider){
        if(
            Yii::$app->request->get(($dataProvider->id ? $dataProvider->id.'-page' : 'page')) &&
            Yii::$app->request->isAjax &&
            Yii::$app->request->post('pager')
        ){
            return true;
        }
        return false;
    }
}
