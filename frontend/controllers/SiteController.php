<?php
namespace frontend\controllers;

use backend\modules\constructor\models\Constructor;
use backend\modules\newsletters\models\NewslettersSubscribers;
use backend\modules\newsletters\models\Subscribe;
use common\models\company\Company;
use common\models\material\Material;
use frontend\models\material\filters\PersonFilter;
use frontend\models\MaterialList;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\web\Response;
use Zelenin\yii\extensions\Rss\RssView;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'newsletters-subscribe' => [
                'class' => '\backend\modules\newsletters\actions\SubscribeSuccessPage',
            ],
            'newsletters-unsubscribe' => [
                'class' => '\backend\modules\newsletters\actions\UnsubscribeSuccessPage',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        //NewslettersSubscribers::subscribe('ki2222222oh3222229912223333384122@in4mail.net', 7);

        $models = Constructor::find()->where(['type' => Constructor::TYPE_MAIN])
            ->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
            ->all();
        return $this->render('index', [
            'models' => $models
        ]);
    }
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionPrivacy()
    {
        return $this->render('privacy');
    }

    public function actionRss()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $materialList = new MaterialList();
        $dataProvider  = $materialList->getDataProvider();
        // Не выводим Партнерские материалы в RSS
        $dataProvider->query->andWhere(['<>', 'mainCategory.slug', 'partnerskiye-materialy']);

        return $this->renderPartial('rss', [
            'models' => $dataProvider->getModels(),
        ]);
    }

    public function actionRssYandex()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $materialList = new MaterialList();
        $dataProvider  = $materialList->getDataProvider();
        // Не выводим Партнерские материалы в RSS
        $dataProvider->query->andWhere(['<>', 'mainCategory.slug', 'partnerskiye-materialy']);

        return $this->renderPartial('rss_yandex', [
            'models' => $dataProvider->getModels(),
        ]);
    }

    public function actionRobotsTxt()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->set('Content-Type', 'text/plain; charset=UTF-8');
        if (YII_ENV === 'prod') {
            $file = Yii::getAlias('@frontend/web/robots.prod.txt');
        } else {
            $file = Yii::getAlias('@frontend/web/robots.dev.txt');
        }
        return file_get_contents($file);
    }
}
