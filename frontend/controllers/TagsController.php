<?php

namespace frontend\controllers;

use common\models\material\MaterialTagsMaterialCategoriesTmpViews;
use frontend\models\material\search\MaterialSearch;
use frontend\traits\Pager;
use Yii;
use common\models\material\MaterialTags;
use yii\web\NotFoundHttpException;

class TagsController extends  \yii\web\Controller
{
    use Pager;

    public function actionIndex($slug)
    {
        $model = $this->findModel($slug);
        MaterialTags::viewIncrement($model->id);
        $searchModel = new MaterialSearch();
        $searchModel->setTagId($model->id);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /**@var $materialCategoriesTmpViews MaterialTagsMaterialCategoriesTmpViews*/
        $materialCategoriesTmpViews = MaterialTagsMaterialCategoriesTmpViews::find()
            ->where(['material_tags_id' => $model->id])
            ->andWhere(['material_categories.tags_page_active' => true])
            ->groupBy(['id', 'material_categories_id'])
            ->joinWith('materialCategories')
            ->all();

        $categories = [];
        foreach ($materialCategoriesTmpViews as $categoriesTmpView){
            $categories[] = $categoriesTmpView->materialCategories;
        }

        if($this->checkRequestPager($dataProvider)){
            return $this->renderPartial('list/_tags_material_all.php',[
                'dataProvider' => $dataProvider
            ]);
        }

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
            'model' => $model,
        ]);
    }

    /**
     * @param string $slug
     * @return null|MaterialTags
     * @throws NotFoundHttpException
     */
    protected function findModel($slug)
    {
        if (($model = MaterialTags::find()->where(['slug' => $slug])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
