<?php

namespace frontend\controllers;

use common\models\Feedback;
use common\models\contacts\Contacts;
use common\models\company\Company;
use Yii;
use yii\web\NotFoundHttpException;

class PoliticsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $companies = Company::find()
            ->joinWith('persons')
            ->where(['code' => ['administration', 'sellers', 'journal']])
            ->indexBy('code')
            ->all();
        return $this->render('index',[
            'companies' => $companies
        ]);
    }
}
