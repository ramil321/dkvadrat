<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'category_flags' => [
        7 => 'news',
        8 => 'society',
        1 => 'politic',
        2 => 'economics',
        11 => 'who_is_who',
        12 => 'person',
        13 => 'company',

    ],
    'materialCategoriesTypes' => [
        'news' => [
            'controllerRoutePage' => 'material/category',
            'controllerRouteDetail' => 'material/detail'
        ],
        'rubric' => [
            'controllerRoutePage' => 'rubric/category',
            'controllerRouteDetail' => 'rubric/detail'
        ],
        'who_is_who' => [
            'controllerRoutePage' => 'who/category',
            'controllerRouteDetail' => 'who/detail'
        ],
        'person' => [
            'controllerRoutePage' => 'who/category',
            'controllerRouteDetail' => 'who/detail'
        ],
        'organization' => [
            'controllerRoutePage' => 'who/category',
            'controllerRouteDetail' => 'who/detail'
        ],
        'society' => [
            'controllerRoutePage' => 'who/category',
            'controllerRouteDetail' => 'who/detail'
        ],
        'economics' => [
            'controllerRoutePage' => 'who/category',
            'controllerRouteDetail' => 'who/detail'
        ],
        'politic' => [
            'controllerRoutePage' => 'who/category',
            'controllerRouteDetail' => 'who/detail'
        ]
    ]
];
