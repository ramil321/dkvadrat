<?php


return [
    'name' => 'Kvadrat',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone' => 'Europe/Samara',
    'modules' => [
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            'treeViewSettings' =>
            [
                'nodeView' => '@backend/modules/pages/views/tree/_form',
            ]
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableConfirmation' => false,
            'enableUnconfirmedLogin' => true,
            'admins' => ['admin'],
            'modelMap' => [
                'User' => 'common\models\user\User',
                'Profile' => 'common\models\user\Profile',
            ],
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
    ],
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => [
                '@app/migrations',
                '@vendor/dektrium/yii2-user/migrations',
                '@yii/rbac/migrations'
            ]
        ]
    ],
    'bootstrap' => [
        'queue',
    ],
    'components' => [
        'subscribe' => [
            'class' => 'backend\modules\newsletters\components\Subscribe',
            'defaultListId' => 1,
            'senderEmail' => 'info@kvadrat.picom.su',
            'subscribePage' => 'http://kvadrat.picom.su/site/newsletters-subscribe/',
            'unsubscribePage' => 'http://kvadrat.picom.su/site/newsletters-unsubscribe/',
        ],
        'resize' => [
            'class' => 'backend\modules\files\components\ResizeImage',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager'  => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],
        'formatter' => [
            'class' => 'common\components\Formatter',
            'timeZone' => 'Europe/Samara',
            'defaultTimeZone' => 'Europe/Samara'
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=sphinx;port=9306;',
            'username' => '',
            'password' => '',
        ],
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db', // DB connection component or its config
            'tableName' => '{{%queue}}', // Table name
            'channel' => 'default', // Queue channel key
            'mutex' => \yii\mutex\MysqlMutex::class, // Mutex used to sync queries
        ],
    ],
    'language' => 'ru-RU',
];
