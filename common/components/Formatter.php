<?php
namespace common\components;


use Yii;
use yii\i18n\Formatter as YiiFormatter;


class Formatter extends YiiFormatter
{
    /**
     * @param int $timestamp
     * @return string
     */
    public function asShortDate($timestamp)
    {
        if(!$timestamp){
            return '';
        }

        $today = strtotime(date("Ymd"));

        if ($timestamp < $today && $timestamp >= $today - 60 * 60 * 24) {
            return 'Вчера, '. $this->asTime($timestamp, 'short');
        } elseif ($timestamp >= $today && $timestamp < $today + 60 * 60 * 24) {
            return 'Сегодня, '. $this->asTime($timestamp, 'short');
        } else {
            return $this->asTime($timestamp, 'php: d.m.Y H:i');
        }
    }

    /**
     * @param int $timestamp
     * @return string
     */
    public function asFullDate($timestamp)
    {
        if(!$timestamp){
            return '';
        }

        $today = strtotime(date("Ymd"));

        if ($timestamp < $today && $timestamp >= $today - 60 * 60 * 24) {
            return 'Вчера, '. $this->asTime($timestamp, 'short');
        } elseif ($timestamp >= $today && $timestamp < $today + 60 * 60 * 24) {
            return 'Сегодня, '. $this->asTime($timestamp, 'short');
        } else {
            return $this->asTime($timestamp, 'php: d F Y H:i');
        }
    }

}