<?php

namespace common\models;

use Yii;
use backend\behaviors\Slug;
use backend\modules\files\models\Files;
use yii\helpers\Url;

/**
 * This is the model class for table "special_projects".
 *
 * @property int $id
 * @property string $short_name
 * @property string $name
 * @property string $description
 * @property int $image_id
 * @property string $slug
 * @property int $position
 * @property int $active
 */
class SpecialProjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'special_projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_id', 'position', 'active'], 'integer'],
            [['short_name','description', 'name', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => Slug::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'transliterator' => 'Russian-Latin/BGN; NFKD',
                //Set this to true, if you want to update a slug when source attribute has been changed
                'forceUpdate' => false
            ]
        ];
    }
    public function fields()
    {
        return [
            'id',
            'name',
            'short_name',
            'position',
            'active',
            'image',
            'description',
            'detail_url' => function () {
                return $this->getUrl();
            },
        ];
    }
    public function extraFields()
    {
        return [
            'sections',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_name' => 'Short Name',
            'name' => 'Name',
            'image_id' => 'Image ID',
            'slug' => 'Slug',
            'position' => 'Position',
            'active' => 'Active',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'image_id']);
    }

    public function getUrl()
    {
        return Url::to(['special-project/view', 'id' => $this->id]);
    }
}