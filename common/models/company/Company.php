<?php

namespace common\models\company;

use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use common\models\person\Person;
use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name
 * @property int $image_id
 * @property string $address
 * @property string $text
 * @property string $phone
 * @property string $email
 * @property Person[] $persons
 */
class Company extends \yii\db\ActiveRecord
{
    public $imageInput;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    public function behaviors()
    {
        return [
            [
                'class' =>   ImageBehavior::className(),
                'inputFileName' => [
                    'imageInput' => 'image_id',
                ]
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['imageInput','safe'],
            [['image_id'], 'integer'],
            [['text'], 'string'],
            [['name','code'],'required'],
            [['name', 'address', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imageInput' => 'Логотип',
            'name' => 'Название',
            'image_id' => 'Image ID',
            'address' => 'Адрес',
            'site' => 'Сайт',
            'text' => 'Краткое описание деятельности',
            'phone' => 'Телефон',
            'email' => 'E-mail',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersons()
    {
        return $this->hasMany(Person::className(), ['company_id' => 'id']);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements($ids = false){
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }
}
