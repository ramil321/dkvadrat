<?php

namespace common\models\journal;

use backend\behaviors\DateConverterBehavior;
use backend\modules\files\behaviors\FileBehavior;
use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use kartik\daterange\DateRangeBehavior;
use Yii;
use common\models\material\Material;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "journal".
 *
 * @property int    $id
 * @property int    $number
 * @property int    $image_id
 * @property int    $date
 * @property int    $date_from
 * @property int    $date_to
 * @property Files  $file;
 * @property int    $file_id
 * @property string $dateFormated
 */
class Journal extends \yii\db\ActiveRecord
{
    public $imageInput;
    public $fileInput;

    public $dateFormated;
    public $dateFormatedFrom;
    public $dateFormatedTo;

    const DATE_FORMAT_FULL = 'd.m.Y H:i:s';
    const DATE_FORMAT_SHORT = 'd.m.Y';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journal';
    }

    public function behaviors()
    {
        return [
            [
                'class' => ImageBehavior::className(),
                'inputFileName' => [
                    'imageInput' => 'image_id',
                ]
            ],
            [
                'class' => FileBehavior::className(),
                'inputFileName' => [
                    'fileInput' => 'file_id',
                ]
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:' . self::DATE_FORMAT_SHORT, // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'dateFormatedFromTmp' => 'date_from', // date is original attribute
                ]
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:' . self::DATE_FORMAT_SHORT, // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'dateFormatedToTmp' => 'date_to', // date is original attribute
                ]
            ],
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'dateFormated',
                'dateStartAttribute' => 'dateFormatedFrom',
                'dateEndAttribute' => 'dateFormatedTo',
            ]
        ];
    }

    public function init()
    {

        $this->on(self::EVENT_BEFORE_VALIDATE, function () {
            $dates = explode(' - ', $this->dateFormated);
            $this->dateFormatedFromTmp = $dates[0];
            $this->dateFormatedToTmp = $dates[1];
        });

        $this->on(self::EVENT_AFTER_FIND, function () {
            $this->dateFormated = $this->dateFormatedFromTmp . ' - ' . $this->dateFormatedToTmp;
        });
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['dateFormated', 'dateFormatedFrom', 'dateFormatedTo'], 'safe'],
            [['image_id', 'file_id'], 'integer'],
            [['number'], 'string'],
            [['imageInput', 'fileInput'], 'file', 'maxSize' => 100*1024*1024],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'file_id']);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements($ids = false)
    {
        if ($ids) {
            return self::find()->where(['id' => $ids])->all();
        }

        return self::find()->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dateFormated' => 'Дата',
            'number' => 'Номер',
            'imageInput' => 'Обложка',
            'fileInput' => 'Журнал в pdf',
            'image_id' => 'Image ID',
            'date' => 'Дата',
            'file_id' => 'Пдф',
        ];
    }

    /**
     * Возвращает уникальный список года выпуска журналов
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getYearJournals(){
        return Yii::$app->db->createCommand("SELECT DISTINCT(YEAR(FROM_UNIXTIME(`date`))) as year FROM journal")->queryColumn();
    }

    public function getMonthFormated(){
        //январь – февраль 2018
        $from = Yii::$app->formatter->asDate($this->date_from, 'LLLL');
        $to = Yii::$app->formatter->asDate($this->date_to, 'LLLL');
        if($from != $to){
            return $from.' - '.$to.' '.date('Y',$this->date_to);
        }else{
            return $to.' '.date('Y',$this->date_to);
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Url::to(['journal/view','id' => $this->id]);
    }
}
