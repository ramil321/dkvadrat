<?php

namespace common\models\person;

use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use common\models\company\Company;
use Yii;

/**
 * This is the model class for table "person".
 *
 * @property int $id
 * @property int $photo_id
 * @property string $fio
 * @property int $company_id
 * @property int $category_id
 * @property string $position
 * @property string $biography
 * @property Files $photo
 * @property string $vk_herf
 * @property string $fb_href
 * @property string $phone_stationary
 * @property string $fax
 * @property string $email
 * @property PersonCategories $category
 */
class Person extends \yii\db\ActiveRecord
{
    public $photoInput;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person';
    }
    public function behaviors()
    {
        return [
            [
                'class' =>   ImageBehavior::className(),
                'inputFileName' => [
                    'photoInput' => 'photo_id',
                ]
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo_id', 'company_id', 'category_id'], 'integer'],
            [['fio', 'category_id', 'biography'], 'required'],
            [['biography'], 'string'],
            [['fio', 'position', 'phone_stationary', 'fax', 'email', 'vk_href', 'fb_href', 'tw_href'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PersonCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photoInput' => 'Фото',
            'photo_id' => 'Photo ID',
            'fio' => 'Ф.И.О',
            'company_id' => 'Компания',
            'category_id' => 'Категория',
            'position' => 'Должность',
            'biography' => 'Биография',
            'phone_stationary' => 'Стационарный',
            'fax' => 'Факс',
            'email' => 'E-mail',
            'vk_href' => 'Ссылка на vk',
            'fb_href' => 'Ссылка facebook',
            'tw_href' => 'Ссылка twitter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PersonCategories::className(), ['id' => 'category_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(Files::className(), ['id' => 'photo_id']);
    }

}
