<?php

namespace common\models\user;

use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "profile".
 * @property string  $last_name
 * @property string  $second_name
 * @property Files photo
 */

class Profile extends \dektrium\user\models\Profile
{

    public $photoInput;

    public function behaviors()
    {
        return array_merge(parent::behaviors(),[
            'image' => [
                'class' =>   ImageBehavior::className(),
                'inputFileName' => [
                    'photoInput' => 'photo_id',
                ]
            ]
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),[
            'last_name'          => 'Фамилия',
            'second_name'          => 'Отчество',
            'photoInput' => 'Фото'
        ]);

    }
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['last_name','second_name'], 'string', 'max' => 255],
            ['photo_id','integer'],
            ['photoInput','file']
        ]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(Files::className(), ['id' => 'photo_id']);
    }
}
