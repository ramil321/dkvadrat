<?php

namespace common\models\user;


use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * @property Profile $profile
 **/
class User extends \dektrium\user\models\User
{
    const ROLE_AUTHOR = 'author';
    const ROLE_ADMIN = 'admin';

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            ['username' => 'Логин']
        );
    }

    public function fields()
    {
        return [
            'id',
            'fullName',
            'photo' => function () {
                return $this->profile->photo->src;
            },
            'materialsCount'
        ];
    }

    /**
     * Finds all users by assignment role
     *
     * @param  \yii\rbac\Role $role
     *
     * @return \yii\db\ActiveQuery
     */
    public static function findByRole($role, $ids = false)
    {
        if ($ids) {
            return static::find()
                ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = id')
                ->where(['auth_assignment.item_name' => $role])
                ->andWhere(['id' => $ids]);
        }

        return static::find()
            ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = id')
            ->where(['auth_assignment.item_name' => $role]);
    }

    public function getFullName()
    {
        $name = [
            $this->profile->name,
            $this->profile->second_name,
            $this->profile->last_name
        ];

        return implode(' ', $name);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /* public function getToken()
     {
         return $this->hasOne('token', ['user_id' => 'id']);
     }
 */
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAuthors($ids = false)
    {
        return self::findByRole(self::ROLE_AUTHOR, $ids);
    }

    public static function findByUsername($username)
    {
        return static::find()->where(['username' => $username])->andWhere(['<', 'blocked_at', '0'])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null, $tokenTime = 3600)
    {
        /*  return static::find()
              ->joinWith('token t')
              ->andWhere(['t.code' => $token,'type' => $type])
              ->andWhere(['>', 't.created_at', time()+$tokenTime])
              ->one();*/
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    private $materialsCount;

    public function setMaterialsCount($value)
    {
        $this->materialsCount = $value;
    }

    public function getMaterialsCount()
    {
        if ($this->materialsCount === null) {

            $this->materialsCount = (new Query())
                ->from('material_user')
                ->where(['user_id' => $this->id])
                ->count();
        }
        return $this->materialsCount;
    }
}
