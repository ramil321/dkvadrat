<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "token".
 *
 * @property int $user_id
 * @property string $code
 * @property int $created_at
 * @property int $type
 *
 * @property User $user
 */
class Token extends \dektrium\user\models\Token
{

    const TYPE_REST_AUTH = 4;

    public function generateToken()
    {
        $this->code = \Yii::$app->security->generateRandomString();
    }

}
