<?php

namespace common\models\banner;

use voskobovich\behaviors\ManyToManyBehavior;
use Yii;

/**
 * This is the model class for table "banner_company".
 *
 * @property int $id
 * @property string $name
 * @property int $banner_advertiser_id
 * @property array $bannerIds
 * @property BannerAdvertiser $bannerAdvertiser
 */
class BannerCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner_company';
    }

    public function behaviors()
    {
        return [
            'many_to_many' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => ['bannerIds' => 'banners']
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public static function getElements($ids = false){
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(
            Banner::className(),
            ['id' => 'banner_id']
        )->viaTable(
            'banner_company_banner',
            ['banner_company_id' => 'id']
        );
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['bannerIds','safe'],
            [['banner_advertiser_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['banner_advertiser_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerAdvertiser::className(), 'targetAttribute' => ['banner_advertiser_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Рекламная компания',
            'banner_advertiser_id' => 'Рекламодатель',
            'bannerIds' => 'Баннеры'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerAdvertiser()
    {
        return $this->hasOne(BannerAdvertiser::className(), ['id' => 'banner_advertiser_id']);
    }
}
