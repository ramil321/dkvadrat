<?php

namespace common\models\banner;

use Yii;

/**
 * This is the model class for table "banner_advertiser".
 *
 * @property int $id
 * @property string $name
 *
 * @property BannerCompany[] $bannerCompanies
 */
class BannerAdvertiser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner_advertiser';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Рекламодатель',
        ];
    }
    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements($ids = false){
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerCompanies()
    {
        return $this->hasMany(BannerCompany::className(), ['banner_advertiser_id' => 'id']);
    }
}
