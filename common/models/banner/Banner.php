<?php

namespace common\models\banner;

use backend\behaviors\DateConverterBehavior;
use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use backend\modules\files\models\ImageStyled;
use backend\modules\rest\models\Tags;
use common\models\material\MaterialCategories;
use common\models\material\MaterialTags;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string $name
 * @property string $image_id
 * @property int $blank
 * @property array $tagIds
 * @property array $categoriesIds
 * @property Tags[] $tags
 * @property MaterialCategories[] $materialCategories
 * @property string $link
 * @property int $banner_position_id
 * @property string $publishDateFormated
 * @property string $publishEndDateFormated
 * @property BannerPosition $bannerPosition
 */
class Banner extends \yii\db\ActiveRecord
{
    const DATE_FORMAT_FULL = 'd.m.Y H:i:s';
    const DATE_FORMAT_SHORT = 'd.m.Y H:i';
    public $imageInput;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' =>   ImageBehavior::className(),
                'inputFileName' => [
                    'imageInput' => 'image_id',
                ]
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:'.self::DATE_FORMAT_SHORT, // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'publishDateFormated' => 'publish_date',
                    'publishEndDateFormated' => 'publish_end_date',
                ]
            ],
            'many_to_many' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'categoriesIds' => [
                        'materialCategories'
                    ],
                    'tagIds' => [
                        'tags',
                    ]
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active'], 'boolean'],
            [['name'], 'required'],
            ['imageInput','file'],
            [['publishDateFormated','publishEndDateFormated', 'tagIds', 'categoriesIds'],'safe'],
            [['blank', 'banner_position_id','image_id'], 'integer'],
            [['name', 'link'], 'string', 'max' => 255],
            [['banner_position_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerPosition::className(), 'targetAttribute' => ['banner_position_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Активность',
            'name' => 'Имя',
            'image_id' => 'Картинка',
            'publishDateFormated' => 'Дата публикации',
            'publishEndDateFormated' => 'Дата окончания поубликации',
            'imageInput' => 'Картинка',
            'blank' => 'Открывать в новом окне',
            'link' => 'Ссылка',
            'advertiser' => 'Рекламодатель',
            'banner_position_id' => 'Позиция банера',
            'categoriesIds' => 'Категории показа',
            'tagIds' => 'Теги показа'
        ];
    }
    /**
     * @return Files
     */
    public function getImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMaterialCategories()
    {
        return $this->hasMany(
            MaterialCategories::className(),
            ['id' => 'material_categories_id']
        )->viaTable(
            'material_categories_banner',
            ['banner_id' => 'id']
        );
    }

    /**
     * @return ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(
            MaterialTags::className(),
            ['id' => 'material_tags_id']
        )->viaTable(
            'material_tags_banner',
            ['banner_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerPosition()
    {
        return $this->hasOne(BannerPosition::className(), ['id' => 'banner_position_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerCompany()
    {
        return $this->hasOne(
            BannerCompany::className(),
            ['id' => 'banner_company_id']
        )->viaTable(
            'banner_company_banner',
            ['banner_id' => 'id']
        );
    }
    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements($ids = false){
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }

    /**
     * @return ImageStyled
     */
    public function getPreviewImageStyled()
    {
        $src = '';
        if($this->image){
            $src = $this->image->src;
        }
        return new ImageStyled(
            $src,
            'center'
        );
    }
}
