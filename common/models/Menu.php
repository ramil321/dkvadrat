<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url', 'type'], 'required'],
            ['active','boolean'],
            [['name', 'url', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
        ];
    }
    public static function getMenu()
    {
        $menuItems = self::find()->where(['type' => 'TOP'])->asArray()->all();
        $arMenu = [];
        foreach ($menuItems as $item){
            $arMenu[] = [
                'label' => $item['name'],
                'url' => $item['url'],
                'options' => ['class' => 'navbar__item'],
                'active' => $item['url'] == Yii::$app->urlManager->createAbsoluteUrl(Yii::$app->request->pathInfo)
            ];
        }
        return $arMenu;
    }
}
