<?php

namespace common\models\press;

use backend\behaviors\DateConverterBehavior;
use backend\modules\files\behaviors\FileBehavior;
use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use common\models\company\Company;
use Yii;

/**
 * This is the model class for table "press_release".
 *
 * @property int $id
 * @property string $name
 * @property int $company_id
 * @property int $file_id
 * @property int $image_id
 * @property int $publish_date
 * @property string $preview_text
 * @property string $detail_text
 *
 * @property Company $company
 */
class PressRelease extends \yii\db\ActiveRecord
{

    public $imageInput;
    public $fileInput;


    const DATE_FORMAT_SHORT = 'd.m.Y H:i';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'press_release';
    }
    public function behaviors()
    {
        return [
            [
                'class' => ImageBehavior::className(),
                'inputFileName' => [
                    'imageInput' => 'image_id',
                ]
            ],
            [
                'class' =>   FileBehavior::className(),
                'inputFileName' => [
                    'fileInput' => 'file_id',
                ]
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:'.self::DATE_FORMAT_SHORT, // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'publishDateFormated' => 'publish_date', // date is original attribute
                ]
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['company_id', 'file_id', 'image_id', 'publish_date'], 'integer'],
            [['preview_text', 'detail_text'], 'string'],
            [['name','publishDateFormated'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'company_id' => 'Компания',
            'imageInput' => 'Изображение',
            'fileInput' => 'Изображение',
            'file_id' => 'File ID',
            'image_id' => 'Image ID',
            'publishDateFormated' => 'Дата публикации',
            'preview_text' => 'Текст превью',
            'detail_text' => 'Детальный текст',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'image_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'file_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements($ids = false){
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }
}
