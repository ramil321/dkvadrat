<?php

namespace common\models\contacts;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $editorial_address
 * @property int $phone_editors
 * @property int $phone_advertising
 * @property string $email
 * @property string $advertisers_href
 * @property string $lat
 * @property string $lng
 * @property string $map_title
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['editorial_address', 'email', 'advertisers_href', 'phone_editors', 'phone_advertising', 'lat', 'lng', 'map_title'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'editorial_address' => 'Адрес редакции',
            'phone_editors' => 'Телефон редакции и производственного отдела',
            'phone_advertising' => 'Телефон отдела рекламы',
            'email' => 'E-mail',
            'advertisers_href' => 'Рекламодателям',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'map_title' => 'Название',
        ];
    }
}
