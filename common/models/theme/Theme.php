<?php

namespace common\models\theme;

use backend\behaviors\DateConverterBehavior;
use backend\behaviors\Slug;
use backend\behaviors\UserAddUpdateBehavior;
use common\models\material\Material;
use voskobovich\behaviors\ManyToManyBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "theme".
 * @property int              $id
 * @property int              $created_at
 * @property int              $updated_at
 * @property string           $createDateFormated
 * @property string           $updateDateFormated
 * @property string           $name
 * @property string           $description
 * @property string           $seo_title
 * @property string           $seo_description
 * @property string           $seo_keywords
 * @property string           $slug
 * @property int              $update_user_id
 * @property int              $insert_user_id
 * @property ThemeMaterials[] $materials
 * @property ThemeMaterials[] $themeMaterials
 */
class Theme extends \yii\db\ActiveRecord
{

    const DATE_FORMAT_FULL = 'd.m.Y H:i:s';
    const DATE_FORMAT_SHORT = 'd.m.Y H:i';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theme';
    }

    public function behaviors()
    {
        return [
            [
                'class' => Slug::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'transliterator' => 'Russian-Latin/BGN; NFKD',
                //Set this to true, if you want to update a slug when source attribute has been changed
                'forceUpdate' => false
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => UserAddUpdateBehavior::className(),
                'updateAttribute' => 'update_user_id',
                'insertAttribute' => 'insert_user_id',
            ],
            'many_to_many' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'materialsIds' => 'materials',
                ],
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:' . self::DATE_FORMAT_SHORT, // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'createDateFormated' => 'created_at', // date is original attribute
                    'updateDateFormated' => 'updated_at', // date is original attribute
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['materialsIds', 'safe'],
            [['created_at', 'updated_at', 'update_user_id', 'insert_user_id'], 'integer'],
            [['description', 'seo_description'], 'string'],
            [['name', 'seo_title', 'seo_keywords', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'createDateFormated' => 'Дата создания',
            'updateDateFormated' => 'Дата обновления',
            'name' => 'Имя',
            'description' => 'Описание',
            'seo_title' => 'Seo заголовок',
            'seo_description' => 'Seo описание',
            'seo_keywords' => 'Seo ключевые слова',
            'slug' => 'Символьный код',
            'update_user_id' => 'Update User ID',
            'insert_user_id' => 'Insert User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public static function getElements($ids = false)
    {
        if ($ids) {
            return self::find()->where(['id' => $ids])->all();
        }

        return self::find()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(
            Material::className(),
            ['id' => 'material_id']
        )->viaTable(
            'theme_material',
            ['theme_id' => 'id']
        );
    }

    private $materialsCount;

    public function setMaterialsCount($value)
    {
        $this->materialsCount = $value;
    }

    public function fields()
    {
        return array_merge(parent::fields(), ['materialsCount']);
    }

    public function getMaterialsCount()
    {
        if ($this->materialsCount === null) {

            $this->materialsCount = (new Query())
                ->from('theme_material')
                ->where(['theme_id' => $this->id])
                ->count();
        }

        return $this->materialsCount;
    }
}
