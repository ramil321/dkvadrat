<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class TopMenu extends Model
{
    public $arMenu;
    const MENU_TYPE = 'TOP';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['arMenu'], 'required'],
        ];
    }

    public function getMenu(){
        return Menu::find()->where(['type' => self::MENU_TYPE])->all();
    }

    public function save(){
        if (!$this->hasErrors()) {
            $menu = Menu::find()->where(['type' => self::MENU_TYPE])->all();
            foreach ($menu as $item){
                $item->delete();
            }
            foreach ($this->arMenu as $item){
               $menu = new Menu();
               $menu->type = self::MENU_TYPE;
               foreach ($item as $key=>$val){
                   $menu->{$key} = $val;
               }
               $menu->save();
               foreach ($menu->getErrors() as $error){
                   $this->addError('arMenu',$error);
               }
            }
        }
    }

}
