<?php

namespace common\models\material;

use Yii;

/**
 * This is the model class for table "material_categories_material".
 *
 * @property int $id
 * @property int $material_categories_id
 * @property int $material_id
 * @property int $is_main
 *
 * @property MaterialCategories $materialCategories
 * @property Material $material
 */
class MaterialCategoriesMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_categories_material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_categories_id', 'material_id'], 'integer'],
            [['is_main'], 'string', 'max' => 1],
            [['material_categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialCategories::className(), 'targetAttribute' => ['material_categories_id' => 'id']],
            [['material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Material::className(), 'targetAttribute' => ['material_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_categories_id' => 'Material Categories ID',
            'material_id' => 'Material ID',
            'is_main' => 'Is Main',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialCategories()
    {
        return $this->hasOne(MaterialCategories::className(), ['id' => 'material_categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterial()
    {
        return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }
}
