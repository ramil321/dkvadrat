<?php

namespace common\models\material\search;
use yii\helpers\ArrayHelper;
use yii\sphinx\ActiveRecord;
use common\models\material\Material;


/*
 * @property string $body
 * @property int $id
 * */
class MaterialSphinxIndex extends ActiveRecord
{

    public static function tableName()
    {
        return 'material_sphinx_index';
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['body'],'safe']
            ]
        );
    }

    public static function indexerAll(){
        $result = [];
        $materials = Material::find()->all();
        if ($materials) {
            foreach ($materials as $material) {
                $index = self::find()->where(['id' => $material->id])->one();
                $tags = implode(" ", ArrayHelper::map($material->tags, 'id', 'name'));
                $result[] = [
                    'id' => $material->id,
                    'name' => $material->name,
                ];
                if ($index) {
                    $index->body = implode(" ", [$material->name, $material->preview_text, $material->detail_text, $tags]);
                    $index->save();
                } else {
                    $index = new MaterialSphinxIndex();
                    $index->id = $material->id;
                    $index->body =  implode(" ", [$material->name, $material->preview_text, $material->detail_text, $tags]);
                    $index->save();
                }
            }
        }
        return $result;
    }
}