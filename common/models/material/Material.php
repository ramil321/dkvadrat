<?php

namespace common\models\material;

use backend\behaviors\DateConverterBehavior;
use backend\behaviors\HitBehavior;
use backend\behaviors\Slug;
use backend\behaviors\UserAddUpdateBehavior;
use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use backend\modules\files\models\ImageStyled;
use common\models\journal\Journal;
use common\models\theme\Theme;
use common\models\user\User;
use usualdesigner\yii2\behavior\HitableBehavior;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;

//use function foo\func;

/**
 * This is the model class for table "material".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property string $name
 * @property string $short_name
 * @property string $preview_text
 * @property string $detail_text
 * @property int $detail_image_id
 * @property int $publish_date
 * @property bool $is_published
 * @property bool $detail_image_option_full_size [tinyint(1)]
 * @property string $detail_image_option_position [varchar(255)]
 * @property string $detail_image_option_author [varchar(255)]
 * @property string $react_tmp
 * @property int $detail_image_crop_id [int(11)]
 * @property int $preview_image_crop_id [int(11)]
 * @property int $preview_image_crop_priority_id [int(11)]
 * @property int $views_count
 * @property int $duration_read
 * @property bool $priority
 * @property bool $show_comments
 * @property bool $show_content
 * @property bool $show_author
 * @property int $themeId
 * @property int $preview_image_id
 * @property int $insert_user_id [int(11)]
 * @property int $update_user_id [int(11)]
 * @property int $journal_id [int(11)]
 * @property int $material_region_id [int(11)]
 * @property bool $show_theme_materials [tinyint(1)]
 *
 *
 * @property Files $detailImage
 * @property Files $previewImage
 * @property Files $previewImageCrop
 * @property Files $previewImageCropPriority
 * @property Files $detailImageCrop
 * @property Files $headlineImage
 * @property string $url
 * @property MaterialCategories[] $materialCategories
 * @property MaterialCategories $mainCategory
 * @property string $slug
 * @property int $mainCategoryId;
 * @property MaterialTags[] $tags
 * @property MaterialCategoriesMaterial[] $materialCategoriesMaterials
 * @property array tagIds
 * @property array authorsIds
 * @property string publishDateFormated
 * @property string createdDateFormated
 * @property string updatedDateFormated
 * @property array convertedTags
 * @property string $status
 */
class Material extends ActiveRecord
{

    const DATE_FORMAT_FULL = 'd.m.Y H:i:s';
    const DATE_FORMAT_SHORT = 'd.m.Y H:i';

    use MaterialValidatorTrait, MaterialEventTrait;

    public $detailImagePositions = ['in_frame', 'center', 'top', 'bottom'];
    public $detailImageInput;
    public $detailImageCropInput;
    public $headlineImageInput;
    public $detailImageDescription;
    public $previewImageInput;
    public $previewImageCropInput;
    public $previewImageCropPriorityInput;
    public $previewImageDescription;
    public $tmpTags = [];
    public $convertedTags = [];
    public $tmpMainCategoryId;
    public $convertedMainCategories;
    public $tmpTheme;
    public $convertedTheme;
    public $month;

    private static $categoriesCache;

    public function behaviors()
    {
        return [
            [
                'class' => Slug::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'transliterator' => 'Russian-Latin/BGN; NFKD',
                //Set this to true, if you want to update a slug when source attribute has been changed
                'forceUpdate' => false
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'hit' => [
                'class' => HitBehavior::className(),
                'attribute' => 'views_count',
                'group' => false,               //group name of the model (class name by default)
                'delay' => 60 * 60,             //register the same visitor every hour
                'table_name' => '{{%material_tags_hits}}'     //table with hits data
            ],
            [
                'class' => UserAddUpdateBehavior::className(),
                'updateAttribute' => 'update_user_id',
                'insertAttribute' => 'insert_user_id',
            ],
            'many_to_many' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'mainCategoryId' => [
                        'materialCategories',
                        'viaTableValues' => [
                            'is_main' => function ($model, $relationName, $attributeName, $relatedPk) {
                                if ($relatedPk == $model->tmpMainCategoryId) {
                                    return true;
                                }
                            }
                        ],
                        'get' => function ($val) {
                            if (is_array($val)) {
                                if ($this->mainCategory) {
                                    return $this->mainCategory->id;
                                }
                            } else {
                                return $val;
                            }
                        }
                    ],
                    'tagIds' => [
                        'tags',
                    ],
                    'authorsIds' => 'authors',
                    'themeId' => [
                        'themes',
                    ],


                ],
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:' . self::DATE_FORMAT_FULL, // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'publishDateFormated' => 'publish_date',
                    'createdDateFormated' => 'created_at',
                    'updatedDateFormated' => 'updated_at',
                ]
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:d.m.Y, H:i', // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'updatedDateFormatedShort' => 'updated_at',
                ]
            ],
            'image' => [
                'class' => ImageBehavior::className(),
                'inputFileName' => [
                    'previewImageInput' => 'preview_image_id',
                    'previewImageCropInput' => 'preview_image_crop_id',
                    'previewImageCropPriorityInput' => 'preview_image_crop_priority_id',
                    'detailImageCropInput' => 'detail_image_crop_id',
                    'detailImageInput' => 'detail_image_id',
                    'headlineImageInput' => 'headline_image_id'
                ]
            ]

        ];
    }

    public function init()
    {
        foreach ([self::EVENT_BEFORE_INSERT, self::EVENT_BEFORE_UPDATE] as $event) {
            $this->on($event, [$this, 'addEmptyTags']);
            $this->on($event, [$this, 'addEmptyTheme']);
            $this->on($event, [$this, 'findTagsByCategories']);
            $this->on($event, [$this, 'durationRead']);
        }

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'sphinxAddIndex']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'sphinxUpdateIndex']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'sphinxDeleteIndex']);

        foreach ([self::EVENT_AFTER_INSERT, self::EVENT_AFTER_UPDATE] as $event) {
            $this->on($event, [$this, 'addTmpViewTags']);
        }
        parent::init();
    }

    /**
     * @return ActiveQuery
     */

    public function getPreviewImageCropPriority()
    {
        return $this->hasOne(Files::className(), ['id' => 'preview_image_crop_priority_id']);
    }

    public function getPreviewImageCrop()
    {
        return $this->hasOne(Files::className(), ['id' => 'preview_image_crop_id']);
    }

    public function getPreviewImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'preview_image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(MaterialRegion::className(), ['id' => 'material_region_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDetailImageCrop()
    {
        return $this->hasOne(Files::className(), ['id' => 'detail_image_crop_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDetailImage()
    {
        return $this->hasOne(Files::className(), ['id' => 'detail_image_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getHeadlineImage()
    {
        return $this->hasOne(Files::class, ['id' => 'headline_image_id']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material';
    }

    /**
     * @param $ids
     *
     * @return array|ActiveRecord[]
     */
    public static function getElements($ids)
    {
        if ($ids) {
            return self::find()->where(['id' => $ids])->all();
        }

        return self::find()->all();
    }

    /**
     * @inheritdoc                    'show_comments' => $material->show_comments,
     * 'show_content' => $material->show_content,
     * 'show_author' => $material->show_author,
     */
    public function rules()
    {
        return [
            [['show_comments', 'show_content', 'show_author'], 'boolean'],
            ['detail_image_option_position', 'in', 'range' => $this->detailImagePositions, 'strict' => true],
            [['mainCategoryId', 'themeId', 'material_region_id'], 'safe'],
            [
                ['journal_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Journal::className(),
                'targetAttribute' => ['journal_id' => 'id']
            ],
            [
                ['mainCategoryId'],
                'exist',
                'skipOnError' => true,
                'targetClass' => MaterialCategories::className(),
                'targetAttribute' => ['mainCategoryId' => 'id']
            ],
            ['authorsIds', 'validateAuthors'],
            [['priority', 'show_theme_materials', 'detail_image_option_full_size'], 'boolean'],
            [['tagIds', 'publishDateFormated', 'publish_date', 'react_tmp'], 'safe'],
            [['name', 'detail_text', 'mainCategoryId'], 'required'],
            [['previewImageDescription', 'detailImageDescription'], 'string'],
            [
                [
                    'previewImageInput',
                    'previewImageCropInput',
                    'previewImageCropPriorityInput',
                    'detailImageInput',
                    'detailImageCropInput',
                    'headlineImageInput'
                ],
                'file'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'detail_image_id',
                    'detail_image_crop_id',
                    'preview_image_id',
                    'preview_image_crop_priority_id',
                    'preview_image_crop_id',
                    'headline_image_id',
                ],
                'integer'
            ],
            [['preview_text', 'detail_text'], 'string'],
            [['name', 'short_name', 'detail_image_option_author'], 'string', 'max' => 255],
            [
                [
                    'previewImageInput',
                    'previewImageCropInput',
                    'previewImageCropPriorityInput',
                    'detailImageInput',
                    'detailImageCropInput',
                    'headlineImageInput',
                ],
                'file'
            ],
            [
                [
                    'created_at',
                    'updated_at',
                    'detail_image_id',
                    'detail_image_crop_id',
                    'preview_image_id',
                    'preview_image_crop_priority_id',
                    'preview_image_crop_id',
                    'headline_image_id',
                    'duration_read',
                    'views_count'
                ],
                'integer'
            ],
            [['preview_text', 'detail_text'], 'string'],
            [['name', 'detail_image_option_author'], 'string', 'max' => 255],
            ['is_published', 'boolean'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'priority' => 'Приоритет',
            'name' => 'Название статьи',
            'short_name' => 'Короткое название',
            'preview_text' => 'Текст анонса',
            'detailImageInput' => 'Детальная картинка',
            'previewImageInput' => 'Картинка анонса',
            'detail_text' => 'Детальный текст',
            'detail_image_id' => 'Detail Image ID',
            'preview_image_id' => 'Preview Image ID',
            'mainCategoryId' => 'Основная категория',
            'authorsIds' => 'Авторы',
            'slug' => 'Символьный код',
            'publishDateFormated' => 'Дата публикации',
            'tagIds' => 'Теги',
            'authors' => 'Авторы',
            'journal_id' => 'Номер журнала',
            'materialCategories' => 'Категория',
            'duration_read' => 'Время прочтения',
            'views_count' => 'Кол-во просмотров',
            'is_published' => 'Материал опубликован',
            'status' => 'Статус',
        ];
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getAuthors()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('material_user', ['material_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMaterialCategoriesVia()
    {
        return $this->hasMany(MaterialCategoriesMaterial::className(), ['material_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMaterialCategories()
    {
        return $this->hasMany(
            MaterialCategories::className(),
            ['id' => 'material_categories_id']
        )->via('materialCategoriesVia');
    }

    /* public function getTagsIds(){
         return $this->getTags();
     }*/

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function getDatePublished()
    {
        $today = strtotime(date("Ymd"));
        if ($this->publish_date < $today && $this->publish_date >= $today - 60 * 60 * 24) {
            return 'Вчера, ' . Yii::$app->formatter->asTime($this->publish_date, 'short');
        }

        if ($this->publish_date >= $today && $this->publish_date < $today + 60 * 60 * 24) {
            return 'Сегодня, ' . Yii::$app->formatter->asTime($this->publish_date, 'short');
        }

        return Yii::$app->formatter->asDate($this->publish_date, 'short');
    }

    public function getTags()
    {
        return $this->hasMany(
            MaterialTags::className(),
            ['id' => 'material_tags_id']
        )->viaTable(
            'material_tags_material',
            ['material_id' => 'id']
        );
    }

    public function getThemes()
    {
        return $this->hasMany(
            Theme::className(),
            ['id' => 'theme_id']
        )->viaTable(
            'theme_material',
            ['material_id' => 'id']
        );
    }

    /**
     * @return ActiveQuery
     */
    public function getJournal()
    {
        return $this->hasOne(Journal::className(), ['id' => 'journal_id']);
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getMainCategory()
    {
        return $this->hasOne(MaterialCategories::className(), ['id' => 'material_categories_id'])
            ->from(['main_category' => MaterialCategories::tableName()])
            ->viaTable('material_categories_material', ['material_id' => 'id'], function ($query) {
                /**@var ActiveQuery $query */
                $query->from(['main_category_material_categories' => MaterialCategoriesMaterial::tableName()]);
                $query->andWhere([
                    'main_category_material_categories.is_main' => true,
                ]);

                return $query;
            });
    }

    /**
     * @param int $news_id
     *
     * @return array|ActiveRecord[]
     */
    public static function getNews($news_id = 7)
    {
        return self::find()
            ->joinWith('materialCategoriesVia')
            ->where(['material_categories_material.material_categories_id' => $news_id])
            ->all();
    }

    /**
     * Возвращает список популярных тегов
     *
     * @param int $news_id
     *
     * @return array|ActiveRecord[]
     */
    public static function getPopularTags($count = 5)
    {
        $rows = (new Query())
            ->select(['material_tags_id as tag_id', 'COUNT(material_tags_id) as count', 'material_tags.name'])
            ->from('material_tags_material')
            ->leftJoin('material_tags', 'material_tags.id = material_tags_material.material_tags_id')
            ->groupBy('material_tags_id')
            ->limit($count)
            ->all();

        return $rows;
    }

    /**
     * Возвращает последние новости
     *
     * @param int $count
     *
     * @return array|ActiveRecord[]
     */
    public static function getLatestNews($count = 5)
    {
        return self::find()
            ->orderBy('publish_date DESC')
            ->where('is_published = true')
            ->andWhere(['<', 'publish_date', strtotime(date("Ymd")) + 60 * 60 * 24])
            ->limit($count)
            ->all();
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function getDateFormat()
    {
        $today = strtotime(date("Ymd"));
        $months = [
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь'
        ];

        if ($this->publish_date < $today && $this->publish_date >= $today - 60 * 60 * 24) {
            return 'Вчера, ' . Yii::$app->formatter->asTime($this->publish_date, 'short');
        }

        if ($this->publish_date >= $today && $this->publish_date < $today + 60 * 60 * 24) {
            return 'Сегодня, ' . Yii::$app->formatter->asTime($this->publish_date, 'short');
        }

        return $months[Yii::$app->formatter->asDate($this->publish_date, 'M')] . ' ' . Yii::$app->formatter->asDate($this->publish_date, 'Y');
    }

    /**
     * @return string
     */
    public function getDurationRead()
    {
        return $this->duration_read ? $this->duration_read . ' мин' : ($this->duration_read === 0 ? 'Меньше минуты' : '');
    }

    public static function viewIncrement($id)
    {
        if ($post = self::findOne($id)) {
            /**@var HitableBehavior $hitBehavior */
            $hitBehavior = $post->getBehavior('hit');
            $hitBehavior->touch();
        }
    }

    /**
     * @return ImageStyled
     */
    public function getPreviewImagePriorityStyle()
    {
        if ($this->previewImageCropPriority) {
            return new ImageStyled(
                $this->previewImageCropPriority->src,
                'top'
            );
        } else {
            return new ImageStyled(
                $this->previewImage->src
            );
        }
    }

    /**
     * @return ImageStyled
     */
    public function getHeadlineImageStyled()
    {
        if ($this->headlineImage) {
            return new ImageStyled(
                $this->headlineImage->src
            );
        }
        return new ImageStyled(
            $this->detailImage->src
        );
    }

    /**
     * @return ImageStyled
     */
    public function getPreviewImageStyled()
    {
        if ($this->previewImageCrop) {
            return new ImageStyled(
                $this->previewImageCrop->src,
                'center'
            );
        }
        return new ImageStyled(
            $this->previewImage->src
        );
    }

    /**
     * @return ImageStyled
     */
    public function getDetailImageStyled()
    {
        if ($this->detailImage) {
            return new ImageStyled(
                $this->detailImage->src,
                $this->detail_image_option_position,
                $this->detail_image_option_full_size
            );
        }
        return new ImageStyled(
            $this->detailImage->src
        );
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $spl = MaterialCategories::getSlugPathsList();
        return Yii::$app->urlManagerFrontend->createUrl($spl[$this->mainCategory->id]['slug'] . '/' . $this->id);
    }

    public function getStatus()
    {
        if ($this->is_published) {
            if ($this->publish_date <= time()) {
                return 'Опубликована';
            } else {
                return 'Ожидает публикации';
            }
        } else {
            return 'Сохранена';
        }
    }
}
