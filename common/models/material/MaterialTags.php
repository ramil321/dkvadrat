<?php

namespace common\models\material;

use backend\behaviors\HitBehavior;
use backend\behaviors\Slug;
use usualdesigner\yii2\behavior\HitableBehavior;
use Yii;
use yii\db\Query;
use yii\helpers\Url;

/**
 * This is the model class for table "material_tags".
 *
 * Class MaterialTags.
 * @property int $id
 * @property string $name
 * @property int $view_count
 * @property string $slug
 *
 * Rest Description: Your endpoint description.
 * Rest Fields: ['id', 'name'].
 * Rest Filters: ['id', 'name'].
 * Rest Expand: ['expandRelation1', 'expandRelation2'].
 *
 *
 */
class MaterialTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_tags';
    }

    public function behaviors()
    {
        return [
            'hit' => [
                'class' => HitBehavior::className(),
                'attribute' => 'view_count',
                'attributeTmp' => 'view_count_tmp',
                'group' => false,               //group name of the model (class name by default)
                'delay' => 60 * 60,             //register the same visitor every hour
                'table_name' => '{{%material_tags_hits}}'     //table with hits data
            ],
            [
                'class' => Slug::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'transliterator' => 'Russian-Latin/BGN; NFKD',
                //Set this to true, if you want to update a slug when source attribute has been changed
                'forceUpdate' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'unique'],
            [['name'], 'string','min' => 2, 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'view_count' => 'Количество просмотров',
            'view_count_tmp' => 'Количество просмотров временное',
        ];
    }

    /**
     * @param array $ids
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements(array $ids = null)
    {
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }

    public function getUrl(){
        return Url::to(['tags/index','slug' => $this->slug]);
    }

    public static function viewIncrement($id)
    {
        if($post = self::findOne($id)){
            /**@var HitableBehavior $hitBehavior */
            $hitBehavior = $post->getBehavior('hit');
            $hitBehavior->touch();
        }
   }


    private $materialsCount;

    public function setMaterialsCount($value)
    {
        $this->materialsCount = $value;
    }

    public function fields()
    {
        return array_merge(parent::fields(), ['materialsCount']);
    }

    public function getMaterialsCount()
    {
        if ($this->materialsCount === null) {

            $this->materialsCount = (new Query())
                ->from('material_tags_material')
                ->where(['material_tags_id' => $this->id])
                ->count();
        }
        return $this->materialsCount;
    }

}
