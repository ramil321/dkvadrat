<?php

namespace common\models\material;

use Yii;

/**
 * This is the model class for table "material_categories_material_tags".
 *
 * @property int $id
 * @property int $material_categories_id
 * @property int $material_tags_id
 *
 * @property MaterialCategories $materialCategories
 * @property MaterialCategories $materialCategories0
 * @property MaterialTags $materialTags
 */
class MaterialCategoriesMaterialTags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_categories_material_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_categories_id', 'material_tags_id'], 'integer'],
            [['material_categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialCategories::className(), 'targetAttribute' => ['material_categories_id' => 'id']],
            [['material_categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialCategories::className(), 'targetAttribute' => ['material_categories_id' => 'id']],
            [['material_tags_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialTags::className(), 'targetAttribute' => ['material_tags_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_categories_id' => 'Material Categories ID',
            'material_tags_id' => 'Material Tags ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialCategories()
    {
        return $this->hasOne(MaterialCategories::className(), ['id' => 'material_categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialTags()
    {
        return $this->hasOne(MaterialTags::className(), ['id' => 'material_tags_id']);
    }
}
