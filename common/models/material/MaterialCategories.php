<?php

namespace common\models\material;

use arogachev\tree\behaviors\NestedSetsManagementBehavior;
use backend\behaviors\Slug;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\base\ErrorException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use arogachev\tree\helpers\ArrayHelper as ArrayHelperDev;
use yii\helpers\Url;

/**
 * This is the model class for table "material_categories".
 *
 * @property int                          $id
 * @property int                          $root
 * @property int                          $lft
 * @property int                          $rgt
 * @property int                          $lvl
 * @property string                       $name
 * @property int                          $active
 * @property int                          $selected
 * @property int                          $disabled
 * @property int                          $readonly
 * @property int                          $visible
 * @property int                          $collapsed
 * @property int                          $movable_u
 * @property int                          $movable_d
 * @property int                          $movable_l
 * @property int                          $movable_r
 * @property int                          $removable
 * @property int                          $removable_all
 * @property int                          $created_at
 * @property int                          $updated_at
 * @property string                       $seo_title
 * @property string                       $seo_description
 * @property string                       $seo_keywords
 * @property string                       $slug
 * @property string                       $flag
 * @property string                       $type_code
 * @property MaterialCategoriesMaterial[] $materialCategoriesMaterials
 */
class MaterialCategories extends \kartik\tree\models\Tree
{
    //public $is_main;
    private static $slugPathListCache;

    private static $saveState = false;

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => Slug::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'slug',
                    'transliterator' => 'Russian-Latin/BGN; NFKD',
                    //Set this to true, if you want to update a slug when source attribute has been changed
                    'forceUpdate' => true
                ],
                [
                    'class' => NestedSetsManagementBehavior::className()
                ],
                'many_to_many' => [
                    'class' => ManyToManyBehavior::className(),
                    'relations' => ['tagIds' => 'tags']
                ],
                'timestamp' => [
                    'class' => TimestampBehavior::className(),
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_categories';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialCategories()
    {
        return $this->hasOne(MaterialCategoriesMaterial::className(), ['id' => 'material_categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialCategoriesMaterial()
    {
        return $this->hasOne(MaterialCategoriesMaterial::className(), ['material_categories_id' => 'id']);
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_BEFORE_INSERT, function () {
            if ($this->lvl >= 3) {
                throw new ErrorException('Максимальный уровень раздела 2');
            }
        });
        $this->on(self::EVENT_BEFORE_UPDATE, function () {
            //   pre($this->tagIds);
        });
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'seo_title' => 'Seo заголовок',
                'seo_keywords' => 'Seo ключевые слова',
                'tags_page_active' => 'Активность на странице тегов',
                'seo_description' => 'Seo описание',
                'slug' => 'Символьный код',
                'tagIds' => 'Привязка раздела к тегам',
                'flag' => 'Метка категории',
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['seo_title', 'seo_keywords', 'seo_description', 'type_code'], 'string'],
                ['tags_page_active', 'boolean'],
                ['tagIds', 'safe']
            ]
        );
    }

    /**
     * Convert a tree into nested arrays. If you use the default function parameters you get
     * a set compatible with Yii2 Menu widget.
     *
     * @param int           $depth
     * @param string        $itemsKey
     * @param callable|null $getDataCallback
     * @return array
     */
    public function toNestedArray($depth = null, $itemsKey = 'items', $getDataCallback = null)
    {
        /** @var MaterialCategories $nodes */
        $nodes = $this->children($depth)->all();

        $exportedAttributes = array_diff(array_keys($this->attributes), ['lft', 'rgt']);

        $trees = [];
        $stack = [];

        foreach ($nodes as $node) {
            if ($getDataCallback) {
                $item = call_user_func($getDataCallback, $node);
            } else {
                $item = $node->toArray($exportedAttributes);
            }
            $item['lvl'] = $node->lvl;
            $item[$itemsKey] = [];
            $l = count($stack);

            while ($l > 0 && $stack[$l - 1]['lvl'] >= $node->lvl) {
                array_pop($stack);
                $l--;
            }

            if ($l == 0) {
                // Assign root node
                $i = count($trees);
                $trees[$i] = $item;
                $stack[] = &$trees[$i];
            } else {
                // Add node to parent
                $i = count($stack[$l - 1][$itemsKey]);
                $stack[$l - 1][$itemsKey][$i] = $item;
                $stack[] = &$stack[$l - 1][$itemsKey][$i];
            }
        }

        return $trees;
    }

    public static function getFullTreeArrayForSelect()
    {
        $result = [];
        $treeArray = self::getFullTreeArray();
        foreach ($treeArray as $item) {
            $result[$item['id']] = $item['name'];
            foreach ($item['children'] as $child) {
                $result[$child['id']] = '--' . $child['name'];
                foreach ($child['children'] as $childChild) {
                    $result[$childChild['id']] = '----' . $childChild['name'];
                }
            }
        }

        return $result;
    }

    /**
     * Получение древа категорий
     * @return array
     * @param callable|null $getDataCallback
     */
    public static function getFullTreeArray($getDataCallback = null)
    {
        $rVal = [];

        /** @var MaterialCategories[] $roots */
        $roots = static::find()->roots()->all();
        foreach ($roots as $key => $root) {
            if ($getDataCallback) {
                $tmp = call_user_func($getDataCallback, $root);
                $tmp['children'] = $root->toNestedArray(2, 'children', $getDataCallback);
                $rVal[] = $tmp;
            } else {
                $rVal[] = [
                    'id' => $root->id,
                    'name' => $root->name,
                    'children' => $root->toNestedArray(2, 'children', function ($node) {
                        return [
                            'id' => $node->id,
                            'name' => $node->name,
                            'type_code' => $node->type_code,
                            'node' => $node
                        ];
                    }),
                ];
            }
        }

        return $rVal;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsVia()
    {
        return $this->hasMany(MaterialCategoriesMaterialTags::className(), ['material_categories_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(
            MaterialTags::className(),
            ['id' => 'material_tags_id']
        )->viaTable(
            'material_categories_material_tags',
            ['material_categories_id' => 'id']
        );
    }

    public static function getPathlist()
    {
        $categoryList = self::getFullTreeArray(function ($data) {
            return ['slug' => $data->slug];
        });

        $pathList = [];
        foreach ($categoryList as $category) {
            $pathList[$category['slug']] = ['slug1' => $category['slug']];
            if(isset($category['children'])) {
                foreach ($category['children'] as $child) {
                    $pathList[$child['slug']] = ['slug2' => $child['slug'], 'slug1' => $category['slug']];
                }
            }
        }

        return $pathList;
    }

    public function getUrl()
    {
        $spl = MaterialCategories::getSlugPathsList();
        return Url::to([$spl[$this->id]['slug'].'/']);
    }

    /**
     * {@inheritdoc}
     * @return ActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return parent::find();
    }

    /**
     * @return array|null
     */
    public function getType()
    {
        if ($type = \Yii::$app->params['materialCategoriesTypes'][$this->type_code]) {
            $type['type_code'] = $this->type_code;
            return $type;
        }
        return null;
    }

    /**
     * @return array|null
     */
    public static function getAllTypes()
    {
        return \Yii::$app->params['materialCategoriesTypes'];
    }

    public function getFlag()
    {
        return $this->type_code;
    }

    /**
     * @return array
     */
    public static function getSlugPathsList()
    {
        if(self::$slugPathListCache){
            return self::$slugPathListCache;
        }
        $categories = MaterialCategories::getFullTreeArray( function(MaterialCategories $node) {
            return [
                'id' => $node->id,
                'slug' => $node->slug,
                'node' => $node
            ];
        });
        $pathLists = [];
        foreach($categories as $category) {
            $pathLists[$category['id']]['slug'] = $category['slug'];
            $pathLists[$category['id']]['node'] = $category['node'];
            foreach ($category['children'] as $child) {
                $pathLists[$child['id']]['slug'] = $category['slug'].'/'.$child['slug'];
                $pathLists[$child['id']]['node'] = $child['node'];
                foreach ($child['children'] as $child2) {
                    $pathLists[$child2['id']]['slug'] = $category['slug'].'/'.$child['slug'].'/'.$child2['slug'];
                    $pathLists[$child2['id']]['node'] = $child2['node'];
                }
            }
        }
        self::$slugPathListCache  = $pathLists;
        return self::$slugPathListCache;
    }

}
