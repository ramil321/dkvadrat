<?php
namespace common\models\material;




use common\models\material\search\MaterialSphinxIndex;
use common\models\theme\Theme;
use yii\helpers\ArrayHelper;
/**
 * class or trait for the {@mixin} annotation
 * @see Material
 */
trait MaterialEventTrait
{
    /*
     * Добавление новых тегов которых нету в базе
     * */
    public function addEmptyTags()
    {
        $materialsTags = MaterialTags::find()->where(['id' => $this->tagIds])->asArray()->all();
        $materialsTags = ArrayHelper::map($materialsTags,'id','id');
        if($this->tagIds) {
            foreach ($this->tagIds as $val) {
                if (!in_array( $val, $materialsTags )) {
                    if (is_string( $val )) {
                        $findTag = MaterialTags::find()->where( ['like', 'name', trim( $val ), false] )->one();
                        if (!$findTag) {
                            $tag = new MaterialTags();
                            $tag->name = trim( $val );
                            $tag->save();
                            if ($tag->id) {
                                $materialsTags[] = $tag->id;
                            }
                        } else {
                            $materialsTags[] = $findTag->id;
                        }
                    }
                }
            }
        }
        $this->tagIds = array_values($materialsTags);
    }

    /*
    * Добавление тем которых нету в базе
    * */
    public function addEmptyTheme()
    {
        if($this->themeId) {
            $themes = Theme::find()->where(['id' => $this->themeId])->select('id')->asArray()->all();
            $themes = ArrayHelper::map($themes,'id','id');
            $val = $this->themeId;
            if (!in_array( $val, $themes )) {
                if (is_string( $val )) {
                    $findTheme = Theme::find()->where( ['like', 'name', trim( $val ), false] )->one();
                    if (!$findTheme) {
                        $theme = new Theme();
                        $theme->name = trim( $val );
                        $theme->save();
                        if ($theme->id) {
                            $themes[] = $theme->id;
                        }
                    } else {
                        $themes[] = $findTheme->id;
                    }
                }else{
                    $themes[] = $this->themeId;
                }
            }
            $this->themeId = $themes;
        }
    }

    /*
     * Привязка категорий из тегов
     * */
    public function findTagsByCategories()
    {
        $this->tmpMainCategoryId = $this->mainCategoryId;
        if($this->mainCategoryId){
            $newVal = [$this->mainCategoryId];
            $tagIds = $this->tagIds;
            $tagIds[] = $this->mainCategoryId;
            if($tagIds){
                $items = MaterialCategoriesMaterialTags::find()
                    ->joinWith('materialCategories')
                    ->where(['material_tags_id' => $tagIds])->all();
                foreach($items as $item){
                    /**@var $item MaterialCategoriesMaterialTags*/
                 //   $item->materialCategories->parents()
                    $newVal[] = $item->material_categories_id;
                }
            }
            /**@var  MaterialCategories $mainCategory*/
            $mainCategory = MaterialCategories::find()->where(['id' => $this->mainCategoryId])->one();
            foreach ($mainCategory->parents()->all() as $parent){
                $newVal[] = $parent->id;
            }
            $this->mainCategoryId =  array_unique($newVal);
        }
    }

    /*
     * Добавление тегов привязанных к разделу
     * */
    public function addTmpViewTags()
    {
        $categoriesIds = [];
        foreach ($this->materialCategories as $category){
            $categoriesIds[] = $category->id;
        }
        foreach ($this->tagIds as $tagId){
            foreach ($categoriesIds as $categoryId){
                $tmpTag = new MaterialTagsMaterialCategoriesTmpViews();
                $tmpTag->material_categories_id = $categoryId;
                $tmpTag->material_tags_id = $tagId;
                $tmpTag->save();
            }
        }
    }

    public function sphinxAddIndex(){
        $tags = implode(" ", ArrayHelper::map($this->tags, 'id', 'name'));
        $index = new MaterialSphinxIndex();
        $index->id = $this->id;
        $index->body = implode(" ", [$this->name, $this->preview_text, $this->detail_text, $tags]);
        $index->save();
   }

    public function sphinxUpdateIndex(){
        $index = MaterialSphinxIndex::find()->where(['id' => $this->id])->one();
        $tags = implode(" ", ArrayHelper::map($this->tags, 'id', 'name'));
        if($index){
            $index->body = implode(" ", [$this->name, $this->preview_text, $this->detail_text, $tags]);
//            $index->body = "";
            $index->save();
        }else{
            $index = new MaterialSphinxIndex();
            $index->id = $this->id;
            $index->body = implode(" ", [$this->name, $this->preview_text, $this->detail_text, $tags]);
            $index->save();
        }
    }
    public function sphinxDeleteIndex(){
        $index = MaterialSphinxIndex::find()->where(['id' => $this->id])->one();
        if($index){
            $index->delete();
        }
    }
    public function durationRead(){
        $per_minutes = 250;
        $this->duration_read = round(count(preg_split('/\s/', $this->detail_text)) /  $per_minutes);
    }
}
