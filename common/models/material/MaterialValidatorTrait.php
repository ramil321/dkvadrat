<?php
namespace common\models\material;



use common\models\user\User;
use yii\helpers\ArrayHelper;

trait MaterialValidatorTrait {
    /*
     * Валидация автора
     * */
    public function validateAuthors($attribute, $params, $validator)
    {
        $authors = ArrayHelper::map(User::getAuthors()->all(),'id','id');
        foreach ($this->$attribute as $item){
            if (!in_array($item, $authors)) {
                $this->addError($attribute, 'Автора не существует');
            }
        }
    }
}
