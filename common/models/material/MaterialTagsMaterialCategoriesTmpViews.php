<?php

namespace common\models\material;

use Yii;

/**
 * This is the model class for table "material_tags_material_categories_tmp_views".
 *
 * @property int $id
 * @property int $material_categories_id
 * @property int $material_tags_id
 *
 * @property MaterialCategories $materialCategories
 * @property MaterialTags $materialTags
 */
class MaterialTagsMaterialCategoriesTmpViews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_tags_material_categories_tmp_views';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_categories_id', 'material_tags_id'], 'required'],
            [['material_categories_id', 'material_tags_id'], 'integer'],
            [['material_categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialCategories::className(), 'targetAttribute' => ['material_categories_id' => 'id']],
            [['material_tags_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialTags::className(), 'targetAttribute' => ['material_tags_id' => 'id']],
            ['material_tags_id', 'unique', 'targetAttribute' => ['material_tags_id', 'material_categories_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_categories_id' => 'Material Categories ID',
            'material_tags_id' => 'Material Tags ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialCategories()
    {
        return $this->hasOne(MaterialCategories::className(), ['id' => 'material_categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialTags()
    {
        return $this->hasOne(MaterialTags::className(), ['id' => 'material_tags_id']);
    }

    public static function findTagsForCategoryId($categoryId){
        /**@var self $tmpTags*/
        $tmpTags = self::find()
            ->joinWith('materialTags')
            ->where(['material_categories_id' => $categoryId])
            ->orderBy(['view_count_tmp' => SORT_DESC])
            ->limit(5)
            ->all();
        $tags = [];
        foreach($tmpTags as $tmpTag){
            $tags[] = $tmpTag->materialTags;
        }
        return $tags;
    }
}
