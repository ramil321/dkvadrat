<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m190110_133644_create_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'editorial_address' => $this->text(),
            'phone_editors' => $this->text(),
            'phone_advertising' => $this->text(),
            'email' => $this->text(),
            'advertisers_href' => $this->text(),
            'lat' => $this->text(),
            'lng' => $this->text(),
            'map_title' => $this->text(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contacts');
    }
}
