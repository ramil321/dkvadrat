<?php

use yii\db\Migration;

/**
 * Handles the creation of table `special_projects`.
 */
class m190220_184053_create_special_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('special_projects', [
            'id' => $this->primaryKey(),
            'template' => $this->text()->notNull(),
            'settings' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('special_projects');
    }
}
