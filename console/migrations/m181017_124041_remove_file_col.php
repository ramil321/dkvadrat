<?php

use yii\db\Migration;

/**
 * Class m181017_124041_remove_file_col
 */
class m181017_124041_remove_file_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('files','src_crop');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181017_124041_remove_file_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181017_124041_remove_file_col cannot be reverted.\n";

        return false;
    }
    */
}
