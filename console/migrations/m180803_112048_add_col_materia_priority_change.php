<?php

use yii\db\Migration;

/**
 * Class m180803_112048_add_col_materia_priority_change
 */
class m180803_112048_add_col_materia_priority_change extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('material','priority',$this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180803_112048_add_col_materia_priority_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_112048_add_col_materia_priority_change cannot be reverted.\n";

        return false;
    }
    */
}
