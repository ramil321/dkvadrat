<?php

use yii\db\Migration;

/**
 * Class m180920_092544_add_col_sub
 */
class m180920_092544_add_col_sub extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters_list','subscribe_body',$this->integer());
        $this->addColumn('newsletters_list','unsubscribe_body',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180920_092544_add_col_sub cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180920_092544_add_col_sub cannot be reverted.\n";

        return false;
    }
    */
}
