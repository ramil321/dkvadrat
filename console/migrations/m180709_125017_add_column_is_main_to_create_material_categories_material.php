<?php

use yii\db\Migration;

/**
 * Class m180709_125017_add_column_is_main_to_create_material_categories_material
 */
class m180709_125017_add_column_is_main_to_create_material_categories_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material_categories_material','is_main',$this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180709_125017_add_column_is_main_to_create_material_categories_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180709_125017_add_column_is_main_to_create_material_categories_material cannot be reverted.\n";

        return false;
    }
    */
}
