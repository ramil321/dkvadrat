<?php

use yii\db\Migration;

/**
 * Class m180808_080822_delete_fk_theme_material
 */
class m180808_080822_delete_fk_theme_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        //$this->dropForeignKey('fk-theme_materials-theme_id','theme_materials');
        //$this->dropForeignKey('fk-theme_materials-material_id','theme_materials');

        $this->dropForeignKey('fk-theme_materials-theme_id','theme_material');
        $this->dropForeignKey('fk-theme_materials-material_id','theme_material');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180808_080822_delete_fk_theme_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180808_080822_delete_fk_theme_material cannot be reverted.\n";

        return false;
    }
    */
}
