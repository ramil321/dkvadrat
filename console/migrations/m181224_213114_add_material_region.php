<?php

use yii\db\Migration;

/**
 * Class m181224_213114_add_material_region
 */
class m181224_213114_add_material_region extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('material_region', ['name' => 'Россия']);
        $this->insert('material_region', ['name' => 'Мир']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181224_213114_add_material_region cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181224_213114_add_material_region cannot be reverted.\n";

        return false;
    }
    */
}
