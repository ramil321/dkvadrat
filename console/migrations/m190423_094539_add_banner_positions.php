<?php

use common\models\banner\BannerPosition;
use yii\db\Migration;

/**
 * Class m190423_094539_add_banner_positions
 */
class m190423_094539_add_banner_positions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->alterColumn('banner_position','template', $this->text());

        $bannerPositions = [
            [
                'name' => "На детальной маленький",
                'code' => "detail_material_small",
                'template' => '<div class="article-page__banner article-page__banner--small">
    <a href="<?=$model->link?>" <?=($model->blank ? \'target="_blank"\' : \'\')?>
       style="background-image: url(<?=Yii::$app->resize->prop($model->getImage()->src, 200, 400)?>)"
       class="banner banner--square-detail"></a>
    <span class="article-page__banner-span"><?= $model->name?></span>
</div>
'
            ],
            [
                'name' => "На детальной средний",
                'code' => "detail_material_medium",
                'template' => '<div class="article-page__banner article-page__banner--medium">
    <a
        href="<?=$model->link?>" <?=($model->blank ? \'target="_blank"\' : \'\')?>
        style="background-image: url(<?=Yii::$app->resize->prop($model->getImage()->src, 200, 600)?>)"
        class="banner banner--rectangle-detail"></a>
    <span class="article-page__banner-span"><?=$model->name?></span>
</div>
'
            ],
            [
                'name' => "Сквозной баннер в шапке",
                'code' => "top_line_all_pages",
                'template' => '
<a href="<?=$model->link?>" <?=($model->blank ? \'target="_blank"\' : \'\')?>
   style="background-image: url(<?=Yii::$app->resize->prop($model->getImage()->src, 1920, 2000)?>)"
   class="banner banner--full-width">
</a>
                '
            ],

        ];
        foreach ($bannerPositions as $bp){
            \Yii::$app->db->createCommand()->insert('banner_position', [
                'template' => $bp['template'],
                'name' => $bp['name'],
                'code' => $bp['code'],
            ])->execute();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190423_094539_add_banner_positions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190423_094539_add_banner_positions cannot be reverted.\n";

        return false;
    }
    */
}
