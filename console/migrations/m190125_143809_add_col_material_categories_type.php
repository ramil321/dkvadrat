<?php

use yii\db\Migration;

/**
 * Class m190125_143809_add_col_material_categories_type
 */
class m190125_143809_add_col_material_categories_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material_categories', 'type_code', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190125_143809_add_col_material_categories_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190125_143809_add_col_material_categories_type cannot be reverted.\n";

        return false;
    }
    */
}
