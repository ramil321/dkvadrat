<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%relation_banners}}`.
 */
class m190423_135223_create_relation_banners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_tags_banner', [
            'id' => $this->primaryKey(),
            'material_tags_id' => $this->integer(),
            'banner_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-material_tags_banner-mt',
            'material_tags_banner',
            'material_tags_id',
            'material_tags',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-material_tags_banner-banner_id',
            'material_tags_banner',
            'banner_id',
            'banner',
            'id',
            'CASCADE'
        );


        $this->createTable('material_categories_banner', [
            'id' => $this->primaryKey(),
            'material_categories_id' => $this->integer(),
            'banner_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-material_categories_banner-mt',
            'material_categories_banner',
            'material_categories_id',
            'material_categories',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-material_categories_banner-banner_id',
            'material_categories_banner',
            'banner_id',
            'banner',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
