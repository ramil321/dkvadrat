<?php

use yii\db\Migration;

/**
 * Class m180803_111630_add_col_materia_priority
 */
class m180803_111630_add_col_materia_priority extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','priority',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180803_111630_add_col_materia_priority cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180803_111630_add_col_materia_priority cannot be reverted.\n";

        return false;
    }
    */
}
