<?php

use yii\db\Migration;

/**
 * Class m180924_142902_add_material_cols
 */
class m180924_142902_add_material_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','show_content',$this->boolean());
        $this->addColumn('material','show_author',$this->boolean());
        $this->addColumn('material','show_comments',$this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180924_142902_add_material_cols cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180924_142902_add_material_cols cannot be reverted.\n";

        return false;
    }
    */
}
