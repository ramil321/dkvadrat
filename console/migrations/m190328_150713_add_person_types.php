<?php

use yii\db\Migration;

/**
 * Class m190328_150713_add_person_types
 */
class m190328_150713_add_person_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('person_categories', 'show_in_contacts', $this->boolean()->defaultValue(1));
        $this->addColumn('person_categories', 'show_in_politics', $this->boolean()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190328_150713_add_person_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190328_150713_add_person_types cannot be reverted.\n";

        return false;
    }
    */
}
