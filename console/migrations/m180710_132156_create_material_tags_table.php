<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_tags`.
 */
class m180710_132156_create_material_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_tags', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'view_count' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('material_tags');
    }
}
