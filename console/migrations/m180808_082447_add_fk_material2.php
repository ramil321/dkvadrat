<?php

use yii\db\Migration;

/**
 * Class m180808_082447_add_fk_material2
 */
class m180808_082447_add_fk_material2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-theme_material-two',
            'theme_material',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180808_082447_add_fk_material2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180808_082447_add_fk_material2 cannot be reverted.\n";

        return false;
    }
    */
}
