<?php

use yii\db\Migration;

/**
 * Class m190213_205506_add_headline_image_to_material
 */
class m190213_205506_add_headline_image_to_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','headline_image_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('material','headline_image_id');

        return false;
    }
}
