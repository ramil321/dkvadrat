<?php

use yii\db\Migration;

/**
 * Class m181017_091608_add_col_files
 */
class m181017_091608_add_col_files extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('files','src_crop',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181017_091608_add_col_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181017_091608_add_col_files cannot be reverted.\n";

        return false;
    }
    */
}
