<?php

use yii\db\Migration;

/**
 * Class m190328_101608_add_banner_position_col
 */
class m190328_101608_add_banner_position_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('banner_position','template',$this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190328_101608_add_banner_position_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190328_101608_add_banner_position_col cannot be reverted.\n";

        return false;
    }
    */
}
