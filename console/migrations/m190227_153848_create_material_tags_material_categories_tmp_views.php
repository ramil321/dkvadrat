<?php

use yii\db\Migration;

/**
 * Class m190227_153848_create_material_tags_material_categories_tmp_views
 */
class m190227_153848_create_material_tags_material_categories_tmp_views extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_tags_material_categories_tmp_views',[
            'id' => $this->primaryKey(),
            'material_categories_id' => $this->integer()->notNull(),
            'material_tags_id' => $this->integer()->notNull()
        ]);


        $this->addForeignKey(
            'fk-tmtmctmp-material_categories_id-mt',
            'material_tags_material_categories_tmp_views',
            'material_categories_id',
            'material_categories',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-tmtmctmp-material_tags_id-material_id',
            'material_tags_material_categories_tmp_views',
            'material_tags_id',
            'material_tags',
            'id',
            'CASCADE'
        );

        $this->addColumn('material_tags', 'views_count_tmp', $this->integer()->defaultValue(0));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190227_153848_create_tags_material_tags_material_categories_tmp_views cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190227_153848_create_tags_material_tags_materail_categories_tmp_views cannot be reverted.\n";

        return false;
    }
    */
}
