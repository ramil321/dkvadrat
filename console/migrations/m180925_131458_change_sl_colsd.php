<?php

use yii\db\Migration;

/**
 * Class m180925_131458_change_sl_colsd
 */
class m180925_131458_change_sl_colsd extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters','message_theme',$this->string());
        $this->addColumn('newsletters','message_from',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180925_131458_change_sl_colsd cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180925_131458_change_sl_colsd cannot be reverted.\n";

        return false;
    }
    */
}
