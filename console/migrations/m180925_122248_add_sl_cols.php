<?php

use yii\db\Migration;

/**
 * Class m180925_122248_add_sl_cols
 */
class m180925_122248_add_sl_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters_list','message_theme',$this->string());
        $this->addColumn('newsletters_list','message_from',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180925_122248_add_sl_cols cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180925_122248_add_sl_cols cannot be reverted.\n";

        return false;
    }
    */
}
