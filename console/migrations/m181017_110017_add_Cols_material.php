<?php

use yii\db\Migration;

/**
 * Class m181017_110017_add_Cols_material
 */
class m181017_110017_add_Cols_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','detail_image_crop_id', $this->integer());
        $this->addColumn('material','preview_image_crop_id', $this->integer());
        $this->addColumn('material','preview_image_crop_priority_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181017_110017_add_Cols_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181017_110017_add_Cols_material cannot be reverted.\n";

        return false;
    }
    */
}
