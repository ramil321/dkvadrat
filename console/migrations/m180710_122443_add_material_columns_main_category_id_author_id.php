<?php

use yii\db\Migration;

/**
 * Class m180710_122443_add_material_columns_main_category_id_author_id
 */
class m180710_122443_add_material_columns_main_category_id_author_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','user_id',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180710_122443_add_material_columns_main_category_id_author_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_122443_add_material_columns_main_category_id_author_id cannot be reverted.\n";

        return false;
    }
    */
}
