<?php

use yii\db\Migration;

/**
 * Class m180918_103436_add_Newsletters_list_table
 */
class m180918_103436_add_Newsletters_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('newsletters_list',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180918_103436_add_Newsletters_list_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180918_103436_add_Newsletters_list_table cannot be reverted.\n";

        return false;
    }
    */
}
