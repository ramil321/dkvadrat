<?php

use yii\db\Migration;

/**
 * Class m190325_110707_change_journals_columns
 */
class m190325_110707_change_journals_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('journal','number',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190325_110707_change_journals_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190325_110707_change_journals_columns cannot be reverted.\n";

        return false;
    }
    */
}
