<?php

use yii\db\Migration;

/**
 * Handles the creation of table `newsletters`.
 */
class m180912_082710_create_newsletters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('newsletters', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'text' => $this->text(),
            'publish_date' => $this->integer(),
        ]);

        $this->createTable('newsletters_subscribers', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
        ]);

        $this->createTable('newsletters_material',[
            'material_id' => $this->integer(),
            'newsletters_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-newsletters_material-material_id',
            'newsletters_material',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-newsletters_material-newsletters_id',
            'newsletters_material',
            'newsletters_id',
            'newsletters',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('newsletters');
    }
}
