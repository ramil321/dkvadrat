<?php

use yii\db\Migration;

/**
 * Class m180906_113530_add_cols_banner
 */
class m180906_113530_add_cols_banner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('banner','publish_date', $this->integer());
        $this->addColumn('banner','publish_end_date', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180906_113530_add_cols_banner cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180906_113530_add_cols_banner cannot be reverted.\n";

        return false;
    }
    */
}
