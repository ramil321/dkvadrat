<?php

use yii\db\Migration;

/**
 * Class m190416_103940_add_company_col_code
 */
class m190416_103940_add_company_col_code extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company','code', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190416_103940_add_company_col_code cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190416_103940_add_company_col_code cannot be reverted.\n";

        return false;
    }
    */
}
