<?php

use yii\db\Migration;

/**
 * Class m180711_120306_add_col_publish_date_to_material
 */
class m180711_120306_add_col_publish_date_to_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','publish_date',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180711_120306_add_col_publish_date_to_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180711_120306_add_col_publish_date_to_material cannot be reverted.\n";

        return false;
    }
    */
}
