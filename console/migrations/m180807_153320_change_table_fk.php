<?php

use yii\db\Migration;

/**
 * Class m180807_153320_change_table_fk
 */
class m180807_153320_change_table_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-theme_materials-theme_id','theme_material');
        $this->dropForeignKey('fk-theme_materials-material_id','theme_material');

        $this->addForeignKey(
            'fk-theme_materials-theme_id',
            'theme_material',
            'theme_id',
            'theme',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-theme_materials-material_id',
            'theme_material',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180807_153320_change_table_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180807_153320_change_table_fk cannot be reverted.\n";

        return false;
    }
    */
}
