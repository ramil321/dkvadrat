<?php

use yii\db\Migration;

/**
 * Class m180924_150437_add_image_col
 */
class m180924_150437_add_image_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile','photo_id',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180924_150437_add_image_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180924_150437_add_image_col cannot be reverted.\n";

        return false;
    }
    */
}
