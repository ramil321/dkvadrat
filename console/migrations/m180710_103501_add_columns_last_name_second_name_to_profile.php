<?php

use yii\db\Migration;

/**
 * Class m180710_103501_add_columns_last_name_second_name_to_profile
 */
class m180710_103501_add_columns_last_name_second_name_to_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile','last_name',$this->string());
        $this->addColumn('profile','second_name',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180710_103501_add_columns_last_name_second_name_to_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_103501_add_columns_last_name_second_name_to_profile cannot be reverted.\n";

        return false;
    }
    */
}
