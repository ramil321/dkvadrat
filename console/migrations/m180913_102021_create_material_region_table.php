<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_region`.
 */
class m180913_102021_create_material_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_region', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ]);

        $this->addColumn('material','material_region_id',$this->integer());
        $this->addColumn('material','show_theme_materials',$this->boolean());

        $this->addForeignKey(
            'fk-material-material_region_id',
            'material',
            'material_region_id',
            'material_region',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('material_region');
    }
}
