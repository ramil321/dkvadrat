<?php

use yii\db\Migration;

/**
 * Class m181010_155633_add_consts_col_class
 */
class m181010_155633_add_consts_col_class extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('constructor_templates','class',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181010_155633_add_consts_col_class cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181010_155633_add_consts_col_class cannot be reverted.\n";

        return false;
    }
    */
}
