<?php

use yii\db\Migration;

/**
 * Class m181109_082057_add_menu_table_col
 */
class m181109_082057_add_menu_table_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu','type',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181109_082057_add_menu_table_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181109_082057_add_menu_table_col cannot be reverted.\n";

        return false;
    }
    */
}
