<?php

use yii\db\Migration;

/**
 * Class m181109_091826_add_menu_table_col_active
 */
class m181109_091826_add_menu_table_col_active extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu','active',$this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181109_091826_add_menu_table_col_active cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181109_091826_add_menu_table_col_active cannot be reverted.\n";

        return false;
    }
    */
}
