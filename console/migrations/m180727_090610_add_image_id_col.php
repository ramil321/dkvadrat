<?php

use yii\db\Migration;

/**
 * Class m180727_090610_add_image_id_col
 */
class m180727_090610_add_image_id_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('area','image_id',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180727_090610_add_image_id_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180727_090610_add_image_id_col cannot be reverted.\n";

        return false;
    }
    */
}
