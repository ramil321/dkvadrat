<?php

use yii\db\Migration;

/**
 * Class m190123_150130_constructor_add_settings_field
 */
class m190123_150130_constructor_add_settings_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('constructor', 'settings', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('constructor', 'settings');
    }
}
