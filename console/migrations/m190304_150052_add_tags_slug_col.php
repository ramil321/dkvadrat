<?php

use yii\db\Migration;

/**
 * Class m190304_150052_add_tags_slug_col
 */
class m190304_150052_add_tags_slug_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material_tags', 'slug', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190304_150052_add_tags_slug_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190304_150052_add_tags_slug_col cannot be reverted.\n";

        return false;
    }
    */
}
