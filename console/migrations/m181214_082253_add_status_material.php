<?php

use yii\db\Migration;

/**
 * Class m181214_082253_add_status_material
 */
class m181214_082253_add_status_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->insert('material_status', ['name' => 'Опубликована', 'code' => 'published']);
//        $this->insert('material_status', ['name' => 'Ожидает публикации', 'code' => 'deferred']);
        $this->insert('material_status', ['name' => 'Cохранена', 'code' => 'saved']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181214_082253_add_status_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_082253_add_status_material cannot be reverted.\n";

        return false;
    }
    */
}
