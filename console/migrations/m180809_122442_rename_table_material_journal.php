<?php

use yii\db\Migration;

/**
 * Class m180809_122442_rename_table_material_journal
 */
class m180809_122442_rename_table_material_journal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('material_journal','journal');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180809_122442_rename_table_material_journal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180809_122442_rename_table_material_journal cannot be reverted.\n";

        return false;
    }
    */
}
