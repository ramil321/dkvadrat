<?php

use yii\db\Migration;

/**
 * Class m181205_181835_add_field_category_flag
 */
class m181205_181835_add_field_category_flag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material_categories', 'flag', $this->tinyInteger(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('material_categories', 'flag');
    }
}
