<?php

use yii\db\Migration;

/**
 * Class m180910_122932_add_cols_to_banner
 */
class m180910_122932_add_cols_to_banner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('banner','active',$this->boolean());

        $this->createTable('banner_advertiser',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable('banner_company',[
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'banner_advertiser_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-banner_advertiser-banner_company',
            'banner_company',
            'banner_advertiser_id',
            'banner_advertiser',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180910_122932_add_cols_to_banner cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180910_122932_add_cols_to_banner cannot be reverted.\n";

        return false;
    }
    */
}
