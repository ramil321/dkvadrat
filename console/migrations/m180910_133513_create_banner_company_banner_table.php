<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banner_company_banner`.
 */
class m180910_133513_create_banner_company_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('banner_company_banner', [
            'id' => $this->primaryKey(),
            'banner_id' => $this->integer(),
            'banner_company_id' => $this->integer()
        ]);

        $this->addForeignKey(
            'fk-banner_company_banner-banner_id',
            'banner_company_banner',
            'banner_id',
            'banner',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-banner_company_banner-banner_company_id',
            'banner_company_banner',
            'banner_company_id',
            'banner_company',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('banner_company_banner');
    }
}
