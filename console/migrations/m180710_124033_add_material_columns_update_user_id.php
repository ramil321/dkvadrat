<?php

use yii\db\Migration;

/**
 * Class m180710_124033_add_material_columns_update_user_id
 */
class m180710_124033_add_material_columns_update_user_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','update_user_id',$this->integer());
        $this->renameColumn('material','user_id','insert_user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180710_124033_add_material_columns_update_user_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_124033_add_material_columns_update_user_id cannot be reverted.\n";

        return false;
    }
    */
}
