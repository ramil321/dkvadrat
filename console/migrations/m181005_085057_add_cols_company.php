<?php

use yii\db\Migration;

/**
 * Class m181005_085057_add_cols_company
 */
class m181005_085057_add_cols_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company','site',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181005_085057_add_cols_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181005_085057_add_cols_company cannot be reverted.\n";

        return false;
    }
    */
}
