<?php

use common\models\banner\BannerPosition;
use yii\db\Migration;

/**
 * Class m190423_110746_change_banner_positions
 */
class m190423_110746_change_banner_positions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        BannerPosition::deleteAll();
        $this->renameColumn('banner_position', 'template', 'template_name');

        $bannerPositions = [
            [
                'name' => "Для конструктора",
                'code' => "main_banner_constructor",
                'template_name' => ''
            ],
            [
                'name' => "На детальной маленький",
                'code' => "detail_material_small",
                'template_name' => '_detail_small'
            ],
            [
                'name' => "На детальной средний",
                'code' => "detail_material_medium",
                'template_name' => '_detail_medium'
            ],
            [
                'name' => "Сквозной баннер в шапке",
                'code' => "top_line_all_pages",
                'template_name' => '_full_width'
            ],

        ];
        foreach ($bannerPositions as $bp) {
            \Yii::$app->db->createCommand()->insert('banner_position', [
                'template_name' => $bp['template_name'],
                'name' => $bp['name'],
                'code' => $bp['code'],
            ])->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190423_110746_change_banner_positions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190423_110746_change_banner_positions cannot be reverted.\n";

        return false;
    }
    */
}
