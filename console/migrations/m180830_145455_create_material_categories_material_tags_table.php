<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_categories_material_tags`.
 */
class m180830_145455_create_material_categories_material_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_categories_material_tags', [
            'id' => $this->primaryKey(),
            'material_categories_id' => $this->integer(),
            'material_tags_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-mctm-material_categories_id-mt',
            'material_categories_material_tags',
            'material_categories_id',
            'material_categories',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-mctm-material_tags_id-material_id',
            'material_categories_material_tags',
            'material_tags_id',
            'material_tags',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('material_categories_material_tags');
    }
}
