<?php

use yii\db\Migration;

/**
 * Class m190305_103530_add_column_special_projects_table
 */
class m190305_103530_add_column_special_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('special_projects', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190305_103530_add_column_special_projects_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_103530_add_column_special_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
