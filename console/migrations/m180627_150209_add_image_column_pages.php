<?php

use yii\db\Migration;

/**
 * Class m180627_150209_add_image_column_pages
 */
class m180627_150209_add_image_column_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('pages','image',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180627_150209_add_image_column_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_150209_add_image_column_pages cannot be reverted.\n";

        return false;
    }
    */
}
