<?php

use yii\db\Migration;

/**
 * Handles the creation of table `person_categories`.
 */
class m181004_110839_create_person_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('person_categories', [
            'id' => $this->primaryKey(),
            'name' =>  $this->string()->notNull(),
        ]);
        $this->addForeignKey(
            'fk-person-category_id',
            'person',
            'category_id',
            'person_categories',
            'id',
            'CASCADE'
        );

        $this->createTable('press_release', [
            'id' => $this->primaryKey(),
            'name' =>  $this->string()->notNull(),
            'company_id' => $this->integer(),
            'file_id' => $this->integer(),
            'image_id' => $this->integer(),
            'publish_date' => $this->integer(),
            'preview_text' => $this->text(),
            'detail_text' => $this->text(),
        ]);

        $this->addForeignKey(
            'fk-press_release-company_id',
            'press_release',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('person_categories');
    }
}
