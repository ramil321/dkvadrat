<?php

use yii\db\Migration;

/**
 * Class m181008_140215_crate_constructor_templates_table
 */
class m181008_140215_crate_constructor_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('constructor_templates',[
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull()->unique(),
            'category' => $this->integer()->notNull(),
            'template' => $this->text()->notNull(),
            'variables' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181008_140215_crate_constructor_templates_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181008_140215_crate_constructor_templates_table cannot be reverted.\n";

        return false;
    }
    */
}
