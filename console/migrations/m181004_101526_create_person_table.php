<?php

use yii\db\Migration;

/**
 * Handles the creation of table `person`.
 */
class m181004_101526_create_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('person', [
            'id' => $this->primaryKey(),
            'photo_id' => $this->integer(),
            'fio' => $this->string()->notNull(),
            'company_id' => $this->integer(),
            'category_id' => $this->integer()->notNull(),
            'position' => $this->string(),
            'biography' => $this->text()->notNull(),
        ]);

        $this->createTable('person_material_tags', [
            'id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'material_tags_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-mctm-person_material_tags-mt',
            'person_material_tags',
            'person_id',
            'person',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-mctm-person_material_tags-material_id',
            'person_material_tags',
            'material_tags_id',
            'material_tags',
            'id',
            'CASCADE'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('person');
    }
}
