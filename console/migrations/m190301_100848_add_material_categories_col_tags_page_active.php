<?php

use yii\db\Migration;

/**
 * Class m190301_100848_add_material_categories_col_tags_page_active
 */
class m190301_100848_add_material_categories_col_tags_page_active extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material_categories', 'tags_page_active', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190301_100848_add_material_categories_col_tags_page_active cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190301_100848_add_material_categories_col_tags_page_active cannot be reverted.\n";

        return false;
    }
    */
}
