<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banner`.
 */
class m180906_101448_create_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('banner', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_id' => $this->string(),
            'blank' => $this->boolean(),
            'link' => $this->string(),
            'banner_position_id' => $this->integer(),
        ]);

        $this->createTable('banner_position', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-banner-banner_position-mt',
            'banner',
            'banner_position_id',
            'banner_position',
            'id',
            'CASCADE'
        );



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('banner');
    }
}
