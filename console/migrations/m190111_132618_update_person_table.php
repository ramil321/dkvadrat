<?php

use yii\db\Migration;

/**
 * Class m190111_132618_update_person_table
 */
class m190111_132618_update_person_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('person', 'phone_stationary', $this->string());
        $this->addColumn('person', 'fax', $this->string());
        $this->addColumn('person', 'email', $this->string());
        $this->addColumn('person', 'vk_href', $this->string());
        $this->addColumn('person', 'fb_href', $this->string());
        $this->addColumn('person', 'tw_href', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190111_132618_update_person_table cannot be reverted.\n";
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190111_132618_update_person_table cannot be reverted.\n";

        return false;
    }
    */
}
