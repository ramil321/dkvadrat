<?php

use yii\db\Migration;

/**
 * Class m190121_100919_add_column_material
 */
class m190121_100919_add_column_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material', 'duration_read', $this->integer());
        $this->addColumn('material', 'views_count', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190121_100919_add_column_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190121_100919_add_column_material cannot be reverted.\n";

        return false;
    }
    */
}
