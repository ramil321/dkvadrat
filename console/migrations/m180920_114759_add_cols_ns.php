<?php

use yii\db\Migration;

/**
 * Class m180920_114759_add_cols_ns
 */
class m180920_114759_add_cols_ns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters_list_newsletters_subscribers','code',$this->string());
        $this->addColumn('newsletters_list_newsletters_subscribers','active',$this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180920_114759_add_cols_ns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180920_114759_add_cols_ns cannot be reverted.\n";

        return false;
    }
    */
}
