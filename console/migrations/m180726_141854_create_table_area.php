<?php

use yii\db\Migration;

/**
 * Class m180726_141854_create_table_area
 */
class m180726_141854_create_table_area extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('area', [
            'id' => $this->primaryKey(),
            'identify' => $this->text(),
            'value' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180726_141854_create_table_area cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180726_141854_create_table_area cannot be reverted.\n";

        return false;
    }
    */
}
