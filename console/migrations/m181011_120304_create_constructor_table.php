<?php

use yii\db\Migration;

/**
 * Handles the creation of table `constructor`.
 */
class m181011_120304_create_constructor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('constructor', [
            'id' => $this->primaryKey(),
            'template' => $this->text()->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('constructor');
    }
}
