<?php

use yii\db\Migration;

/**
 * Class m180712_132907_seo
 */
class m180712_132907_seo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('{{%seo}}', [
            'id' => 'pk',
            'item_id' => 'int(11)',
            'modelName' => 'VARCHAR(150) NOT NULL',
            'h1' => 'VARCHAR(255) NULL',
            'title' => 'VARCHAR(255) NULL',
            'keywords' => 'VARCHAR(255) NULL',
            'description' => 'VARCHAR(522) NULL',
            'text' => 'TEXT NULL',
            'meta_index' => 'VARCHAR(255) NULL',
            'redirect_301' => 'VARCHAR(522) NULL',
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180712_132907_seo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180712_132907_seo cannot be reverted.\n";

        return false;
    }
    */
}
