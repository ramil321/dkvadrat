<?php

use yii\db\Migration;

/**
 * Class m181213_182830_remove_field_flag
 */
class m181213_182830_remove_field_flag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('material_categories', 'flag');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('material_categories', 'flag', $this->tinyInteger(1));
    }
}
