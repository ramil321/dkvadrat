<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_tags_material`.
 */
class m180710_135512_create_material_tags_material_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_tags_material', [
            'id' => $this->primaryKey(),
            'material_tags_id' => $this->integer(),
            'material_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-material_tags_material-material_tags_material-mt',
            'material_tags_material',
            'material_tags_id',
            'material_tags',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-material_tags_material-material_tags_material-material_id',
            'material_tags_material',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('material_tags_material');
    }
}
