<?php

use yii\db\Migration;

/**
 * Class m181004_114139_add_fk_company_id
 */
class m181004_114139_add_fk_company_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-person-company_id',
            'person',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181004_114139_add_fk_company_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181004_114139_add_fk_company_id cannot be reverted.\n";

        return false;
    }
    */
}
