<?php

use yii\db\Migration;

/**
 * Class m180712_141354_add_slug_materials
 */
class m180712_141354_add_slug_materials extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','slug',$this->string());
        $this->addColumn('material_categories','slug',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180712_141354_add_slug_materials cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180712_141354_add_slug_materials cannot be reverted.\n";

        return false;
    }
    */
}
