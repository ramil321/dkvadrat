<?php

use yii\db\Migration;

/**
 * Class m190412_103710_set_null_to_material
 */
class m190412_103710_set_null_to_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('material',['views_count' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190412_103710_set_null_to_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_103710_set_null_to_material cannot be reverted.\n";

        return false;
    }
    */
}
