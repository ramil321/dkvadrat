<?php

use yii\db\Migration;

/**
 * Class m181003_142325_react_tmp
 */
class m181003_142325_react_tmp extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','react_tmp',$this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181003_142325_react_tmp cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181003_142325_react_tmp cannot be reverted.\n";

        return false;
    }
    */
}
