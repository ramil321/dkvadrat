<?php

use yii\db\Migration;

/**
 * Class m180925_123545_change_sl_cols
 */
class m180925_123545_change_sl_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('newsletters_list','subscribe_body',$this->text());
        $this->alterColumn('newsletters_list','unsubscribe_body',$this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180925_123545_change_sl_cols cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180925_123545_change_sl_cols cannot be reverted.\n";

        return false;
    }
    */
}
