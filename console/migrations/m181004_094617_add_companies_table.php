<?php

use yii\db\Migration;

/**
 * Class m181004_094617_add_companies_table
 */
class m181004_094617_add_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company',[
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'image_id' => $this->integer(),
            'address' => $this->string(),
            'text' => $this->text(),
            'phone' => $this->string(),
            'email' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181004_094617_add_companies_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181004_094617_add_companies_table cannot be reverted.\n";

        return false;
    }
    */
}
