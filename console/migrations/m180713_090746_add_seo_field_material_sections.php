<?php

use yii\db\Migration;

/**
 * Class m180713_090746_add_seo_field_material_sections
 */
class m180713_090746_add_seo_field_material_sections extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material_categories','seo_title',$this->string());
        $this->addColumn('material_categories','seo_description',$this->string());
        $this->addColumn('material_categories','seo_keywords',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180713_090746_add_seo_field_material_sections cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180713_090746_add_seo_field_material_sections cannot be reverted.\n";

        return false;
    }
    */
}
