<?php

use yii\db\Migration;

/**
 * Class m180807_144350_change_table_name
 */
class m180807_144350_change_table_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameTable('theme_materials','theme_material');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180807_144350_change_table_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180807_144350_change_table_name cannot be reverted.\n";

        return false;
    }
    */
}
