<?php

use yii\db\Migration;

/**
 * Class m180808_081041_add_fk_material
 */
class m180808_081041_add_fk_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-theme_material-one',
            'theme_material',
            'theme_id',
            'theme',
            'id',
            'CASCADE'
        );



       /* $this->addForeignKey(
            'fk-theme_materials-one',
            'theme_material',
            'theme_id',
            'theme',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-theme_materials-two',
            'theme_material',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );
*/
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180808_081041_add_fk_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180808_081041_add_fk_material cannot be reverted.\n";

        return false;
    }
    */
}
