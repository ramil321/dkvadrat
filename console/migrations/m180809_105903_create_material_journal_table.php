<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_journal`.
 */
class m180809_105903_create_material_journal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_journal', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull(),
            'image_id' => $this->integer(),
            'date' => $this->integer(),
            'file_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('material_journal');
    }
}
