<?php

use yii\db\Migration;

/**
 * Class m180713_092359_change_type_seo_field_material_sections
 */
class m180713_092359_change_type_seo_field_material_sections extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('material_categories','seo_description',$this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180713_092359_change_type_seo_field_material_sections cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180713_092359_change_type_seo_field_material_sections cannot be reverted.\n";

        return false;
    }
    */
}
