<?php

use yii\db\Migration;

/**
 * Class m180925_142816_add_cols_material
 */
class m180925_142816_add_cols_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','detail_image_option_full_size',$this->boolean());
        $this->addColumn('material','detail_image_option_position',$this->string());
        $this->addColumn('material','detail_image_option_author',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180925_142816_add_cols_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180925_142816_add_cols_material cannot be reverted.\n";

        return false;
    }
    */
}
