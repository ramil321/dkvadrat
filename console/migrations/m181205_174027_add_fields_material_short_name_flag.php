<?php

use yii\db\Migration;

/**
 * Class m181205_174027_add_fields_material_short_name_flag
 */
class m181205_174027_add_fields_material_short_name_flag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','short_name',$this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('material', 'short_name');
    }
}
