<?php

use common\models\banner\Banner;
use common\models\banner\BannerPosition;
use yii\db\Migration;

/**
 * Class m190328_114803_add_banners_positions
 */
class m190328_114803_add_banners_positions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**@var Banner $model*/
        $model = TEST;
        $bannerPositions = [
            [
                'name' => "Для конструктора",
                'code' => "main_banner_constructor",
                'template' => ''
            ]

        ];
        foreach ($bannerPositions as $bp){
            $bannerPosition = new BannerPosition();
            $bannerPosition->template = $bp['template'];
            $bannerPosition->name = $bp['name'];
            $bannerPosition->code = $bp['code'];
            $bannerPosition->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190328_114803_add_banners_positions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190328_114803_add_banners_positions cannot be reverted.\n";

        return false;
    }
    */
}
