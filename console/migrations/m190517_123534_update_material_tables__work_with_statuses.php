<?php

use yii\db\Migration;

/**
 * Class m190517_123534_update_material_tables__work_with_statuses
 */
class m190517_123534_update_material_tables__work_with_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('{{material}}', 'is_published', $this->boolean()->defaultValue(false)->after('publish_date'));
        $this->dropForeignKey('fk-material-material_status_id', '{{material}}');
        Yii::$app->db->createCommand()->update('{{material}}', ['is_published' => true], 'material_status_id = 1')->execute();
        $this->dropColumn('{{material}}', 'material_status_id');
        $this->dropTable('{{material_status}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190517_123534_update_material_tables__work_with_statuses cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190517_123534_update_material_tables__work_with_statuses cannot be reverted.\n";

        return false;
    }
    */
}