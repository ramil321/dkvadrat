<?php

use yii\db\Migration;

/**
 * Class m181015_101706_add_journal_cols
 */
class m181015_101706_add_journal_cols extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('journal','date_from',$this->integer());
        $this->addColumn('journal','date_to',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181015_101706_add_journal_cols cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_101706_add_journal_cols cannot be reverted.\n";

        return false;
    }
    */
}
