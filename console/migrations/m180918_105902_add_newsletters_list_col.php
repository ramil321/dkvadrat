<?php

use yii\db\Migration;

/**
 * Class m180918_105902_add_newsletters_list_col
 */
class m180918_105902_add_newsletters_list_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters_list','unisender_list_id',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180918_105902_add_newsletters_list_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180918_105902_add_newsletters_list_col cannot be reverted.\n";

        return false;
    }
    */
}
