<?php

use yii\db\Migration;

/**
 * Class m181009_100059_add_const_col
 */
class m181009_100059_add_const_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('constructor_templates','description',$this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181009_100059_add_const_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181009_100059_add_const_col cannot be reverted.\n";

        return false;
    }
    */
}
