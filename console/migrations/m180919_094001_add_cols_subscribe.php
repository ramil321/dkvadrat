<?php

use yii\db\Migration;

/**
 * Class m180919_094001_add_cols_subscribe
 */
class m180919_094001_add_cols_subscribe extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters','newsletters_list_id',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180919_094001_add_cols_subscribe cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180919_094001_add_cols_subscribe cannot be reverted.\n";

        return false;
    }
    */
}
