<?php

use yii\db\Migration;

/**
 * Class m180918_103951_add_newsletters_list_relation
 */
class m180918_103951_add_newsletters_list_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $this->createTable('newsletters_list_newsletters_subscribers',[
            'id' => $this->primaryKey(),
            'newsletters_list_id' => $this->integer(),
            'newsletters_subscribers_id' => $this->integer(),
        ]);


        $this->addForeignKey(
            'fk-newsletters_lis-newsletters_list_id',
            'newsletters_list_newsletters_subscribers',
            'newsletters_list_id',
            'newsletters_list',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-newsletters_lis-newsletters_subscribers_id',
            'newsletters_list_newsletters_subscribers',
            'newsletters_subscribers_id',
            'newsletters_subscribers',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180918_103951_add_newsletters_list_relation cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180918_103951_add_newsletters_list_relation cannot be reverted.\n";

        return false;
    }
    */
}
