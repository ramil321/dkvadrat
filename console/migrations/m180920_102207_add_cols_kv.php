<?php

use yii\db\Migration;

/**
 * Class m180920_102207_add_cols_kv
 */
class m180920_102207_add_cols_kv extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$this->addColumn('newsletters_list','subscribe_body',$this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180920_102207_add_cols_kv cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180920_102207_add_cols_kv cannot be reverted.\n";

        return false;
    }
    */
}
