<?php

use yii\db\Migration;

/**
 * Class m200814_142739_add_newsletter_col_is_send
 */
class m200814_142739_add_newsletter_col_is_send extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters','is_send', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200814_142739_add_newsletter_col_is_send cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200814_142739_add_newsletter_col_is_send cannot be reverted.\n";

        return false;
    }
    */
}
