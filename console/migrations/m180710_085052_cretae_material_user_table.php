<?php

use yii\db\Migration;

/**
 * Class m180710_085052_cretae_material_user_table
 */
class m180710_085052_cretae_material_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_user', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'material_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-material_user-material_user-user_id',
            'material_user',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-material_user-material_user-material_id',
            'material_user',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180710_085052_cretae_material_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_085052_cretae_material_user_table cannot be reverted.\n";

        return false;
    }
    */
}
