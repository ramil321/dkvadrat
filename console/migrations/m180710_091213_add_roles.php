<?php

use common\models\user\User;
use yii\db\Migration;


/**
 * Class m180710_091213_add_roles
 */
class m180710_091213_add_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {



        $transaction = $this->getDb()->beginTransaction();
        $user = \Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
            'email'    => 'admin@example.com',
            'username' => 'admin',
            'password' => '123456',
        ]);
        if (!$user->insert(false)) {
            $transaction->rollBack();
            return false;
        }
        $user->confirm();
        $transaction->commit();


        $role = Yii::$app->authManager->createRole('admin');
        $role->description = 'Администратор';
        Yii::$app->authManager->add($role);

        $role = Yii::$app->authManager->createRole('author');
        $role->description = 'Автор';
        Yii::$app->authManager->add($role);

        Yii::$app->authManager->assign( Yii::$app->authManager->getRole('admin'),$user->id);
        Yii::$app->authManager->assign( Yii::$app->authManager->getRole('author'),$user->id);

        //$user = new User();
        //$user->
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180710_091213_add_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_091213_add_roles cannot be reverted.\n";

        return false;
    }
    */
}
