<?php

use yii\db\Migration;

/**
 * Class m180809_094540_add_material_status_id
 */
class m180809_094540_add_material_status_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('material','material_status_id',$this->integer());
        $this->addForeignKey(
            'fk-material-material_status_id',
            'material',
            'material_status_id',
            'material_status',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180809_094540_add_material_status_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180809_094540_add_material_status_id cannot be reverted.\n";

        return false;
    }
    */
}
