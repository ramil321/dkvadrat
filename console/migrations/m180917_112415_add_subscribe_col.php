<?php

use yii\db\Migration;

/**
 * Class m180917_112415_add_subscribe_col
 */
class m180917_112415_add_subscribe_col extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('newsletters','unisender_message_id',$this->integer());
        $this->addColumn('newsletters','unisender_campaign_id',$this->integer());
        $this->addColumn('newsletters','unisender_list_id',$this->integer());

        $this->createTable('unisender_log',[
            'id' => $this->primaryKey(),
            'request' => $this->text(),
            'reponse' => $this->text()
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180917_112415_add_subscribe_col cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180917_112415_add_subscribe_col cannot be reverted.\n";

        return false;
    }
    */
}
