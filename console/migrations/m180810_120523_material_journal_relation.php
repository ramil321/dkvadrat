<?php

use yii\db\Migration;

/**
 * Class m180810_120523_material_journal_relation
 */
class m180810_120523_material_journal_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('material','journal_id',$this->integer());
        $this->addForeignKey(
            'fk-material-journal_id',
            'material',
            'journal_id',
            'journal',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180810_120523_material_journal_relation cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180810_120523_material_journal_relation cannot be reverted.\n";

        return false;
    }
    */
}
