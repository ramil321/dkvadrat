<?php

use yii\db\Migration;

/**
 * Class m190304_144949_change_type_view_count
 */
class m190304_144949_change_type_view_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('material_tags', 'view_count', $this->integer()->defaultValue(0));
        $this->renameColumn('material_tags', 'views_count_tmp' , 'view_count_tmp');

        \common\models\material\MaterialTags::updateAll(['view_count' => 0, 'view_count_tmp' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190304_144949_change_type_view_count cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190304_144949_change_type_view_count cannot be reverted.\n";

        return false;
    }
    */
}
