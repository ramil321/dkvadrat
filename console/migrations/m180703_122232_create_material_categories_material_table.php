<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material_categories_material`.
 */
class m180703_122232_create_material_categories_material_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material_categories_material', [
            'id' => $this->primaryKey(),
            'material_categories_id' => $this->integer(),
            'material_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-material_categories_material-material_categories_id_',
            'material_categories_material',
            'material_categories_id',
            'material_categories',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-material_categories_material-material_id',
            'material_categories_material',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('material_categories_material');
    }
}
