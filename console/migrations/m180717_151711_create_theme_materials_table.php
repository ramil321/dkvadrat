<?php

use yii\db\Migration;

/**
 * Handles the creation of table `theme_materials`.
 */
class m180717_151711_create_theme_materials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $this->createTable('theme_materials', [
            'id' => $this->primaryKey(),
            'theme_id' => $this->integer(),
            'material_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-theme_materials-theme_id',
            'theme_materials',
            'theme_id',
            'theme',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-theme_materials-material_id',
            'theme_materials',
            'material_id',
            'material',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('theme_materials');
    }
}
