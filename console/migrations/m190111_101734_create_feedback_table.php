<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m190111_101734_create_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'phone' => $this->text(),
            'email' => $this->text(),
            'description' => $this->text(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback');
    }
}
