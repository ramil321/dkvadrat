<?php

use yii\db\Migration;

/**
 * Handles the creation of table `material`.
 */
class m180703_121200_create_material_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('material', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'preview_text' => $this->text()->notNull(),
            'detail_text' => $this->text()->notNull(),
            'detail_image_id' => $this->integer(),
            'preview_image_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('material');
    }
}
