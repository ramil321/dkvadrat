<?php

use yii\db\Migration;

/**
 * Class m190226_145441_add_column_special_projects_table
 */
class m190226_145441_add_column_special_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('special_projects', 'short_name', $this->string());
        $this->addColumn('special_projects', 'name', $this->string());
        $this->addColumn('special_projects', 'image_id', $this->integer());
        $this->addColumn('special_projects', 'slug', $this->string());
        $this->addColumn('special_projects', 'position', $this->integer());
        $this->addColumn('special_projects', 'active', $this->boolean());

        $this->dropColumn('special_projects', 'template');
        $this->dropColumn('special_projects', 'settings');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190226_145441_add_column_special_projects_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190226_145441_add_column_special_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
