<?php
namespace console\controllers;

use common\models\material\Material;
use common\models\material\MaterialCategories;
use common\models\material\MaterialTags;
use common\models\material\MaterialTagsMaterialCategoriesTmpViews;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class TagsController extends Controller
{

    public function actionIndex()
    {

        MaterialTagsMaterialCategoriesTmpViews::deleteAll();
        /**
         * @var MaterialCategories[] $materialCategories
         */
        $materialCategories = MaterialCategories::find()->all();
        foreach ($materialCategories as $category){
            /** @var Material[] $materials */
            $materials = Material::find()
                ->joinWith('materialCategories')
                ->joinWith('tags')
                ->where(['material_categories.id' => $category->id])
                ->all();
            $tagIds = [];
            foreach ($materials as $material){
                $tagIds =  ArrayHelper::merge(ArrayHelper::map($material->tags, 'id', 'id'), $tagIds);
            }
            foreach ($tagIds as $tagId){
                $tmpTag = new MaterialTagsMaterialCategoriesTmpViews();
                $tmpTag->material_categories_id = $category->id;
                $tmpTag->material_tags_id = $tagId;
                $tmpTag->save();
            }
        }

        MaterialTags::updateAll(['view_count_tmp' => 0]);

    }

}