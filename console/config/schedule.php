<?php

use omnilight\scheduling\Schedule;


/** @var Schedule $schedule */

$schedule->command('queue/run')->everyMinute();
$schedule->command('newsletter-work')->everyMinute();