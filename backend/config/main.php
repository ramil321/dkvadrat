<?php


$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'controllerMap' => [
        'banner-advertiser' => '\backend\controllers\banner\BannerAdvertiserController',
        'banner' => '\backend\controllers\banner\BannerController',
        'banner-position' => '\backend\controllers\banner\BannerPositionController',
        'banner-company' => '\backend\controllers\banner\BannerCompanyController',

        'material-categories' => '\backend\controllers\material\MaterialCategoriesController',
        'material' => '\backend\controllers\material\MaterialController',
        'material-region' => '\backend\controllers\material\MaterialRegionController',
        'material-status' => '\backend\controllers\material\MaterialStatusController',
        'material-tags' => '\backend\controllers\material\MaterialTagsController',
    ],
    'modules' => [
        'pages' => [
            'class' => 'backend\modules\pages\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ],
        /*main-local params*/
        'newsletters' => [
            'class' => 'backend\modules\newsletters\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ],
        /*main-local params*/
        'area' => [
            'class' => 'backend\modules\area\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ],
        'rest' => [
            'class' => 'backend\modules\rest\Module',
        ],
        'settings' => [
            'class' => 'backend\modules\settings\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ],
        'constructor' => [
            'class' => 'backend\modules\constructor\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ]
            ],
        ],

    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'site',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if (Yii::$app->getErrorHandler()->exception !== null) {
                    $response->data = [
                        'errors' => [
                            [
                                'name' => 'Error',
                                'message' => Yii::$app->getErrorHandler()->exception->getMessage()
                            ]
                        ],
                        'result' => null,
                    ];
                }
            },
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,

            'rules' => include 'url-rules.php',
        ],
        /*main-local params*/
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => 'http://kvadrat/frontend/web',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => include Yii::getAlias('@common/config/url-rules.php'),
        ],
        'urlManagerBackend' => [
            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        /*main-local params*/
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/views/user'
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyASDmvI7vqSQEfjFakkkpm0weXlM7-cHW8',
                        'language' => 'ru',
                        'version' => '3.1.18',
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
