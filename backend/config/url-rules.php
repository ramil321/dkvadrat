<?php
return [
    new \yii\web\GroupUrlRule([
        'prefix' => 'rest',
        'rules' => [
            ['pattern'=>'tags/find-by-name/<q:>','route'=>'tags/find-by-name','suffix'=>'/'],

            ['pattern'=>'themes/find-by-name/<q:>','route'=>'themes/find-by-name','suffix'=>'/'],
            ['pattern'=>'themes/<id:>','route'=>'themes','suffix'=>'/'],

            ['pattern'=>'materials/find','route'=>'materials/find','suffix'=>'/'],
            ['pattern'=>'materials/create','route'=>'materials/create','suffix'=>'/'],
            ['pattern'=>'materials/priority/<category:>','route'=>'materials/priority','suffix'=>'/'],
            ['pattern'=>'materials/<id:>','route'=>'materials','suffix'=>'/'],
            ['pattern'=>'materials/update/<id:>','route'=>'materials/update','suffix'=>'/'],

            ['pattern'=>'constructor-templates/update/<code:>','route'=>'constructor-templates/update','suffix'=>'/'],

            ['pattern'=>'constructor/update/<id:>','route'=>'constructor/update','suffix'=>'/'],
            ['pattern'=>'constructor/special-projects-list/<id:>','route'=>'constructor/special-projects-list','suffix'=>'/'],
            ['pattern'=>'special-projects/','route'=>'special-projects/index','suffix'=>'/'],
            ['pattern'=>'special-projects/update/<id:>','route'=>'special-projects/update','suffix'=>'/'],
            ['pattern'=>'special-projects/delete/<id:>','route'=>'special-projects/delete','suffix'=>'/'],
            ['pattern'=>'constructor/create','route'=>'constructor/create','suffix'=>'/'],
            ['pattern'=>'constructor/list','route'=>'constructor/list','suffix'=>'/'],
            ['pattern'=>'constructor/render-block','route'=>'constructor/render-block','suffix'=>'/'],
            ['pattern'=>'constructor/<id:>','route'=>'constructor/index','suffix'=>'/'],
            ['pattern'=>'constructor/delete/<id:>','route'=>'constructor/delete','suffix'=>'/'],

            ['pattern'=>'multi-list/<q:>','route'=>'multi-list','suffix'=>'/'],

            ['pattern'=>'menu/top/save','route'=>'menu/top-save','suffix'=>'/'],
            ['pattern'=>'menu/top','route'=>'menu/top','suffix'=>'/'],

            ['pattern'=>'banners','route'=>'banners','suffix'=>'/'],
            ['pattern'=>'banners/update/<id:>','route'=>'banners/update','suffix'=>'/'],
            ['pattern'=>'banners/create','route'=>'banners/create','suffix'=>'/'],

        ]
    ])
];