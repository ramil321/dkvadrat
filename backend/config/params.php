<?php
return [
    /*params-local params*/
    'swaggerRestUrl' => 'http://kvadrat/backend/web/rest/',
    'constructorSectionDefaultSettings' => [
        'autoUpdate' => false,
        'isHidden' => false,
        'isHiddenOnMobile' => false,
        'hasLinkedPage' => false,
        'linkedPage' => '',
        'isTagsVisible' => true
    ],
    'specialProjectsSectionDefaultSettings' => [
        'isHidden' => false,
    ]
    /*params-local params*/,
];