<?php
namespace backend\widgets;


use dosamigos\ckeditor\CKEditor;
use dosamigos\ckeditor\CKEditorWidgetAsset;
use lo\widgets\modal\ModalAjax;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;


class CkEditorDefaultWidget extends CKEditor
{
    /**
     * Registers CKEditor plugin
     * @codeCoverageIgnore
     */
    protected function registerPlugin()
    {
        $this->options['rows'] = 6;
        $this->preset = 'preset';
        $this->clientOptions = [
            'filebrowserUploadUrl' => Url::to(['/rest/image/create-ck-editor']),
            'toolbarGroups' => [
                ['name' => 'undo'],
                ['name' => 'styles', 'groups' => 'styles'],
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                ['name' => 'links', 'groups' => ['links']],
                ['name' => 'paragraph', 'groups' => ['list', 'indent', 'align']],
                ['name' => 'insert'],
                ['name' => 'tools'],

            ],
            'removeButtons' => 'Styles,Font,FontSize,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe',
        ];

        $js = [];

        $view = $this->getView();

        CKEditorWidgetAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : '{}';

        $js[] = "CKEDITOR.replace('$id', $options);";
        $js[] = "dosamigos.ckEditorWidget.registerOnChangeHandler('$id');";

        if (isset($this->clientOptions['filebrowserUploadUrl']) || isset($this->clientOptions['filebrowserImageUploadUrl'])) {
            $js[] = "dosamigos.ckEditorWidget.registerCsrfImageUploadHandler();";
        }

        $view->registerJs(implode("\n", $js));
    }
}
