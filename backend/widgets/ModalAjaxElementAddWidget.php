<?php
namespace backend\widgets;


use lo\widgets\modal\ModalAjax;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;


class ModalAjaxElementAddWidget extends Widget
{
    public $url;
    public $selectId;
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return ModalAjax::widget([
          //  'id' => 'addElement',
            'header' => 'Добавить элемент',
            'toggleButton' => [
                'label' => 'Добавить элемент',
                'class' => 'btn btn-primary'
            ],
            'url' => Url::to([$this->url]), // Ajax view with form to load
            'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
            'events'=>[
                ModalAjax::EVENT_MODAL_SUBMIT => new JsExpression("
                    function(event, data, status, xhr, selector) {
                        console.log(selector);
                        var id = $(data).filter('.ajax-form-success').attr('data-new_id');
                        var name = $(data).filter('.ajax-form-success').attr('data-new_name');   
              
                        if(id){
                            $('#$this->selectId').append('<option value=\"'+id+'\">'+name+'</option>');
                            var value = $('#$this->selectId').val();
                            if($.isArray(value)){
                                value.push(id);
                                $('#$this->selectId').val(value).trigger('change');
                            }else{
                                $('#$this->selectId').val(id).trigger('change');
                            } 
                        }
                    }
                ")
            ]
        ]);
    }
}
