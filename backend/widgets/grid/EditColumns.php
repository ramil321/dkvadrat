<?php
namespace backend\widgets\grid;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class EditColumns extends Widget
{
    public $model;
    public $field;
    public $value;
    public $edit;
    private $result;
    /**
     * {@inheritdoc}
     */
    public function run()
    {



        $this->result .=  Html::tag('div',$this->value,['class' => 'edit-column-grid-view']);
        $this->result .=  Html::tag('div',$this->edit,[
            'class' => 'edit-column-grid-edit',
            'style' => 'display: none',
            'data-id' => $this->model->id,
            'data-name' => $this->field,
            'data-form-name' => $this->model->formName(),
        ]);

        return $this->result;
    }
}
