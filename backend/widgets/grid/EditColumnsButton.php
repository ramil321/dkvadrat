<?php

namespace backend\widgets\grid;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;


class EditColumnsButton extends Widget
{
    public $model;
    public $saveUrl;
    private $result;

    /**
     * {@inheritdoc}
     */
    public function run()
    {

        $this->result .= '<div class="edit-column-grid-button">'
            . Html::button('Изменить элементны', ['id' => 'edit_columns_button_active', 'class' => 'btn btn-success']) .
            '   ' . Html::button('Изменить выбранные элементны', ['id' => 'edit_columns_button_select', 'class' => 'btn btn-success']) . '</div>';
        $this->result .= '<div style="display: none" class="edit-column-grid-button-active">'
            . Html::button('Сохранить', ['id' => 'edit_columns_button_active_apply', 'class' => 'btn btn-success'])
            . '   ' . Html::button('Отменить', ['id' => 'edit_columns_button_active_close', 'class' => 'btn btn-danger']) . '</div>';

        $script = <<< JS
            $(function() {
                
                function save(group = false) {
                    if(group){
                        var material_is_published = $('.group_select .group-edit-is-published').val();
                        var priority = $('.group_select .group-edit-priority').val();
                        var publishDate = $('.group_select .group-edit-publish-date').val();
                        var ids = [];
                        $('input[name="selection[]"]').map(function(i,k){
                            if(k.checked){
                                ids.push(k.value) 
                            }
                        });
                        if(ids.length > 0){
                            var value = {};
                            if(material_is_published)
                                value['is_published'] = material_is_published; 
                            if(priority)
                                value['priority'] = priority; 
                            if(publishDate)
                                value['publishDateFormated'] = publishDate;
                            $('#edit_columns_button_active_apply').attr("disabled", true);
                            $('#edit_columns_button_active_apply').text('Сохранение...');
                            $.ajax({
                                type: "POST",
                                url: '$this->saveUrl',
                                data:  {'Material':{
                                    'ids':ids,
                                    'value': value,
                                    'group': true
                                }},
                                success: function(data) {
                                    location.reload();
                                },
                                dataType: 'json'
                            });                        
                        }
                    } else {
                        var  arSave = {};
                        $('.edit-column-grid-edit').each(function() {
                            var id = $(this).attr('data-id');
                            var name = $(this).attr('data-name');
                            var formName = $(this).attr('data-form-name');
                            var val = $(this).find('.edit-input').val();
                                     
                            if(!arSave[formName]){
                                arSave[formName] = {};
                            }
                            if(!arSave[formName][id]){
                                arSave[formName][id] = {};
                            }
                            if(!arSave[formName][id][name]){
                                arSave[formName][id][name] = {};
                            }
                            
                            arSave[formName][id][name]  = val;
                 
                        });
                        $('#edit_columns_button_active_apply').attr("disabled", true);
                        $('#edit_columns_button_active_apply').text('Сохранение...');
                        $.ajax({
                            type: "POST",
                            url: '$this->saveUrl',
                            data: arSave,
                            success: function(data) {
                                location.reload();
                            },
                            dataType: 'json'
                        });
                    }
                }
                
                $('#edit_columns_button_active').click(function(){
                    $('.edit-column-grid-edit').show();
                    $('.edit-column-grid-view').hide();
                    $('.edit-column-grid-button').hide();
                    $('.edit-column-grid-button-active').show();
                });
                $('#edit_columns_button_active_apply').click(function() {
                    if(!$('.group_select').hasClass('hidden')){
                        save(true);
                    }else{
                        save();
                    }
                });
                $('#edit_columns_button_active_close').click(function() {
                    $('.edit-column-grid-edit').hide();
                    $('.edit-column-grid-view').show();
                    $('.edit-column-grid-button').show();
                    $('.edit-column-grid-button-active').hide();
                    $('.group_select').addClass('hidden');
                });
                
                //Group edit
                $('#edit_columns_button_select').click(function(){
                    $('.edit-column-grid-button').hide();
                    $('.edit-column-grid-button-active').show();
                    $('#data-picker-slect-fields').show();
                    $('.group_select').removeClass('hidden');                    
                });
            });
JS;
        $this->view->registerJs($script);

        return $this->result;
    }
}
