<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\widgets;

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use yii\base\Widget;
use yii\helpers\Html;

class CoordinatesPicker extends Widget
{
    public $model;

    public $latAttributeName = 'lat';

    public $lngAttributeName = 'lng';

    public $markerTitle;

    public $popupContent;

    public function run()
    {
        $lat = $this->model->{$this->latAttributeName};
        $lng = $this->model->{$this->lngAttributeName};

        if ($lat && $lng) {
            $cor = new LatLng(['lat' => $lat, 'lng' => $lng]);
        } else {
            $cor = new LatLng(['lat' => 54.986196, 'lng' => 30.199504]);
        }
        $map = new Map([
            'center' => $cor,
            'zoom' => 14,
            'height' => 625,
            'width' => '100%',
        ]);
        $marker = new Marker([
            'position' => $cor,
            'draggable' => true,
            'title' => $this->markerTitle,
            'icon' => 'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_blue.png',
        ]);
        $js = "
                    document.getElementById('" . Html::getInputId($this->model, $this->latAttributeName) . "').value = this.getPosition().lat();
                    document.getElementById('" . Html::getInputId($this->model, $this->lngAttributeName) . "').value = this.getPosition().lng();
                ";
        $event = new \dosamigos\google\maps\Event([
            'trigger' => 'drag',
            'js' => $js,
        ]);
        $marker->addEvent($event);
        if ($lat && $lng && $this->popupContent) {
            $marker->attachInfoWindow(new InfoWindow(['content' => $this->popupContent]));
        }
        $map->addOverlay($marker);
        return $map->display();
    }
}
