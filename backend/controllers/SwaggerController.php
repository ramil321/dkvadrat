<?php

namespace backend\controllers;

use Yii;
use common\models\theme\Theme;
use backend\models\theme\search\ThemeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SwaggerController implements the CRUD actions for Theme model.
 */
class SwaggerController extends Controller
{
    public $layout = '@backend/views/layouts/clear';

    public function actionIndex()
    {
        return $this->render('index');
    }

}
