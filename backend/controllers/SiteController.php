<?php
namespace backend\controllers;


use backend\models\material\search\MaterialSearch;
use common\models\material\Material;
use common\models\material\search\MaterialSphinxIndex;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\sphinx\Query;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\UploadedFile;

/**
 * @SWG\Swagger(
 *     basePath="/",
 *     produces={"application/json"},
 *     consumes={"application/x-www-form-urlencoded"},
 *     @SWG\Info(version="1.0", title="Simple API"),
 * )
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['editor','logout', 'index','upload-preview','login', 'error', 'search','docs','json-schema'],
                        'allow' => true,
                        'roles' => ['admin','author'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModelMaterial = new MaterialSearch();
        $dataProviderMaterial = $searchModelMaterial->search(Yii::$app->request->queryParams);
        $dataProviderMaterial->query->limit(10);
        $dataProviderMaterial->pagination = false;
        $dataProviderMaterial->query->orderBy(['publish_date' => SORT_DESC]);

        return $this->render('index',
            [
                'searchModelMaterial' => $searchModelMaterial,
                'dataProviderMaterial' => $dataProviderMaterial,
            ]
        );
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionUploadPreview(){

        $model = new Material();
        $image = UploadedFile::getInstance( $model, 'detailImageInput' );

        $jsonGallery['initialPreviewAsData'] = 'true';
        $jsonGallery['initialPreview'] = array();
        $jsonGallery['initialPreviewConfig'] = array();
            $jsonGallery['initialPreview'][] = $image->tempName;
            $jsonGallery['initialPreviewConfig'][] =
                array(
                    'caption' => 'imageName',
                    'size' => '',
                    'width' => '',
                    'url' => '#',
                    'key' => '1'
                );

        return json_encode($jsonGallery);
    }

    public function generateGalleryArrayFromModel($allGallery){
        $jsonGallery['initialPreviewAsData'] = 'true';
        $jsonGallery['initialPreview'] = array();
        $jsonGallery['initialPreviewConfig'] = array();
        foreach($allGallery as $key=>$val){
            $jsonGallery['initialPreview'][] = $val->image_src;
            $jsonGallery['initialPreviewConfig'][] =
                array(
                    'caption' => $val->image_name,
                    'size' => '',
                    'width' => '',
                    'url' => Url::to(['gallery/delete', 'id' => $val->id]),
                    'key' => $val->id
                );
        }
        return $jsonGallery;
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
