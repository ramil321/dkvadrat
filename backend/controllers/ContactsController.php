<?php

namespace backend\controllers;
use common\models\contacts\Contacts;
use Yii;


class ContactsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if(Contacts::findOne(1) == null){
            $model = new Contacts();
        } else {
            $model = $this->findModel(1);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('contacts');
        }
        return $this->render('index',[
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Contacts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
