<?php

namespace backend\controllers\material;

use backend\behaviors\Slug;
use common\models\material\MaterialCategories;
use backend\models\material\search\MaterialSearch;
use common\models\material\MaterialTags;
use Yii;
use common\models\material\Material;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\material\search\MaterialSphinxIndex;

/**
 * MaterialController implements the CRUD actions for Material model.
 */
class MaterialController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'author'],
                    ],
                ],
            ],
            [
                'class' => Slug::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'transliterator' => 'Russian-Latin/BGN; NFKD',
                //Set this to true, if you want to update a slug when source attribute has been changed
                'forceUpdate' => true
            ],
        ];
    }

    public function actions()
    {
        return [
            'edit-column' => [
                'class' => 'backend\actions\grid\EditColumnsAction',
                'modelClass' => Material::className(),
                'updateFields' => ['priority','publishDateFormated']
            ],
            'ajax-materials-find' => [
                'class' => 'backend\actions\ajax\FindLikeAjaxAction',
                'modelClass' => Material::className()
            ]
        ];
    }

    public function createUpdateActionParams()
    {
        $mc = MaterialCategories::getFullTreeArray(function ($data) {
            return [
                'name' => $data->name,
                'id' => $data->id,
            ];
        });
        $dropDownCategories = array();
        foreach ($mc as $val) {
            foreach ($val['children'] as $child) {
                $dropDownCategories[$val['name']][$child['id']] = $child['name'];
            }
        }
        $authors = \common\models\user\User::getAuthors()->all();
        return ['authors' => $authors, 'dropDownCategories' => $dropDownCategories];
    }

    /**
     * Lists all Material models.
     * @return mixed
     */
//    public function actionEditColumn()
//    {
////        return 'asas';
//        $post = Yii::$app->request->post();
//        if (Yii::$app->request->isAjax && $post) {
//            if(isset($post['group'])) {
//                foreach ($post['ids'] as $id) {
//                    $model = Material::find()->where(['id' => $id])->one();
//                    $model->setAttributes($post['value']);
//                    if ($model->validate($post['value']) && isset($model)) {
//                        $model->save();
//                    }
//                }
//            } else {
//                foreach ($post['Material'] as $id => $material) {
//                    $model = Material::find()->where(['id' => $id])->one();
//                    $model->setAttributes($material);
//                    if ($model->validate($material)) {
//                        $model->save();
//                    }
//                }
//            }
//            return true;
//        }
//        return false;
//    }

    public function actionIndex()
    {
        $searchModel = new MaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new \common\models\material\Material();

        return $this->render('index',
            ArrayHelper::merge($this->createUpdateActionParams(),
                [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                ]
            )
        );
    }

    public function actionDeleteAllIndex(){
        MaterialSphinxIndex::deleteAll('id <> -1');

        return $this->render('search');
    }

    public function actionAddIndex()
    {
        $result = [];
        if(Yii::$app->request->get('search')) {
            $result = MaterialSphinxIndex::indexerAll();
        }

        return $this->render('search',[
            'result' => $result,
        ]);
    }

    /**
     * Displays a single Material model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Material model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Material();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Материал успешно создан');
            return $this->redirect(['index']);
        }

        return $this->render('create',
            ArrayHelper::merge($this->createUpdateActionParams(),
                [
                    'model' => $model,
                ]
            )
        );
    }

    /**
     * Updates an existing Material model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Материал успешно обновлен');
            return $this->redirect(['index']);
        }

        return $this->render('update',
            ArrayHelper::merge($this->createUpdateActionParams(),
                [
                    'model' => $model,
                ]
            )
        );
    }

    public function actionColumnUpdate()
    {
        $result = ['success' => '', 'errors' => []];
        $updateFields = ['priority', 'publishDateFormated'];
        $postMaterials = Yii::$app->request->post('Material');
        $materials = Material::find()->where(['id' => array_keys($postMaterials)])->all();

        foreach ($materials as $material) {
            foreach ($postMaterials[$material->id] as $key => $val) {
                if (in_array($key, $updateFields)) {
                    $material->{$key} = $val;
                }
            };
            $material->save();
            $result['errors'] = ArrayHelper::merge($result['errors'], $material->getErrors());
        }
        if (!$result['errors']) {
            Yii::$app->session->setFlash('success', 'Материалы успешно обновлены');
        } else {
            Yii::$app->session->setFlash('error', implode('<br/>', $result['errors']));
        }
        return json_encode($result);
    }

    /**
     * Deletes an existing Material model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Материал успешно удален');
        return $this->redirect(['index']);
    }

    /*
     * Аякс поиск тегов
     * */
    public function actionAjaxTags($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, name AS text')
                ->from('material_tags')
                ->where(['like', 'name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => MaterialTags::find($id)->name];
        }
        return $out;
    }

    /**
     * Finds the Material model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Material the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Material::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
