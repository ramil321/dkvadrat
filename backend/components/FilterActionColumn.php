<?php
namespace backend\components;

use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;

class FilterActionColumn extends ActionColumn
{
    public $filter;

    /**
     * Renders the filter cell content.
     * The default implementation simply renders a blank space.
     * This method may be overridden to customize the rendering of the filter cell (if any).
     * @return string the rendering result
     */
    protected function renderFilterCellContent()
    {
        if(!$this->filter) {
            return Html::a(
                '<span class="glyphicon glyphicon-refresh"></span>',
                [Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],
                ['title' => 'Сбросить фильтры']
            );
        }
        return $this->filter;
    }
}