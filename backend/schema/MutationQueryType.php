<?php

namespace backend\schema;

use backend\schema\MutationTypes;
use common\models\material\Material;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


class MutationQueryType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function() {
                return [
                    'material' => [
                        'type' => MutationTypes::material(),
                        'args' => [
                            'id' => Type::int(),
                        ],
                        'resolve' => function($root, $args) {
                            return Material::find()->where($args)->one();
                        },
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}