<?php

namespace backend\schema;

use backend\schema\types\AuthorType;
use common\models\journal\Journal;
use common\models\material\Material;
use common\models\material\MaterialTags;
use common\models\user\User;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


class QueryType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'fields' => function() {
                return [
                    // в принципе на поле user можно остановиться, в случае
                    // если нам нужно обращаться к данным лиш конкретного пользователя
                    // но если нам нужны данные с другими привязками добавим
                    // для примера еще полей

                    'author' => [
                        // без дополтинельных параметров
                        // просто вернет нам списох всех
                        // адресов
                        'type' => Type::listOf(Types::author()),

                        // добавим фильтров для интереса
                        'args' => [
                            'id' => Type::listOf(Type::int()),
                        ],
                        'resolve' => function($root, $args) {
                            $query = User::getAuthors($args['id']);
                            return $query->all();
                        }
                    ],
                    'journal' => [
                        // без дополтинельных параметров
                        // просто вернет нам списох всех
                        // адресов
                        'type' => Type::listOf(Types::journal()),

                        // добавим фильтров для интереса
                        'args' => [
                            'id' => Type::listOf(Type::int()),
                        ],
                        'resolve' => function($root, $args) {
                            $query = Journal::find();

                            if (!empty($args)) {
                                $query->where($args);
                            }
                            return $query->all();
                        }
                    ],
                    'tags' => [
                        // без дополтинельных параметров
                        // просто вернет нам списох всех
                        // адресов
                        'type' => Type::listOf(Types::tags()),

                        // добавим фильтров для интереса
                        'args' => [
                            'id' => Type::listOf(Type::int()),
                        ],
                        'resolve' => function($root, $args) {

                            $query = MaterialTags::find();

                            if (!empty($args)) {
                                $query->where($args);
                            }
                            return $query->all();
                        }
                    ],
                    'material' => [
                        // без дополтинельных параметров
                        // просто вернет нам списох всех
                        // адресов
                        'type' => Type::listOf(Types::material()),

                        // добавим фильтров для интереса
                        'args' => [
                            'id' => [
                                'type' => Type::listOf(Type::int()),
                                'description' => 'Ид материалов'
                            ],
                            'like_name' => [
                                'type' => Type::string(),
                                'description' => 'Поиск по имени'
                            ],
                        ],
                        'resolve' => function($root, $args) {
                            $query = Material::find()->with('materialCategoriesVia.materialCategories');
                            if (!empty($args)) {
                                $query->where($args);
                            }
                            return $query->all();
                        }
                    ]/*,
                    'materialCategories' => [
                        // без дополтинельных параметров
                        // просто вернет нам списох всех
                        // адресов
                        'type' => Type::listOf(Types::materialCategories()),

                        // добавим фильтров для интереса
                        'args' => [
                            'id' => [
                                'type' => Type::listOf(Type::int()),
                                'description' => 'Ид материалов'
                            ],
                            'like_name' => [
                                'type' => Type::string(),
                                'description' => 'Поиск по имени'
                            ],
                        ],
                        'resolve' => function($root, $args) {
                            $query = Material::find();

                            if (!empty($args)) {
                                $query->where($args);
                            }
                            return $query->all();
                        }
                    ],*/
                ];
            }
        ];

        parent::__construct($config);
    }
}