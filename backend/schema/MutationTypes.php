<?php

namespace backend\schema;

use backend\schema\types\AuthorType;
use backend\schema\types\JournalType;
use backend\schema\types\MaterialCategoriesViaType;
use backend\schema\types\MaterialType;
use backend\schema\types\mutations\MaterialMutationType;
use backend\schema\types\TagsType;


class MutationTypes
{
    private static $query;
    private static $mutation;

    private static $tags;
    private static $material;
    private static $author;
    private static $city;
    private static $materialCategories;
    private static $journal;


    public static function query()
    {
        return self::$query ?: (self::$query = new MutationQueryType());
    }
    public static function material()
    {
        return self::$material ?: (self::$material = new MaterialMutationType());
    }
}