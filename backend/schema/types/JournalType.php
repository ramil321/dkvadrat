<?php

namespace backend\schema\types;

use backend\schema\Types;
use common\models\user\User;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class JournalType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'description' => 'Журналы',
            'fields' => function() {
                return [
                    'id' => [
                        'type' => Type::int(),
                    ],
                    'number' => [
                        'type' => Type::int(),
                        'description' => 'Номер журнала',
                    ],
                    'dateFormated' => [
                        'type' => Type::string(),
                        'description' => 'Дата журнала',
                    ],
                    'image' => [
                        'type' => Type::string(),
                        'description' => 'Постер журнала',
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }

}