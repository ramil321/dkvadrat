<?php
namespace backend\schema\types\mutations;

use common\models\material\Material;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MaterialMutationType extends ObjectType
{

    public static function getDefaultArgs(){
       return [
            'material_status_id'  => [
                'type' => Type::int(),
                'description' => 'Ид статуса',
            ],
            'journal_id'  => [
                'type' => Type::int(),
                'description' => 'Ид журнала',
            ],
            'name'  => [
                'type' => Type::string(),
                'description' => 'Название статьи',
            ],
            'priority'  => [
                'type' => Type::boolean(),
                'description' => 'Приоритет',
            ],
            'publishDateFormated' => [
                'type' => Type::string(),
                'description' => 'Дата публикации',
            ],
            'materialCategories' => [
                'type' => Type::listOf(Type::int()),
                'description' => 'Ид категорий материала',
            ],
            'materialCategoriesMainCategory' => [
                'type' => Type::listOf(Type::int()),
                'description' => 'Ид основной категории',
            ],
            'authorsIds' => [
                'type' => Type::listOf(Type::int()),
                'description' => 'Список ид авторов',
            ],
            'tagIds' => [
                'type' => Type::listOf(Type::int()),
                'description' => 'Список ид тегов',
            ],
            'detail_text' => [
                'type' => Type::listOf(Type::int()),
                'description' => 'Текст статьи',
            ],
        ];
    }
    public function __construct()
    {
        $config = [
            'fields' => function() {
                return [
                    // для теста реализуем здесь
                    // один метод для изменения данных
                    // объекта User
                    'update' => [
                        // какой должен быть возвращаемый тип
                        // здесь 2 варианта - либо
                        // булев - удача / неудача
                        // либо же сам объект типа User.
                        // позже мы поговорим о валидации
                        // тогда всё станет яснее, а пока
                        // оставим булев для простоты
                        'type' => Type::boolean(),
                        'description' => 'Обновить материал',
                        'args' => self::getDefaultArgs(),
                        'resolve' => function(Material $material, $args) {
                            // ну а здесь всё проще простого,
                            // т.к. библиотека уже все проверила за нас:
                            // есть ли у нас юзер, правильные ли у нас
                            // аргументы и всё ли пришло, что необходимо
                            $material->setAttributes($args);
                            return $material->save();
                        }
                    ],
                    'add' => [
                        // какой должен быть возвращаемый тип
                        // здесь 2 варианта - либо
                        // булев - удача / неудача
                        // либо же сам объект типа User.
                        // позже мы поговорим о валидации
                        // тогда всё станет яснее, а пока
                        // оставим булев для простоты
                        'type' => Type::boolean(),
                        'description' => 'Добавить материал',
                        'args' => self::getDefaultArgs(),
                        'resolve' => function(Material $material, $args) {
                            $material = new Material();
                            $material->setAttributes($args);
                            return $material->save();
                        }
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }
}