<?php

namespace backend\schema\types;


use backend\schema\Types;
use common\models\material\Material;

use common\models\material\MaterialCategories;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


class MaterialType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'description' => 'Материал',
            'fields' => function() {
                return [
                    'id' => [
                        'type' => Type::int(),
                    ],
                    'name' => [
                        'description' => 'Название материала',
                        'type' => Type::string(),
                    ],
                    'slug' => [
                        'description' => 'Символьный код',
                        'type' => Type::string(),
                    ],
                    'tags' => [
                        'description' => 'Теги',
                        'type' => Type::listOf(Types::tags()),
                    ],
                    'authors' => [
                        'description' => 'Авторы',
                        'type' => Type::listOf(Types::author()),
                    ],
                    'journal' => [
                        'description' => 'Журнал',
                        'type' => Types::journal(),
                    ],
                    'priority' => [
                        'description' => 'Приоритет',
                        'type' => Type::boolean(),
                    ],
                    'publishDateFormated' => [
                        'description' => 'Дата публикации',
                        'type' => Type::string(),
                    ],
                    'materialCategories' => [
                        'type' => Type::listOf(Types::materialCategoriesVia()),
                        'resolve' => function(Material $materialCategories, $args) {
                            $result = [];
                            foreach($materialCategories->materialCategoriesVia as $key=>$value){
                                $result[$key]['is_main'] = $value->is_main;
                                $result[$key]['name'] = $value->materialCategories->name;
                                $result[$key]['id'] = $value->materialCategories->id;
                            }
                           return $result;
                        }
                    ],
                    'materialCategoriesMainCategory' => [
                        'type' => Types::materialCategories(),
                    ]
                    // остальные поля не столь интересны
                    // посему оставляю их вам на
                    // личное растерзание
                ];
            }
        ];

        parent::__construct($config);
    }

}