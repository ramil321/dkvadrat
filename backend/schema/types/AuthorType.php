<?php

namespace backend\schema\types;

use backend\schema\Types;
use common\models\user\User;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class AuthorType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'description' => 'Авторы',
            'fields' => function() {
                return [
                    'id' => [
                        'type' => Type::int(),
                    ],
                    'fullName' => [
                        'type' => Type::string(),
                        'description' => 'Полное имя',
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }

}