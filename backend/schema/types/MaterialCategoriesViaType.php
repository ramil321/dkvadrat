<?php

namespace backend\schema\types;

use backend\schema\Types;
use common\models\material\MaterialCategories;
use common\models\user\User;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MaterialCategoriesViaType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'description' => 'Категории материалов с отметкой главной категории',
            'fields' => function() {
                return [
                    'id' => [
                        'type' => Type::int(),
                    ],
                    'name' => [
                        'type' => Type::string(),
                        'description' => 'Полное имя',
                    ],
                    'is_main' => [
                        'type' => Type::boolean(),
                    ]
                ];
            }
        ];

        parent::__construct($config);
    }

}