<?php

namespace backend\schema\types;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class TagsType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'description' => 'Категории',
            'fields' => function() {
                return [
                    'id' => [
                        'type' => Type::int(),
                    ],
                    'name' => [
                        'description' => 'Тег',
                        'type' => Type::string(),
                    ],
                ];
            }
        ];

        parent::__construct($config);
    }

}