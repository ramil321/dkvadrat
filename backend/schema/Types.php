<?php

namespace backend\schema;

use backend\schema\types\AuthorType;
use backend\schema\types\JournalType;
use backend\schema\types\MaterialCategoriesViaType;
use backend\schema\types\MaterialType;
use backend\schema\types\TagsType;

class Types
{
    private static $query;
    private static $mutation;

    private static $tags;
    private static $material;
    private static $author;
    private static $city;
    private static $materialCategories;
    private static $journal;


    public static function query()
    {
        return self::$query ?: (self::$query = new QueryType());
    }

    public static function tags()
    {
        return self::$tags ?: (self::$tags = new TagsType());
    }
    public static function material()
    {
        return self::$material ?: (self::$material = new MaterialType());
    }
    public static function author()
    {
        return self::$author ?: (self::$author = new AuthorType());
    }
    public static function journal()
    {
        return self::$journal ?: (self::$journal = new JournalType());
    }
    public static function materialCategoriesVia()
    {
        return self::$materialCategories ?: (self::$materialCategories = new MaterialCategoriesViaType());
    }
    public static function materialCategories()
    {
        return self::$materialCategories ?: (self::$materialCategories = new MaterialCategoriesType());
    }
}