<?php

namespace backend\actions\grid;

use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "material".
 *
 * @property ActiveRecord $modelClass
 * */
class EditColumnsAction extends Action
{

    public $successMessage = 'Материалы успешно обновлены';
    public $modelClass = '';
    public $updateFields = [];

    public function run()
    {
        $result = ['success' => '', 'errors' => []];
        $model = $this->modelClass;
        $path = array_pop(explode('\\', $model));//todo PHP_EOL
        $postMaterials = Yii::$app->request->post($path);
        if ($postMaterials) {
            if (isset($postMaterials['group'])) {
                $materials = $model::find()->where(['id' => $postMaterials['ids']])->all();
                foreach ($materials as $material) {
                    foreach ($postMaterials['value'] as $key => $val) {
                        $material->{$key} = $val;
                    }
                    $material->save();
                    $result['errors'] = ArrayHelper::merge($result['errors'], $material->getErrors());
                }
            } else {
                $materials = $model::find()->where(['id' => array_keys($postMaterials)])->all();
                foreach ($materials as $material) {
                    foreach ($postMaterials[$material->id] as $key => $val) {
                        if (in_array($key, $this->updateFields)) {
                            $material->{$key} = $val;
                        }
                    };
                    $material->save();
                    $result['errors'] = ArrayHelper::merge($result['errors'], $material->getErrors());
                }
            }
            if (!$result['errors']) {
                Yii::$app->session->setFlash('success', $this->successMessage);
            } else {
                Yii::$app->session->setFlash('error', implode('<br/>', $result['errors']));
            }
            return json_encode($result);
        }
    }
}