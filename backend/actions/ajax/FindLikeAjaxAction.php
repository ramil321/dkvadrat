<?php
namespace backend\actions\ajax;

use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
/**
 * This is the model class for table "material".
 *
 * @property ActiveRecord $modelClass
 * */
class FindLikeAjaxAction extends Action
{
    public $arrayResult = false;
    public $nameField = 'name';
    public $successMessage = 'Материалы успешно обновлены';
    public $qField = 'text';
    public $modelClass = '';
    public $updateFields = [];

    public function run($q = null, $id = null)
    {
        $model = $this->modelClass;
        if(!$this->arrayResult) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        }
        $out = ['results' => ['id' => '', $this->nameField => '']];
        if (!is_null($q)) {
            $data = $model::find()
                ->select(['id',$this->nameField.' as '.$this->qField])
                ->where(['like', $this->nameField, $q])
                ->asArray()
                ->all();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, $this->nameField => $model::find($id)->{$this->nameField}];
        }
        return $out;
    }
}