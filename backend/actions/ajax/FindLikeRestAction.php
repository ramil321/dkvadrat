<?php

namespace backend\actions\ajax;

use yii\base\Action;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "material".
 * @property ActiveRecord $modelClass
 * */
class FindLikeRestAction extends Action
{
    public $arrayResult = false;
    public $nameField = 'name';
    public $successMessage = 'Материалы успешно обновлены';
    public $modelClass = '';
    public $updateFields = [];

    public function run($q = null, $id = null)
    {
        $model = $this->modelClass;

        $query = $model::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page'),
            ]
        ]);
        $query->select(['id', $this->nameField])
            ->andFilterWhere(['like', $this->nameField, $q]);

        return $dataProvider;
    }
}