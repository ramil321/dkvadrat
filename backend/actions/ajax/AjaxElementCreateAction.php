<?php
namespace backend\actions\ajax;

use Yii;
use yii\base\Action;

class AjaxElementCreateAction extends Action
{

    public $modelClass;
    public $formView;

    public function run()
    {
        $model = new $this->modelClass;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success','Сохранено');
            return $this->controller->renderAjax( '../ajax-form/_success' , [
                'model' => $model,
            ] );
        }

        return $this->controller->renderAjax( $this->formView , [
            'model' => $model,
        ] );
    }
}