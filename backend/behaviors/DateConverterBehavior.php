<?php

namespace backend\behaviors;

use Yii;
use yii\helpers\FormatConverter;

/**
 * Description of DateConverter
 *
 * ~~~
 * // attach as behavior
 * [
 *     'class' => 'mdm\converter\DateConverter',
 *     'logicalFormat' => 'php:d/m/Y',
 *     'attributes => [
 *         'createdDate' => 'created_date',
 *         'deliveryDate' => 'delivery_date',
 *     ]
 * ]
 *
 * // then attribute directly
 * $model->createdDate = '24/10/2014'; // equivalent with $model->created_date = '2014-10-24'
 * ~~~
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class DateConverterBehavior extends \yii\base\Behavior
{

    public $attributes = [];
    public $logicalFormat;
    public $physicalFormat;

    /**
     * @inheritdoc
     */
    protected function convertToLogical($value, $attribute)
    {
        if (!($value)) {
            return null;
        }

        return Yii::$app->formatter->asTime($value,$this->logicalFormat);
    }


    /**
     * @inheritdoc
     */
    protected function convertToPhysical($value, $attribute)
    {
       // echo($value);
       // echo($this->physicalFormat);
        if (!($value)) {
            return null;
        }
      //  echo('tut = '.Yii::$app->formatter->asDatetime($value.':00',$this->physicalFormat));
        //$date = @date_create_from_format($this->_phpLogicalFormat, $value);
      //  echo(Yii::$app->formatter->asTimestamp($value,$this->logicalFormat));
        return Yii::$app->formatter->asTimestamp($value,$this->logicalFormat);
       // return $date === false ? null : $date->format($this->_phpPhysicalFormat);
    }

    public function __get($name)
    {
        if (isset($this->attributes[$name])) {
            $attrValue = $this->owner->{$this->attributes[$name]};
            return $this->convertToLogical($attrValue,$name);
        } else {
            return parent::__get($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (isset($this->attributes[$name])) {

            $this->owner->{$this->attributes[$name]} = $this->convertToPhysical($value,$name);
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return isset($this->attributes[$name]) || parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        return isset($this->attributes[$name]) || parent::canSetProperty($name, $checkVars);
    }

}