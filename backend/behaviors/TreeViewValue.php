<?php


namespace backend\behaviors;

use yii\base\Model;
use yii\base\Behavior;
use yii\base\InvalidParamException;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * DateRangeBehavior automatically fills the specified attributes with the parsed date range values.
 *
 * @author Cosmo <daixianceng@gmail.com>
 */
class TreeViewValue extends Behavior
{

    public $attribute;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'convertVal',
            ActiveRecord::EVENT_AFTER_UPDATE=> 'convertVal',
            ActiveRecord::EVENT_BEFORE_INSERT => 'convertVal',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'convertVal',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'convertVal',
        ];
    }

    public function convertVal()
    {

        $val = $this->owner->{$this->attribute};

        if( is_string($val) && $val ) {
            $arCats = explode( ',', $val );
            $this->owner->{$this->attribute} = $arCats;
        }
    }

}
