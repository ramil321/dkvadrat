<?php


namespace backend\behaviors;

use yii\base\Model;
use yii\base\Behavior;
use yii\base\InvalidParamException;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * DateRangeBehavior automatically fills the specified attributes with the parsed date range values.
 *
 * @author Cosmo <daixianceng@gmail.com>
 */
class UserAddUpdateBehavior extends Behavior
{

    public $updateAttribute = 'update_user_id';
    public $insertAttribute = 'insert_user_id';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'insertEvent',
            ActiveRecord::EVENT_AFTER_UPDATE=> 'updateEvent',
        ];
    }

    public function insertEvent()
    {
        $this->owner->{$this->insertAttribute} = \Yii::$app->user->id;
    }
    public function updateEvent()
    {
        $this->owner->{$this->updateAttribute} = \Yii::$app->user->id;
    }
}
