<?php

namespace backend\models\banner\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\banner\Banner;
use yii\db\Expression;

/**
 * BannerSearch represents the model behind the search form of `common\models\banner\Banner`.
 */
class BannerSearch extends Banner
{
    public $advertiser;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'blank', 'banner_position_id','advertiser'], 'integer'],
            [['name', 'image_id', 'link','active'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Banner::find();

        $query->joinWith('bannerCompany.bannerAdvertiser');
        $query->joinWith('image');
        $query->joinWith('bannerPosition');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'active' => $this->active,
            'banner.id' => $this->id,
            'blank' => $this->blank,
            'banner_position_id' => $this->banner_position_id,
            'banner_advertiser.id' => $this->advertiser,
        ]);

        $query->andFilterWhere(['like', 'banner.name', $this->name])
            ->andFilterWhere(['like', 'image_id', $this->image_id])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
