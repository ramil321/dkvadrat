<?php

namespace backend\models\press\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\press\PressRelease;

/**
 * PressReleaseSearch represents the model behind the search form of `common\models\press\PressRelease`.
 */
class PressReleaseSearch extends PressRelease
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'file_id', 'image_id', 'publish_date'], 'integer'],
            [['name', 'preview_text', 'detail_text'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PressRelease::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'file_id' => $this->file_id,
            'image_id' => $this->image_id,
            'publish_date' => $this->publish_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'preview_text', $this->preview_text])
            ->andFilterWhere(['like', 'detail_text', $this->detail_text]);

        return $dataProvider;
    }
}
