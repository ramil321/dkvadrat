<?php

namespace backend\models\person\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\person\Person;

/**
 * PersonSerach represents the model behind the search form of `common\models\person\Person`.
 */
class PersonSerach extends Person
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'photo_id', 'company_id', 'category_id'], 'integer'],
            [['fio', 'position', 'biography'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Person::find();

        // add conditions that should always apply here
        $query->joinWith('company');
        $query->joinWith('category');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'photo_id' => $this->photo_id,
            'company_id' => $this->company_id,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'biography', $this->biography]);

        return $dataProvider;
    }
}
