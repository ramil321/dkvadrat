<?php

namespace backend\models\material\search;

use backend\modules\rest\models\Materials;
use common\models\material\Material;
use kartik\daterange\DateRangeBehavior;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * MaterialSearch represents the model behind the search form of `common\models\material\Material`.
 */
class MaterialSearch extends Materials
{
    public $createdAtRange;
    public $createdAtStart;
    public $createdAtEnd;

    public $updatedAtRange;
    public $updatedAtStart;
    public $updatedAtEnd;

    public $categoriesIds;
    public $tagIds = null;
    public $themeIds;
    public $materialCategories;

    const STATUS_PUBLISHED = 1;
    const STATUS_PENDING = 2;
    const STATUS_SAVED = 3;

    const STATUSES = [
        self::STATUS_PUBLISHED => 'Опубликована',
        self::STATUS_PENDING => 'Ожидает публикации',
        self::STATUS_SAVED => 'Сохранена',
    ];

    public $status;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),
            [
                [
                    'class' => DateRangeBehavior::className(),
                    'attribute' => 'createdAtRange',
                    'dateStartAttribute' => 'createdAtStart',
                    'dateEndAttribute' => 'createdAtEnd',
                ],
                [
                    'class' => DateRangeBehavior::className(),
                    'attribute' => 'updatedAtRange',
                    'dateStartAttribute' => 'updatedAtStart',
                    'dateEndAttribute' => 'updatedAtEnd',
                ]
            ]);
    }

    public function setTagIds($val)
    {
        $this->tagIds = $val;
    }

    public function getTagIds()
    {
        return $this->tagIds;
    }

    public function setThemeIds($val)
    {
        $this->themeIds = $val;
    }

    public function getThemeIds()
    {
        return $this->themeIds;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createdAtRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['updatedAtRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            ['priority', 'boolean'],
            [['id', 'created_at', 'updated_at', 'detail_image_id', 'preview_image_id'], 'integer'],
            [
                [
                    'name',
                    'preview_text',
                    'detail_text',
                    'authorsIds',
                    'categoriesIds',
                    'tagIds',
                    'themeIds',
                    'materialCategories',
                    'journal_id',
                ],
                'safe'
            ],
            ['status', 'in', 'range' => array_keys(self::STATUSES)],
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'createdAtRange' => 'Дата создания',
            'materialCategories' => 'Категории',
            'updatedAtRange' => 'Дата обновления',
            'themeIds' => 'Темы'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Material::find();
        $query->alias('material');
        $query->groupBy('material.id');
        $query->joinWith('tags', true);
        $query->joinWith('themes', true);
        $query->joinWith('authors.profile', true);
        $query->joinWith('materialCategories', true);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [
                    'authorsIds' => [
                        'asc' => ['material_user.user_id' => SORT_ASC],
                        'desc' => ['material_user.user_id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'materialCategories' => [
                        'asc' => ['material_categories.name' => SORT_ASC],
                        'desc' => ['material_categories.name' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'created_at',
                    'id',
                    'priority',
                    'publish_date',
                    'name',
                    'preview_text',
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'material.id' => $this->id,
            'priority' => $this->priority,
            'detail_image_id' => $this->detail_image_id,
            'preview_image_id' => $this->preview_image_id,
            'journal_id' => $this->journal_id,
            'user.id' => $this->authorsIds,
            'material_tags.id' => $this->tagIds,
            'theme.id' => $this->themeIds,
            'material_categories.id' => $this->materialCategories,
        ]);

        $query->andFilterWhere(['between', 'material.created_at', $this->createdAtStart, $this->createdAtEnd]);
        $query->andFilterWhere(['between', 'material.created_at', $this->updatedAtStart, $this->updatedAtEnd]);

        $query->andFilterWhere(['like', 'material.name', $this->name])
            ->andFilterWhere(['like', 'preview_text', $this->preview_text])
            ->andFilterWhere(['like', 'detail_text', $this->detail_text]);

        if ($this->status) {
            switch ($this->status) {
                case self::STATUS_PUBLISHED:
                    $query->andWhere('material.is_published = true');
                    $query->andWhere(['<=', 'material.publish_date', time()]);
                    break;
                case self::STATUS_PENDING:
                    $query->andWhere('material.is_published = true');
                    $query->andWhere(['>', 'material.publish_date', time()]);
                    break;
                case self::STATUS_SAVED:
                    $query->andWhere('material.is_published = false');
                    break;
            }
        }

        return $dataProvider;
    }
}
