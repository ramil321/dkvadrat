<?php


namespace backend\modules\files\models;


/**
 * This is the model class for table "registrations".

 */
class ImageStyled
{
    /**
     * @var string
     */
    public $src;
    /**
     * @var array
     */
    public $cssClass = [
        'center' => 'object-center ',
        'top' => 'object-up',
        'bottom' => 'object-down',
        'in_frame' => 'object-embed'
    ];
    /**
     * @var array
     */
    public $dataAttributes = [
        'center' => [
            'data-object-fit="cover"',
            'data-object-position="center center"'
        ],
        'top' => [
            'data-object-fit="cover"',
            'data-object-position="center top"'
        ],
        'bottom' => [
            'data-object-fit="cover"',
            'data-object-position="center bottom"'
        ],
        'in_frame' => [
            'data-object-fit="contain"',
            'data-object-position="center center"'
        ],
    ];
    /**
     * @var string
     */
    public $position;

    /**
     * @var boolean
     */
    public $full;



    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @return mixed
     */
    public function getCssClass()
    {
        return $this->cssClass[$this->position];
    }

    /**
     * @return string
     */
    public function getDataAttributes()
    {
        return implode(' ', $this->dataAttributes[$this->position]);
    }

    /**
     * @return mixed
     */
    public function getFullClass()
    {
        if($this->full){
            return 'preview-image--full-length';
        }
    }

    /**
     * ImageStyled constructor.
     * @param $src
     * @param $position
     * @param bool $full
     */
    public function __construct($src, $position = null, $full = false)
    {
        $this->full = $full;
        $this->src = $src;
        $this->position = $position;
    }
}