<?php

namespace backend\modules\files\behaviors;

use backend\modules\files\models\Files;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\web\UploadedFile;

class ImageBehavior extends Behavior
{

    const MIN_SIZE = 1;
    const MAX_SIZE = 10000;


    public $inputFileName = [['image' => 'image_id']];

    public $modelFieldFileName = 'name';
    public $modelFieldFileSrc = 'src';
    public $modelFieldFileDescription = 'description';
    public $forModel = false;
    public $uploadPath = '/upload/images/';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'saveFileEvent',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'saveFileEvent',
        ];
    }

    /**
     * @param array          $attributes
     * @param ImageInterface $image
     *
     * @return mixed
     */
    public function cropPosition($attributes, ImageInterface $image)
    {
        if (
            strlen($attributes['x']) > 0
            && strlen($attributes['y']) > 0
            && strlen($attributes['width']) > 0
            && strlen($attributes['height']) > 0
        ) {
            $imageWidth = $image->getSize()->getWidth();
            $imageHeight = $image->getSize()->getHeight();
            $x = ceil($attributes['x']);
            $y = ceil($attributes['y']);
            $width = ceil($attributes['width']);
            $height = ceil($attributes['height']);
            if ($x < 0) {
                $width = max($width + $x, self::MIN_SIZE);
                $x = 0;
            }
            if ($y < 0) {
                $height = max($height + $y, self::MIN_SIZE);
                $y = 0;
            }
            if ($x > $imageWidth - self::MIN_SIZE) {
                $x = $imageWidth - self::MIN_SIZE;
            }
            if ($y > $imageHeight - self::MIN_SIZE) {
                $y = $imageHeight - self::MIN_SIZE;
            }
            if ($x + $width > $imageWidth) {
                $width = $imageWidth - $x;
            }
            if ($y + $height > $imageHeight) {
                $height = $imageHeight - $y;
            }
            $image->crop(
                new Point($x, $y),
                new Box($width, $height)
            );
        }

        return $image;
    }

    /**
     * @param                $ratio
     * @param ImageInterface $image
     *
     * @return ImageInterface
     */
    public function cropByRatio($ratio, ImageInterface $image)
    {
        $parsedRatio = sscanf($ratio, '%d:%d');
        if(count($parsedRatio) > 0 && $parsedRatio[0] !== null) {
            $imageSize = $image->getSize();
            $inputWidth = $imageSize->getWidth();
            $inputHeight = $imageSize->getHeight();
            if ($inputWidth < $inputHeight) {
                sort($parsedRatio);
            } else {
                asort($parsedRatio);
            }
            list($aspectWidth, $aspectHeight) = $parsedRatio;

            if (($aspectWidth / $aspectHeight) > ($inputWidth / $inputHeight)) {
                $outputWidth = $inputWidth;
                $outputHeight = $inputWidth * $aspectHeight / $aspectWidth;
            } elseif (($aspectWidth / $aspectHeight) < ($inputWidth / $inputHeight)) {
                $outputWidth = $inputHeight * $aspectWidth / $aspectHeight;
                $outputHeight = $inputHeight;
            } else {
                $outputWidth = $inputWidth;
                $outputHeight = $inputHeight;
            }

            $box = new Box($outputWidth, $outputHeight);

            return $image->thumbnail($box, ImageInterface::THUMBNAIL_OUTBOUND);
        }
        return $image;
    }

    public function saveFile(UploadedFile $file, $path, $attributes)
    {
        if (preg_match('/.gif$/i', $path)) {
            // GIF изображения сохраняем как есть.
            $file->saveAs($path);
        } else {
            $image = Image::getImagine()->open($file->tempName);
            if ($attributes) {
                $image = $this->cropPosition($attributes, $image);
            }
            Image::autorotate($image);
            $image->save($path);
        }
    }


    public function save($model, $inputName, $modelField)
    {
        if (Yii::$app->request->isConsoleRequest) {
            return $model;
        }
        if ($model && ($model->load( Yii::$app->request->post() ) || $this->forModel)) {

            $filesModel = false;
            if ($this->forModel) {
                $file = UploadedFile::getInstanceByName($inputName);
            } else {
                $file = UploadedFile::getInstance($model, $inputName);
            }
            $className = \yii\helpers\StringHelper::basename(get_class($model));
            $post = Yii::$app->request->post($className);
            if ($file !== null) {
                $filesModel = new Files();

                $filesModel->{$this->modelFieldFileName} = $file->name;

                $filePath = $this->getFilepath($className, $file->name);

                $filesModel->{$this->modelFieldFileSrc} = $filePath;

                $descriptionName = $inputName . '_description';
                if (isset($post[$descriptionName])) {
                    $filesModel->{$this->modelFieldFileDescription} = $post[$descriptionName];
                }
                if (
                    isset($model->x)
                    && isset($model->y)
                    && isset($model->width)
                    && isset($model->height)
                ) {
                    $attributes = $model->getAttributes(['x', 'y', 'width', 'height']);
                }
                $this->saveFile($file, $_SERVER['DOCUMENT_ROOT'] . $filePath, $attributes);

                $filesModel->save();
                $model->{$modelField} = $filesModel->id;
            } else {
                if ($this->forModel) {
                    $model->addError('empty_image', 'Передайте картинку');

                    return $model;
                }
                if (!$model->isNewRecord) {
                    $removeName = $inputName . '_remove';
                    if (isset($post[$removeName])) {
                        $model->{$modelField} = '';
                    }
                    $currentFile = Files::find()->where(['id' => $model->{$modelField}])->one();
                    if ($currentFile) {
                        $descriptionName = $inputName . '_description';
                        if (isset($post[$descriptionName])) {
                            $currentFile->{$this->modelFieldFileDescription} = $post[$descriptionName];
                        }
                        $currentFile->save();
                    }
                }
            }

            if ($this->forModel) {
                return $filesModel;
            }

            return $model;
        }
    }


    public function getFilepath($subfolder, $filename)
    {
        $filename = $this->generateFilename($filename);
        $dirname = $this->generateDirname($subfolder, $filename);
        self::createDir($_SERVER['DOCUMENT_ROOT'] . $dirname);

        return $dirname . '/' . $filename;
    }

    public function generateFilename($filename)
    {
        return md5($filename . time() . mt_rand()) . '.' . pathinfo($filename, PATHINFO_EXTENSION);
    }

    public function generateDirname($subfolder, $filename)
    {
        return $this->uploadPath . strtolower($subfolder) . '/' . substr($filename, 0, 3);
    }

    public function saveFileEvent($event)
    {
        foreach ($this->inputFileName as $inputName => $modelField) {
            $event->sender = $this->save($event->sender, $inputName, $modelField);
        }
    }

    private static function createDir($path)
    {
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }
    }

}