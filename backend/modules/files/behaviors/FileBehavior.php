<?php

namespace backend\modules\files\behaviors;

use yii\web\UploadedFile;

class FileBehavior extends ImageBehavior
{

    public $inputFileName = [['file' => 'file_id']];
    public $uploadPath = '/upload/files/';

    public function saveFile(UploadedFile $file,$path,$attributes)
    {
        $file->saveAs($path);
    }
}