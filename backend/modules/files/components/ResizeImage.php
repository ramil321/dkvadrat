<?php

namespace backend\modules\files\components;

use Imagine\Image\Box;
use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;
use yii\imagine\Image;

class ResizeImage extends Component
{

    public $resizePath = '/upload/resize';

    const RESIZE_IMAGE_PROPORTIONAL = 1;
    const RESIZE_IMAGE_CROP = 2;

    public function crop($path, $width = 100, $height = 100, $quality = 100){
        return $this->resizeImage($path, self::RESIZE_IMAGE_CROP, $width, $height, $quality);
    }
    public function prop($path, $width = 100, $height = 100, $quality = 100){
        return $this->resizeImage($path, self::RESIZE_IMAGE_PROPORTIONAL, $width, $height, $quality);
    }
    private function resizeImage($path, $resizeType = 1, $width = 100, $height = 100, $quality = 100){

        if(!$path){
            return '';
        }

        if (preg_match('/.gif$/i', $path)) {
            // GIF изображения сохраняем как есть.
            return $path;
        }

        $path = str_replace(Yii::$app->urlManagerFrontend->baseUrl,'',$path);
       /* if(!is_array(getimagesize($path))){
            return '';
        }*/
        $fileResizePath = $this->resizePath.$path;
        $pathParts = pathinfo($fileResizePath);
        $resizeFileDir = $pathParts['dirname'];

        $resizeFileName = $pathParts['filename'].implode('-',[$resizeType,$width,$height,$quality]).'.'.$pathParts['extension'];
        $resizeFilePath = $_SERVER['DOCUMENT_ROOT']. $resizeFileDir .'/'.$resizeFileName;

        if (!file_exists($resizeFilePath)) {

            if(file_exists($_SERVER['DOCUMENT_ROOT']. $path)) {

                self::createDir( $_SERVER['DOCUMENT_ROOT']. $resizeFileDir . '/' );
                if ($resizeType == 1) {
                    Image::getImagine()
                        ->open( $_SERVER['DOCUMENT_ROOT']. $path )
                        ->thumbnail( new Box( $width, $height ) )
                        ->save( $resizeFilePath, ['quality' => $quality] );
                }
                if ($resizeType == 2) {
                    Image::thumbnail( $_SERVER['DOCUMENT_ROOT']. $path, $width, $height )
                        ->save( $resizeFilePath, ['quality' => $quality] );
                }
            }
        }
        return Yii::$app->urlManagerFrontend->createUrl($resizeFileDir.'/'.$resizeFileName);
    }
    private static function createDir($path){
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }
    }
}