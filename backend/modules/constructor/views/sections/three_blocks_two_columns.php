<div class="template--three_blocks_two_columns">
    <div class="template__wrapper">
        <div class="container">
            <h2 class="template__title">Медиа</h2>
            <ul class="template__list template__list--media">
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="video-svg">
                                            <use xlink:href="#video"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
                <li class="template__item">
                    <div class="item item--media">
                        <figure class="item__figure">
                            <div class="item__image"><img src="images/journal/image1.jpg" alt="" class="item__img"></div><a href="#" class="item__category">Новости</a>
                            <figcaption class="item__figcaption">
                                <h4 class="item__title"><a href="#" class="item__title-link">
                                        <svg viewBox="0 0 14 9" class="photo-svg">
                                            <use xlink:href="#photo"></use>
                                        </svg>Алексей Прасолов: «Мы сверяем часы»</a></h4>
                            </figcaption>
                        </figure>
                    </div>
                </li>
            </ul>
        </div>
        <div class="template__nav">
            <div class="slider__prev-wrapper">
                <button class="slider__prev-button">
                    <svg viewBox="0 0 10 18" class="arrow-left">
                        <use xlink:href="#arrow-left"></use>
                    </svg>
                </button>
            </div>
            <div class="slider__next-wrapper">
                <button class="slider__next-button">
                    <svg viewBox="0 0 10 18" class="arrow-right">
                        <use xlink:href="#arrow-right"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
</div>