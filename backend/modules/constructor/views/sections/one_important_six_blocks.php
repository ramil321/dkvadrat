<div class="section-dots">
    <div class="section-dots__title-wrapper">
        <div class="section-dots__title-header">
            <h2 class="section-dots__title"><?=$name?></h2>
            <div class="section-dots__right">
                <div class="popular">
                    <ul class="popular__list">
                        <?foreach($tags as $tag):?>
                            <li class="popular__item"><a href="<?=$tag->getUrl()?>" class="popular__link"><?=$tag->name?></a></li>
                        <?endforeach?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="dots">
            <div class="dots__wrapper">
                <div class="dots__line"></div>
                <div class="dots__line"></div>
            </div>
        </div>
    </div>
    <ul class="template template--one_important_six_blocks">
        <li data-position="0" class="template__item">
        </li>
        <li data-position="1" class="template__item">
        </li>
        <li data-position="2" class="template__item">
        </li>
        <li data-position="3" class="template__item">
        </li>
        <li data-position="4" class="template__item">
        </li>
        <li data-position="5" class="template__item">
        </li>
        <li data-position="6" class="template__item">
        </li>
        <li data-position="7" class="template__item">
        </li>
    </ul>
</div>