<div class="section-dots">
    <div class="section-dots__title-wrapper">
        <div class="section-dots__title-header">
            <h2 class="section-dots__title"><?=$name?></h2>
            <div class="section-dots__right">
                <div class="popular">
                    <ul class="popular__list">
                        <?foreach($tags as $tag):?>
                            <li class="popular__item"><a href="<?=$tag->getUrl()?>" class="popular__link"><?=$tag->name?></a></li>
                        <?endforeach?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="dots">
            <div class="dots__wrapper">
                <div class="dots__line"></div>
                <div class="dots__line"></div>
            </div>
        </div>
    </div>
    <ul class="template template--one_important_four_blocks">
        <li class="template__item" data-position="0">
        </li>
        <li class="template__item" data-position="1">
        </li>
        <li class="template__item" data-position="2">
        </li>
        <li class="template__item" data-position="3">
        </li>
        <li class="template__item" data-position="4">
        </li>
    </ul>
</div>