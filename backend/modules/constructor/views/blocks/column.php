<?if (! $model) return;?>
<div class="item item--news-day-small"><a href="<?=$model->url?>" class="item__block-link"></a>
    <figure class="item__figure">
        <a href="<?= $model->mainCategory->url ?>" class="item__category">
            <?= $model->mainCategory->name ?>
        </a>
        <div class="item__image">
            <img src="<?=$model->detailImage->src?>" alt="" class="item__img">
        </div>
        <figcaption class="item__figcaption"><span style="display: none" class="item__span"></span>
            <h5 class="item__title"><?=$model->name?></h5>
            <div class="item__info info-news">
                <ul class="info-news__list">
                    <li class="info-news__item">
                        <svg viewBox="0 0 12 12" class="time-svg">
                            <use xlink:href="#time"></use>
                        </svg>
                        <span class="info-news__span"><?=Yii::$app->formatter->asShortDate($model->publish_date);?></span>
                    </li>
                    <li class="info-news__item">
                        <svg viewBox="0 0 21 12" class="view-svg">
                            <use xlink:href="#view"></use>
                        </svg>
                        <span class="info-news__span"><?=number_format($model->views_count, 0, '.', ' ')?></span>
                    </li>
                    <?if($model->durationRead):?>
                    <li class="info-news__item">
                        <svg viewBox="0 0 12 14" class="timer-svg">
                            <use xlink:href="#timer"></use>
                        </svg>
                        <span class="info-news__span"><?=$model->durationRead?></span>
                    </li>
                    <?endif;?>
                </ul>
            </div>
        </figcaption>
    </figure>
</div>
