<div class="subscribe-social">
    <div class="subscribe-social__wrapper">
        <div class="subscribe-social__dots-line">
            <div class="dots">
                <div class="dots__wrapper">
                    <div class="dots__line"></div>
                    <div class="dots__line"></div>
                </div>
            </div>
        </div><span class="subscribe-social__span">Подписывайтесь на наши страницы в социальных сетях</span>
        <div class="subscribe-social__list">
            <div class="social">
                <ul class="social__list">
                    <li class="social__item"><a href="#" class="social__link social__link--facebook">
                            <div class="social__icon">
                                <svg viewBox="0 0 10 21" class="facebook-svg">
                                    <use xlink:href="#facebook"></use>
                                </svg>
                            </div></a></li>
                    <li class="social__item"><a href="#" class="social__link social__link--vk">
                            <div class="social__icon">
                                <svg viewBox="0 0 24 13" class="vk-svg">
                                    <use xlink:href="#vk"></use>
                                </svg>
                            </div></a></li>
                    <li class="social__item"><a href="#" class="social__link social__link--ok">
                            <div class="social__icon">
                                <svg viewBox="0 0 13 21" class="ok-svg">
                                    <use xlink:href="#ok"></use>
                                </svg>
                            </div></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>