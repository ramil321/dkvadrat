<div class="email-sub">
    <div class="email-sub__wrapper">
        <div class="email-sub__dots-line">
            <div class="dots">
                <div class="dots__wrapper">
                    <div class="dots__line"></div>
                    <div class="dots__line"></div>
                </div>
            </div>
        </div><span class="email-sub__span">E-mail, чтобы подписаться на рассылку</span>
        <form class="email-sub__form">
            <input type="email" required="" class="email-sub__input">
            <button type="submit" class="email-sub__button">Подписаться</button>
        </form>
    </div>
</div>