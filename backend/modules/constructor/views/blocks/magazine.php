<div class="read-number">
    <div class="read-number__wrapper">
        <div class="read-number__dots-line">
            <div class="dots">
                <div class="dots__wrapper">
                    <div class="dots__line"></div>
                    <div class="dots__line"></div>
                </div>
            </div>
        </div>
        <div class="read-number__image"><img src="./images/shine.png" alt=""></div><a href="" class="read-number__button">Читать номер</a>
    </div>
</div>