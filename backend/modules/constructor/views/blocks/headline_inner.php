<?if (! $model) return;?>
<div class="home__header" style="background: url(<?=$model->getHeadlineImageStyled()->getSrc()?>) center center no-repeat;     background-size: cover;" data-position="0">
    <div class="container">
        <div class="home__header-wrapper">
            <?=\frontend\widgets\currency\Currency::widget()?>
            <div class="home__news home-news">
                <a href="<?=$model->mainCategory->url?>" class="home-news__category"><?=$model->mainCategory->name?></a>
                <a href="<?=$model->url?>" class="home-news__title"><?=$model->name?></a></div>
            <div class="home__news-feed-wrapper">

                <div class="home__news-feed">
                    <?=\frontend\widgets\shortnews\ShortNewsWidget::widget();?>
                </div>
            </div>
        </div>
    </div>
</div>