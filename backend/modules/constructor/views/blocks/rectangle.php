<?if (! $model) return;?>
<div class="item item--no-time">
    <figure class="item__figure">
        <div class="item__figure-top">
            <a href="<?= $model->mainCategory->url; ?>" class="item__category"><?= $model->mainCategory->name ?></a>
        </div>
        <div class="item__image">
            <img src="<?=$model->getPreviewImageStyled()->getSrc();?>" alt="" class="item__img <?=$model->getPreviewImageStyled()->getCssClass()?> ">
        </div>
        <figcaption class="item__figcaption">
            <h4 class="item__title"> <a href="<?= $model->url?>" class="item__title-link"><?= $model->name?></a></h4>
        </figcaption>
    </figure>
</div>