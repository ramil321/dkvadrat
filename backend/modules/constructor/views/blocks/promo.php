<?if(!$model) return '';?>
<a
    href="<?=$model->link?>" <?=($model->blank ? 'target="_blank"' : '')?>
    style="background-image: url(<?=Yii::$app->resize->prop($model->getPreviewImageStyled()->getSrc(), 500, 500)?>)"
    class="banner banner--square">
</a>