<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\constructor\models\ConstructorTemplates */

$this->title = 'Обновить шаблон: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны конструктора', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="constructor-templates-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
