<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\constructor\models\ConstructorTemplates */

$this->title = 'Создать шаблон конструктора';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны конструктора', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="constructor-templates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
