<?php

namespace backend\modules\constructor\models;


use backend\modules\constructor\models\blocks\BlockInterface;

class RenderBlocks
{

    public $renderRest = false;

    protected $blockNamespace = '\\backend\\modules\\constructor\\models\\blocks\\';

    public function __construct($renderRest = false)
    {
        $this->renderRest = $renderRest;
    }

    public function getTemplates()
    {
        $templates = ConstructorTemplates::find()->indexBy('code')->asArray()->all();
        foreach ($templates as $key => $value) {
            if (empty($value['class'])) {
                $value['class'] = $this->blockNamespace . 'BlockMaterial';
            } else {
                $value['class'] = $this->blockNamespace . $value['class'];
            }
            $templates[$key] = $value;
        }

        return $templates;

    }

    public function render($blocks)
    {
        $templates = $this->getTemplates();
        $resultBlocks = [];
        foreach ($blocks as $key => $block) {
            if (empty($block['template_code']) || !array_key_exists($block['template_code'], $templates)) {
                continue;
            }
            $template = $templates[$block['template_code']];
            $block['class'] = $template['class'];
            /** @var BlockInterface $blockContent */
            $blockContent = \Yii::createObject($block);
            $blockContent->setTemplate($template);
            if($this->renderRest){
                $resultBlock = $blockContent->getResultRest();
            }else{
                $resultBlock = $blockContent->getResult();
                $resultBlock['value'] = $this->flushContent($blockContent->getResult(), $blockContent->getTemplate()['code']);
            }

            $resultBlocks[$resultBlock['position']] = $resultBlock;
        }

        return $resultBlocks;
    }

    public function flushContent($result, $template)
    {
        if(!$template){
            return '';
        }
        return $result ? \Yii::$app->view->renderFile('@backend/modules/constructor/views/blocks/'.$template.'.php', $result) : '';
    }
}
