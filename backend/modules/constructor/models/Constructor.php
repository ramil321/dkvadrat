<?php

namespace backend\modules\constructor\models;

use yii\helpers\Json;

/**
 * This is the model class for table "constructor".
 * @property int    $id
 * @property int    $special_projects_id
 * @property int    $position
 * @property string $template
 * @property string $settings
 * @property string $type
 */
class Constructor extends \yii\db\ActiveRecord
{
    const TYPE_MAIN = 'main';
    const TYPE_SPEC = 'spec';

    public $request;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'constructor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position', 'special_projects_id'], 'integer'],
            [['template', 'settings', 'type'], 'string'],
            [['template'], 'validateTemplate'],
            [['type'], 'validateType'],
            ['request', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template' => 'Template',
            'settings' => 'Settings'
        ];
    }

    public function validateTemplate()
    {
        $template_code = $this->request['template_code'];
        if (!ConstructorTemplates::find()->where(['code' => $template_code])->one()) {
            $this->addError('validateTemplate', 'Шаблона ' . $template_code . ' не существует');
        }
    }

    public function validateType()
    {
        $type = $this->request['type'];
        if ($type != self::TYPE_SPEC && $type != self::TYPE_MAIN) {
            $this->addError('validateType', 'Значение ' .$type . ' не верное, должно быть ' . self::TYPE_SPEC . ' либо ' . self::TYPE_MAIN);
        }
    }

    public function beforeValidate()
    {
        $this->setAttributs();
        return parent::beforeValidate();
    }

    public function setAttributs()
    {
        $this->special_projects_id = isset($this->request['special_projects_id']) ? $this->request['special_projects_id'] : null;
        $this->position = isset($this->request['position']) ? $this->request['position'] : null;
        $this->type = isset($this->request['type']) ? $this->request['type'] : null;

        if ($this->type == self::TYPE_SPEC){
            $defaultSettings = \Yii::$app->params['specialProjectsSectionDefaultSettings'];
        } else {
            $defaultSettings = \Yii::$app->params['constructorSectionDefaultSettings'];
        }

        if (isset($this->request['settings'])) {
            $settings = $this->request['settings'];
            $settings = array_intersect_key($defaultSettings, $settings);
            $this->settings = Json::encode($settings);
        } else {
            $this->settings = Json::encode($defaultSettings);
        }
        $this->template = Json::encode([
            'name' => $this->request['name'],
            'tagIds' => $this->request['tagIds'],
            'blocks' => $this->request['blocks'],
            'template_code' => $this->request['template_code'],
        ]);
    }

}
