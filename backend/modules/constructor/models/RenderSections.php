<?php

namespace backend\modules\constructor\models;


use backend\modules\rest\models\Tags;
use Symfony\Component\DomCrawler\Crawler;
use yii\helpers\ArrayHelper;

class RenderSections
{
    public $renderRest = false;

    public function __construct($renderRest = false)
    {
        $this->renderRest = $renderRest;
    }

    public function getTemplates()
    {
        return ConstructorTemplates::find()->indexBy('code')->asArray()->all();
    }

    public function render($sections, $type = Constructor::TYPE_MAIN)
    {
        $templates = $this->getTemplates();
        $tagIds = [];
        $tags = [];
        foreach ($sections as $key => $section) {
            $sections[$key]['template'] = $templates[$section['template_code']]['template'];
            if (is_array($section['tagIds'])) {
                $tagIds = ArrayHelper::merge($section['tagIds'], $tagIds);
            }
        }
        $sb = new RenderSectionSettings($type);
        $rb = new RenderBlocks($this->renderRest);
        if ($tagIds) {
            $tags = Tags::find()->where(['id' => $tagIds])->indexBy('id')->all();
        }

        foreach ($sections as $key => $section) {
            $sections[$key]['tags'] = [];
            if (is_array($section['tagIds'])) {
                $sections[$key]['tags'] = array_map(function ($tag) use ($tags) {
                    return $tags[$tag];
                }, $section['tagIds']);
                unset($sections[$key]['tagIds']);
            }
            $renderedBlocks = [];
            if (is_array($section['blocks'])) {
                $renderedBlocks = $rb->render($section['blocks']);
            }
            $sections[$key]['blocks'] = $renderedBlocks;
            $sections[$key]['settings'] = $sb->render($section['settings']);
        }

        return $this->processTemplate($sections);
    }

    public function flushContent($result, $template)
    {
        if(!$template){
            return '';
        }
        return $result ? \Yii::$app->view->renderFile('@backend/modules/constructor/views/sections/'.$template.'.php', $result) : '';
    }

    protected function processTemplate($sections)
    {
        foreach ($sections as $key => $section) {
            if ($section['template']) {
                $section['template'] = $this->flushContent($section,$section['template_code']);
                $xml = new Crawler($section['template']);
                foreach ($xml->filterXPath('//*[@data-position]') as $rowNode) {
                    $position = $rowNode->getAttribute('data-position');
                    $rowNode->nodeValue = $section['blocks'][$position]['value'];
                }
                $html = $xml->filter('body')->html();
                /** минификация $html */
                $html = preg_replace(['/<!--(.*)-->/Uis', "/[[:blank:]]+/"], ['', ' '], $html);
                $html = str_replace(["\r", "\n", "\t"], '', $html);
                $sections[$key]['blocks'] = array_values($sections[$key]['blocks']);
                $sections[$key]['value'] = htmlspecialchars_decode($html);
            }
        }

        return $sections;
    }

}