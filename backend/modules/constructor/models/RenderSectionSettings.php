<?php

namespace backend\modules\constructor\models;

use yii\helpers\ArrayHelper;

class RenderSectionSettings
{
    protected $defaultSettings;

    public function __construct($type)
    {
        if($type == Constructor::TYPE_MAIN){
            $this->defaultSettings = \Yii::$app->params['constructorSectionDefaultSettings'];
        } elseif ($type == Constructor::TYPE_SPEC){
            $this->defaultSettings = \Yii::$app->params['specialProjectsSectionDefaultSettings'];
        }
    }

    public function render($settings)
    {
        return ArrayHelper::merge($this->defaultSettings, $settings ? $settings : []);
    }
}