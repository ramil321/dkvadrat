<?php

namespace backend\modules\constructor\models;

use Yii;

/**
 * This is the model class for table "constructor_templates".
 *
 * @property int $id
 * @property string $code
 * @property int $category
 * @property string $template
 * @property string $variables
 */
class ConstructorTemplates extends \yii\db\ActiveRecord
{
    public $categories = [
        'Секции',
        'Элементы'
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'constructor_templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'category', 'template'], 'required'],
            [['category'], 'integer'],
            [['template', 'variables','description'], 'string'],
            [['code','class'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Уникальный код шаблона',
            'category' => 'Категория',
            'template' => 'Шаблон',
            'variables' => 'Возможные переменные шаблона',
            'description' => 'Описание'
        ];
    }
}
