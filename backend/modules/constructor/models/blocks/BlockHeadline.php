<?php

namespace backend\modules\constructor\models\blocks;

use Yii;
use yii\db\ActiveRecord;

class BlockHeadline implements BlockInterface
{
    public $class = 'backend\modules\rest\models\Materials';
    public $template;
    public $position;
    public $material_id;

    /**
     * @param null $data
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function blocksContent($data = null)
    {
        /** @var ActiveRecord $model */
        $model = Yii::createObject([
            'class' => $this->class,
        ]);
        $material = $model::findOne(['id' => $this->material_id]);
        if ($material) {
            return $material->toArray([
                'id',
                'name',
                'detail_url',
                'short_name',
                'mainCategory',
                'previewImage',
                'publishDateFormated',
                'headlineImage'
            ]);
        }

        return null;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function getResultRest()
    {
        return [
            'code' => $this->template['code'],
            'template' => $this->template['template'],
            'variables' => $this->template['variables'],
            'material' => $this->blocksContent(),
            'position' => $this->position
        ];
    }


    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getResult()
    {
        return [
            'code' => $this->getTemplate()['code'],
            'template' => $this->getTemplate()['template'],
            'variables' => $this->getTemplate()['variables'],
            'material' => $this->blocksContent(),
            'position' => $this->position
        ];
    }
}
