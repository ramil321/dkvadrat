<?php

namespace backend\modules\constructor\models\blocks;

use common\models\material\Material;
use common\models\material\MaterialCategories;
use Yii;
use yii\db\ActiveRecord;

class Banner implements BlockInterface
{
    public $class = 'backend\modules\rest\models\Banner';
    public $template;
    public $position;
    public $material_id;

    public function getResult()
    {
        if($this->material_id === null) {
            return [];
        }
        /** @var ActiveRecord $model */
        $model = Yii::createObject([
            'class' => $this->class,
        ]);

        $item = $model::findOne(['id' => $this->material_id]);

        return [
            'code' => $this->getTemplate()['code'],
            'template' => $this->getTemplate()['template'],
            'variables' => $this->getTemplate()['variables'],
            'model' => $item,
            'position' => $this->position
        ];
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function getResultRest()
    {
        return [
            'code' => $this->template['code'],
            'template' => $this->template['template'],
            'variables' => $this->template['variables'],
            'position' => $this->position,
            'model' => $this->getResult(),
        ];
    }
}
