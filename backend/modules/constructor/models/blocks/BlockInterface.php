<?php

namespace backend\modules\constructor\models\blocks;

interface BlockInterface
{
    public function getTemplate();

    public function setTemplate($template);

    public function getResultRest();

    public function getResult();
}
