<?php

namespace backend\modules\constructor\models\blocks;

use backend\modules\rest\models\Materials;
use Yii;
use yii\db\ActiveRecord;

class BlockLatestMaterials implements BlockInterface
{
    public $class = 'backend\modules\rest\models\Materials';
    public $template;
    public $position;
    public $limit = 5;

    public function blocksContent()
    {
        /** @var ActiveRecord $model */
        $model = Yii::createObject([
            'class' => $this->class,
        ]);

        $materials = $model::find()
            ->select(['material.id', 'material.name', 'material.priority', 'material.publish_date'])
            ->joinWith('materialCategories', true)
            ->orderBy(['publish_date' => SORT_DESC])
            ->limit($this->limit)
            ->all();
        $result = [];
        foreach ($materials as $material) {
            $result[] = $material->toArray([
                'id',
                'name',
                'detail_url',
                'short_name',
                'mainCategory',
                'previewImage',
                'publishDateFormated'
            ]);
        }

        return $result;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function getResult()
    {
        // TODO: Implement getResult() method.
    }

    public function getResultRest()
    {
        return [
            'code' => $this->getTemplate()['code'],
            'template' => $this->getTemplate()['template'],
            'variables' => $this->getTemplate()['variables'],
            'materials' => $this->blocksContent(),
            'position' => $this->position
        ];
    }
}
