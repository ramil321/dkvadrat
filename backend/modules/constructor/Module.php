<?php

namespace backend\modules\constructor;

/**
 * settings module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\constructor\controllers';

    public $templates = [
      'template1' => [
          'layoutPath' => '#'
      ]
    ];

    public $accessRoles = null;
    public $accessClass = null;
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
