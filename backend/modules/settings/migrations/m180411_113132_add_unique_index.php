<?php

use yii\db\Migration;

/**
 * Class m180411_113132_add_unique_index
 */
class m180411_113132_add_unique_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('settings_unique_key_section', '{{%settings}}', ['section', 'key'], true);
    }

    public function safeDown()
    {
        $this->dropIndex('settings_unique_key_section', '{{%settings}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180411_113132_add_unique_index cannot be reverted.\n";

        return false;
    }
    */
}
