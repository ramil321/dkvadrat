<?php

namespace backend\modules\area\widgets\modal;

use lo\widgets\modal\ModalAjaxAsset;
use yii\base\InvalidConfigException;

use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * Class ModalAjax
 *
 * @package lo\widgets\modal
 * @author  Lukyanov Andrey <loveorigami@mail.ru>
 */
class ModalAjax extends \lo\widgets\modal\ModalAjax
{


    public static $modalsString;

    /**
     * @inheritdocs
     */
    public function init()
    {

        Widget::init();

        $this->initOptions();

        echo $this->renderToggleButton() . "\n";


        self::$modalsString .= Html::beginTag('div', $this->options) . "\n";
        self::$modalsString .= Html::beginTag('div', ['class' => 'modal-dialog ' . $this->size]) . "\n";
        self::$modalsString .= Html::beginTag('div', ['class' => 'modal-content']) . "\n";
        self::$modalsString .= $this->renderHeader() . "\n";
        self::$modalsString .= $this->renderBodyBegin() . "\n";

        if ($this->selector) {
            $this->mode = self::MODE_MULTI;
        }
    }

    /**
     * @inheritdocs
     */
    public function run()
    {

        Widget::run();

        self::$modalsString .= "\n" . $this->renderBodyEnd();
        self::$modalsString .= "\n" . $this->renderFooter();
        self::$modalsString .= "\n" . Html::endTag('div'); // modal-content
        self::$modalsString .= "\n" . Html::endTag('div'); // modal-dialog
        self::$modalsString .= "\n" . Html::endTag('div');

        $this->registerPlugin('modal');


        /** @var View */
        $view = $this->getView();
        $id = $this->options['id'];

        ModalAjaxAsset::register($view);

        if (!$this->url && !$this->selector) {
            return;
        }

        switch ($this->mode) {
            case self::MODE_SINGLE:
                $this->registerSingleModal($id, $view);
                break;

            case self::MODE_MULTI:
                $this->registerMultyModal($id, $view);
                break;
        }

        if (!isset($this->events[self::EVENT_MODAL_SUBMIT])) {
            $this->defaultSubmitEvent();
        }

        $this->registerEvents($id, $view);
    }

}
