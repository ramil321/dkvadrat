<?php

use backend\modules\files\widgets\ImageInputWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
s
?>


<?php Pjax::begin([
    'enablePushState' => false,
    'enableReplaceState' => false,
]) ?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>


    <?= $form->field($model, 'imageInput')->widget(ImageInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'image',
        'descriptionLabel' => 'Автор',
        'inputAttribute' => 'imageInput',
    ]   );   ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="btn btn-default btn-xs spoiler-trigger" data-toggle="collapse">Снипет изображения</button>
        <span> {image_src} - путь до изображения</span>
    </div>
    <div class="panel-collapse collapse out">
        <div class="panel-body">
            <?= $form->field($model, 'value')->textarea(['rows' => '6'])->label(false); ?>
        </div>
    </div>
</div>
<?
$js  = <<< JS
$(function(){
    $(".spoiler-trigger").click(function() {
		$(this).parent().next().collapse('toggle');
	});
})
JS;
$this->registerJs($js);
?>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

