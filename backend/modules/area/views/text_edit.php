<?php

use backend\modules\area\models\Area;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


?>


<?php Pjax::begin([
    'enablePushState' => false,
    'enableReplaceState' => false,
]) ?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

    <?= Alert::widget() ?>
    <?= $form->field($model, 'value')->textarea(['rows' => '8']); ?>
    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

