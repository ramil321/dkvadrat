<?php
namespace backend\modules\rest;
/**
 * @OA\Schema()
 */
class ApiResponse
{
    /**
     * @OA\Property(format="int32")
     * @var int
     */
    public $code;
    /**
     * @OA\Property
     * @var string
     */
    public $result;
    /**
     * @OA\Property
     * @var string
     */
    public $errors;
}