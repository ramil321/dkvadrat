<?php

namespace backend\modules\rest;

use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\Cors;
use yii\web\ForbiddenHttpException;

class ActiveController extends \yii\rest\ActiveController
{
    public $serializer = [
        'collectionEnvelope' => 'items',
        'class' => 'yii\rest\Serializer'
    ];

    public function actions()
    {
        return [];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        unset($behaviors['rateLimiter']);
        $behaviors['corsFilter'] = [
            'class' => Cors::class,
            'cors' => [
                'Access-Control-Allow-Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'GET', 'PUT', 'DELETE', 'PATCH', 'OPTIONS'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Max-Age' => 3600,                 // Cache (seconds)
                'Access-Control-Expose-Headers' => ['*'],
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['admin', 'author'],
                ],
            ],
            'denyCallback' => function ($rule, $action) {
                throw new ForbiddenHttpException('You are not allowed to access this page');
            }
        ];
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST', 'OPTIONS'],
            'update' => ['PUT', 'PATCH', 'OPTIONS'],
            'delete' => ['DELETE', 'OPTIONS'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterAction($action, $result)
    {
        $newResult = ['errors' => [], 'result' => []];
        if (is_object($result)) {
            if (get_class(new ActiveDataProvider()) == get_class($result)) {

            } else {
                $newResult['errors'] = $result->getErrors();
            }
            if ($newResult['errors']) {
                return $this->serializeData($newResult);
            }
        }

        $result = parent::afterAction($action, $result);

        if ($result['errors']) {
            $newResult['errors'] = $result['errors'];
            unset($result['errors']);
        }
        if ($result['_meta']) {
            $newResult['_meta'] = $result['_meta'];
            unset($result['errors']);
        }

        $newResult['result'] = isset($result['items']) ? $result['items'] : $result;

        return $this->serializeData($newResult);
    }
}