<?php

namespace backend\modules\rest\controllers;


use backend\modules\rest\models\Users;
use common\models\journal\Journal;
use backend\modules\rest\ActiveController;
use common\models\Menu;
use common\models\TopMenu;

/**
 * @OA\Tag(
 *   name="journals",
 *   description="Журналы",
 * )
 */


class MenuController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Menu';
    /**
     * @OA\Get(path="/menu/top/",
     *   tags={"menu"},
     *   operationId="menu",
     *   summary="Меню",
     *   description="",
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Post(path="/menu/top/save/",
     *   tags={"menu"},
     *   summary="Сохранить меню",
     *   operationId="menu_top_save",
     *   description="",
     *   @OA\RequestBody(
     *       description="",
     *       required=true,
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *               type="array",
     *               @OA\Items(ref="#/components/schemas/Menu")
     *           )
     *       )
     *   ),
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     *
     */
    public function actions()
    {
        return [];
    }

    public function actionTop(){
        $tm = new TopMenu();
        return $tm->getMenu();
    }
    public function actionTopSave(){
        $result = [];
        $tm = new TopMenu();
        $saveMenu = [];
        foreach(\Yii::$app->request->getBodyParams() as $val){
           $saveMenu[] = json_decode($val);
        }
        if ($tm->load(['arMenu' => $saveMenu], '') && $tm->validate()) {
            $tm->save();
        }
       // pre($tm->getErrors());
        $result = $tm->arMenu;
        if($tm->getErrors()) {
            $result['errors'] = $tm->getErrors('arMenu');
        }

        return $result;
    }
}
