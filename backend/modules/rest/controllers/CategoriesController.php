<?php

namespace backend\modules\rest\controllers;

use common\models\material\MaterialCategories;
use backend\modules\rest\ActiveController;


/**
 * @OA\Tag(
 *   name="categories",
 *   description="Категории материалов",
 * )
 */
class CategoriesController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Categories';

    /**
     * @OA\Get(path="/categories/",
     *   tags={"categories"},
     *   operationId="categories_list",
     *   summary="Список категорий материалов",
     *   description="",
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actions()
    {
        return [];
    }

    public function actionIndex()
    {
        $categories = MaterialCategories::getFullTreeArray(function ($item) {
            /** @var $item MaterialCategories */
            return [
                'id' => $item->id,
                'name' => $item->name,
                'flag' => $item->getFlag(),
                'url' => $item->getUrl()
            ];

        });

        return $categories;
    }
}
