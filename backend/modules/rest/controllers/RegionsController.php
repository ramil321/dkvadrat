<?php

namespace backend\modules\rest\controllers;


use backend\modules\rest\models\Users;
use common\models\journal\Journal;
use backend\modules\rest\ActiveController;
use common\models\material\Material;
use common\models\material\MaterialRegion;

/**
 * @OA\Tag(
 *   name="journals",
 *   description="Журналы",
 * )
 */


class RegionsController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Users';
    /**
     * @OA\Get(path="/regions/",
     *   tags={"materials"},
     *   operationId="regions_list",
     *   summary="Список регионов",
     *   description="",
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */
    public function actions()
    {
        return [];
    }

    public function actionIndex(){
        $result = [];
        $regions = MaterialRegion::find()->all();
        foreach($regions as $region){
            $result[] = [
                'id' => $region->id,
                'name' => $region->name,
            ];
        }
        return $result;
    }
}
