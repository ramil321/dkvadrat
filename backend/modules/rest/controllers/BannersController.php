<?php

namespace backend\modules\rest\controllers;


use backend\models\banner\search\BannerSearch;
use backend\modules\rest\models\Banner;
use common\models\journal\Journal;
use backend\modules\rest\ActiveController;
use common\models\material\Material;
use common\models\material\MaterialRegion;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * @OA\Tag(
 *   name="banners",
 *   description="Баннеры",
 * )
 */


class BannersController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Banner';
    /**
     * @OA\Get(path="/banners/",
     *   tags={"banners"},
     *   operationId="banners",
     *   summary="Список баннеров",
     *   description="",
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Страница",
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="per-page",
     *     in="query",
     *     description="Количество на странице",
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Post(path="/banners/create/",
     *   tags={"banners"},
     *   summary="Создать баннер",
     *   operationId="banners_create",
     *   description="",
     *     @OA\RequestBody(
     *         description="",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Banner"),
     *     ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     *
     * @OA\Put(path="/banners/update/{id}/",
     *   tags={"banners"},
     *   summary="Обновить баннер",
     *   operationId="banner_update",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид баннера",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\RequestBody(
     *      description="",
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/Banner"),
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */
    public function actions()
    {
        return [];
    }

    public function actionIndex(){

        $params = \Yii::$app->request->bodyParams;

        $query = Banner::find();

        $query->joinWith('bannerCompany.bannerAdvertiser');
        $query->joinWith('image');
        $query->joinWith('bannerPosition');
        $query->where(['banner_position.code' => Banner::BANNER_POSITION]);
        $query->asArray();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $dataProvider->pagination->page = $params['page'];
        $dataProvider->pagination->pageSize = $params['per-page'];

        return $dataProvider;

    }

    public function actionCreate()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $request = Yii::$app->getRequest()->getBodyParams();
        if ($model->load($request, '')) {
            $model->save();
        }
        return $model;
    }

    public function actionUpdate($id)
    {
        $model = Banner::find()->where(['id' => $id])->one();
        $request = Yii::$app->getRequest()->getBodyParams();
        if ($model->load($request, '') && !$model->hasErrors()) {
            $model->save();
        }
        return $model;
    }

}
