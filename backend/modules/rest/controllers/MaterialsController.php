<?php

namespace backend\modules\rest\controllers;

use backend\modules\rest\ActiveController;
use backend\modules\rest\models\Materials;
use backend\modules\rest\models\search\MaterialsSearch;
use Yii;
use yii\filters\VerbFilter;


class MaterialsController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Materials';

    /**
     * @OA\Post(path="/materials/create/",
     *   tags={"materials"},
     *   summary="Добавить материал",
     *   operationId="materials_create",
     *   description="",
     *     @OA\RequestBody(
     *         description="Pet object that needs to be added to the store",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/PostMaterials"),
     *     ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Get(path="/materials/{id}/",
     *   tags={"materials"},
     *   operationId="materials_find",
     *   summary="Получить материал по id",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид материала",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse"))),
     * )
     * @OA\Get(path="/materials/priority/{section_id}/",
     *   tags={"materials"},
     *   operationId="materials_priority",
     *   summary="Приоритетный материал по id",
     *   description="",
     *   @OA\Parameter(
     *     name="section_id",
     *     in="path",
     *     description="Ид раздела",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse"))),
     * )
     * @OA\Put(path="/materials/update/{id}/",
     *   tags={"materials"},
     *   operationId="materials_update_by_id",
     *   summary="Обновить материал по id",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид материала",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\RequestBody(
     *       description="",
     *       required=true,
     *       @OA\JsonContent(ref="#/components/schemas/PostMaterials"),
     *    ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse"))),
     * )
     * @OA\Get(path="/materials/find/",
     *   tags={"materials"},
     *   operationId="materials_list",
     *   summary="Список материалов",
     *   description="",
     *   @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="Фильтр по названию материала",
     *     @OA\Schema(
     *         type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="materialCategories[]",
     *     in="query",
     *     description="Фильтр по категории",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *         ),
     *   ),
     *   @OA\Parameter(
     *     name="authorsIds[]",
     *     in="query",
     *     description="Авторы (передавать массив id пользователей)",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *         ),
     *   ),
     *   @OA\Parameter(
     *     name="tagIds[]",
     *     in="query",
     *     description="Теги (передавать массив id тегов)",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *         ),
     *   ),
     *   @OA\Parameter(
     *     name="themeIds[]",
     *     in="query",
     *     description="Темы (передавать массив id тем)",
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *         ),
     *   ),
     *   @OA\Parameter(
     *     name="createdAtRange",
     *     in="query",
     *     description="Фильтр по дате создания материала (08.11.2018 00:00:00-07.12.2018 23:59:59)",
     *     @OA\Schema(
     *        example="08.11.2018 00:00:00 - 07.12.2018 23:59:59",
     *        type="string"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Страница",
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="per-page",
     *     in="query",
     *     description="Количество на странице",
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse"))),
     * )
     */
    public function actions()
    {
        return [
            'create' => [
                'class' => 'yii\rest\CreateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->createScenario,
            ],
            'update' => [
                'class' => 'yii\rest\UpdateAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario' => $this->updateScenario,
            ],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors(); // TODO: Change the autogenerated stub
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                '*' => ['POST', 'GET', 'OPTIONS', 'PUT'],
            ],
        ];


        return $behaviors;
    }


    public function actionPriority($category)
    {
        $materials = Materials::find()
            ->select(['material.id', 'material.name', 'material.priority'])
            ->joinWith('materialCategories', true)
            ->where(['material_categories.id' => $category, 'priority' => true])
            ->orderBy(['publish_date' => SORT_DESC])
            ->limit(1)
            ->asArray()
            ->all();

        return $materials;
    }

    public function actionFind()
    {
        $searchModel = new MaterialsSearch();
        $res = $searchModel->search(Yii::$app->request->get());

        return $res;
    }

    public function actionIndex($id)
    {
        if ($id) {
            $materials = Materials::find();
            $materials->andWhere(['id' => $id]);
            return $materials->one();
        }

        return null;
    }
}
