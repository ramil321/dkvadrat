<?php
namespace backend\modules\rest\controllers;

use OpenApi\Annotations\OpenApi;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\helpers\Url;
use yii\web\Controller;
/**
 *
 * @OA\Server(url="/admin/rest/")
 * @OA\Server(url="/backend/web/rest/")
 * @OA\Info(
 *     title="Kvadrat API",
 *     version="1.0",
 *
 * )
 *
 * @OA\Tag(
 *   name="all",
 *   description="Общие",
 * )
 */


class DefaultController extends Controller
{


   /* public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];
        return $behaviors;
    }*/
    /**
     * @inheritdoc
     */
    public function actionIndex(){
       // echo($_SERVER['DOCUMENT_ROOT'].'/backend/modules/rest/');

        $openapi = \OpenApi\scan($_SERVER['DOCUMENT_ROOT'].'/backend/modules/rest/');
        header('Content-Type: application/x-yaml');
        return $openapi->toYaml();
    }
}