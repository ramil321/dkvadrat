<?php

namespace backend\modules\rest\controllers;


use backend\modules\rest\ActiveController;
use backend\modules\rest\models\Authors;
use yii\data\ActiveDataProvider;


/**
 * @OA\Tag(
 *   name="users",
 *   description="Пользователи",
 * )
 */
class UsersController extends ActiveController
{
    public $modelClass = Authors::class;

    /**
     * @OA\Get(path="/users/authors/",
     *   tags={"users"},
     *   operationId="users_authors_list",
     *   summary="Список авторов",
     *   description="",
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse"))),
     * )
     */
    public function actions()
    {
        return [];
    }

    /*
        public function init()
        {
            parent::init();
            \Yii::$app->user->enableSession = false;
        }
    */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        /*  $behaviors['authenticator'] = [
              'class' => HttpBasicAuth::className(),
              'auth' => [$this, 'actionAuth']
          ];*/

        //Yii::$app->authManager

        return $behaviors;
    }

    /*
        public function actionLogin()
        {
            $model = new Login();
            $model->load(Yii::$app->request->bodyParams, '');
            if ($token = $model->auth()) {
                return $token;
            } else {
                return $model;
            }
        }
    */
    public function actionAuthors()
    {
        $query = Authors::findByRole(Authors::ROLE_AUTHOR);
        $query->with('profile');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]],
            'pagination' => [
                'pageSize' => \Yii::$app->request->getQueryParam('per-page')
            ]
        ]);

        return $dataProvider;
    }
}
