<?php

namespace backend\modules\rest\controllers;

use backend\modules\constructor\models\Constructor;
use backend\modules\constructor\models\RenderBlocks;
use backend\modules\constructor\models\RenderSections;
use backend\modules\constructor\models\SpecialProjects;
use backend\modules\rest\ActiveController;
use backend\modules\rest\models\constructor\ConstructorRenderBlockPost;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\rest\DeleteAction;
use yii\web\ServerErrorHttpException;

/**
 * @OA\Tag(
 *   name="constructor",
 *   description="Конструктор",
 * )
 */
class ConstructorController extends ActiveController
{
    public $modelClass = '\backend\modules\constructor\models\Constructor';

    /**
     * @OA\Post(path="/constructor/render-block/",
     *   tags={"constructor"},
     *   summary="Отрендерить блок",
     *   operationId="onstructor_render_block",
     *   description="",
     *     @OA\RequestBody(
     *         description="Pet object that needs to be added to the store",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ConstructorRenderBlockPost"),
     *     ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Post(path="/constructor/create/",
     *   tags={"constructor"},
     *   summary="Создать секцию",
     *   operationId="constructor_create",
     *   description="",
     *     @OA\RequestBody(
     *         description="",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ConstructorSectionsPost"),
     *     ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Get(path="/constructor/{id}/",
     *   tags={"constructor"},
     *   summary="Получить отрендеренную секцию по ид",
     *   operationId="constructor_id",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид секции",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Get(path="/constructor/list/",
     *   tags={"constructor"},
     *   summary="Получить все секции",
     *   operationId="constructor_id_ist",
     *   description="",
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Get(path="/constructor/special-projects-list/{id}/",
     *   tags={"constructor"},
     *   summary="Получить секции спецпроекта",
     *   operationId="constructor_special_projects_list",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид спецпроекта",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   description="",
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Put(path="/constructor/update/{id}/",
     *   tags={"constructor"},
     *   summary="Обновить секцию",
     *   operationId="constructor_update",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид секции",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\RequestBody(
     *      description="",
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/ConstructorSectionsPost"),
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Delete(path="/constructor/delete/{id}/",
     *   tags={"constructor"},
     *   summary="Удалить секцию",
     *   operationId="constructor_delete",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид секции",
     *     required=true,
     *     @OA\Schema(
     *        type="integer"
     *     )
     *   ),
     *   @OA\Response(response=204, description="successful delete",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actions()
    {
        $actions = parent::actions();
        $actions['delete'] = [
            'class' => DeleteAction::class,
            'modelClass' => Constructor::class,
        ];
        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                '*' => ['POST', 'GET', 'OPTIONS', 'PUT', 'DELETE'],
            ],
        ];

        return $behaviors;
    }

    public function actionCreate()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $request = Yii::$app->getRequest()->getBodyParams();
        if ($model->load(['request' => $request], '') && $model->validate()) {
            $model->save();
        }

        return $model;
    }

    public function actionUpdate($id)
    {
        $model = Constructor::find()->where(['id' => $id])->one();
        $request = Yii::$app->getRequest()->getBodyParams();
        if ($model && $model->load(['request' => $request], '') && $model->validate()) {
            $model->save();
        }

        return $model;
    }

    public function actionIndex($id)
    {
        $model = Constructor::find()->where(['id' => $id])->one();
        $rs = new RenderSections(true);
        $template = Json::decode($model->template);
        $template['settings'] = Json::decode($model->settings);
        $template['id'] = Json::decode($model->id);
        $res = $model ? [$template] : [];

        return $rs->render($res);
    }

    public function actionList()
    {
        $res = [];
        $models = Constructor::find()->where(['type' => Constructor::TYPE_MAIN])
            ->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
            ->all();
        foreach ($models as $model) {
            $template = Json::decode($model->template);
            $template['settings'] = Json::decode($model->settings);
            $template['id'] = Json::decode($model->id);
            $template['position'] = Json::decode($model->position);
            $res[] = $template;
        }
        $rs = new RenderSections(true);
        return $rs->render($res, Constructor::TYPE_MAIN);
    }

    public function actionSpecialProjectsList($id)
    {
        $models = Constructor::find()->where(['type' => Constructor::TYPE_SPEC])->andWhere(['special_projects_id' => $id])->all();
        $specialPojects = SpecialProjects::find()->where(['id' => $id])->one();
        $res = [];
        foreach ($models as $model) {
            $template = Json::decode($model->template);
            $template['settings'] = Json::decode($model->settings);
            $template['id'] = Json::decode($model->id);
            $res[] = $template;
        }
        $rs = new RenderSections(true);

        if($specialPojects){
            $specialPojects = $specialPojects->toArray();
            $specialPojects['sections'] = $rs->render($res, Constructor::TYPE_SPEC);;
        }
        return $specialPojects;
    }

    public function actionRenderBlock()
    {
        $model = new ConstructorRenderBlockPost();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->validate()) {
            $rb = new RenderBlocks();

            return $rb->render([['material_id' => $model->material_id, 'template_code' => $model->code]]);
        }

        return $model;
    }
}
