<?php

namespace backend\modules\rest\controllers;


use backend\modules\rest\models\Themes;
use backend\modules\rest\ActiveController;

/**
 * @OA\Tag(
 *   name="themes",
 *   description="Темы",
 * )
 */


class ThemesController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Tags';
    /**
     * @OA\Get(path="/themes/find-by-name/{themename}/",
     *   summary="Получение темы по имени",
     *   tags={"themes"},
     *   operationId="find_theme_by_name",
     *   description="",
     *   @OA\Parameter(
     *     name="themename",
     *     in="path",
     *     description="Поиск по названию",
     *     required=true,
     *     @OA\Schema(
     *         type="string"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     *
     *
     *
     * @OA\Get(path="/themes/{id}/",
     *   summary="Получение темы по ид",
     *   tags={"themes"},
     *   operationId="find_theme_by_id",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Поиск по ид",
     *     required=true,
     *     @OA\Schema(
     *         type="string"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actions(){
        return [
            'find-by-name' => [
                'class' => 'backend\actions\ajax\FindLikeRestAction',
                'modelClass' => Themes::className()
            ]
        ];
    }
    public function actionIndex($id){
        if($id) {
            $res = [];
            $themes = Themes::find()
                ->joinWith(['materials' => function ($q) {
                    $q->limit(5);
                }])
           //     ->joinWith('materials', true)
                ->where( ['theme.id' => $id] )
                ->orderBy(['material.publish_date' => SORT_DESC])
                ->limit(5)
                ->asArray()
                ->all();
           // pre($themes);
            foreach($themes as $key=>$theme){
                $res = [
                    'id' => $theme['id'],
                    'name' => $theme['name'],
                ];
                foreach($theme['materials'] as $material){
                    $res['materials'][] = [
                        'id' => $material['id'],
                        'name' => $material['name'],
                        'url' => '#'
                    ];
                }
            }
            return $res;
        }
    }
}
