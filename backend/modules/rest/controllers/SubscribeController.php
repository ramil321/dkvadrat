<?php

namespace backend\modules\rest\controllers;


use backend\modules\newsletters\models\Subscribe;
use backend\modules\rest\ActiveController;

/**
 * @OA\Tag(
 *   name="subscribe",
 *   description="Подписки",
 * )
 */
class SubscribeController extends ActiveController
{
    public $modelClass = '\backend\modules\constructor\models\NewslettersSubscribers';
    /**
     * @OA\Post(path="/subscribe/subscribe-default/",
     *   tags={"subscribe"},
     *   summary="Подписатся",
     *   operationId="subscribe_default",
     *   description="",
     *     @OA\RequestBody(
     *         description="",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/NewslettersSubscribers"),
     *     ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actions()
    {
        return [];
    }

    public function actionSubscribeDefault()
    {
        $formSubscribe = new Subscribe();
        $formSubscribe->load(\Yii::$app->request->post(), '');
        if ($formSubscribe->validate()) {
            $resultSubscribe = \Yii::$app->subscribe->subscribe($formSubscribe);
            $errorsLists = $resultSubscribe->getErrors();
        } else {
            $errorsLists = $formSubscribe->getErrors();
        }

        foreach ($errorsLists as $errorList) {
            foreach ($errorList as $error) {
                $result['errors'][] = $error;
            }
        }

        if (! $result['errors']) {
            $result['items'] = ['id' => $resultSubscribe->id];
        }


        return $result;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['access']);
        return $behaviors;
    }
}
