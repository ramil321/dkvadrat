<?php

namespace backend\modules\rest\controllers;


use common\models\material\MaterialTags;
use backend\modules\rest\ActiveController;


/**
 * @OA\Tag(
 *   name="tags",
 *   description="Теги",
 * )
 */


class TagsController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Tags';
    /**
     * @OA\Get(path="/tags/find-by-name/{tagname}/",
     *   summary="Получение тега по имени",
     *   tags={"tags"},
     *   operationId="find_tag_by_name",
     *   description="",
     *   @OA\Parameter(
     *     name="tagname",
     *     in="path",
     *     description="Поиск по названию",
     *     required=true,
     *     @OA\Schema(
     *         type="string"
     *     )
     *   ),
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actions(){
        return [
            'find-by-name' => [
                'class' => 'backend\actions\ajax\FindLikeRestAction',
                'modelClass' => MaterialTags::className()
            ]
        ];
    }
    public function actionCreate(){

    }
}
