<?php

namespace backend\modules\rest\controllers;


use backend\modules\rest\ActiveController;
use Yii;
use yii\data\DataProviderInterface;

class MultiListController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Tags';
    public $controllersNameSpace = '\backend\modules\rest\controllers';
    public $modelsVariants = [
        'categories' => ['CategoriesController', 'actionIndex'],
        'journals' => ['JournalsController', 'actionIndex'],
        'authors' => ['UsersController', 'actionAuthors'],
        'regions' => ['RegionsController', 'actionIndex'],
    ];

    /**
     * @OA\Get(path="/multi-list/list/",
     *   summary="Получение множество моделей одновременно",
     *   tags={"all"},
     *   operationId="find_all",
     *   description="",
     *   @OA\Parameter(
     *         name="models[]",
     *         in="query",
     *         description="Список моделей [categories,authors,journals,regions]",
     *         required=true,
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="string"),
     *         ),
     *         style="form"
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actions()
    {
        return [];
    }

    public function actionIndex(array $models)
    {
        $controllersList = $models;
        $result = [];
        foreach ($controllersList as $key) {

            if (array_key_exists($key, $this->modelsVariants)) {
                $controller = $this->modelsVariants[$key][0];
                $obj = Yii::createObject(
                    ['class' => $this->controllersNameSpace . '\\' . $controller],
                    [$key, $this->module]
                );
                $objRes = call_user_func([$obj, $this->modelsVariants[$key][1]]);
                if ($objRes instanceof DataProviderInterface) {
                    $result[$key] = $objRes->getModels();
                } else {
                    $result[$key] = $objRes;
                }
            }
        }

        return $result;
    }
}
