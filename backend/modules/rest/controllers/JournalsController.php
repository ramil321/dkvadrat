<?php

namespace backend\modules\rest\controllers;


use backend\modules\rest\models\Users;
use common\models\journal\Journal;
use backend\modules\rest\ActiveController;
use yii\helpers\VarDumper;
use yii\base\Controller;

/**
 * @OA\Tag(
 *   name="journals",
 *   description="Журналы",
 * )
 */
class JournalsController extends ActiveController
//class JournalsController extends Controller
{
    public $modelClass = '\backend\modules\rest\models\Journal';

    /**
     * @OA\Get(path="/journals/",
     *   tags={"journals"},
     *   operationId="journals_list",
     *   summary="Список журналов",
     *   description="",
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     *
     * @OA\Get(path="/journals/last/",
     *   tags={"journals"},
     *   operationId="journals_last",
     *   summary="Последний журнал",
     *   description="",
     *   @OA\Response(response=200, description="successful operation", @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actionIndex()
    {
        $result = [];
        $journals = Journal::find()->orderBy(['date' => SORT_DESC])->all();
        foreach ($journals as $journal) {
            $result[] = [
                'id' => $journal->id,
                'number' => $journal->number,
                'dateFormated' => $journal->dateFormated,
                'image' => $journal->image->src
            ];
        }

        return $result;
    }


    /**
     * @return array
     */
    public function actionLast()
    {
        $journal = Journal::find()->orderBy(['date' => SORT_DESC])->one();

        return [
            'id' => $journal->id,
            'image' => $journal->image->src
        ];
    }
}
