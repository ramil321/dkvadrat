<?php

namespace backend\modules\rest\controllers;


use backend\modules\files\models\Files;
use backend\modules\rest\ActiveController;
use backend\modules\rest\models\Images;
use Yii;


/**
 * @OA\Tag(
 *   name="files",
 *   description="Работа с файлами",
 * )
 */
class ImageController extends ActiveController
{
    public $modelClass = '\backend\modules\rest\models\Image';

    /**
     * @OA\Post(path="/image/create/",
     *   tags={"files"},
     *   summary="Добавить материал",
     *   operationId="image_create",
     *   description="",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="x",
     *                     property="x",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     description="y",
     *                     property="y",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     description="width",
     *                     property="width",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     description="height",
     *                     property="height",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     description="ID файла",
     *                     property="image_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     description="Картинка для загрузки",
     *                     property="inputImage",
     *                     type="file",
     *                 ),
     *                 required={"file"}
     *             )
     *         ),
     *     ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Get(path="/image/",
     *   summary="Список картинок",
     *   tags={"files"},
     *   description="",
     *   operationId="image_list",
     *   @OA\Parameter(
     *         name="images[]",
     *         in="query",
     *         description="Ид картинок [1,2,3]",
     *         required=true,
     *         @OA\Schema(
     *           type="array",
     *           @OA\Items(type="integer"),
     *         ),
     *         style="form"
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */

    public function actionCreateCkEditor()
    {
        $images = new Images(['ckEditor' => true]);

        if ($images->load(Yii::$app->request->post(), '') && $images->validate()) {
            $funcNum = Yii::$app->request->get('CKEditorFuncNum');
            $src = $images->addImageCkEditor()->getPath();
            echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'
                . $funcNum . '", "' . $src . '", "Файл загружен" );</script>';
            die();
            // return $images->addImageCkEditor();
        }
    }

    public function actionCreate()
    {
        if(Yii::$app->request->post('image_id') !== null && Yii::$app->request->post('image_id') !== '') {
            $images = new Images(['fromExisting' => true, 'id' => Yii::$app->request->post('image_id')]);
            $images->attributes = Yii::$app->request->post();
            return $images->addImageFromExisting();
        }

        $images = new Images();

        if ($images->load(Yii::$app->request->post(), '') && $images->validate()) {
            return $images->addImage();
        }
    }

    public function actionIndex(array $images)
    {
        if ($images) {
            return Files::find()->where(['id' => $images])->all();
        }
    }
}
