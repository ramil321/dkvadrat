<?php

namespace backend\modules\rest\controllers;

use backend\modules\area\models\Area;
use backend\modules\rest\models\constructor\ConstructorRenderBlockPost;
use backend\modules\constructor\models\RenderBlocks;
use backend\modules\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use backend\modules\constructor\models\SpecialProjects;
use backend\modules\constructor\models\RenderSections;
use backend\modules\constructor\models\Constructor;
use yii\rest\DeleteAction;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use Yii;

class SpecialProjectsController extends ConstructorController
{
    public $modelClass = '\backend\modules\constructor\models\SpecialProjects';

    /**
     * @OA\Get(path="/special-projects/list/",
     *   tags={"special-projects"},
     *   summary="Получить все спецпроекты",
     *   operationId="special_projects_id_ist",
     *   description="",
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Get(path="/special-projects/",
     *   tags={"special-projects"},
     *   summary="Получить список спецпроектов",
     *   operationId="special_projects_index",
     *   description="",
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Post(path="/special-projects/create/",
     *   tags={"special-projects"},
     *   summary="Создать спецпроект",
     *   operationId="special_projects_create",
     *   description="",
     *     @OA\RequestBody(
     *         description="",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SpecialProjectsTemplates"),
     *     ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Put(path="/special-projects/update/{id}/",
     *   tags={"special-projects"},
     *   summary="Обновить спецпроект",
     *   operationId="special_projects_update",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид секции",
     *     required=true,
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\RequestBody(
     *      description="",
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/SpecialProjectsTemplates"),
     *   ),
     *   @OA\Response(response=200, description="successful operation",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     * @OA\Delete(path="/special-projects/delete/{id}/",
     *   tags={"special-projects"},
     *   summary="Удалить спецпроекты",
     *   operationId="special_projects_delete",
     *   description="",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Ид секции",
     *     required=true,
     *     @OA\Schema(
     *        type="integer"
     *     )
     *   ),
     *   @OA\Response(response=204, description="successful delete",
     *     @OA\Schema(ref="/backend/modules/rest/ApiResponse")),
     * )
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['delete'] = [
            'class' => DeleteAction::class,
            'modelClass' => SpecialProjects::class,
        ];
        return $actions;
    }

    public function actionIndex($id = null)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SpecialProjects::find(),
        ]);

        return $dataProvider;
    }

    public function actionList()
    {
        $res = [];
        $models = Constructor::find()->where(['type' => Constructor::TYPE_SPEC])->all();
        foreach ($models as $model) {
            $template = Json::decode($model->template);
            $template['settings'] = Json::decode($model->settings);
            $template['id'] = Json::decode($model->special_projects_id);
            $res[] = $template;
        }
        $rs = new RenderSections(true);

        return $rs->render($res);
    }

    public function actionCreate()
    {
        /** @var ActiveRecord $model */
        $model = new $this->modelClass;
        $request = Yii::$app->getRequest()->getBodyParams();
        if ($model->load($request, '')) {
            $model->save();
        }
        return $model;
    }

    public function actionUpdate($id)
    {
        $model = SpecialProjects::find()->where(['id' => $id])->one();
        $request = Yii::$app->getRequest()->getBodyParams();
        if ($model->load($request, '') && !$model->hasErrors()) {
            $model->save();
        }
        return $model;
    }
}
