<?php

namespace backend\modules\rest\models;

/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="id",
 *   type="int",
 *   description="Ид"
 * )
 * @OA\Property(
 *   property="name",
 *   type="string",
 *   description="Имя тега"
 * )
 */
class Tags extends \common\models\material\MaterialTags
{

}
