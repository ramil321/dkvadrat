<?php

namespace backend\modules\rest\models\search;

use backend\models\material\search\MaterialSearch as BaseMaterialSearch;
use backend\modules\rest\models\Materials;
use yii\data\ActiveDataProvider;


/**
 * @OA\Schema()
 * @OA\Property(
 *   property="name",
 *   example="",
 *   type="string",
 *   description="Фильтр по названию материала"
 * ),
 * @OA\Property(
 *   property="materialCategories",
 *   example="",
 *   type="integer",
 *   description="Фильтр по категории"
 * ),
 * @OA\Property(
 *   property="authorsIds",
 *   type="array",
 *   @OA\Items(
 *     type="integer",
 *   ),
 *   description="Авторы (передавать массив id пользователей)"
 * ),
 * @OA\Property(
 *   property="tagIds",
 *   type="array",
 *   @OA\Items(
 *     type="integer",
 *   ),
 *   description="Теги (передавать массив id тегов)"
 * ),
 * @OA\Property(
 *   property="themeIds",
 *   type="array",
 *   @OA\Items(
 *     type="integer",
 *   ),
 *   description="Темы (передавать массив id тем)"
 * ),
 * @OA\Property(
 *   property="createdAtRange",
 *   example="08.11.2018 00:00:00 - 07.12.2018 23:59:59",
 *   type="string",
 *   description="Фильтр по дате создания материала"
 * )
 * @OA\Property(
 *   property="per-page",
 *   example="",
 *   type="string",
 *   description="Количество элементов на странице"
 * )
 * @OA\Property(
 *   property="page",
 *   example="1",
 *   type="string",
 *   description="Страница"
 * )
 */
class MaterialsSearch extends BaseMaterialSearch
{
    public function search($params)
    {
        $query = Materials::find();
        $query->joinWith(['mainCategory', 'themes']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => $params['per-page']
            ]
        ]);
        $this->load($params, '');

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'priority' => $this->priority,
            'user.id' => $this->authorsIds,
            'material_tags.id' => $this->tagIds,
            'theme.id' => $this->themeIds,
            'material_categories.id' => $this->materialCategories,
        ]);

        $query->andFilterWhere(['between', 'material.created_at', $this->createdAtStart, $this->createdAtEnd]);
        $query->andFilterWhere(['between', 'material.updated_at', $this->updatedAtStart, $this->updatedAtEnd]);
        $query->andFilterWhere(['like', 'material.name', $this->name]);

        return $dataProvider;
    }
}
