<?php

namespace backend\modules\rest\models\post\constructor;

/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="code",
 *   example="block_1",
 *   type="string",
 *   description="Уникальный код щаблона"
 * )
 * @OA\Property(
 *   property="category",
 *   type="integer",
 *   description="Секция = 0, Элемент = 1"
 * )
 * @OA\Property(
 *   property="template",
 *   type="string",
 *   example="<div><h1><?=$name?></h1></div>",
 *   description="Шаблон блока или элемента, можно использовать php и html. Доступные переменные смотрите в variables.
 *   Для сецкий указывать data атрибуты для шаблонов и позиций data-templates='block_1,block_2' data-position='1'
 *   "
 * )
 * @OA\Property(
 *   property="variables",
 *   type="string",
 *   example="",
 *   description="Список переменных доступных в шаблоне"
 * )
 * @OA\Property(
 *   property="class",
 *   type="string",
 *   description="Класс обработки для Php"
 * )
 * @OA\Property(
 *   property="description",
 *   type="string",
 *   description="Описание"
 * )
 */


class ConstructorTemplates extends \backend\modules\constructor\models\ConstructorTemplates
{

}
