<?php

namespace backend\modules\rest\models\constructor;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="position",
 *   example="1",
 *   type="integer",
 *   description="Позиция блока в шаблоне"
 * )
 * @OA\Property(
 *   property="template_code",
 *   example="four_blocks",
 *   type="string",
 *   description="Код шаблона блока"
 * )
 * @OA\Property(
 *   property="material_id",
 *   example="10",
 *   type="integer",
 *   description="Ид материала"
 * )
 */

class ConstructorBlockPost
{

}
