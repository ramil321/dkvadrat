<?php

namespace backend\modules\rest\models\constructor;

use backend\modules\constructor\models\ConstructorTemplates;
use Yii;
use yii\base\Model;

/**
 * @OA\Schema()
 * @OA\Property(
 *   property="code",
 *   example="four_blocks",
 *   type="string",
 *   description="Уникальный код щаблона"
 * )
 * @OA\Property(
 *   property="material_id",
 *   type="integer",
 *   description="Ид материала"
 * )
 */
class ConstructorRenderBlockPost extends Model
{
    public $material_id;
    public $code;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'code'], 'required'],
            ['code', 'validateCode']
        ];
    }

    public function validateCode()
    {

        $template = ConstructorTemplates::find()->where(['code' => $this->code])->one();
        if ($template['class']) {
            $modelTemplate = Yii::createObject([
                'class' => $template['class'],
            ]);
            $model = Yii::createObject([
                'class' => $modelTemplate->class,
            ]);
            if ($this->material_id) {
                if (!$model::find()->where(['id' => $this->material_id])->one()) {
                    $this->addError('code', 'Элемент с material_id = ' . $this->material_id . ' не найден');
                }
            }
        } else {
            $this->addError('code', 'Не указан класс для обработки');
        }
    }
}
