<?php

namespace backend\modules\rest\models\constructor;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="name",
 *   type="string",
 *   description="Заголовок секции"
 * )
 * @OA\Property(
 *   property="type",
 *   example="main",
 *   type="string",
 *   description="Тип (main или spec)"
 * )
 * @OA\Property(
 *   property="special_projects_id",
 *   type="integer",
 *   description="Ид спецпроекта"
 * )
 * @OA\Property(
 *   property="position",
 *   type="integer",
 *   description="Номер позиции"
 * )
 * @OA\Property(
 *   property="template_code",
 *   example="four_blocks",
 *   type="string",
 *   description="Уникальный код щаблона"
 * )
 * @OA\Property(
 *   property="tagIds",
 *   type="array",
 *   @OA\Items(
 *     type="integer",
 *   ),
 *   description="Теги (передавать массив id тегов )"
 * )
 * @OA\Property(
 *   property="blocks",
 *   type="array",
 *   @OA\Items(
 *     ref="#/components/schemas/ConstructorBlockPost",
 *   ),
 *   description="Массив блоков"
 * )
 */


//blocks

class ConstructorSectionsPost
{

}
