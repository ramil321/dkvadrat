<?php

namespace backend\modules\rest\models\SpecialProjects;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="short_name",
 *   type="string",
 *   description="Короткое название"
 * )
 * @OA\Property(
 *   property="name",
 *   type="string",
 *   description="Заголовок секции"
 * )
 * @OA\Property(
 *   property="description",
 *   type="string",
 *   description="Описание"
 * )
 * @OA\Property(
 *   property="image_id",
 *   type="integer",
 *   description="Изображение-обложка"
 * )
 * @OA\Property(
 *   property="position",
 *   type="integer",
 *   description="Позиция блока"
 * )
 * @OA\Property(
 *   property="active",
 *   type="integer",
 *   description="Активна ли (0,1)"
 * )
 */

class SpecialProjectsTemplates
{

}
