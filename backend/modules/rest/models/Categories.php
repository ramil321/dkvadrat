<?php

namespace backend\modules\rest\models;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="id",
 *   type="integer",
 *   description="Ид"
 * )
 * @OA\Property(
 *   property="name",
 *   type="string",
 *   description="Название категории"
 * )
 */
class Categories extends \common\models\material\MaterialCategories
{

}
