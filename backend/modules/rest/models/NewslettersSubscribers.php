<?php

namespace backend\modules\rest\models;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="email",
 *   type="string",
 *   description="Почта"
 * )
 */
class NewslettersSubscribers extends \backend\modules\newsletters\models\NewslettersSubscribers
{

}
