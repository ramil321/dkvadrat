<?php

namespace backend\modules\rest\models;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="id",
 *   type="integer",
 *   description="Ид"
 * )
 * @OA\Property(
 *   property="fullName",
 *   type="string",
 *   description="Полное имя"
 * )
 * @OA\Property(
 *   property="photo",
 *   type="string",
 *   description="Фото"
 * )
 */
class Authors extends \common\models\user\User
{


}
