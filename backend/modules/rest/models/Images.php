<?php

namespace backend\modules\rest\models;

use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use yii\base\Model;
use yii\imagine\Image;

/**
 * @OA\Schema()
 * @OA\Property(
 *   property="id",
 *   type="integer",
 *   description="Ид"
 * )
 * @OA\Property(
 *   property="name",
 *   type="string",
 *   description="Имя картинки"
 * )
 * @OA\Property(
 *   property="src",
 *   type="string",
 *   description="Путь к картинке"
 * )
 */
class Images extends Model
{

    public $id;
    public $name;

    public $x;
    public $y;
    public $width;
    public $height;

    public $src;
    public $inputImage;
    public $upload;
    public $ckEditor = false;
    public $fromExisting = false;

    public function init()
    {
        if ($this->ckEditor) {
            $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'addImageCkEditor']);
        } elseif ($this->fromExisting) {
            $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'addImageFromExisting']);
        } else {
            $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'addImage']);
        }
        parent::init();
    }


    public function createThumbnail(Files $file, $attributes)
    {
        $newFile = new Files();
        $newFile->attributes = $attributes;
        $newFile->name = $file->name;
        $className = \yii\helpers\StringHelper::basename(__CLASS__);
        $ibh = new ImageBehavior();
        $path = $ibh->getFilepath($className, $file->name);
        $image = Image::getImagine()->open($_SERVER['DOCUMENT_ROOT'] . $file->originalSrc);
        if (
            array_key_exists('x', $attributes)
            && array_key_exists('y', $attributes)
            && array_key_exists('height', $attributes)
            && array_key_exists('width', $attributes)
        ) {
            $image = $ibh->cropPosition($attributes, $image);
        } elseif (array_key_exists('ratio', $attributes)) {
            Image::autorotate($image);
            $image = $ibh->cropByRatio($attributes['ratio'], $image);
        }
        $image->save($_SERVER['DOCUMENT_ROOT'] . $path);
        $newFile->src = $path;
        $newFile->save();
        $newFile->src = $_SERVER['REQUEST_SCHEME'] . '://' .$_SERVER['HTTP_HOST'] . $newFile->src;
        return $newFile;
    }

    public function addImageFromExisting()
    {
        $model = Files::find()->where(['id' => $this->id])->one();

        return $this->createThumbnail($model, $this->getAttributes(['x', 'y', 'width', 'height']));
    }

    public function addImage()
    {
        $ibh = new ImageBehavior(['forModel' => true]);
        $image = $ibh->save($this, 'inputImage', 'id');
        $image->src = $_SERVER['REQUEST_SCHEME'] . '://' .$_SERVER['HTTP_HOST'] . $image->src;
        return $image;
    }

    public function addImageCkEditor()
    {
        $ibh = new ImageBehavior(['forModel' => true]);

        return $ibh->save($this, 'upload', 'id');
    }

    public function rules()
    {
        return [
            [['x', 'y', 'width', 'height'], 'safe'],
            //[['x','y','width','height'],'integer'],
            // [['inputImage'], 'required'],
            [['inputImage', 'upload'], 'file'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }
}
