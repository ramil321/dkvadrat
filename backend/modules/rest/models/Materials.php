<?php

namespace backend\modules\rest\models;

use backend\modules\files\models\Files;

/**
 * @OA\Schema()
 * @OA\Property(
 *   property="id",
 *   type="integer",
 *   description="Ид"
 * )
 * @OA\Property(
 *   property="name",
 *   example="Название статьи",
 *   type="string",
 *   description="Название статьи"
 * )
 * @OA\Property(
 *   property="priority",
 *   type="boolean",
 *   description="Приоритет"
 * )
 * @OA\Property(
 *   property="show_theme_materials",
 *   type="boolean",
 *   description="Показ материалов по теме"
 * )
 * @OA\Property(
 *   property="show_comments",
 *   type="boolean",
 *   description="Комментарии (отображает/скрывает блок с комментариями)"
 * )
 * @OA\Property(
 *   property="show_content",
 *   type="boolean",
 *   description="Содержание (отображает/скрывает содержание по подзаголовкам материала)"
 * )
 * * @OA\Property(
 *   property="show_author",
 *   type="boolean",
 *   description="Автор (отображает/скрывает указание на автора в материале)"
 * )
 * @OA\Property(
 *   property="slug",
 *   type="string",
 *   description="Символьный код"
 * )
 * @OA\Property(
 *   property="region",
 *   ref="#/components/schemas/Regions",
 *   description="Регион материала"
 * )
 * @OA\Property(
 *   property="is_published",
 *   type="boolean",
 *   description="Материал опубликован"
 * )
 * @OA\Property(
 *   property="journal_id",
 *   ref="#/components/schemas/Journals",
 *   description="Статус материала"
 * )
 * @OA\Property(
 *   property="publishDateFormated",
 *   example="09.08.2018 05:25",
 *   type="string",
 *   description="Дата публикации"
 * )
 * @OA\Property(
 *   property="createdDateFormated",
 *   example="09.08.2018 05:25",
 *   type="string",
 *   description="Дата создания"
 * )
 * @OA\Property(
 *   property="updatedDateFormated",
 *   example="09.08.2018 05:25",
 *   type="string",
 *   description="Дата обновления"
 * )
 * @OA\Property(
 *   property="updatedDateFormatedShort",
 *   type="string",
 *   description="Дата обновления"
 * )
 * @OA\Property(
 *   property="mainCategory",
 *   ref="#/components/schemas/Categories",
 *   description="Основная категория"
 * )
 * @OA\Property(
 *   property="theme",
 *   ref="#/components/schemas/Themes",
 *   description="Тема"
 * )
 * @OA\Property(
 *   property="authors",
 *   type="array",
 *   @OA\Items(
 *     ref="#/components/schemas/Authors",
 *   ),
 *   description="Пользователи (передавать массив id пользователей)"
 * )
 * @OA\Property(
 *   property="tags",
 *   type="array",
 *   @OA\Items(
 *     ref="#/components/schemas/Tags",
 *   ),
 *   description="Пользователи (передавать массив id пользователей)"
 * )
 * @OA\Property(
 *   property="preview_image_id",
 *   type="integer",
 *   description="Ид картинки превью"
 * )
 * @OA\Property(
 *   property="preview_image_crop_id",
 *   type="integer",
 *   description="Ид картинки превью"
 * )
 * @OA\Property(
 *   property="preview_image_crop_priority_id",
 *   type="integer",
 *   description="Ид картинки превью"
 * )
 * @OA\Property(
 *   property="detail_image_id",
 *   type="integer",
 *   description="Ид детальной картинки"
 * )
 * @OA\Property(
 *   property="detail_image_crop_id",
 *   type="integer",
 *   description="Ид детальной картинки"
 * )
 * @OA\Property(
 *   property="detail_image_option_position",
 *   type="string",
 *   description="Константа, строка одна из ['in_frame','center','top','bottom']"
 * )
 * @OA\Property(
 *   property="detail_image_option_full_size",
 *   type="boolean",
 *   description="Во всю ширину"
 * )
 * @OA\Property(
 *   property="detail_image_option_author",
 *   type="string",
 *   description="Источник или автор"
 * )
 * @OA\Property(
 *   property="preview_text",
 *   type="string",
 *   description="Текст анонса"
 * )
 * @OA\Property(
 *   property="detail_text",
 *   type="string",
 *   description="Детальный текст"
 * )
 * @OA\Property(
 *   property="react_tmp",
 *   type="string",
 *   description="Хранилка для реакта"
 * )
 */
class Materials extends \common\models\material\Material
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['image']);

        return $behaviors;
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'priority',
            'show_theme_materials',
            'slug',
            'show_comments',
            'show_content',
            'show_author',
            'journal_id',
            'is_published' => function () {
                return (bool)$this->is_published;
            },
            'publishDateFormated',
            'createdDateFormated',
            'updatedDateFormated',
            'updatedDateFormatedShort',
            'authors' => function () {
                return $this->getAuthorRest();
            },
            'region',
            'tags',
            'mainCategory' => function () {
                return [
                    'id' => $this->mainCategory->id,
                    'name' => $this->mainCategory->name,
                    'slug' => $this->mainCategory->slug,
                    'flag' => $this->mainCategory->getFlag(),
                ];
            },
            'theme' => function () {
                if ($this->themes) {
                    foreach ($this->themes as $theme) {
                        $res[] = ['id' => $theme['id'], 'name' => $theme['name']];
                    }

                    return reset($res);
                }

                return null;
            },
            'preview_text',
            'detail_text',
            'previewImage',
            'previewImageCrop',
            'previewImageCropPriority',
            'detailImage',
            'detailImageCrop',
            'headlineImage',
            'detail_image_option_full_size',
            'detail_image_option_position',
            'detail_image_option_author',
            'react_tmp',
            'short_name',
            'detail_url' => function () {
                return $this->getUrl();
            },
        ];
    }

    public function beforeSave($insert)
    {
        if (empty($this->preview_image_crop_priority_id)) {
            $preview = Files::find()->where(['id' => $this->preview_image_id])->one();
            if ($preview) {
                $image = (new Images())->createThumbnail($preview, ['ratio' => '1:1']);
                $this->preview_image_crop_priority_id = $image->id;
            }
        }

        if (empty($this->preview_image_crop_id)) {
            $preview = Files::find()->where(['id' => $this->preview_image_id])->one();
            if ($preview) {
                $image = (new Images())->createThumbnail($preview, ['ratio' => '2:1']);
                $this->preview_image_crop_id = $image->id;
            }
        }

        return parent::beforeSave($insert);
    }

    public function extraFields()
    {
        return [
            'previewImageSrc' => function () {
                return $this->previewImage->src;
            },
            'previewImageCropSrc' => function () {
                return $this->previewImageCrop->src;
            },
            'previewImageCropPrioritySrc' => function () {
                return $this->previewImageCrop->src;
            },
            'headlineImageSrc' => function () {
                return $this->headlineImage->src;
            },
        ];
    }

    public function getAuthorRest()
    {
        $res = [];
        foreach ($this->authors as $author) {
            $res[] = [
                'id' => $author->id,
                'fullName' => $author->fullName
            ];
        }

        return $res;
    }

    public function getTagsRest()
    {
        $res = [];
        foreach ($this->tags as $tag) {
            $res[] = [
                'id' => $tag->id,
                'name' => $tag->name
            ];
        }

        return $res;
    }
}
