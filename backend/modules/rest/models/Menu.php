<?php

namespace backend\modules\rest\models;
use common\models\material\MaterialRegion;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="name",
 *   type="string",
 *   description="Имя"
 * )
 * @OA\Property(
 *   property="url",
 *   type="string",
 *   description="Ссылка"
 * )
 * @OA\Property(
 *   property="active",
 *   type="boolean",
 *   example="1",
 *   description="Активность"
 * )
 */
class Menu extends \common\models\Menu
{

}
