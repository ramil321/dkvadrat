<?php

namespace backend\modules\rest\models;
use common\models\material\MaterialRegion;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="id",
 *   type="integer",
 *   description="Ид"
 * )
 * @OA\Property(
 *   property="name",
 *   type="string",
 *   description="Имя"
 * )
 */
class Regions extends MaterialRegion
{

}
