<?php

namespace backend\modules\rest\models;


/**
 * @OA\Schema()
 *
 * @OA\Property(
 *   property="id",
 *   type="integer",
 *   description="Ид"
 * )
 * @OA\Property(
 *   property="number",
 *   type="integer",
 *   description="Номер журнала"
 * )
 */
class Journals extends \common\models\journal\Journal
{

}
