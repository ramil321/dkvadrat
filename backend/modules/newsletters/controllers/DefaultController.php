<?php

namespace backend\modules\newsletters\controllers;

use backend\modules\newsletters\models\UnisenderApi;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Default controller for the `newsletters` module
 */
class DefaultController extends Controller
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $api = Yii::$app->unisender;
        if(Yii::$app->request->get('hook')) {
            $api->setHook( $api->subscribePage, ['subscribe' => '*'] );
            $api->setHook( $api->unsubscribePage, ['unsubscribe' => '*'] );
            Yii::$app->session->setFlash('success','Хук установлен');
            return $this->redirect(['/newsletters/']);
        }

        $listHooks = $api->listHooks();
        return $this->render('index',[
            'listHooks' => $listHooks
        ]);
    }
}
