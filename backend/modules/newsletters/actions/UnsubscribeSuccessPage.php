<?php
namespace backend\modules\newsletters\actions;

use backend\modules\newsletters\models\Newsletters;
use backend\modules\newsletters\models\NewslettersListNewslettersSubscribers;
use backend\modules\newsletters\models\NewslettersSubscribers;
use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


class UnsubscribeSuccessPage extends Action
{

    public function run()
    {
        $code = Yii::$app->request->get('code');
        if ($code) {
            $rel = NewslettersListNewslettersSubscribers::find()->where( [
                'code' => $code
            ] )->one();
            if ($rel) {
                $rel->active = false;
                $rel->save();
            }
        }
        return $this->controller->render('@backend/modules/newsletters/views/default/unsubscribe');
    }
}