<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\newsletters\models\search\NewslettersListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории подписок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletters-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать категорию подписок', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
         //   'unisender_list_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
