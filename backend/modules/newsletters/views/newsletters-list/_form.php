<?php

use backend\modules\newsletters\models\NewslettersSubscribers;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\newsletters\models\NewslettersList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletters-list-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message_theme')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model,'subscribersIds')->dropDownList(ArrayHelper::map(NewslettersSubscribers::getElements(),'id','email'),['multiple' => true]);?>

    <?
        $subscribeDefault = <<<HTML
    <p><strong>Здравствуйте!</strong></p>
    <p>Вы почти подписались! Нажмите на кнопку ниже, чтобы&nbsp;<strong>подтвердить&nbsp;</strong>адрес {{Email}}:</p>
    <p>Перейдите по&nbsp;<a href="{{ConfirmUrl}}">ссылке</a>&nbsp;чтобы активировать подписку</p>
HTML;
        ?>
    <p>
        Снипет для email на который пришло письмо {{Email}}.
        Снипет для ссылки подтверждения подписки {{ConfirmUrl}}
    </p>

    <?= $form->field($model, 'subscribe_body')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',
        'options' => ['value' => ($model->isNewRecord ? $subscribeDefault : $model->subscribe_body)],
    ]) ?>

    <?//= $form->field($model,'subscribe_body')->textarea(['value' => $htmlUnsubscribeBody]);?>

    <?//= $form->field($model,'unsubscribe_body')->textarea();?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
