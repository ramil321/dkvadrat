<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\newsletters\models\search\NewslettersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рассылки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletters-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать рассылку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'label' => 'Дата создания',
                'format' => 'raw',
                'attribute' => 'publish_date',
                'value' => function($model){
                    return $model->publishDateFormated;
                },
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'publishAtRange',
                    'convertFormat' => true,
                    'startAttribute' => 'publishAtStart',
                    'endAttribute' => 'publishAtEnd',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd.m.Y H:i:s'
                        ]
                    ]
                ]),
            ],
           // 'text:ntext',
//            'publish_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
