<?php

use backend\modules\newsletters\models\NewslettersList;
use dosamigos\ckeditor\CKEditor;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\newsletters\models\Newsletters */
/* @var $form yii\widgets\ActiveForm */
?>




<div class="newsletters-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'name')->textInput(
        [
            'maxlength' => true,
            'disabled' => $model->is_send
        ]
    ) ?>


    <?= $form->field($model, 'message_theme')->textInput(
        [
            'maxlength' => true,
            'disabled' => $model->is_send
        ]
    ) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',
    ]) ?>

    <?= $form->field($model, 'newsletters_list_id')->dropDownList(ArrayHelper::map(NewslettersList::getElements(),'id','name')); ?>


    <?=
    $form->field($model, 'publishDateFormated')->widget(    DateTimePicker::className(),[
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        // 'convertFormat' => true,
        'value' => $model->publishDateFormated,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy hh:ii',
            'autoclose'=>true,
            'weekStart'=>1,
        ],
        'options' => [
            'autocomplete' => 'off'
        ],
        'disabled' => $model->is_send
    ]);
    ?>

    <div class="form-group">
        <?if($model->is_send):?>
        <?= Html::submitButton('Рассылка разослана',
                [
                        'class' => 'btn btn-success',
                        'disabled' => true
                ]
            );
        ?>
        <?else:?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <?endif?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
