<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\newsletters\models\Newsletters */

$this->title = 'Создать рассылку';
$this->params['breadcrumbs'][] = ['label' => 'Рассылки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletters-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
