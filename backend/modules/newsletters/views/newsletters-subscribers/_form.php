<?php

use backend\modules\newsletters\models\NewslettersList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\newsletters\models\NewslettersSubscribers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletters-subscribers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>


    <?//= $form->field($model,'newslettersListIds')->dropDownList(ArrayHelper::map(NewslettersList::getElements(),'id','name'),['multiple' => true]);?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
