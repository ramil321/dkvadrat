<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\newsletters\models\NewslettersSubscribers */

$this->title = 'Create Newsletters Subscribers';
$this->params['breadcrumbs'][] = ['label' => 'Newsletters Subscribers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletters-subscribers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
