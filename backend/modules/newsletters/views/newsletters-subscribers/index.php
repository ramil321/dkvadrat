<?php

use backend\modules\newsletters\models\NewslettersList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\newsletters\models\search\NewslettersSubscribersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletters-subscribers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Newsletters Subscribers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            [

                'format' => 'raw',
                'attribute' => 'newsletters_list_id',
                'value' => function($model){
                    return Html::ul(ArrayHelper::map($model->newslettersList,'id','name'),['style' => ['margin-left' => '-1em']]);
                },
                'filter' => ArrayHelper::map(NewslettersList::getElements(),'id','name'),
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
