<main class="main-dk">
    <div class="contacts page">
        <div class="page__wrapper">
            <div class="contacts__top-wrapper page__wrapper-top">
                <div class="container">
                    <h1 class="page__title">Подписка успешно оформлена</h1>
                    Благодарим за проявленный интерес к нашему изданию! Вы успешно подписались на рассылку.
                </div>
            </div>
            <div class="container">

            </div>
        </div>
    </div>
</main>