<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;


?>
<div class="newsletters-default-index">
    <h1>Unisender</h1>
    <p>
        Установить хук вы можете по этой <?=Html::a('ссылке',['/newsletters/','hook' => 'y']);?>
    </p>
    <p>
        Список хуков:
        <?=Html::ol(ArrayHelper::map($listHooks['result'],'id','url'))?>
    </p>
</div>
