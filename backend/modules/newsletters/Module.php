<?php

namespace backend\modules\newsletters;

/**
 * newsletters module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\newsletters\controllers';
    public $subscribePage;
    public $unsubscribePage;
    public $key;
    public $senderEmail;
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
