<?php
namespace backend\modules\newsletters\console;

use backend\modules\newsletters\jobs\SendMessage;
use backend\modules\newsletters\models\Newsletters;
use backend\modules\newsletters\models\NewslettersSubscribers;
use common\models\material\Material;
use common\models\material\MaterialCategories;
use yii\console\Controller;

class NewslettersWorkController extends Controller
{
    public function actionIndex()
    {
        $newsLetters = Newsletters::find()
            ->where(['<','publish_date',time()])
            ->andWhere(['is_send' => false])
            ->orderBy(['publish_date' => 'ASC'])
            ->one();
        /** @var NewslettersSubscribers[] $subscribers */
        $subscribers = $newsLetters->list->activeSubscribers;

        if (! $subscribers) {
            return;
        }

        $newsLetters->is_send = true;
        $newsLetters->save();

        foreach ($subscribers as $subscriber) {
            \Yii::$app->queue->delay(1)->push(new SendMessage([
                'text' => $newsLetters->text,
                'messageFrom' => $newsLetters->message_from,
                'email' => $subscriber->email,
                'messageTheme' => $newsLetters->message_theme,
            ]));
        }
    }
}
