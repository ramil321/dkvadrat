<?php

namespace backend\modules\newsletters\models;

use Yii;

/**
 * This is the model class for table "newsletters_list_newsletters_subscribers".
 *
 * @property int $id
 * @property int $newsletters_list_id
 * @property int $newsletters_subscribers_id
 * @property string $code
 * @property bool $active
 * @property NewslettersList $newslettersList
 * @property NewslettersSubscribers $newslettersSubscribers
 */
class NewslettersListNewslettersSubscribers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'newsletters_list_newsletters_subscribers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['newsletters_list_id', 'newsletters_subscribers_id'], 'integer'],
            [['newsletters_list_id', 'newsletters_subscribers_id'], 'required'],
            [['newsletters_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewslettersList::className(), 'targetAttribute' => ['newsletters_list_id' => 'id']],
            [['newsletters_subscribers_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewslettersSubscribers::className(), 'targetAttribute' => ['newsletters_subscribers_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'newsletters_list_id' => 'Newsletters List ID',
            'newsletters_subscribers_id' => 'Newsletters Subscribers ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewslettersList()
    {
        return $this->hasOne(NewslettersList::className(), ['id' => 'newsletters_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewslettersSubscribers()
    {
        return $this->hasOne(NewslettersSubscribers::className(), ['id' => 'newsletters_subscribers_id']);
    }
}
