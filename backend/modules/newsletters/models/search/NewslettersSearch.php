<?php

namespace backend\modules\newsletters\models\search;

use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\newsletters\models\Newsletters;
use yii\helpers\ArrayHelper;

/**
 * NewslettersSearch represents the model behind the search form of `\backend\modules\newsletters\models\Newsletters`.
 */
class NewslettersSearch extends Newsletters
{
    public $publishAtRange;
    public $publishAtStart;
    public $publishAtEnd;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'publish_date','publishDateFormated'], 'integer'],
            [['publishAtRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['name', 'text'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),
            [
                [
                    'class' => DateRangeBehavior::className(),
                    'attribute' => 'publishAtRange',
                    'dateStartAttribute' => 'publishAtStart',
                    'dateEndAttribute' => 'publishAtEnd',
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newsletters::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['between', 'publish_date',$this->publishAtStart, $this->publishAtEnd]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
