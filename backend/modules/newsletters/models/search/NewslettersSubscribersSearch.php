<?php

namespace backend\modules\newsletters\models\search;

use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\newsletters\models\NewslettersSubscribers;
use yii\helpers\ArrayHelper;

/**
 * NewslettersSubscribersSearch represents the model behind the search form of `\backend\modules\newsletters\models\NewslettersSubscribers`.
 */
class NewslettersSubscribersSearch extends NewslettersSubscribers
{

    /**
     * {@inheritdoc}
     */
    public $newsletters_list_id;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['email','newsletters_list_id'], 'safe'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewslettersSubscribers::find();
        $query->joinWith('newslettersList');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['newsletters_list_newsletters_subscribers.newsletters_list_id' => $this->newsletters_list_id]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
