<?php

namespace backend\modules\newsletters\models;

use backend\behaviors\DateConverterBehavior;
use common\models\material\Material;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;

/**
 * This is the model class for table "newsletters".
 *s
 * @property int $id
 * @property string $name
 * @property string $text
 * @property int $publish_date
 * @property int $newsletters_list_id
 * @property int $unisender_message_id
 * @property int $unisender_campaign_id
 * @property int $unisender_list_id
 * @property string $message_theme
 * @property string $message_from
 * @property boolean $is_send
 * @property NewslettersList[] $list
 * @property NewslettersMaterial[] $newslettersMaterials
 */
class Newsletters extends \yii\db\ActiveRecord
{

    const DATE_FORMAT_FULL = 'd.m.Y H:i:s';
    const DATE_FORMAT_SHORT = 'd.m.Y H:i';
    public function behaviors()
    {
        return [
            'many_to_many' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => [ 'materialIds' => 'materials']
            ],
            [
                'class' => DateConverterBehavior::className(),
                'logicalFormat' => 'php:'.self::DATE_FORMAT_SHORT, // default to locale format
                'physicalFormat' => 'php:U', // database level format, default to 'Y-m-d'
                'attributes' => [
                    'publishDateFormated' => 'publish_date',
                ]
            ]
        ];
    }

    public static function activeSubscribe($email){

        Yii::$app->request->baseUrl;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'newsletters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','message_theme'], 'required'],
            [['text'], 'string'],
            [['is_send'], 'boolean'],
            ['publish_date', function ($attribute, $params) {
              /*  if ($this->$attribute <= time()) {
                    $this->addError($attribute, 'Токен должен содержать буквы или цифры.');
                }*/
            }],
            [['publish_date','newsletters_list_id'], 'integer'],
            [['name','publishDateFormated','message_theme','message_from'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Текст рассылки',
            'newsletters_list_id' => 'Список рассылки',
            'publishDateFormated' => 'Дата рассылки',
            'publish_date' => 'Дата рассылки',
            'message_theme' => 'Тема письма',
            'message_from' => 'От кого',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(
            Material::className(),
            ['id' => 'material_id']
        )->viaTable(
            'newsletters_material',
            ['newsletters_id' => 'id']
        );
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(NewslettersList::className(), ['id' => 'newsletters_list_id']);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements($ids = false){
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }
}
