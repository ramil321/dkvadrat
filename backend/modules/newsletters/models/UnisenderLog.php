<?php

namespace backend\modules\newsletters\models;

use Yii;

/**
 * This is the model class for table "unisender_log".
 *
 * @property int $id
 * @property string $request
 * @property string $reponse
 */
class UnisenderLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unisender_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request', 'reponse'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request' => 'Request',
            'reponse' => 'Reponse',
        ];
    }
}
