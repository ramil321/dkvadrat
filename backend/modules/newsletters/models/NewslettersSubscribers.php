<?php

namespace backend\modules\newsletters\models;

use common\models\material\Material;
use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\base\ModelEvent;

/**
 * This is the model class for table "newsletters_subscribers".
 *
 * @property int $id
 * @property string $email
 */
class NewslettersSubscribers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'newsletters_subscribers';
    }

    public function behaviors()
    {
        return [
            'many_to_many' => [
                'class' => ManyToManyBehavior::className(),
                'relations' => ['newslettersListIds' => 'newslettersList']
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewslettersList()
    {
        return $this->hasMany(
            NewslettersList::className(),
            ['id' => 'newsletters_list_id']
        )->viaTable(
            'newsletters_list_newsletters_subscribers',
            ['newsletters_subscribers_id' => 'id']
        );
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'unique'],
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'newsletters_list_id' => 'Категория подписчиков'
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getElements($ids = false){
        if($ids){
            return self::find()->where(['id' => $ids])->all();
        }else{
            return self::find()->all();
        }
    }
}
