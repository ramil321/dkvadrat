<?php

namespace backend\modules\newsletters\models;

use Yii;

/**
 * This is the model class for table "unisender_log".
 *
 * @property int $id
 * @property string $request
 * @property string $reponse
 */
class Subscribe extends \yii\base\Model
{
    public $email;
    public $listId;
    public $unsubscribeListId;

    private $newSubscribe = false;
    private $newUnsubscribe = false;

    public function init()
    {
        $this->listId = Yii::$app->subscribe->defaultListId;
        parent::init();
    }

    public function validateList()
    {
        if($this->listId){
            $ns = NewslettersSubscribers::find()->joinWith( 'newslettersList' )->where( [
                'email' => $this->email,
                'newsletters_list_newsletters_subscribers.newsletters_list_id' => $this->listId
            ] )->asArray()->one();
            if($ns) {
                $this->addError('listId','Вы уже подписаны');
            }else{
                $this->newSubscribe = true;
            }
        }
    }

    public function validateUnsubscribeList()
    {
        if($this->unsubscribeListId){
            $ns = NewslettersSubscribers::find()->joinWith( 'newslettersList' )->where( [
                'email' => $this->email,
                'newsletters_list_newsletters_subscribers.newsletters_list_id' => $this->unsubscribeListId
            ] )->asArray()->one();
            if($ns) {
                $this->newUnsubscribe = true;
            }else{
                $this->addError('unsubscribeListId','Вы не подписаны');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['listId', 'required', 'when' => function ($model) {
                if(!$model->unsubscribeListId){
                    return true;
                }
            }, 'whenClient' => "function (attribute, value) { 
                if(!$('#newsletterssubscribers-unsubscribelistid').val()){
                    return true;
                }
            }"],
            ['unsubscribeListId', 'required', 'when' => function ($model) {
                if(!$model->listId){
                    return true;
                }
            }, 'whenClient' => "function (attribute, value) {
                if(!$('#newsletterssubscribers-listid').val()){
                    return true;
                }
            }"],
            [['email'], 'required'],
            [['email'], 'email'],
            [['listId'], 'exist', 'skipOnError' => true, 'targetClass' => NewslettersList::className(), 'targetAttribute' => 'id'],
            [['unsubscribeListId'], 'exist', 'skipOnError' => true, 'targetClass' => NewslettersList::className(), 'targetAttribute' => 'id'],
            ['listId','validateList'],
            ['unsubscribeListId','validateUnsubscribeList'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'newsletters_list_id' => 'Категория подписчиков'
        ];
    }


}
