<?php

namespace backend\modules\newsletters\jobs;

use yii\base\BaseObject;
use yii\queue\JobInterface;

class SendMessage extends BaseObject implements JobInterface
{
    public $text;
    public $messageFrom;
    public $email;
    public $messageTheme;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        $senderEmail = \Yii::$app->subscribe->senderEmail;

        if (! $senderEmail) {
            return;
        }

        \Yii::$app->mailer
            ->compose('@backend/modules/newsletters/views/mail/layout')
            ->setFrom($senderEmail)
            ->setTo($this->email)
            ->setSubject($this->messageTheme)
            ->setHtmlBody($this->text)
            ->send();
    }
}