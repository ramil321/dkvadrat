<?php

namespace backend\modules\newsletters\jobs;

use backend\modules\newsletters\models\NewslettersList;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class SendSubscribeMessage extends BaseObject implements JobInterface
{
    public $to;
    public $listId;
    public $code;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        $senderEmail = \Yii::$app->subscribe->senderEmail;

        if (! $senderEmail) {
            return;
        }

        $nList = NewslettersList::find()->where(['id' => $this->listId])->one();
        \Yii::$app->mailer
            ->compose('@backend/modules/newsletters/views/mail/layout')
            ->setFrom($senderEmail)
            ->setTo($this->to)
            ->setSubject($nList->message_theme)
            ->setHtmlBody($nList->getBody($this->to, $this->code))
            ->send();
    }
}