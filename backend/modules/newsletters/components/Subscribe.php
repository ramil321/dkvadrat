<?php
namespace backend\modules\newsletters\components;


use backend\modules\newsletters\jobs\SendSubscribeMessage;
use backend\modules\newsletters\models\NewslettersList;
use backend\modules\newsletters\models\NewslettersListNewslettersSubscribers;
use backend\modules\newsletters\models\NewslettersSubscribers;
use  backend\modules\newsletters\models\Subscribe as FormSubscribe;

class Subscribe
{

    public $defaultListId = '6';
    public $senderEmail = 'info@kvadrat.picom.su';
    public $subscribePage = 'http://kvadrat.picom.su/frontend/web/site/newsletters-subscribe/';
    public $unsubscribePage = 'http://kvadrat.picom.su/frontend/web/site/newsletters-unsubscribe/';

    /**
     * @param string $code
     * @return string
     */
    public function getSuccessSubscribeUrl($code)
    {
        return $this->subscribePage. '?code='. $code;
    }


    /**
     * @param FormSubscribe $subscribe
     * @return NewslettersSubscribers
     * @throws \yii\base\Exception
     */
    public function subscribe(FormSubscribe $subscribe)
    {
        $newslettersSubscriber = new NewslettersSubscribers();
        $newslettersSubscriber->email = $subscribe->email;
        $newslettersSubscriber->save();
        $code = \Yii::$app->security->generatePasswordHash($newslettersSubscriber->id.'secretns');

        \Yii::$app->queue->delay(1)->push(new SendSubscribeMessage([
            'to' => $subscribe->email,
            'listId' => $subscribe->listId,
            'code' => $code,
        ]));

        $nsRel = new NewslettersListNewslettersSubscribers();
        $nsRel->newsletters_list_id = $subscribe->listId;
        $nsRel->newsletters_subscribers_id = $newslettersSubscriber->id;
        $nsRel->code = $code;
        $nsRel->active = false;
        $nsRel->save();
        return $newslettersSubscriber;
    }
}