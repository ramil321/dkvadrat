<?php

namespace backend\modules\pages\widgets\treeview;

use yii\web\AssetBundle;


/**
 * Asset bundle for TreeView widget.
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since  1.0
 */
class TreeViewAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/pages/widgets/treeview/';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/main.css',
    ];
    public $js = [
        'js/main.js',
    ];
}
