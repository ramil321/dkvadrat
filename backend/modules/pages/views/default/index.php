<div class="pages-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>
<?
use backend\modules\pages\models\Pages;
use backend\modules\pages\models\Tree;
use backend\modules\pages\widgets\treeview\TreeViewMaterialWidget;
use kartik\tree\TreeView;

echo TreeViewMaterialWidget::widget([
    // single query fetch to render the tree
    'query'             => Pages::find()->addOrderBy('root, lft'),
    'topRootAsHeading' => true, // this will override the headingOptions
    'rootOptions' => ['label' => 'Разделы'],
    'isAdmin'           => true,                       // optional (toggle to enable admin mode)
    'displayValue'      => 1,                           // initial display value
    'softDelete'      => false,                        // normally not needed to change
    //'cacheSettings'   => ['enableCache' => true]      // normally not needed to change
]);

?>