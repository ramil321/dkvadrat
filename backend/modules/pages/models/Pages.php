<?php


namespace backend\modules\pages\models;

use backend\modules\files\behaviors\ImageBehavior;
use backend\modules\files\models\Files;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

class Pages extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                ImageBehavior::className(),
                TimestampBehavior::className()
            ]
        );
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImageObj()
    {
        return $this->hasOne(Files::className(), ['id' => 'image']);
    }

    public static function tableName()
    {
        return 'pages';
    }

}