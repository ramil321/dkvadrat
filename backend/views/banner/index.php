<?php

use common\models\banner\BannerAdvertiser;
use common\models\banner\BannerPosition;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\banner\search\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать баннер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'active:boolean',
            [

                'format' => 'raw',
                'attribute' => 'image_id',
                'value' => function($model){
                    return Html::img(Yii::$app->resize->prop($model->image->src,150,150));
                }
            ],
            [

                'format' => 'raw',
                'attribute' => 'banner_position_id',
                'filter' => ArrayHelper::map(BannerPosition::getElements(),'id','name'),
                'value' => function($model){
                    return $model->bannerPosition->name;
                }
            ],



            [

                'format' => 'raw',
                'attribute' => 'advertiser',
                'value' => function($model){
                    return $model->bannerCompany->bannerAdvertiser->name;
                },
                'filter' => ArrayHelper::map(BannerAdvertiser::getElements(),'id','name'),
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
