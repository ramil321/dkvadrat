<?php


use common\widgets\Alert;
use yii\widgets\Pjax;


?>


<div class="pjax-block">
<?= Alert::widget() ?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
</div>
