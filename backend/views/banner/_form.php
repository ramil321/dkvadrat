<?php

use backend\modules\files\widgets\ImageInputWidget;
use common\models\banner\BannerPosition;
use common\models\material\MaterialCategories;
use common\widgets\Alert;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\banner\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'active')->checkbox(['checked ' => ($model->isNewRecord)]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageInput')->widget(ImageInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'image',
        'inputAttribute' => 'imageInput',
        'descriptionActive' => false,
    ]   );   ?>

    <?=
    $form->field($model, 'publishDateFormated')    ->widget(    DateTimePicker::className(),[
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        // 'convertFormat' => true,
        'value' => $model->publishDateFormated,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy hh:ii',
            'autoclose'=>true,
            'weekStart'=>1,
        ]
    ]);
    ?>
    <?=
    $form->field($model, 'publishEndDateFormated')    ->widget(    DateTimePicker::className(),[
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        // 'convertFormat' => true,
        'value' => $model->publishEndDateFormated,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy hh:ii',
            'autoclose'=>true,
            'weekStart'=>1,
        ]
    ]);
    ?>
    <?= $form->field($model, 'categoriesIds')->dropDownList(MaterialCategories::getFullTreeArrayForSelect(),[
        'prompt' => '-',
        'multiple' => true
    ]);?>

    <?
    echo $form->field($model, 'tagIds')->widget(Select2::classname(), [
//         'initValueText' => 'Выберите теги', // set the initial display text
        'data' => ArrayHelper::map($model->tags,'id','name'),
        'options' => ['placeholder' => 'Выберите теги ...','multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
            'allowClear' => true,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['material/ajax-tags']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(tags) { return tags.text; }'),
            'templateSelection' => new JsExpression('function (tags) { return tags.text; }'),
        ],
    ]);

    ?>

    <?= $form->field($model, 'blank')->checkbox(); ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'banner_position_id')->dropDownList(ArrayHelper::map(BannerPosition::getElements(),'id','name')); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
