<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\banner\BannerCompany */

$this->title = 'Создать рекламную компанию';
$this->params['breadcrumbs'][] = ['label' => 'Рекламные компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
