<?php

use backend\widgets\ModalAjaxElementAddWidget;
use common\models\banner\BannerAdvertiser;
use common\models\banner\BannerCompany;
use kartik\select2\Select2;
use lo\widgets\modal\ModalAjax;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\banner\BannerCompany */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin([
    'enablePushState' => false,
    'enableReplaceState' => false,
]) ?>
<div class="banner-company-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
    <?= $form->field($model, 'banner_advertiser_id')->dropDownList(ArrayHelper::map(BannerAdvertiser::getElements(),'id','name')) ?>
    <?
        echo ModalAjaxElementAddWidget::widget(['url' => '/banner-advertiser/ajax-advertiser-create','selectId' => 'bannercompany-banner_advertiser_id']);
    ?>
    </div>
    <div class="form-group">
    <?

    echo $form->field($model, 'bannerIds')->widget(Select2::classname(), [
        // 'initValueText' => 'Выберите теги', // set the initial display text
        'data' => ArrayHelper::map($model->banners,'id','name'),
        'options' => ['placeholder' => 'Выберите баннеры ...','multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
            // 'tags' => true,
            //'tokenSeparators' => [','],
            // 'allowClear' => true,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/banner/ajax-banner-find']),
                'dataType' => 'json',
            ],
        ],
    ]);


    ?>
    <?
        echo ModalAjaxElementAddWidget::widget(['url' => '/banner/ajax-banner-create','selectId' => 'bannercompany-bannerids']);
    ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>