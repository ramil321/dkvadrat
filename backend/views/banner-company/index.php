<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\banner\search\BannerCompany */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рекламные компании';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-company-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать рекламную компанию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'banner_advertiser_id',

            ['class' => 'backend\components\FilterActionColumn','template'=>'{update} {delete}','header'=>'Действия',],
        ],
    ]); ?>
</div>
