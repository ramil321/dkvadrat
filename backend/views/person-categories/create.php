<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\person\PersonCategories */

$this->title = 'Создать категорию для персон';
$this->params['breadcrumbs'][] = ['label' => 'Категории для персон', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
