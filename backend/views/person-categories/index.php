<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\person\search\PersonCategoriesSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории для персон';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать категорию для персон', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            ['class' => 'backend\components\FilterActionColumn','template'=>'{update} {delete}','header'=>'Действия',],
        ],
    ]); ?>
</div>
