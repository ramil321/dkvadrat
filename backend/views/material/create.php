<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\material\Material */

$this->title = 'Создать материал';
$this->params['breadcrumbs'][] = ['label' => 'Материалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'authors' => $authors,
        'dropDownCategories' => $dropDownCategories
    ]) ?>

</div>
