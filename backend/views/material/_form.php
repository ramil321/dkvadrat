<?php

use backend\modules\files\widgets\ImageInputWidget;
use backend\modules\pages\widgets\treeview\TreeViewMaterialWidget;
use common\models\journal\Journal;
use common\models\material\Material;
use common\models\material\MaterialCategories;
use common\models\material\MaterialRegion;
use common\models\material\MaterialStatus;
use common\models\theme\Theme;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use kartik\tree\TreeViewInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\material\Material */
/* @var $form yii\widgets\ActiveForm */


/*
$group = Material::findOne(1);
foreach($group->materialCategories as $user)
{
    $user->is_main[1];
}*/

/*foreach($model->materialCategories as $val){
    echo($val->is_main);
}*/

//pre($model->materialCategories);
?>

<div class="material-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'material_status_id')->dropDownList(ArrayHelper::map(MaterialStatus::getElements(),'id','name')) ?>

    <?= $form->field($model, 'material_region_id')->dropDownList(ArrayHelper::map(MaterialRegion::getElements(),'id','name')) ?>

    <?= $form->field($model, 'show_theme_materials')->checkbox() ?>

    <?= $form->field($model, 'journal_id')->dropDownList(ArrayHelper::map(Journal::getElements(),'id','number'),['multiple' => true])?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <label class="control-label">Кол-во просмотров: </label><?=$model->views_count ? $model->views_count : 0?>

    <?= $form->field($model, 'duration_read')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'priority')->checkbox()?>

    <?= ($model->slug ? $form->field($model, 'slug')->textInput(['disabled' => true]) : '' ) ?>
    <?=
    $form->field($model, 'publishDateFormated')    ->widget(    DateTimePicker::className(),[
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
       // 'convertFormat' => true,
        'value' => $model->publishDateFormated,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy hh:ii',
            'autoclose'=>true,
            'weekStart'=>1,
        ]
    ]);
    ?>

    <?= $form->field($model, 'mainCategoryId')->dropDownList(ArrayHelper::map(MaterialCategories::find()->addOrderBy('root, lft')->all(),'id','name'),['prompt' => '-']);?>

    <?= $form->field($model, 'themeId')->dropDownList(ArrayHelper::map(Theme::getElements(),'id','name'),['prompt' => '-']);?>

    <?= $form->field($model, 'authorsIds')->dropDownList(ArrayHelper::map($authors,'id','fullName'),['multiple' => true])?>


    <?
    echo $form->field($model, 'tagIds')->widget(Select2::classname(), [
//         'initValueText' => 'Выберите теги', // set the initial display text
        'data' => ArrayHelper::map($model->tags,'id','name'),
        'options' => ['placeholder' => 'Выберите теги ...','multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
            'allowClear' => true,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['material/ajax-tags']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(tags) { return tags.text; }'),
            'templateSelection' => new JsExpression('function (tags) { return tags.text; }'),
        ],
    ]);

    ?>

    <?= $form->field($model, 'preview_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'previewImageInput')->widget(ImageInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'previewImage',
        'descriptionLabel' => 'Автор',
        'inputAttribute' => 'previewImageInput',
        'descriptionActive' => true,
    ]   );   ?>


    <?= $form->field($model, 'detail_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'detailImageInput')->widget(ImageInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'detailImageInput',
        'descriptionLabel' => 'Автор',
        'inputAttribute' => 'detailImageInput',
        'descriptionActive' => true,
    ]   );   ?>




    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
