<?php
use yii\helpers\Html;

?>
    <div class="btn-group">
        <?= Html::a('Индексировать', ['material/add-index', 'search' => true], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Удалить индексы', ['material/delete-all-index'], ['class' => 'btn btn-danger']) ?>
    </div><br/><br/><br/>
<?php

if($result) {
    foreach ($result as $material) {
        echo $material['id'] . ' ' . $material['name'] . '<br>';
    }
}