<?php

use backend\models\material\search\MaterialSearch;
use common\models\material\MaterialCategories;
use common\models\material\MaterialTags;
use common\models\theme\Theme;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kartik\tree\TreeViewInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use common\models\journal\Journal;

/* @var $this yii\web\View */
/* @var $model MaterialSearch */
/* @var $searchModel common\models\material\MaterialSearch */
/* @var $form yii\widgets\ActiveForm */

?>
<?=Html::button('Показать фильтры',['class' => 'spoiler-trigger btn btn-primary','style' => 'margin-bottom:10px; width:100%'])?>
<?
$js  = <<< JS
$(function(){
    $(".spoiler-trigger").click(function() {
		$(this).next().collapse('toggle');
		if($(this).next().attr('aria-expanded') == 'false'){
		    $(this).text('Показать фильтры');
		}else{
		    $(this).text('Скрыть фильтры');
		}
	});
})
JS;
$this->registerJs($js);
?>

<div class="material-search collapse">


    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'authorsIds')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($authors,'id','fullName'),
        'options' => ['placeholder' => 'Авторы','multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
            'tags' => true,
        ],
    ]);?>

    <?= $form->field($model, 'createdAtRange')->widget(DateRangePicker::classname(), [
        'model' => $searchModel,
        'attribute' => 'createdAtRange',
        'convertFormat' => true,
        'startAttribute' => 'createdAtStart',
        'endAttribute' => 'createdAtEnd',
        'pluginOptions' => [
            'locale' => [
                'format' => 'd.m.Y H:i:s'
            ]
        ],
        'options' => [
            'autocomplete' => 'off',
            'class' => 'form-control'
        ]
    ]);?>

    <?= $form->field($model, 'updatedAtRange')->widget(DateRangePicker::classname(), [
        'model' => $searchModel,
        'attribute' => 'updatedAtRange',
        'convertFormat' => true,
        'startAttribute' => 'updatedAtStart',
        'endAttribute' => 'updatedAtEnd',
        'pluginOptions' => [
            'locale' => [
                'format' => 'd.m.Y H:i:s'
            ]
        ],
        'options' => [
            'autocomplete' => 'off',
            'class' => 'form-control'
        ]
    ]);?>
    <?= $form->field($model, 'journal_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Journal::getElements(),'id','number'),
        'options' => ['multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
            'tags' => true,
        ],
    ]);?>

    <div class="form-group">
        <?= $form->field($model, 'materialCategories')->dropDownList(MaterialCategories::getFullTreeArrayForSelect(),['prompt' => 'Выберите категорию...']); ?>
    </div>

    <?= $form->field($model, 'priority')->dropDownList(['Обычный','Высокий'],['prompt' => 'Выберите приоритет...']) ?>

    <?
    if(!$model->tagIds){
        $model->tagIds = null;
    }
    echo $form->field($model, 'tagIds')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(MaterialTags::getElements($model->tagIds),'id','name'),
        'options' => ['placeholder' => 'Выберите теги ...','multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
            'ajax' => [
                'url' => \yii\helpers\Url::to(['material-tags/ajax-tags-find']),
                'dataType' => 'json',
            ],
        ],
    ]);

    ?>
    <?
    echo $form->field($model, 'themeIds')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Theme::getElements($model->themeIds),'id','name'),
        'options' => ['placeholder' => 'Выберите теги ...','multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
            'ajax' => [
                'url' => \yii\helpers\Url::to(['theme/ajax-theme-find']),
                'dataType' => 'json',
            ],
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::a(
                'Сбросить',
                [Yii::$app->controller->id.'/'.Yii::$app->controller->action->id],
                ['class' => 'btn btn-default']
            );
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
