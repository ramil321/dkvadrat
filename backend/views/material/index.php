<?php

use backend\models\material\search\MaterialSearch;
use backend\widgets\grid\EditColumns;
use backend\widgets\grid\EditColumnsButton;
use common\models\material\MaterialCategories;
use kartik\daterange\DateRangePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\material\Material */
/* @var $searchModel backend\models\material\search\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Материалы';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="material-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search',
        [
            'model' => $searchModel,
            'searchModel' => $searchModel,
            'authors' => $authors
        ]); ?>

    <p>
        <?= Html::a('Создать материал', ['/editor'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'table-layout:fixed;'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 70px;'],
            ],
            [
                'attribute' => 'priority',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width: 100px;', 'class' => 'text-center'],
                'value' => function ($model) {
                    return EditColumns::widget([
                        'model' => $model,
                        'field' => 'priority',
                        'value' => ($model->priority ? 'Высокий' : 'Обычный'),
                        'edit' => Html::dropDownList('priority', $model->priority,
                            ['Обычный', 'Высокий'],
                            ['class' => 'form-control edit-input',
                            ])
                    ]);

                },
            ],
            [
                'label' => 'Дата публикации',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width: 155px;', 'class' => 'text-center'],
                'attribute' => 'publish_date',
                'value' => function ($model) {
                    return EditColumns::widget([
                        'model' => $model,
                        'field' => 'publishDateFormated',
                        'value' => $model->publishDateFormated,
                        'edit' => DateTimePicker::widget(['type' => DateTimePicker::TYPE_INPUT,
                            // 'convertFormat' => true,
                            'model' => $model,
                            'name' => Html::getInputName($model, 'publishDateFormated'),
                            'value' => $model->publishDateFormated,
                            'pluginOptions' => [
                                'format' => 'dd.mm.yyyy hh:ii:ss',
                                'autoclose' => true,
                                'weekStart' => 1,
                            ],
                            'options' => [
                                'class' => 'edit-input'
                            ]
                        ])
                    ]);

                },
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createdAtRange',
                    'convertFormat' => true,
                    'startAttribute' => 'createdAtStart',
                    'endAttribute' => 'createdAtEnd',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd.m.Y H:i:s'
                        ]
                    ]
                ]),
            ],

            [
                'label' => 'Дата создания',
                'format' => 'raw',
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return $model->createdDateFormated;
                },
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createdAtRange',
                    'convertFormat' => true,
                    'startAttribute' => 'createdAtStart',
                    'endAttribute' => 'createdAtEnd',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'd.m.Y H:i:s'
                        ]
                    ]
                ]),
            ],
            [

                'format' => 'raw',
                'attribute' => 'authorsIds',
                'value' => function ($model) {
                    return Html::ul(ArrayHelper::map($model->authors, 'id', 'fullName'), ['style' => ['margin-left' => '-1em']]);
                },
                'filter' => Html::activeDropDownList($searchModel, 'authorsIds', ArrayHelper::map($authors, 'id', 'fullName'), ['class' => 'form-control', 'prompt' => '']),
            ],
            [
                'format' => 'raw',
                'attribute' => 'materialCategories',
                'filter' => MaterialCategories::getFullTreeArrayForSelect(),
                'value' => function ($model) {
                    return Html::ul(ArrayHelper::map($model->materialCategories, 'id', 'name'), ['style' => ['margin-left' => '-1em']]);
                },
            ],
            [
                'attribute' => 'status',
                'filter' => MaterialSearch::STATUSES,
            ],
            'name',
            [
                'class' => 'yii\grid\CheckboxColumn',
            ],
            ['class' => 'backend\components\FilterActionColumn', 'template' => '{update} {view} {delete}', 'header' => 'Действия',
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return $model->getUrl();
                    }
                    if ($action === 'update') {
                        return Url::to('admin/editor/?id=' . $key, true);
                    }
                    if ($action === 'delete') {
                        return Url::to(['delete?id=' . $key], true);
                    }
                }
            ],
        ],

    ]); ?>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row group_select hidden">
        <div class="col-md-3">
            Дата публикации
            <?= Html::tag('div', DateTimePicker::widget(['type' => DateTimePicker::TYPE_INPUT,
                'model' => $model,
                'name' => Html::getInputName($model, 'publishDateFormated'),
                'value' => $model->publishDateFormated,
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy hh:ii',
                    'autoclose' => true,
                    'weekStart' => 1,
                ],
                'options' => [
                    'class' => 'group-edit-publish-date'
                ]
            ]), [
                'id' => 'data-picker-slect-fields',
            ]); ?></div>
        <div class="col-md-3">
            Приоритет
            <?= Html::tag('div', Html::dropDownList('priority', $model->priority,
                ['Обычный', 'Высокий'],
                ['class' => 'form-control group-edit-priority', 'prompt' => ''
                ])
            ); ?>
        </div>
        <div class="col-md-3">
            Материал опубликован
            <?= Html::tag('div', Html::dropDownList('priority', $model->is_published,
                [
                    1 => 'Опубликован',
                    0 => 'Не опубликован'
                ],
                ['class' => 'form-control group-edit-is-published', 'prompt' => ''
                ])
            ); ?>
        </div>
        <div class="col-md-3">
            <!--        Снять с публикации-->
        </div>
    </div>
    <br>
    <?php ActiveForm::end() ?>


    <?= EditColumnsButton::widget([
        'saveUrl' => Url::to(['/material/edit-column'])
    ]) ?>

</div>
