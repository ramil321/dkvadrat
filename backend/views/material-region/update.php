<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\material\MaterialRegion */

$this->title = 'Обновить регион материалов: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Регионы материалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="material-region-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
