<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\material\MaterialRegion */

$this->title = 'Создать регион';
$this->params['breadcrumbs'][] = ['label' => 'Регионы материалов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-region-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
