<?php

use common\models\company\Company;
use common\models\person\PersonCategories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\person\search\PersonSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Персоны';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать персону', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fio',
            [

                'format' => 'raw',
                'attribute' => 'company_id',
                'value' => function($model){
                    return $model->company->name;
                },
                'filter' => ArrayHelper::map(Company::getElements(),'id','name'),
            ],
            [

                'format' => 'raw',
                'attribute' => 'category_id',
                'value' => function($model){
                    return $model->category->name;
                },
                'filter' => ArrayHelper::map(PersonCategories::getElements(),'id','name'),
            ],
            //'position',
            //'biography:ntext',

            ['class' => 'backend\components\FilterActionColumn','template'=>'{update} {delete}','header'=>'Действия',],
        ],
    ]); ?>
</div>
