<?php

use backend\modules\files\widgets\ImageInputWidget;
use backend\widgets\CkEditorDefaultWidget;
use backend\widgets\ModalAjaxElementAddWidget;
use common\models\company\Company;
use common\models\person\PersonCategories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\person\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(Company::getElements(),'id','name')) ?>
        <?
        echo ModalAjaxElementAddWidget::widget(['url' => '/company/ajax-company-create','selectId' => 'person-company_id']);
        ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(PersonCategories::getElements(),'id','name')) ?>
        <?
        echo ModalAjaxElementAddWidget::widget(['url' => '/person-categories/ajax-category-create','selectId' => 'person-category_id']);
        ?>
    </div>

    <?= $form->field($model, 'photoInput')->widget(ImageInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'photo',
        'inputAttribute' => 'photoInput',
        'descriptionActive' => false,
    ]   );   ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biography')->widget(CkEditorDefaultWidget::className()) ?>

    <?= $form->field($model, 'email')->input('email') ?>

    <?= $form->field($model, 'phone_stationary')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7 (999) 999 99 99',
    ]); ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7 (999) 999 99 99',
    ]); ?>

    <?= $form->field($model, 'vk_href')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fb_href')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tw_href')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
