<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->user->isGuest) {
        echo Nav::widget( [
            'options' => ['class' => 'nav navbar-nav'],
            'items' => [
                ['label' => 'Материалы', 'items' => [
                    ['label' => 'Список материалов', 'url' => ['/material']],
                    ['label' => 'Категории материалов', 'url' => ['/material-categories']],
                    ['label' => 'Регионы', 'url' => ['/material-region']],
                    ['label' => 'Теги', 'url' => ['/material-tags']],
                    ['label' => 'Темы', 'url' => ['/theme']],
                    ['label' => 'Журналы', 'url' => ['/journal']],
                    ['label' => 'Поиск', 'url' => ['/material/add-index']],
                ]],
                ['label' => 'Баннеры', 'items' => [
                    ['label' => 'Баннеры', 'url' => ['/banner']],
                    ['label' => 'Рекламные компании', 'url' => ['/banner-company']],
                    ['label' => 'Рекламодатели', 'url' => ['/banner-advertiser']],
                    ['label' => 'Позиции баннеров', 'url' => ['/banner-position']],
                ]],
                ['label' => 'Рассылка', 'items' => [
                    ['label' => 'Рассылки', 'url' => ['/newsletters/newsletters']],
                    ['label' => 'Категории', 'url' => ['/newsletters/newsletters-list']],
                    ['label' => 'Подписчики', 'url' => ['/newsletters/newsletters-subscribers']],
                    ['label' => 'Обратная связь', 'url' => ['/feedback/index']],
                ]],
                ['label' => 'ПКП', 'items' => [
                    ['label' => 'Пресс-релизы', 'url' => ['/press-release']],
                    ['label' => 'Компании', 'url' => ['/company']],
                    ['label' => 'Персоны', 'url' => ['/person']],
                    ['label' => 'Категории персон', 'url' => ['/person-categories']],
                    ['label' => 'Контакты', 'url' => ['/contacts']],
                ]],
                ['label' => 'Пользователи', 'url' => ['/user/admin']]
                //  ['label' => 'Change password', 'url' => ['/site/change-admin-password']],*/
            ],
        ] );
    }
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label' => 'Выйти (' . Yii::$app->user->identity->username . ')', 'url' => ['/site/logout']];
    }
    echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>

        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<?= \backend\modules\area\widgets\modal\ModalAjax::$modalsString; ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
