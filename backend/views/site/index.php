<?php

/* @var $this yii\web\View */

use backend\modules\area\widgets\AreaText;
use backend\modules\area\widgets\AreaImage;
use backend\modules\pages\widgets\treeview\TreeViewMaterialWidget;
use backend\widgets\grid\EditColumns;
use common\models\material\MaterialStatus;
use kartik\daterange\DateRangePicker;
use kartik\datetime\DateTimePicker;
use kartik\tree\TreeView;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Yii Application';


?>
<div class="site-index">

    <div class="jumbotron">
        <h1>ДЕЛОВОЙ КВАДРАТ</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Последние новости</h2>
                <?= GridView::widget([
                    'dataProvider' => $dataProviderMaterial,
                    'filterModel' => $searchModelMaterial,
                    'options' => ['style' => 'table-layout:fixed;'],
                    'columns' => [
                        [
                            'label' => 'Дата публикации',
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'width: 155px;', 'class' => 'text-center'],
                            'attribute' => 'publish_date',
                            'value' => function ($model) {
                                return EditColumns::widget([
                                    'model' => $model,
                                    'field' => 'publishDateFormated',
                                    'value' => $model->publishDateFormated,
                                    'edit' => DateTimePicker::widget(['type' => DateTimePicker::TYPE_INPUT,
                                        // 'convertFormat' => true,
                                        'model' => $model,
                                        'name' => Html::getInputName($model, 'publishDateFormated'),
                                        'value' => $model->publishDateFormated,
                                        'pluginOptions' => [
                                            'format' => 'dd.mm.yyyy hh:ii',
                                            'autoclose' => true,
                                            'weekStart' => 1,
                                        ],
                                        'options' => [
                                            'class' => 'edit-input'
                                        ]
                                    ])
                                ]);

                            },
                            'filter' => false,
                        ],
                        'name',
                        ['class' => 'backend\components\FilterActionColumn', 'template' => '{update} {delete}', 'header' => 'Действия',
                            'urlCreator' => function ($action, $model, $key, $index) {
                                if ($action === 'update') {
                                    return Url::to('admin/editor/?id='.$key, true);
                                }
                                if ($action === 'delete') {
                                    return Url::to(['material/delete?id='.$key], true);
                                }
                            }
                        ],
                    ],

                ]); ?>
            </div>
            <div class="col-lg-4">
                <h2>Действия</h2>
                <p style="margin-top: 40px;">
                    <a class="btn-lg btn-success" href="<?=Url::to(['/editor'])?>">Создать новость</a>
                </p>
                <p style="margin-top: 40px;">
                    <a class="btn-lg btn-success" href="<?=Url::to(['/constructor'])?>">Сконструировать главную</a>
                </p>
            </div>
        </div>

    </div>
</div>
