<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\CoordinatesPicker;
?>
<?php $form = ActiveForm::begin(); ?>

<p><b>Основная информация</b></p>

<div class="panel panel-default">
    <div class="panel-body">
        <?= $form->field($model, 'editorial_address')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'phone_editors')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99',
        ]); ?>
        <?= $form->field($model, 'phone_advertising')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99',
        ]); ?>
        <?= $form->field($model, 'email')->input('email') ?>
        <?= $form->field($model, 'advertisers_href')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'lng')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'map_title')->textInput(['maxlength' => true]) ?>
        <p><b>Выбор координат</b></p>
        <?= CoordinatesPicker::widget([
            'model' => $model,
            'markerTitle' => 'as',
        ])?>
    </div>
</div>


<div class="form-group">
    <?= Html::submitButton('Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
