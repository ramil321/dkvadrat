<?php

use common\models\company\Company;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\press\search\PressReleaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пресс-релизы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="press-release-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать пресс-релиз', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'format' => 'raw',
                'attribute' => 'company_id',
                'value' => function($model){
                    return $model->company->name;
                },
                'filter' => ArrayHelper::map(Company::getElements(),'id','name'),
            ],
            //'publish_date',
            //'preview_text:ntext',
            //'detail_text:ntext',

            ['class' => 'backend\components\FilterActionColumn','template'=>'{update} {delete}','header'=>'Действия',],
        ],
    ]); ?>
</div>
