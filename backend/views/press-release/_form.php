<?php

use backend\modules\files\widgets\FileInputWidget;
use backend\modules\files\widgets\ImageInputWidget;
use backend\widgets\CkEditorDefaultWidget;
use backend\widgets\ModalAjaxElementAddWidget;
use common\models\company\Company;
use dosamigos\ckeditor\CKEditor;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\press\PressRelease */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="press-release-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'publishDateFormated')->widget(    DateTimePicker::className(),[
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        // 'convertFormat' => true,
        'value' => $model->publishDateFormated,
        'pluginOptions' => [
            'format' => 'dd.mm.yyyy hh:ii',
            'autoclose'=>true,
            'weekStart'=>1,
        ],
        'options' => [
            'autocomplete' => 'off'
        ]
    ]);
    ?>
    <div class="form-group">
        <?= $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(Company::getElements(),'id','name')) ?>
        <?
        echo ModalAjaxElementAddWidget::widget(['url' => '/company/ajax-company-create','selectId' => 'press-release-company_id']);
        ?>
    </div>


    <?= $form->field($model, 'imageInput')->widget(ImageInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'image',
        'inputAttribute' => 'imageInput',
        'descriptionActive' => false,
    ]   );   ?>

    <?= $form->field($model, 'fileInput')->widget(FileInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'file',
        'inputAttribute' => 'fileInput',
        'descriptionActive' => false,
        'pluginOptions' => [
            'allowedFileExtensions'=>['pdf'],
            'initialPreview' =>[$model->file->src],
            'initialPreviewConfig'=> [[
                'type' => "pdf",
                'caption' => $model->file->name,
            ]],
            'previewFileType' => 'pdf',
        ]
    ]   );   ?>


    <?= $form->field($model, 'preview_text')->widget(CkEditorDefaultWidget::className()) ?>
    <?= $form->field($model, 'detail_text')->widget(CkEditorDefaultWidget::className()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
