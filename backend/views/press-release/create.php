<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\press\PressRelease */

$this->title = 'Создать пресс-релиз';
$this->params['breadcrumbs'][] = ['label' => 'Пресс-релизы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="press-release-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
