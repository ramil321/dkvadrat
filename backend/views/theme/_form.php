<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\theme\Theme */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="theme-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= ($model->slug ? $form->field($model, 'slug')->textInput(['disabled' => 'true']) : ''); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>


    <?
    echo $form->field($model, 'materialsIds')->widget(Select2::classname(), [
        // 'initValueText' => 'Выберите теги', // set the initial display text
        'data' => ArrayHelper::map($model->materials,'id','name'),
        'options' => ['placeholder' => 'Выберите теги ...','multiple' => true,],
        'showToggleAll' => false,
        'pluginOptions' => [
           // 'tags' => true,
            //'tokenSeparators' => [','],
           // 'allowClear' => true,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['material/ajax-materials-find']),
                'dataType' => 'json',
            ],
        ],
    ]);

    ?>




    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
