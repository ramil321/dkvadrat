<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\banner\BannerPosition */

$this->title = 'Создать позицию баннера';
$this->params['breadcrumbs'][] = ['label' => 'Позиции баннеров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-position-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
