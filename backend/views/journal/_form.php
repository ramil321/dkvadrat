<?php

use backend\modules\files\widgets\FileInputWidget;
use backend\modules\files\widgets\ImageInputWidget;
use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\journal\Journal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="journal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'imageInput')->widget(ImageInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'image',
        'inputAttribute' => 'imageInput',
        'descriptionActive' => false,
    ]   );   ?>

    <?= $form->field($model, 'dateFormated')->widget(DateRangePicker::classname(), [
        'model' => $model,
        'attribute' => 'date',
        'convertFormat' => true,
        'startAttribute' => 'dateFormatedFrom',
        'endAttribute' => 'dateFormatedTo',
        'pluginOptions' => [
            'locale' => [
                'format' => 'd.m.Y'
            ]
        ],
        'options' => [
            'autocomplete' => 'off',
            'class' => 'form-control'
        ]
    ]);?>

    <?= $form->field($model, 'fileInput')->widget(FileInputWidget::classname(), [
        'model' => $model,
        'modelImage' => 'file',
        'inputAttribute' => 'fileInput',
        'descriptionActive' => false,
        'pluginOptions' => [
            'allowedFileExtensions'=>['pdf'],
            'initialPreview' =>[$model->file->src],
            'initialPreviewConfig'=> [[
                'type' => "pdf",
                'caption' => $model->file->name,
            ]],
            'previewFileType' => 'pdf',
        ]
    ]   );   ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
