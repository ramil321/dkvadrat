<?php

use kartik\daterange\DateRangePicker;
use kartik\datetime\DateTimePicker;
use kartik\tree\TreeView;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\material\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории материалов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?

    echo TreeView::widget([
        // single query fetch to render the tree
        'query'             => \common\models\material\MaterialCategories::find()->addOrderBy('root, lft'),
       // 'topRootAsHeading' => true, // this will override the headingOptions
        'rootOptions' => ['label' => 'Разделы'],
        'isAdmin'           => true,                       // optional (toggle to enable admin mode)
        'displayValue'      => 1,
        'softDelete'      => false,
        'nodeView' => '@backend/modules/pages/views/tree-categories/_form',
        //'softDelete'      => true,                        // normally not needed to change
        //'cacheSettings'   => ['enableCache' => true]      // normally not needed to change
    ]);


    ?>

</div>
