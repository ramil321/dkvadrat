<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\material\MaterialTags */

$this->title = 'Create Material Tags';
$this->params['breadcrumbs'][] = ['label' => 'Material Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-tags-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
