#!/bin/bash  
echo "Run deploy editor"
cd editor
git pull origin dev
read -p "Press enter to continue"
docker exec -it kvadrat_php sh -c "cd /var/www/kvadrat/editor && yarn deploy"
