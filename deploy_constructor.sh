#!/bin/bash
echo "Run deploy constructor"
cd constructor
git pull origin master
read -p "Press enter to continue"
docker exec -it kvadrat_php sh -c "cd /var/www/kvadrat/constructor && yarn deploy"